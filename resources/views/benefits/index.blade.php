@extends('layouts.dashboard')

@section('title', 'Benefit')

@section('content')
<div class="dashboard-right cust-dashboard-md-right cust-client-dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>Benefits</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">If you need help creating a Benefit.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <div id="export-buttons" style=""><div class="dt-buttons"><button class="dt-button buttons-excel buttons-html5 btn-custom outline-btn" tabindex="0" aria-controls="invoice-list-table" type="button"><span>Export in excel</span></button> <button class="dt-button buttons-pdf buttons-html5 btn-custom outline-btn" tabindex="0" aria-controls="invoice-list-table" type="button"><span>Export in pdf</span></button> </div></div>
            <a href="{{ route('benefits.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> New Benefit</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="benefits-list-table">
                            <thead>
                                <tr>
                                    <th>Sr Number</th>
                                    <th>Employee</th>
                                    <th>Type</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Frequency</th>
                                    <th>Frequency Type</th>
                                    <th>Amount</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
								@forelse ($benefits as $key => $benefit)
                                <tr>
                                    <!-- <td>{{ $benefit->internal_number }}</td> -->
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $benefit->employee->name }}</td>
                                    <td>{{ $benefit->type }}</td>
                                    <td>{{ dateFormat($benefit->start_date) }}</td>
                                    <td>{{ dateFormat($benefit->end_date) }}</td>
                                    <td>{{ showIfAvailable($benefit->frequency) }}</td>
                                    <td>{{ __('labels.'.$benefit->frequency_type) }}</td>
                                    <td>{{ $benefit->amount }}</td>
                                    <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul> 
                                                
                                                <li><a href="{{route('benefits.edit', $benefit)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-benefit-form-' . $benefit->id }}" href="{{route('benefits.destroy', $benefit)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['benefits.destroy', $benefit], 'id' => "destroy-benefit-form-{$benefit->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
                                    </div>
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="7" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.benefit') }}!</p></div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('benefits.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label').' '.__('labels.benefit') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse    
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
