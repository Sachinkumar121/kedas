@extends('layouts.dashboard')

@section('title', 'benefit')

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>Edit benefit</h2>
        {{ Form::open(array('method'=>'PUT','route' => ['benefits.update', $benefit], 'id' => 'add-edit-benefit-form')) }}
            @csrf
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Employee <span>*</span></label>
                                {!! Form::select('employee_id', $employee_options, old('contact_id', $benefit->employee_id), ['class'=>"form-control single-search-selection", 'id'=>"employee_id"]) !!}
                                @error('employee')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="type_account" class="col-sm-12">{{ __('labels.types') }} <span>*</span></label>
                                <div class="dropdown hierarchy-select hierarchy-form-element" id="type-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="type-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="type-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-level="1" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            @foreach($type_accounts_options as $type_accounts_option)
                                                <a class="dropdown-item" data-value="{{$type_accounts_option->id}}" data-level="{{$type_accounts_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$type_accounts_option->custom_name}}</span>
                                                    @if($type_accounts_option->level == 1)
                                                    <span class="detail-type">{{ $type_accounts_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="type_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('type_account', $benefit->type_account) }}" />
                                </div>
                                @error('type_account')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                       
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Start Date <span>*</span></label>
                                 <input id="payroll_start_date" name="start_date" class="form-control"
                                required="required" value="{{old('payroll_start_date', $benefit->start_date)}}"
                                autocomplete="off" />
                                @error('start_date')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">End Date <span>*</span></label>
                                <input id="payroll_end_date" name="end_date" class="form-control"
                                required="required" value="{{old('end_date', $benefit->end_date)}}"
                                autocomplete="off" />
                                @error('end_date')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Observations </label>
                                <textarea class="form-control" id="observations" name="observation">{{old('observation', $benefit->observation)}}</textarea>
                            </div>                        
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Frequency <span>*</span></label>
                                <div class="wrap-two-cell-group">
                                    <div class="left">
                                        {!! Form::select('frequency_type', $frequency_type_option, old('contact_id', $benefit->frequency_type) , ['class'=>"form-control col-5 single-search-selection", 'id'=>"frequency_type"]) !!}</div>
                                    <div class="right">
                                         <input type="text" class="form-control" name="frequency" id="frequency" value="{{old('frequency',$benefit->frequency)}}">
                                    </div>
                                </div>
                            </div>  
                            
                            <div class="wrap-checkbox-block">
                                @if (old('_token') === null)
                                    <input type="radio" name="recurring_type" id="recurring" value="recurring" {{ $benefit->recurring_type == "recurring" ? "checked" : "" }}>
                                    <label for="recurring">Recurring</label>
                                    <input type="radio" name="recurring_type" id="on_time" value="on_time" {{ $benefit->recurring_type == "on_time" ? "checked" : "" }} >
                                    <label for="on_time">On Time</label>
                                @else
                                    <input type="radio" name="recurring_type" id="recurring" value="recurring" {{ old("recurring_type") == "recurring" ? "checked" : "" }} >
                                    <label for="recurring">Recurring</label>
                                    <input type="radio" name="recurring_type" id="on_time" value="on_time" {{ old("recurring_type") == "on_time" ? "checked" : "" }} >
                                    <label for="on_time">On Time</label>
                                @endif
                            </div> 

                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Amount <span>*</span></label>
                                <input type="text" class="form-control" name="amount" id="contact" value="{{old('amount', $benefit->amount)}}">
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="expense_account" class="col-sm-12">{{ __('labels.expenses') }} {{ __('labels.account') }} <span>*</span></label>
                                <div class="dropdown hierarchy-select hierarchy-form-element" id="expense-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="expense-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="expense-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-level="1" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            @foreach($expense_accounts_options as $expense_accounts_option)
                                                <a class="dropdown-item" data-value="{{$expense_accounts_option->id}}" data-level="{{$expense_accounts_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$expense_accounts_option->custom_name}}</span>
                                                    @if($expense_accounts_option->level == 1)
                                                    <span class="detail-type">{{ $expense_accounts_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="expense_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('expense_account', $benefit->expense_account) }}" />
                                </div>
                                @error('expense_account')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                       
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                <div class="form-group popup-btns">
                    <a href="{{ route('benefits.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom">{{ __('labels.update') }}</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
