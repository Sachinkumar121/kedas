@extends('layouts.dashboard')

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>Import {{ slugToStr($resource, '-') }}</h2>
        <!-- <h3>You can import up to 1,000 items per Excel file.</h3> -->
        <p>{{ __('labels.please_excel_fields_having_line') }} {{ implode(', ', $mustHaveFields) }} {{ __('labels.fields') }}.</p>
        <div class="item-management">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <form class="form-horizontal" method="POST" action="{{ route('import_process', $resource) }}" enctype="multipart/form-data" id="resource-import-form">
                        {{ csrf_field() }}
                        <div class="item-management-box">
                            <div class="file-input">
                                <label for="file" class="col-md-4 control-label" id="import-file-preview-label">{{ __('labels.excel_file_to_import') }}</label>
                                <input id="file" type="file" class="form-control" name="file" data-label_id="import-file-preview-label" />
                                @error('file')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <p>{{ __('labels.download_sample_template') }} <a href="{{ asset('sample/'.$resource.'-import.xlsx') }}" download>{{ __('labels.click_here') }}</a>.</p>
                            <button type="submit" name="import" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.import') }}</button>          
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection