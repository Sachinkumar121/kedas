@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.fixed_assets'))
@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block"
            href="{{ route('fixed-assets.index') }}">{{ __('labels.back') }}</a>

        <h2>{{ __('labels.fixed_asset') }}</h2>
   
        <div class="action-btns-view show-wrap-btns">
            {{-- <div class="show-wrap-btns-inner">
                <a class="btn-custom print-this-invoice" data-id="{{ $invoice->id }}" data-type="{{ $viewType }}"><i class="fa fa-print"></i> {{ __('labels.print') }}</a>
                <a href="{{route('invoices.pdf', ['invoice' => $invoice, 'view' => $viewType]) }}" class="btn-custom" id="invoice-download-btn"><i class="fa fa-download"></i> {{ __('labels.download') }}</a>
                <a role="button" data-toggle="modal" data-target="#invoiceEmailModal" href="javascript:void(0)" class="btn-custom"><i class="fa fa-envelope"></i> {{ __('labels.send_by_mail') }}</a>
            </div> --}}
        </div>
        <div class="default-form view-form">
            <div class="generate-invoice">
                <div class="invoice-detail-count">
                    <div class="invoice-detail-count-item">
                        <span class="title">{{ __('labels.price_of_purchase') }}</span>
                        <span class="value total-value">{{ moneyFormat($fixedAsset->price) }}</span>
                    </div>
                </div>
                <div id="invoice-custom-wrap-block" class="view-form-custom custom-view-info-client">
                    <table class="table">
                        <tr>
                            <td class="cutom-td"><b>{{ __('labels.item_name') }}</b></td>
                            <td>{{ $fixedAsset->name }}</td>
                            <td><b>{{ __('labels.date_of_purchase') }}</b></td>
                            <td>{{ dateFormat($fixedAsset->date_of_purchase) }}</td>
                        </tr>
                        <tr>
                            <td class="cutom-td"><b>{{ __('labels.price_of_purchase') }}</b></td>
                            <td>{{ moneyFormat($fixedAsset->price) }}</td>
                            <td><b>{{ __('labels.serial_number') }}</b></td>
                            <td>{{ showIfAvailable($fixedAsset->serial_number) }}</td>
                        </tr>
                        <tr>
                            <td class="cutom-td"><b>{{ __('labels.item_category') }}</b></td>
                            <td>{{ $fixedAsset->itemCategory ? showIfAvailable($fixedAsset->itemCategory->name) : 'N/A' }}</td>
                            <td><b>{{ __('labels.years') }}</b></td>
                            <td>{{ $fixedAsset->years }}</td>
                        </tr>
                        <tr>
                            <td class="cutom-td"><b>{{ __('labels.description') }}</b></td>
                            <td>{{ showIfAvailable($fixedAsset->description) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="tabs-custom default-form view-table-info">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#fa-depr">{{ __('labels.depreciation') }}</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="fa-depr" class="tab-pane generate-invoice fade in active show">
                        <div class="add-form">                           
                            <div class="default-table">
                                <div class="list-table-custom">
                                    <table class="table" id="depreciation_timeline_table" name="table">
                                        <thead>
                                            <tr>
                                                <th>Year</th>
                                                <th>Opening Value</th>
                                                <th>Depreciation</th>
                                                <th>Closing Value</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                
                                            </tr>
                                        </tbody>
                                    </table>                           
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    let total = {!! json_encode($fixedAsset->price) !!};
    let years = {!! json_encode($fixedAsset->years) !!};
    let cal_type = {!! json_encode($fixedAsset->cal_type) !!};
    let scrap_value = {!! json_encode($fixedAsset->scrap_percentage) !!};
    generateDepreciationTimeline(years, total, cal_type, scrap_value);
</script>
@endpush