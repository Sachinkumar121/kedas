@extends('layouts.dashboard')

@section('title', trans('labels.edit').' '.trans('labels.fixed_asset'))

@section('content')

@inject('controller', 'App\Http\Controllers\Controller')

<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.edit').' '.__('labels.fixed_asset') }}</h2>
        <div class="default-form add-product-form invoice-details-form">
            {{ Form::open(array('method'=>'PUT', 'route' => ['fixed-assets.update', $fixedAsset], 'id' => 'add-edit-fixed-assets-form', 'enctype' => 'multipart/form-data')) }}
                <div class="generate-invoice">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-12">{{ __('labels.name') }} <span>*</span></label>
                                <input class="form-control" type="text" name="name" placeholder="{{ __('labels.name') }}" required value="{{ old('name', $fixedAsset->name) }}" autocomplete="off" />
                                @error('name')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="price" class="col-sm-12">{{ __('labels.price_of_purchase') }} <span>*</span></label>
                                <input class="form-control" type="text" name="price" placeholder="{{ __('labels.price') }}" required value="{{ old('price', $fixedAsset->price) }}" autocomplete="off" />
                                @error('price')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="account" class="col-sm-12">{{ __('labels.account') }} <span>*</span></label>
                                <div class="dropdown hierarchy-select hierarchy-form-element" id="fixed-asset-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="fixed-asset-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="fixed-asset-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-level="1" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            @foreach($fixed_assets_accounts_options as $fixed_assets_accounts_option)
                                                <a class="dropdown-item" data-value="{{$fixed_assets_accounts_option->id}}" data-level="{{$fixed_assets_accounts_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$fixed_assets_accounts_option->custom_name}}</span>
                                                    @if($fixed_assets_accounts_option->level == 1)
                                                    <span class="detail-type">{{ $fixed_assets_accounts_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('account', $fixedAsset->account) }}" />
                                </div>
                                @error('account')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="description" class="col-sm-12">{{ __('labels.description') }}</label>
                                <textarea class="form-control" name="description" placeholder="{{ __('labels.description') }}" autocomplete="off">{{ old('description', $fixedAsset->description) }}</textarea>
                                @error('description')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="serial_number" class="col-sm-12">{{ __('labels.serial_number') }}</label>
                                <input class="form-control" type="text" name="serial_number" placeholder="{{ __('labels.serial_number') }}" value="{{ old('serial_number', $fixedAsset->serial_number) }}" autocomplete="off" />
                                @error('serial_number')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group create-new">
                                <label for="item_category" class="col-sm-12 creat-nw-cat">{{ __('labels.item_category') }}</label>
                                {!! Form::select('item_category_id', $item_category_options, $fixedAsset->item_category_id, ['class'=>"form-control single-search-selection", 'id'=>"item_category"]) !!}
                            </div>
                            <div class="form-group">
                                <label for="expense_account" class="col-sm-12">{{ __('labels.expenses') }} {{ __('labels.account') }} <span>*</span></label>
                                <div class="dropdown hierarchy-select hierarchy-form-element" id="expense-account-multilevelselect">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" id="expense-account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="expense-account-multilevelselect-button">
                                        <div class="hs-searchbox">
                                            <input type="text" class="form-control" autocomplete="off">
                                        </div>
                                        <div class="hs-menu-inner">
                                            <a class="dropdown-item" data-value="" data-level="1" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                            @foreach($expense_accounts_options as $expense_accounts_option)
                                                <a class="dropdown-item" data-value="{{$expense_accounts_option->id}}" data-level="{{$expense_accounts_option->level}}" href="javascript::void(0)">
                                                    <span class="account-name">{{$expense_accounts_option->custom_name}}</span>
                                                    @if($expense_accounts_option->level == 1)
                                                    <span class="detail-type">{{ $expense_accounts_option->parent->name }}</span>
                                                    @endif
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <input class="d-none" name="expense_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('expense_account', $fixedAsset->expense_account) }}" />
                                </div>
                                @error('expense_account')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="cal_type" class="col-sm-12">{{ __('labels.cal_type') }} <span>*</span></label>
                                {!! Form::select('cal_type', $cal_type_options, $fixedAsset->cal_type, ['class'=>"form-control", 'id'=>"cal_type"]) !!}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 prduct-pic">
                            <div class="form-group file-input file-input-large">
                                @if(!$fixedAsset->image)
                                <label for="image" id="image-preview-label"><i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>{{ __('labels.image_of_item') }}</label>
                                @endif

                                @if($fixedAsset->image)
                                    <img src="{{ URL::asset($fixedAsset->image_url) }}" alt="{{ $fixedAsset->name }}" id="image-preview" />
                                @else
                                    <img src="" id="image-preview" />
                                @endif
                                <input type="file" id="inventory-image" name="image" class="file-upload-preview" accept="image/*" data-img_id="image-preview" data-msg-accept="{{ __('messages.invalid_image_file_error') }}" data-label_id="image-preview-label">
                                <a class="upload-button" data-preview_section="inventory-image"><i class="fa fa-pencil" aria-hidden="true"></i> {{ __('labels.edit') }}</a>
                                @error('image')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <p>{{ __('labels.total').' '. __('labels.price') }}</p>
                                <h3 id="item_total">{{  moneyFormat($fixedAsset->price) }} }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row inventory-field-container" id="inventory_details">
                        <div class="col-sm-12">
                            <h3>{{ __('labels.depreciation_per_year') }}</h3>
                            <table id="depreciation_items">
                                <thead>
                                    <tr>
                                        <th>{{ __('labels.percentage') }}</th>
                                        <th>{{ __('labels.for_year') }}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count(old('depreciation_items', [])))
                                        @foreach (old('depreciation_items', []) as $key => $product)
                                            @include('app.depreciation-item-row', ['row_number' => $key])
                                        @endforeach
                                    @else
                                        @foreach ($fixedAsset->depreciation as $key => $item)
                                            <tr>
                                                <input type="hidden" name="depreciation_items[{{$key}}][id]" value="{{$item->id ?: 0}}">
                                                <td class="td-year">
                                                    <input type="text" placeholder="{{ __('labels.year') }}"
                                                        name="depreciation_items[{{$key}}][year]" id="depreciation_items[{{$key}}][year]" class="form-control" readonly
                                                        value="{{ $item->year }}" />
                                                </td>
                                                <td class="td-disc" style="text-align: center;">
                                                    <input type="number" min="0" max="99" placeholder="%"
                                                    pattern="^\d*(\.\d{0,2})?$"
                                                    data-msg="{{ __('validation.regex', ['attribute' => __('labels.percentage')]) }}"
                                                    name="depreciation_items[{{$key}}][percentage]" id="depreciation_items[{{$key}}][percentage]"
                                                    class="form-control"
                                                    value="{{ old('depreciation_items')[$key]['percentage'] ?: $item->percentage }}" />
                                                </td>
                                                <td>
                                                    @if($key != 0)
                                                        <a class="delete-icon remove-depr-row" data-row_number="{{$key}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <a class="btn-custom add-depreciation-row mb-3 mt-3"><i class="fa fa-plus" aria-hidden="true"></i>
                            {{ __('labels.add_row') }}</a>                            
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="legal_category" class="col-sm-12">{{ __('labels.legal_category') }} <span>*</span></label>
                                {!! Form::select('legal_category', $legal_category_options, $fixedAsset->legal_category, ['class'=>"form-control", 'id'=>"legal_category"]) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="percentage">{{ __('labels.percentage') }} <span>*</span></label>
                                <input class="form-control" type="text" name="percentage" placeholder="{{ __('labels.percentage') }}(%)" required value="{{ old('percentage', $fixedAsset->percentage) }}" autocomplete="off" />
                                @error('percentage')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="location" class="col-sm-12">{{ __('labels.location') }}</label>
                                {!! Form::select('location', $location_options, $fixedAsset->location, ['class'=>"form-control", 'id'=>"location"]) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="book_value" class="col-sm-12">{{ __('labels.book_value') }} <span>*</span></label>
                                <input class="form-control" type="text" name="book_value" placeholder="{{ __('labels.book_value') }}" required value="{{ old('book_value', $fixedAsset->book_value) }}" autocomplete="off" />
                                @error('book_value')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="date_of_purchase">{{ __('labels.date_of_purchase') }}</label>
                                <input id="date_of_purchase" class="form-control" name="date_of_purchase" value="{{old('date_of_purchase', $fixedAsset->date_of_purchase)}}"
                                autocomplete="off" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="final_date">{{ __('labels.final_date') }}</label>
                                <input id="final_date" class="form-control" name="final_date" value="{{old('final_date', $fixedAsset->final_date)}}"
                                autocomplete="off" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group popup-btns">                    
                    <a href="{{ route('fixed-assets.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom">{{ __('labels.update') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
