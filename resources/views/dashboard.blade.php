@extends('layouts.dashboard')

@section('title', trans('labels.dashboard'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="dashboard-max-w">
        <h2>{{ __('labels.dashboard') }}</h2>
        <div class="dashboard-bottom">
            <div class="dashboard-cont dashboard-cont-full">
                <div class="dashboard-cont-left">
                    <h3>{{ __('labels.sales_expenses') }}</h3>
                </div>
                <!-- <div class="dashboard-cont-right">
                    <a href="javascript:void(0);" class="view-btn">Pending</a>
                    <a href="javascript:void(0);" class="view-btn">Paid</a>
                </div> -->
                <ul class="nav nav-tabs" id="barChartTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="pending-tab" data-toggle="tab" href="#pending" role="tab" aria-controls="pending" aria-selected="true">{{ __('labels.pending') }}</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="paid-tab" data-toggle="tab" href="#paid" role="tab" aria-controls="paid" aria-selected="false">{{ __('labels.paid') }}</a>
                  </li>
                </ul>
                <div class="clearfix"></div>
                <div class="dasboard-select-wrap">
                    <select name="filter-graph-data" class="form-control dasboard-filter-block form-control">
                        <option value="this_week">{{ __('labels.this_week') }}</option>
                        <option value="last_7_days">{{ __('labels.last_7_days') }}</option>
                        <option value="this_month">{{ __('labels.this_month') }}</option>
                        <option value="last_month">{{ __('labels.last_month') }}</option>
                        <option value="other_pass_months">{{ __('labels.other_pass_months') }}</option>
                    </select>
                    <select id="last-months-drop" class="pass-month-select form-control" name="last-months-drop" style="display:none">
                        <option value="">{{ __('labels.select_last_month') }}</option>
                    </select>
                </div>
                <div class="tab-content" id="barChartTabContent">
                    <div class="tab-pane fade show active" id="pending" role="tabpanel" aria-labelledby="pending-tab">
                        <div class="dash-img-sec">
                            <canvas id="sales-expenses-pending-chart"></canvas>
                            <div class="chart-no-data" id="sales-expenses-pending-no-data">{{ __('labels.nothing_to_display') }}</div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="paid" role="tabpanel" aria-labelledby="paid-tab">
                        <div class="dash-img-sec">
                            <canvas id="sales-expenses-paid-chart"></canvas>
                            <div class="chart-no-data" id="sales-expenses-paid-no-data">{{ __('labels.nothing_to_display') }}</div>
                        </div>
                    </div>
                </div>
                <!-- <div class="dash-para">
                    <p>Income and expenses only (includes unpaid invoices and bills).</p>
                </div> -->
                <!-- <div class="dashboard-cont-left">
                    <ul>
                        <li><span><img src="{{ asset('images/inflow-icon.png') }}"></span>Income</li>
                        <li><span><img src="{{ asset('images/outflow-icon.png') }}"></span>Expense</li>
                    </ul>
                </div> -->
                <!-- <div class="dashboard-cont-right">
                    <p>Last 6 Months</p>
                </div> -->
            </div>
            <div class="dashboard-cont">
                <div class="dashboard-cont-left">
                    <h3>{{ __('labels.sales_best_clients') }}</h3>
                </div>
                <div class="clearfix"></div>
                <div class="dash-img-sec">
                    <canvas id="sales-pie-chart"></canvas>
                    <div class="chart-no-data" id="sales-no-data">{{ __('labels.nothing_to_display') }}</div>
                </div>
            </div>
            <div class="dashboard-cont">
                <div class="dashboard-cont-left">
                    <h3>{{ __('labels.expenses_suppliers') }}</h3>
                </div>
                <div class="clearfix"></div>
                <div class="dash-img-sec">
                    <canvas id="expenses-pie-chart"></canvas>
                    <div class="chart-no-data" id="expenses-no-data">{{ __('labels.nothing_to_display') }}</div>
                </div>
            </div>

            <!-- <div class="dashboard-cont">
                <div class="dashboard-cont-left">
                    <h3>Cash Flow</h3>
                </div>
                <div class="dashboard-cont-right"> <a href="javascript:void(0);" class="view-btn">View Report</a> </div>
                <div class="clearfix"></div>
                <div class="dash-para">
                    <p>Cash coming in and going out of your business.</p>
                </div>
                <div class="dashboard-cont-left">
                    <ul>
                        <li><span><img src="{{ asset('images/inflow-icon.png') }}"></span>Inflow</li>
                        <li><span><img src="{{ asset('images/outflow-icon.png') }}"></span>outflow</li>
                    </ul>
                </div>
                <div class="dashboard-cont-right">
                    <p>Last 12 Months</p>
                </div>
                <div class="clearfix"></div>
                <div class="dash-img-sec"><canvas id="cashFlowChart"></canvas></div>
            </div>
            <div class="dashboard-cont">
                <div class="dashboard-cont-left">
                    <h3>Profit & Loss</h3>
                </div>
                <div class="dashboard-cont-right"> <a href="javascript:void(0);" class="view-btn">View Report</a> </div>
                <div class="clearfix"></div>
                <div class="dash-para">
                    <p>Income and expenses only (includes unpaid invoices and bills).</p>
                </div>
                <div class="dashboard-cont-left">
                    <ul>
                        <li><span><img src="{{ asset('images/inflow-icon.png') }}"></span>Income</li>
                        <li><span><img src="{{ asset('images/outflow-icon.png') }}"></span>Expense</li>
                    </ul>
                </div>
                <div class="dashboard-cont-right">
                    <p>Last 12 Months</p>
                </div>
                <div class="clearfix"></div>
                <div class="dash-img-sec"><canvas id="cashFlowChart1"></canvas></div>
            </div> -->
        </div>
        <!-- <div class="payable-sec">
            <h3>Payable & Owing</h3>
            <div class="invoice-sec">
                <div class="invoice-cont">
                    <div class="coming-due">
                        <table class="table">
                            <tr>
                                <th colspan="2">Invoices payable to you</th>
                            </tr>
                            <tr class="red-row">
                                <td>Coming Due </td>
                                <td>$14,78,924</td>
                            </tr>
                            <tr>
                                <td>1-30 days overdue</td>
                                <td>$4000.00</td>
                            </tr>
                            <tr>
                                <td>31-60 days overdue</td>
                                <td>$1460.00</td>
                            </tr>
                            <tr>
                                <td>61-90 days overdue</td>
                                <td>$1870.00</td>
                            </tr>
                            <tr>
                                <td>> 90 days overdue</td>
                                <td>$6040.00</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="invoice-cont">
                    <div class="coming-due">
                        <table class="table">
                            <tr>
                                <th colspan="2">Bills you owe</th>
                            </tr>
                            <tr class="red-row">
                                <td>Coming Due</td>
                                <td>$8,78,924</td>
                            </tr>
                            <tr>
                                <td>1-30 days overdue</td>
                                <td>$1000.00</td>
                            </tr>
                            <tr>
                                <td>31-60 days overdue</td>
                                <td>$1460.00</td>
                            </tr>
                            <tr>
                                <td>61-90 days overdu</td>
                                <td>$3870.00</td>
                            </tr>
                            <tr>
                                <td>> 90 days overdue</td>
                                <td>$2040.00</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="net-sec">
            <div class="net-text">
                <div class="net-cont">
                    <h3>Net Income</h3>
                    <div class="coming-due">
                        <table class="table">
                            <tr>
                                <th>Fiscal Year</th>
                                <th>2019</th>
                                <th>2020</th>
                            </tr>
                            <tr>
                                <td>Income</td>
                                <td>19,05,496.45</td>
                                <td>2,808,552.75</td>
                            </tr>
                            <tr>
                                <td>Expense</td>
                                <td>3,395.88</td>
                                <td>5,054.87</td>
                            </tr>
                            <tr>
                                <td>Net Income</td>
                                <td>19,02,100.57   </td>
                                <td>28,03,497.88</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="net-cont">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <h3>Expenses Breakdown</h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 text-right">
                            <ul>
                                <li>This Year</li>
                                <li>This Month</li>
                            </ul>
                        </div>
                    </div>
                    <div class="expens-img"><canvas id="myChart"></canvas></div>
                </div>
            </div>
        </div> -->
    </div>
</div>
<div class="clearfix"></div>
@endsection
