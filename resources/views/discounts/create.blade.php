@extends('layouts.dashboard')

@section('title', 'Discount')

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>New Discount</h2>
        <form method="POST" action="{{ route('discounts.store') }}" id="add-edit-discount-form">
            @csrf
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Employee <span>*</span></label>
                                {!! Form::select('employee_id', $employee_options, '', ['class'=>"form-control single-search-selection", 'id'=>"employee_id"]) !!}
                                @error('employee')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                           
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Types <span>*</span></label>
                                {!! Form::select('type', $type_option, '', ['class'=>"form-control single-search-selection", 'id'=>"type"]) !!}
                                @error('type')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Start Date <span>*</span></label>
                                 <input id="payroll_start_date" name="start_date" class="form-control"
                                required="required" value="{{old('payroll_start_date', '')}}"
                                autocomplete="off" />
                                @error('start_date')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">End Date <span>*</span></label>
                                <input id="payroll_end_date" name="end_date" class="form-control"
                                required="required" value="{{old('end_date', '')}}"
                                autocomplete="off" />
                                @error('end_date')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Observations </label>
                                <textarea class="form-control" id="observations" name="observation">{{old('observation')}}</textarea>
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Frequency <span>*</span></label>
                                <div class="wrap-two-cell-group">
                                    <div class="left">
                                        {!! Form::select('frequency_type', $frequency_type_option, '', ['class'=>"form-control col-5 single-search-selection", 'id'=>"frequency_type"]) !!}</div>
                                    <div class="right">
                                         <input type="text" class="form-control" name="frequency" id="frequency" value="{{old('frequency')}}">
                                    </div>
                                </div>
                            </div>  
                            
                            <div class="wrap-checkbox-block">
                                @if (old('_token') === null)
                                    <input type="radio" name="recurring_type" id="recurring" value="recurring" checked>
                                @else
                                <input type="radio" name="recurring_type" id="recurring" value="recurring" {{ old("recurring_type") == "recurring" ? "checked" : "" }} >
                                @endif
                                <label for="recurring">Recurring</label>
                                <input type="radio" name="recurring_type" id="on_time" value="on_time" {{ old("recurring_type") == "on_time" ? "checked" : "" }} >
                                <label for="on_time">On Time</label>
                            </div> 
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Amount <span>*</span></label>
                                <input type="text" class="form-control" name="amount" id="contact" value="{{old('amount')}}">
                            </div>                        
                        </div>
                         <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Incomes <span>*</span></label>
                                <div class="">
                                 {!! Form::select('incomes', $incomes_option, '', ['class'=>"form-control single-search-selection", 'id'=>"incomes"]) !!}
                                <div class="">
                                @error('incomes')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('discounts.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
