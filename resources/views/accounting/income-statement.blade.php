@extends($isRawRequest ? 'layouts.raw' : 'layouts.dashboard' )

@section('title', trans('labels.income_statement'))

@section('content')

@if(!$isRawRequest)
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>

    <div class="invoice-container charts-block">

        <h2>{{ __('labels.income_statement') }}</h2>
@endif
        <form style="display: none;">
            <div class="wrap-list-head">
                <select name="is_period" class="form-control dasboard-filter-block form-control wrap-list-cell">
                    <option value="this_week">{{ __('labels.this_week') }}</option>
                    <option value="last_7_days">{{ __('labels.last_7_days') }}</option>
                    <option value="this_month">{{ __('labels.this_month') }}</option>
                    <option value="last_month">{{ __('labels.last_month') }}</option>
                    <option value="other_pass_months">{{ __('labels.other_pass_months') }}</option>
                </select>
                <strong>{{ __('labels.from') }} *</strong>
                <input id="is_start_date" class="form-control wrap-list-cell" name="start_date" value="{{ old('start_date', request()->start_date ?? getFirstDayofMonth()) }}" autocomplete="off" />

                <strong>{{ __('labels.until') }} *</strong>
                <input id="is_end_date" class="form-control wrap-list-cell" name="end_date" value="{{ old('end_date', request()->end_date ?? getLastDayofMonth()) }}" autocomplete="off" />

                <button class="btn-custom">{{ __('labels.generate_reports') }}</button>
            </div>
        </form>
        <div class="row accounting-report-block">
            <div class="col-lg-12">
                <div class="wrap-list-cells">
                    <div class="list-sub-heading-row">
                        <span>{{ __('labels.account') }}</span>
                        <span class="total">{{ __('labels.total') }}</span>
                    </div>
                    <div id="gp-accordion" class="nlevel-list-group">
                        <!-- Repeat this for main account -->
                        @foreach($gpAccountingHeads as $accountingHead)
                        <div class="card">
                            <!-- Main account heading -->
                            <div class="card-header" id="heading-{{$accountingHead->id}}">

                                <a class="collapsed" role="button" data-toggle="collapse"
                                    href="#collapse-{{$accountingHead->id}}" aria-expanded="false"
                                    aria-controls="collapse-{{$accountingHead->id}}">
                                    <span class="list-heading">

                                        {{ $accountingHead->custom_name }}</span>
                                    <span class="list-amount">

                                        {{ moneyFormat($mapIdToAmount[$accountingHead->id] ?? 0, null, 2, true) }}</span>
                                </a>

                            </div>
                            @if($accountingHead->hasChildren())
                            @include('includes.chart-children-amount', ['accountingHeads' => $accountingHead->children,
                            'accountingHead' => $accountingHead, 'level' => 1, 'parent' => 'gp-accordion'])
                            @endif
                        </div>
                        @endforeach
                    </div>

                </div>
                <div class="list-sub-footer-row">
                    <span>{{ __('labels.gross_profit') }}</span>
                    <span class="total">{{ moneyFormat($grossProfit, null, 2, true) }}</span>
                </div>
            </div>

            <div class="col-lg-12 mt-5">
                <div class="wrap-list-cells">
                    <div class="list-sub-heading-row">
                        <span>{{ __('labels.account') }}</span>
                        <span class="total">{{ __('labels.total') }}</span>
                    </div>
                    <div id="op-accordion" class="nlevel-list-group">
                        <!-- Repeat this for main account -->
                        @foreach($opAccountingHeads as $accountingHead)
                        <div class="card">
                            <!-- Main account heading -->
                            <div class="card-header" id="heading-{{$accountingHead->id}}">

                                <a class="collapsed" role="button" data-toggle="collapse"
                                    href="#collapse-{{$accountingHead->id}}" aria-expanded="false"
                                    aria-controls="collapse-{{$accountingHead->id}}">
                                    <span class="list-heading">{{ $accountingHead->custom_name }}</span>
                                    <span
                                        class="list-amount">{{ moneyFormat($mapIdToAmount[$accountingHead->id] ?? 0, null, 2, true) }}</span>
                                </a>

                            </div>
                            @if($accountingHead->hasChildren())
                            @include('includes.chart-children-amount', ['accountingHeads' => $accountingHead->children,
                            'accountingHead' => $accountingHead, 'level' => 1, 'parent' => 'op-accordion'])
                            @endif
                        </div>
                        @endforeach
                    </div>
                </div>

                <div class="list-sub-footer-row">
                    <span>{{ __('labels.operating_profit') }}</span>
                    <span class="total">{{ moneyFormat($operatingProfit, null, 2, true) }}</span>
                </div>

            </div>

            <div class="col-lg-12 mt-5">
                <div class="wrap-list-cells">
                    <div class="list-sub-heading-row">
                        <span>{{ __('labels.account') }}</span>
                        <span class="total">{{ __('labels.total') }}</span>
                    </div>
                    <div id="np-accordion" class="nlevel-list-group">
                        <!-- Repeat this for main account -->
                        @foreach($npAccountingHeads as $accountingHead)
                        <div class="card">
                            <!-- Main account heading -->
                            <div class="card-header" id="heading-{{$accountingHead->id}}">

                                <a class="collapsed" role="button" data-toggle="collapse"
                                    href="#collapse-{{$accountingHead->id}}" aria-expanded="false"
                                    aria-controls="collapse-{{$accountingHead->id}}">
                                    <span class="list-heading">{{ $accountingHead->custom_name }}</span>
                                    <span
                                        class="list-amount">{{ moneyFormat($mapIdToAmount[$accountingHead->id] ?? 0, null, 2, true) }}</span>
                                </a>
                            </div>
                            @if($accountingHead->hasChildren())
                            @include('includes.chart-children-amount', ['accountingHeads' => $accountingHead->children,
                            'accountingHead' => $accountingHead, 'level' => 1, 'parent' => 'np-accordion'])
                            @endif
                        </div>
                        @endforeach
                    </div>
                </div>
                
                <div class="list-sub-footer-row">
                    <span>{{ __('labels.net_income') }}</span>
                    <span class="total">{{ moneyFormat($netIncome, null, 2, true) }}</span>
                </div>
            </div>
        </div>
@if(!$isRawRequest)
    </div>
</div>
<div class="clearfix"></div>
@endif

@endsection