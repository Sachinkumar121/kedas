@inject('controller', 'App\Http\Controllers\Controller')

@if($type == $controller::CONTROL)
{!! Form::select('account-control', $account_control_options, '', ['class'=>"form-control single-search-selection", 'id'=>"account-control-options", 'required' => 'required'], $account_control_attributes) !!}
@else
{{-- Form::select('parent-account', $account_control_options, '', ['class'=>"form-control single-search-selection", 'id'=>"all-coa-options", 'required' => 'required', 'style'=>'display:none;']) --}}

<div class="dropdown hierarchy-select" id="multilevelselect" style="display: none;">
    <button type="button" class="btn btn-secondary dropdown-toggle" id="multilevelselect-button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false"></button>
    <div class="dropdown-menu" aria-labelledby="multilevelselect-button">
        <div class="hs-searchbox">
            <input type="text" class="form-control" autocomplete="off">
        </div>
        <div class="hs-menu-inner">
            <a class="dropdown-item" data-value="" data-level="1" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
            @foreach($account_control_options as $account_control_option)
            	<a class="dropdown-item" data-value="{{$account_control_option->id}}" data-level="{{$account_control_option->level}}" href="javascript::void(0)">{{$account_control_option->name}}</a>
            @endforeach
        </div>
    </div>
    <input class="d-none" name="parent-account" readonly="readonly" aria-hidden="true" type="text" />
    <input type="hidden" name="account-level" value="1">
</div>
@endif