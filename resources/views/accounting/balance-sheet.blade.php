@extends($isRawRequest ? 'layouts.raw' : 'layouts.dashboard' )

@section('title', trans('labels.balance_sheet'))

@section('content')

@if(!$isRawRequest)
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>

    <div class="invoice-container charts-block balance-list-block">

        <h2>{{ __('labels.balance_sheet') }}</h2>
@endif
        <form style="display: none;">
            <div class="wrap-list-head">
                    <strong>{{ __('labels.until') }} *</strong>
                    <input id="bs_end_date" class="form-control wrap-list-cell" name="end_date" value="{{ old('end_date', request()->end_date ?? getLastDayofMonth()) }}" autocomplete="off" />
                    <button class="btn-custom">{{ __('labels.generate_reports') }}</button>
            </div>
        </form>
        <div class="row accounting-report-block">
            <div class="col-lg-6">
                <div class="wrap-list-cells">
                    <h3>{{ __('labels.assets') }}</h3>
                    <div class="list-sub-heading-row">
                        <span>{{ __('labels.account') }}</span>
                        <span class="total">{{ __('labels.total') }}</span>
                    </div>
                    <div id="assets-accordion" class="nlevel-list-group">
                        <!-- Repeat this for main account -->
                        @foreach($assetsAccountingHeads as $accountingHead)
                        <div class="card">
                            <!-- Main account heading -->
                            <div class="card-header" id="heading-{{$accountingHead->id}}">
                                <a class="collapsed" role="button" data-toggle="collapse"
                                    href="#collapse-{{$accountingHead->id}}" aria-expanded="false"
                                    aria-controls="collapse-{{$accountingHead->id}}">
                                    <span class="list-heading">{{ $accountingHead->custom_name }}</span>
                                    <span class="list-amount">{{ moneyFormat($mapIdToAmount[$accountingHead->id] ?? 0, null, 2, true) }}</span>
                                </a>

                            </div>
                            @if($accountingHead->hasChildren())
                            @include('includes.chart-children-amount', ['accountingHeads' => $accountingHead->children,
                            'accountingHead' => $accountingHead, 'level' => 1, 'parent' => 'assets-accordion'])
                            @endif
                        </div>
                        @endforeach
                    </div>

                </div>
                <div class="list-sub-footer-row">
                    <span>{{ __('labels.total') }} {{ __('labels.assets') }}</span>
                    <span class="total">{{ moneyFormat($totalAssets, null, 2, true) }}</span>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="wrap-list-cells">
                    <h3>{{ __('labels.liabilities') }}</h3>
                    <div class="list-sub-heading-row">
                        <span>{{ __('labels.account') }}</span>
                        <span class="total">{{ __('labels.total') }}</span>
                    </div>
                    <div id="lib-accordion" class="nlevel-list-group">
                        <!-- Repeat this for main account -->
                        @foreach($libAccountingHeads as $accountingHead)
                        <div class="card">
                            <!-- Main account heading -->
                            <div class="card-header" id="heading-{{$accountingHead->id}}">
                                <a class="collapsed" role="button" data-toggle="collapse"
                                    href="#collapse-{{$accountingHead->id}}" aria-expanded="false"
                                    aria-controls="collapse-{{$accountingHead->id}}">
                                    <span class="list-heading">{{ $accountingHead->custom_name }}</span>
                                    <span class="list-amount">{{ moneyFormat($mapIdToAmount[$accountingHead->id] ?? 0, null, 2, true) }}</span>
                                </a>
                            </div>
                            @if($accountingHead->hasChildren())
                            @include('includes.chart-children-amount', ['accountingHeads' => $accountingHead->children,
                            'accountingHead' => $accountingHead, 'level' => 1, 'parent' => 'lib-accordion'])
                            @endif
                        </div>
                        @endforeach
                    </div>
                    <h3>{{ __('labels.equity') }}</h3>
                    <div class="list-sub-heading-row">
                        <span>{{ __('labels.account') }}</span>
                        <span class="total">{{ __('labels.total') }}</span>
                    </div>
                    <div id="equity-accordion" class="nlevel-list-group">
                        <!-- Repeat this for main account -->
                        @foreach($equityAccountingHeads as $accountingHead)
                        <div class="card">
                            <!-- Main account heading -->
                            <div class="card-header" id="heading-{{$accountingHead->id}}">
                                <a class="collapsed" role="button" data-toggle="collapse"
                                    href="#collapse-{{$accountingHead->id}}" aria-expanded="false"
                                    aria-controls="collapse-{{$accountingHead->id}}">
                                    <span class="list-heading">{{ $accountingHead->custom_name }}</span>
                                    <span class="list-amount">{{ moneyFormat($mapIdToAmount[$accountingHead->id] ?? 0, null, 2, true) }}</span>
                                </a>

                            </div>
                            @if($accountingHead->hasChildren())
                            @include('includes.chart-children-amount', ['accountingHeads' => $accountingHead->children,
                            'accountingHead' => $accountingHead, 'level' => 1, 'parent' => 'equity-accordion'])
                            @endif
                        </div>
                        @endforeach
                    </div>
                </div>

                <div class="list-sub-footer-row">
                    <span>{{ __('labels.total') }} {{ __('labels.liabilities') }} + {{ __('labels.equity') }}</span>
                    <span class="total">{{ moneyFormat($totalLib + $totalEquity, null, 2, true) }}</span>
                </div>
            </div>
        </div>
@if(!$isRawRequest)
    </div>
</div>
<div class="clearfix"></div>
@endif

@endsection
