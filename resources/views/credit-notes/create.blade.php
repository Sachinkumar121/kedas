@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.credit_note'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new').' '.__('labels.credit_note') }}</h2>
        <div class="default-form add-invoice-form">
            <form method="POST" action="{{ route('credit-notes.store') }}" id="add-credit-notes-form" enctype="multipart/form-data">
                @csrf
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-md-6">
                            <label for="contact">{{ __('labels.contact') }} <span>*</span></label>
                            {!! Form::select('contact_id', $contact_options, old('contact_id', null), ['class'=>"form-control single-search-selection", 'id'=>"contact_id", 'required' => 'required'], $contact_attributes) !!}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="date">{{ __('labels.date') }} <span>*</span></label>
                            <input id="credit_note_date" class="form-control" name="credit_note_date" required="required" value="{{old('credit_note_date', getTodayDate())}}" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="notes">{{ __('labels.notes') }}</label>
                            <textarea class="form-control" name="notes" id="notes">{{old('notes', null)}}</textarea>
                        </div>
                        <div class="form-group col-md-6 custom-select-controller">
                            <label for="ncf_id">{{ __('labels.ncf') }}</label>
                            {!! Form::select('ncf_id', $ncf_options, old('ncf_id', null), ['class'=>"form-control single-search-selection", 'id'=>"ncf_id"]) !!}
                        </div>
                        <div class="currency_exchange_cnt col-lg-6">
                            <div class="row">
                                <div class="form-group col-lg-4 col-md-12">
                                    <label for="currency">{{ __('labels.currency') }} {{ __('labels.local') }}</label>
                                    <input type="text" name="currency" class="form-control"
                                        value="{{ Auth::user()->company->currency->name }} ({{ Auth::user()->company->currency->symbol }})"
                                        readonly />
                                </div>
                                <div class="form-group col-lg-4 col-md-12 custom-select-controller">
                                    <label for="currency">{{ __('labels.exchange_currency') }}</label>
                                    {!! Form::select('exchange_currency', $currency_options, old('exchange_currency',
                                    null), ['class'=>"form-control single-search-selection", 'id'=>"exchange-currency"],
                                    $currency_attributes) !!}
                                    @error('exchange_currency')
                                    <span class="error" role="alert">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col-lg-4 col-md-12">
                                    <label for="currency">{{ __('labels.exchange_rate') }}</label>
                                    <input type="number" id="exchange_rate" name="exchange_rate" class="form-control"
                                        value="{{ old('exchange_rate') ?: null }}" autocomplete="off" min="0.01" step="any"/>
                                    @error('exchange_rate')
                                    <span class="error" role="alert">{{ $message }}</span>
                                    @enderror
                                    <label class="currency_exchange_inner" style="display:none">1 <span
                                        class="exchange_currency_label"></span> =
                                    {{ Auth::user()->company->currency->code }}
                                    ({{ Auth::user()->company->currency->symbol }}) <span
                                        class="exchange_currency_rate"></span> </label>
                                </div>
                    
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="image">{{ __('labels.documents') }} ({{ __('labels.pdf') }}, {{ __('labels.image') }})</label>
                            <div class="form-control dropzone" id="document-dropzone">
                                
                            </div>
                            <span class="error" role="alert" id="document-error" style="display:none"></span>
                        </div>
                    </div>
                    <div class="default-table add-invoice-table">
                        <div class="list-table-custom">
                            <table class="table table-striped" id="invoice_items">
                                <thead>
                                    <tr>
                                        <th class="th-item">{{ __('labels.item') }}</th>
                                        <th class="th-reference">{{ __('labels.reference') }}</th>
                                        <th class="th-price">{{ __('labels.price') }}</th>
                                        <th class="th-disc">{{ __('labels.disc') }} %</th>
                                        <th class="th-tax">{{ __('labels.tax') }}</th>
                                        <th class="th-tax_amount">{{ __('labels.tax_amount') }}</th>
                                        <!-- <th class="th-description">{{ __('labels.description') }}</th> -->
                                        <th class="th-quantity">{{ __('labels.quantity') }}</th>
                                        <th class="th-total">{{ __('labels.total') }}</th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="td-item">
                                            {!! Form::select('products[0][item]', $inventory_options, old('products[0][item]', null), ['class'=>"form-control small-single-search-selection", 'id'=>"products[0][item]", 'required' => 'required'], $inventory_attributes) !!}
                                        </td>
                                        <td class="td-reference">
                                            <input type="text" placeholder="{{ __('labels.reference') }}" name="products[0][reference]" id="reference" class="form-control" value="{{ old('products')[0]['reference'] ?: null }}" />
                                        </td>
                                        <td class="td-price">
                                            <input type="text" placeholder="{{ __('labels.unit_price') }}" name="products[0][price]" id="unitprice" class="form-control" readonly value="{{ old('products')[0]['price'] ?: null }}" />
                                        </td>
                                        <td class="td-disc" style="text-align: center;">
                                            <input type="number" min="0" max="99" placeholder="%" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}" name="products[0][discount]" id="discount" class="form-control" value="{{ old('products')[0]['discount'] ?: 0 }}" />
                                        </td>
                                        <td class="td-tax">
                                            {!! Form::select('products[0][tax]', $tax_options, old('products[0][tax]', null), ['class'=>"form-control small-single-search-selection", 'id'=>"products[0][tax]"], $tax_attributes) !!}
                                        </td>
                                        <td class="td-tax_amount">
                                            <input type="text" placeholder="{{ __('labels.tax_amount') }}" name="products[0][tax_amount]" id="products[0][tax_amount]" class="form-control" readonly value="{{ old('products')[0]['tax_amount'] ?: null }}" />
                                        </td>
                                        <td class="td-quantity">
                                            <input type="number" placeholder="1" name="products[0][quantity]" id="quantity" class="form-control" min="1" value="{{ old('products')[0]['quantity'] ?: 1 }}">
                                        </td>
                                        <td class="td-total">
                                            <span id="products[0][total]">{{Auth::user()->company->currency->symbol}}0.00</span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    @foreach (old('products', []) as $key => $product)
                                        @if ($key == 0)
                                            @continue
                                        @endif
                                        @include('app.item-row', ['row_number' => $key])
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <a class="btn-custom add-row"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.add_row') }}</a>
                    </div>
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-8 total-table">
                            <div class="total-table">
                                <table>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{Auth::user()->company->currency->symbol}}0.00</td>
                                    </tr>
                                    <tr class="total_exchanged_rate" style="display:none">
                                        <th>{{ __('labels.total') }} <span class="total-ex-currency-label"></span></th>
                                        <td> <span class="total-ex-currency-label-symbol"></span><span
                                                id="total-ex-currency-rate"></span></td>
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                            <label class="total_currency_exchange_inner" style="display:none">1 <span
                                                    class="exchange_currency_label"></span> =
                                                {{ Auth::user()->company->currency->code }}
                                                ({{ Auth::user()->company->currency->symbol }}) <span
                                                    class="exchange_currency_rate"></span> </label>
                                        </th>
                                    </tr>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group popup-btns">
                    <a href="{{ route('credit-notes.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom documents-submit-btn">{{ __('labels.save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/invoices.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/currency-exchange.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom-dropzone.js') }}"></script>
    <script type="text/javascript">
        addDropZone(null, 'both', 10);
    </script>
@endpush