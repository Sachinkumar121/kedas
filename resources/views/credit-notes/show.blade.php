@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.credit_notes'))
@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block" href="{{ route('credit-notes.index') }}">{{ __('labels.back') }}</a>
        <h2>{{ __('labels.credit_note') }}</h2>
        <div class="action-btns-view">
            <a target="_blank" href="{{route('credit-notes.print', $creditNote)}}" class="btn-custom"><i class="fa fa-print"></i> {{ __('labels.print') }}</a>
            <a href="{{route('credit-notes.pdf', $creditNote)}}" class="btn-custom"><i class="fa fa-download"></i> {{ __('labels.download') }}</a>
            <a role="button" data-toggle="modal" data-target="#creditNoteEmailModal" href="javascript:void(0)" class="btn-custom"><i class="fa fa-envelope"></i> {{ __('labels.send_by_mail') }}</a>
        </div>
        <div class="default-form view-form">
            <div class="generate-invoice">
                <div class="view-form-custom credit-show-table-wrap">
                    <table class="table credit-show-table">
                        <tr>
                            <td><strong>{{ __('labels.client') }}</strong></td>
                            @if($creditNote->contact instanceof App\Supplier)
                            <td><a href="{{route('suppliers.show', $creditNote->contact)}}">{{ $creditNote->contact->name }}</a></td>
                            @else
                            <td><strong><a href="{{route('clients.show', $creditNote->contact)}}"><b>{{ $creditNote->contact->name }}</b></a></strong></td>
                            @endif
                            <td style="text-align:right;"><strong>{{ __('labels.date') }}</strong></td>
                            <td>{{ dateFormat($creditNote->start_date) }}</td>
                        </tr>
                        <tr>
                            <td><strong>{{ __('labels.notes') }}</strong></td>
                            <td class="descrip-notes"><span>{{ $creditNote->notes }}</span></td>
                        </tr>
                        @if($creditNote->ncf)
                        <tr>
                            <td style="text-align:left;"><b>{{ __('labels.ncf') }}</b></td>
                            <td>{{ showIfAvailable($creditNote->ncf_number) }}</td>
                            <td style="text-align:right;"><b>{{ __('labels.invoice_with') }}</b></td>
                            <td>{{ showIfAvailable($creditNote->ncf->name) }}</td>
                        </tr>
                        @endif
                        @if($creditNote->currency)
                        <tr>
                            <td style="text-align:left;"><b>{{ __('labels.currency') }}</b></td>
                            <td>{{ $creditNote->currency->code }}</td>
                        </tr>
                        @endif
                    </table>
                </div>
                @if ($creditNote->documents->isNotEmpty())
                    <div class="form-group col-sm-12 col-lg-12 documents-container">
                        <label for="name">{{ __('labels.documents') }}: </label>
                        <div class="wrap-document-elements">
                            <div class="documents mt-2" itemscope itemtype="http://schema.org/ImageGallery">
                                @foreach($creditNote->documents->where('ext' ,'!=' , 'pdf') as $document)
                                    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                    <a href="{{ asset('public/storage/documents/'.$document->name) }}" itemprop="contentUrl" data-size="1024x1024">
                                    <img src="{{ asset('public/storage/documents/'.$document->name) }}" itemprop="thumbnail" alt="Image description" />
                                </a>
                                </figure>
                                @endforeach
                            </div>
                            <div class="pdf-documents">
                                @foreach($creditNote->documents->where('ext', 'pdf') as $document)
                                <a href="{{ asset('public/storage/documents/'.$document->name) }}" target="__blank"><img src="{{ asset('images/pdf.png') }}" itemprop="thumbnail" /></a>
                                @endforeach

                            </div>
                        </div>
                        
                        <!-- Root element of PhotoSwipe. Must have class pswp. -->
                        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                            <!-- Background of PhotoSwipe. 
                                It's a separate element, as animating opacity is faster than rgba(). -->
                            <div class="pswp__bg"></div>
                            <!-- Slides wrapper with overflow:hidden. -->
                            <div class="pswp__scroll-wrap">
                                <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
                                <!-- don't modify these 3 pswp__item elements, data is added later on. -->
                                <div class="pswp__container">
                                    <div class="pswp__item"></div>
                                    <div class="pswp__item"></div>
                                    <div class="pswp__item"></div>
                                </div>
                                <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                                <div class="pswp__ui pswp__ui--hidden">
                                    <div class="pswp__top-bar">
                                        <!--  Controls are self-explanatory. Order can be changed. -->
                                        <div class="pswp__counter"></div>
                                        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                                        <button class="pswp__button pswp__button--share" title="Share"></button>
                                        <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                                        <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                                        <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                                        <!-- element will get class pswp__preloader--active when preloader is running -->
                                        <div class="pswp__preloader">
                                            <div class="pswp__preloader__icn">
                                                <div class="pswp__preloader__cut">
                                                    <div class="pswp__preloader__donut"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                        <div class="pswp__share-tooltip"></div>
                                    </div>
                                    <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                                    </button>
                                    <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                                    </button>
                                    <div class="pswp__caption">
                                        <div class="pswp__caption__center"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="default-table add-invoice-table credit-invoice-table">
                    <div class="list-table-custom">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.item') }}</th>
                                    <th>{{ __('labels.reference') }}</th>
                                    <th>{{ __('labels.price') }}</th>
                                    <th>{{ __('labels.disc') }} %</th>
                                    <th>{{ __('labels.tax') }}</th>
                                    <th>{{ __('labels.tax_amount') }}</th>
                                    <th>{{ __('labels.description') }}</th>
                                    <th>{{ __('labels.quantity') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($creditNote->items as $item)
                                <tr>
                                    <td><a href="{{route('inventories.show', $item->inventory)}}">{{$item->inventory->name}}</a></td>
                                    <td>{{$item->reference}}</td>
                                    <td>{{moneyFormat($item->price,null,2,true)}}</td>
                                    <td>{{addZeros($item->discount,2,true)}}%</td>
                                    <td>{{$item->tax ? addZeros($item->tax->percentage,2,true).'%' : 'N/A'}}</td>
                                    <td>{{moneyFormat($item->calculation['taxAmount'],null,2,true)}}</td>
                                    <td>{{showIfAvailable($item->inventory->description)}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{moneyFormat($item->calculation['baseTotal'] - $item->calculation['discountAmount'],null,2,true)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-8 total-table">
                        <div class="total-table">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{moneyFormat($creditNote->calculation['baseTotal'],null,2,true)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{moneyFormat($creditNote->calculation['discountAmount'],null,2,true)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{moneyFormat($creditNote->calculation['subtotal'],null,2,true)}}</td>
                                    </tr>
                                    <tr class="tax_row">
                                        <th>{{ __('labels.tax') }}</th>
                                        <td>{{moneyFormat($creditNote->calculation['taxAmount'],null,2,true)}}</td>
                                    </tr>
                                    {{-- @foreach($creditNote->calculation['taxInfo'] as $taxInfo)
                                    <tr class="tax_row">
                                        <th>{{$taxInfo['formatted_name']}}</th>
                                        <td>{{moneyFormat($taxInfo['tax'])}}</td>
                                    </tr>
                                    @endforeach --}}
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{moneyFormat($creditNote->calculation['total'],null,2,true)}}</td>
                                    </tr>
                                    @if($creditNote->currency)
                                    <tr class="total_exchanged_rate">
                                        <th>{{ __('labels.total') }} <span class="total-ex-currency-label">{{ $creditNote->currency->code }}</span></th>
                                        <td>
                                            <span class="total-ex-currency-label-symbol">{{ $creditNote->currency->code }} {{ $creditNote->currency->symbol }}</span><span id="total-ex-currency-rate">{{ addZeros($creditNote->exchange_rate_total,6,true) }}</span>
                                        </td>
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                            <label class="total_currency_exchange_inner">1 
                                                <span class="exchange_currency_label">{{ $creditNote->currency->code }}</span> 
                                                = {{ Auth::user()->company->currency->code }} ({{ Auth::user()->company->currency->symbol }}) 
                                                <span class="exchange_currency_rate">{{ addZeros($creditNote->exchange_currency_rate,6,true) }}</span> 
                                            </label>
                                        </th>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="default-table client-invoices-table">
                        <h2>{{ __('labels.credit_to_invoices') }}</h2>
                        <div class="list-table-custom">
                            <table class="table table-striped" id="client_invoices">
                                <thead>
                                    <tr>
                                        <th>{{ __('labels.number') }}</th>
                                        <th>{{ __('labels.total') }}</th>
                                        <th>{{ __('labels.paid') }}</th>
                                        <th>{{ __('labels.pending') }}</th>
                                        <th>{{ __('labels.amount') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($creditNote->invoices->isNotEmpty())
                                        @foreach($creditNote->invoices as $index => $creditInvoice)
                                        <tr>
                                            <td>{{ $index+1 }}</td>
                                            <td>{{ moneyFormat($creditInvoice->calculation['total']) }}</td>
                                            <td>{{ moneyFormat($creditInvoice->calculation['paidAmount']) }}</td>
                                            <td>{{ moneyFormat($creditInvoice->calculation['pendingAmount']) }}</td>
                                            <td>{{ moneyFormat($creditInvoice->pivot->amount) }}</td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr class="empty-row">
                                            <td colspan="5"><p>{{ __('messages.invoice_has_not_credit_note') }}</p></td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
        
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
<div class="modal fade" id="creditNoteEmailModal" tabindex="-1" role="dialog" aria-labelledby="creditNoteEmailModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form id="send-invoice-mail" action="{{ route('credit-notes.mail', ['credit_note' => $creditNote]) }}" method="POST">
        @csrf
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="creditNoteEmailModalLabel">{{ __('labels.send_credit_note_to_emails') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <input type="text" class="form-control" required="required" name="emails" placeholder="{{ __('labels.separating_addresses_by_comma') }}" value="{{ $creditNote->contact->email ? $creditNote->contact->email.',': null }}">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('labels.cancel') }}</button>
            <button type="submit" class="btn btn-primary">{{ __('labels.send') }}</button>
          </div>
        </div>
    </form>
  </div>
</div>
@endsection
@push('before-body-scripts')
<script type="text/javascript" src="{{ asset('js/custom-photoswipe.js') }}"></script>
<script type="text/javascript">
	initPhotoSwipeFromDOM('.documents');
</script>
@endpush
