@extends('layouts.dashboard')

@section('title', trans('labels.roles'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.roles') }}</h2>
        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('roles.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label') }} {{ __('labels.role') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="roles-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.name') }}</th>
                                    <th>{{ __('labels.permissions') }}</th>
                                    <th class="to-show">{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($roles as $role)
                                <tr>
                                    <td>{{ ucfirst($role->custom_name) }}</td>
                                    <td>{{ $role->permissions->isNotEmpty() ? ($role->getCustomPermissionNames()->implode(', ')) : 'N/A' }}</td>
                                    <td class="to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('roles.edit', $role)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-role-form-' . $role->id }}" href="{{route('roles.destroy', $role)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['roles.destroy', $role], 'id' => "destroy-role-form-{$role->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                            <tr class="no-data-row">
                            </tr>
                            @endforelse                                
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
