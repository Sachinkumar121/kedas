@extends('layouts.dashboard')

@section('title', trans('labels.edit').' '.trans('labels.role'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.edit') }} {{ __('labels.role') }}</h2>
        {{ Form::open(array('method'=>'PUT','route' => ['roles.update', $role], 'id' => 'add-edit-role-form')) }}
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="name">{{ __('labels.name') }}</label>
                            <input class="form-control" type="text" name="name" placeholder="{{ __('labels.name') }}" required value="{{ old('name', $role->custom_name) }}" autocomplete="off" />
                            @error('name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="password">{{ __('labels.assign_permissions') }}</label>
                            {!! Form::select('permissions', $permission_options, old('permissions', ($assignedPermissions ?: null)), ['class'=>"form-control multiple-search-selection", 'multiple'=>'multiple', 'name'=>'permissions[]']) !!}
                            @error('permissions')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('roles.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.update') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
