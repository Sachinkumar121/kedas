@extends('layouts.dashboard')

@section('title', trans('labels.leave_types'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.leave_types') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating_an').' '.__('labels.leave_type') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('leave-types.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.leave_type') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="leave-types-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.name') }}</th>
                                    <th>{{ __('labels.number_of_days') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($leaveTypes as $type)
                                <tr>
                                    <td>{{ $type->name }}</td>
                                    <td>{{ $type->number_of_days }}</td>
                                    <td class="to-show">
                                        <div class="inner-to-show">
                                            <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                            <div class="dropdown-menu">
                                                <ul>
                                                    <li><a href="{{route('leave-types.edit', $type)}}">{{ __('labels.edit') }}</a></li>
                                                    <li><a class="delete_resource" data-resource="{{ 'destroy-leave-type-form-' . $type->id }}" href="{{route('leave-types.destroy', $type)}}">{{ __('labels.delete') }}</a></li>
                                                    {{ Form::open(array('method'=>'DELETE','route' => ['leave-types.destroy', $type], 'id' => "destroy-type-form-{$type->id}", 'style' => 'display: none')) }}
                                                    {!! Form::close() !!}
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                            <tr class="no-data-row">
                                <td colspan="7" rowspan="2" align="center">
                                    <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.leave_type') }}!</p></div>
                                    <div class="invoice-btns">
                                        <a href="{{ route('leave-types.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.leave_type') }}</a>
                                    </div>
                                </td>
                            </tr>
                            @endforelse                                
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
