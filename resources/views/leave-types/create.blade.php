@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.leave_type'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new').' '.__('labels.leave_type') }}</h2>
        <form method="POST" action="{{ route('leave-types.store') }}" id="add-edit-leave-type-form" enctype="multipart/form-data">
            @csrf
            <div class="default-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">{{ __('labels.name') }} <span>*</span></label>
                                <input class="form-control" type="text" name="name" placeholder="{{ __('labels.name') }}" required value="{{ old('name') }}" autocomplete="off" />
                                @error('name')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                            <div class="form-group">
                                <label for="number_of_days">{{ __('labels.number_of_days') }} <span>*</span></label>
                                <input type="number" min="1" step="1" name="number_of_days" id="number_of_days" class="form-control" value="{{ old('number_of_days', null) }}" required="required">
                                @error('number_of_days')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('leave-types.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
