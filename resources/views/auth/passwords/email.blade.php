@extends('layouts.auth')

@section('title', trans('labels.forgot_password'))

@section('content')
<div class="reset-form-bg outer-form-section">
    <div class="inner-sec">
        <div class="form-logo"><a href="{{ route('register') }}"><img src="{{ asset('images/form-logo.png') }}"></a>
            <h3>{{ __('labels.reset_your_password') }}</h3>
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{{ route('password.email') }}" class="custom-form-forgot-password" id="forgot-password-form">
                @csrf
                <ul>
                    <li>
                        <input id="email" type="email" placeholder="{{ __('labels.email_address') }}" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="error" role="alert">{{ $message }}</span>
                        @enderror
                    </li>
                    <li>
                        <input type="submit" name="" value="{{ __('labels.send_password_reset_link') }}" class="btn-custom">
                    </li>
                    <li>
                        <a class="btn btn-link" href="{{ route('login') }}">
                            {{ __('labels.back_to_login') }}
                        </a>
                    </li>
                    <li>
                        <p>{{ __('labels.dont_have_account') }}<a class="btn btn-link" href="{{ route('register') }}">{{ __('labels.sign_up_now') }}</a></p>
                    </li>
                    @include('includes.locale-selector')
                </ul>
            </form>
        </div>
    </div>
</div>
@endsection
