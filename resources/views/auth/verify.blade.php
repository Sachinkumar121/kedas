@extends('layouts.auth')

@section('title', trans('labels.verify_account'))

@section('header')
<header class="small-header">
    <div class="header-left">
        <div class="logo-sec">
            <a href="{{ route('dashboard') }}"><img src="{{ asset('images/form-logo.png') }}"></a>
        </div>
    </div>
    <div class="header-right">
        <div class="help-center">
            <ul>
                @include('includes.locale-selector')
            </ul>
        </div>
        <div class="john-sec">
            <a href="javascript:void(0);" class="dropdown-toggle username" data-toggle="dropdown">
            <span><img src="{{ asset('images/john-sec.png') }}" alt="{{ Auth::user()->name }}" /></span>
            <span class="username-wrap">{{ Auth::user()->name }}</span>
            </a>
            <div class="dropdown-menu">
                <ul>
                    <li class="user-name-li"><a href="javascript:void(0);"><span>{{ Auth::user()->name }}</span></a></li>
                    <li><a href="javascript:void(0);">{{ Auth::user()->email }}</a></li>
                    <li class="signout"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign Out</a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</header>
@endsection

@section('content')
<div class="reset-form-bg outer-form-section">
    <div class="inner-sec">
        @if (session('resent'))
            <div class="alert alert-success" role="alert">
                {{ __('messages.verification_link_sent') }}
            </div>
        @endif
        <div class="verify-email">
            <h3>{{ __('labels.verify_your_email_address') }}</h3>
            <p> {{ __('labels.check_your_email_for_verification_link') }}
            {{ __('labels.if_you_did_not_receive_email') }},</p>
            <a class="btn-custom" href="{{ route('verification.resend') }}"
                onclick="event.preventDefault();
                        document.getElementById('resend-verification-form').submit();">
                {{ __('labels.click_here_to_request_another') }}
            </a>

            <form id="resend-verification-form" class="d-inline" method="POST" action="{{ route('verification.resend') }}" style="display: none;">
                @csrf
            </form>
        </div>
    </div>
</div>
@endsection
