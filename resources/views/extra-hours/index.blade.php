@extends('layouts.dashboard')

@section('title', 'Extra Hours')

@section('content')
<div class="dashboard-right cust-dashboard-md-right cust-client-dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>Extra Hours</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">If you need help creating a Extra Hours.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <div id="export-buttons" style=""><div class="dt-buttons"><button class="dt-button buttons-excel buttons-html5 btn-custom outline-btn" tabindex="0" aria-controls="invoice-list-table" type="button"><span>Export in excel</span></button> <button class="dt-button buttons-pdf buttons-html5 btn-custom outline-btn" tabindex="0" aria-controls="invoice-list-table" type="button"><span>Export in pdf</span></button> </div></div>
            <a href="{{ route('extra-hours.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> New Extra Hours</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                       <table class="table table-striped" id="extra-hours-list-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Employee</th>
                                    <th>Type</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                    <th>Total Ex Hours</th>
                                    <th>Ex Hours Price</th>
                                    <th>Ex Hrs Amount</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                               @forelse ($extraHours as $key => $extraHour)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $extraHour->employee->name }}</td>
                                    <td>{{ $extraHour->type }}</td>
                                    <td>{{ timeFormat($extraHour->start_time) }}</td>
                                    <td>{{ timeFormat($extraHour->end_time) }}</td>
                                    <td>{{ $extraHour->total_extra_hours }}</td>
                                    <td>{{ moneyFormat($extraHour->hours_price) }}</td>
                                    <td>{{ moneyFormat($extraHour->extra_hours_total) }}</td>
                                    <td class="to-show">
                                    <div class="inner-to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('extra-hours.edit', $extraHour)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-extra-hour-form-' . $extraHour->id }}" href="{{route('extra-hours.destroy', $extraHour)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['extra-hours.destroy', $extraHour], 'id' => "destroy-extra-hour-form-{$extraHour->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
                                    </div>
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="7" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.extra_hour') }}!</p></div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('extra-hours.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label').' '.__('labels.extra_hour') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse   
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
