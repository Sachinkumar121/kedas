@extends('layouts.dashboard')

@section('title', trans('labels.currency').' '.trans('labels.exchange_rate'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.currency') }} {{ __('labels.exchange_rate') }}</h2>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="user-currency-rates-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.currency') }}</th>
                                    <th>{{ __('labels.rate') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($currency_rates as $currency_rate)
                                <tr>
                                    <td>{{ $currency_rate->currency->name }}</td>
                                    <td>{{ moneyFormat($currency_rate->rate) }}</td>
                                </tr>
                            @empty
                            <tr class="no-data-row">
                                <td colspan="7" rowspan="2" align="center">
                                    <div class="message"><p>{{ __('labels.you_have_not_yet_created') }} {{ __('labels.currency') }} {{ __('labels.exchange_rate') }}!</p></div>
                                    <div class="invoice-btns">
                                    </div>
                                </td>
                            </tr>
                            @endforelse                                
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
