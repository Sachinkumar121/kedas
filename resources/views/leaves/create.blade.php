@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.leave'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new').' '.__('labels.leave') }}</h2>
        <form method="POST" action="{{ route('leaves.store') }}" id="add-edit-leaves-form" enctype="multipart/form-data">
            @csrf
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Employee <span>*</span></label>
                                {!! Form::select('employee_id', $employee_options, '', ['class'=>"form-control single-search-selection", 'id'=>"employee_id"]) !!}
                                @error('employee')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="form-group col-md-6">
                            <label for="leave_type">{{ __('labels.leave_type') }} <span>*</span></label>
                            {!! Form::select('leave_type', $leave_type_options, '', ['class'=>"form-control single-search-selection", 'id'=>"leave_type"]) !!}
                            @error('leave_type')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror

                            <div class="pt-4">
                                @if (old('_token') === null)
                                <input type="radio" class="form-check-input" id="paid" name="is_paid" value="0" checked>
                                @else
                                <input type="radio" class="form-check-input" id="paid" name="is_paid" value="0" {{ old("is_paid") == 0 ? "checked" : "" }}>
                                @endif
                                <label class="form-check-label mr-2" for="paid">Paid</label>
                                <input type="radio" class="form-check-input" id="unpaid" name="is_paid" value="1" {{ old("is_paid") == 1 ? "checked" : "" }}>
                                <label class="form-check-label" for="unpaid">Unpaid</label>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="date">{{ __('labels.from_date') }} <span>*</span></label>
                            <input id="leave_from_date" class="form-control" name="from_date"
                                required="required" value="{{old('from_date')}}"
                                autocomplete="off" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="date">{{ __('labels.to_date') }} <span>*</span></label>
                            <input id="leave_to_date" class="form-control" name="to_date"
                                required="required" value="{{old('to_date')}}"
                                autocomplete="off" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="number_of_days">{{ __('labels.number_of_days') }} <span>*</span></label>
                            <input class="form-control" type="text" name="number_of_days" placeholder="{{ __('labels.number_of_days') }}" required value="{{ old('number_of_days') }}" autocomplete="off" readonly id="number_of_days" />
                            @error('number_of_days')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                       
                        <div class="form-group col-md-6">
                            <label for="note">{{ __('labels.important_note') }} </label>
                            <textarea class="form-control" name="note"
                                id="note">{{ old('note', null) }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('leaves.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
