@extends('layouts.dashboard')

@section('title', trans('labels.leave'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right cust-client-dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.leaves') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.leave') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('leaves.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_leave') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="leaves-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.employee') }}</th>
                                    <th>{{ __('labels.leave_type') }}</th>
                                    <th>{{ __('labels.from_date') }}</th>
                                    <th>{{ __('labels.to_date') }}</th>
                                    <th>{{ __('labels.number_of_days') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($leaves as $application)
                                <tr>
                                    <td>{{ $application->employee->name }}</td>
                                    <td>{{ $application->leave_type }}</td>
                                    <td>{{ dateFormat($application->application_from_date) }}</td>
                                    <td>{{ dateFormat($application->application_to_date) }}</td>
                                    <td>{{ $application->number_of_days }}</td>
                                    <td class="to-show">
                                        <div class="inner-to-show">
                                            <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                            <div class="dropdown-menu">
                                                <ul>
                                                    <li><a href="{{route('leaves.edit', $application)}}">{{ __('labels.edit') }}</a></li>
                                                    <li><a class="delete_resource" data-resource="{{ 'destroy-leave-form-' . $application->id }}" href="{{route('leaves.destroy', $application)}}">{{ __('labels.delete') }}</a></li>
                                                    {{ Form::open(array('method'=>'DELETE','route' => ['leaves.destroy', $application], 'id' => "destroy-leave-form-{$application->id}", 'style' => 'display: none')) }}
                                                    {!! Form::close() !!}
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                            <tr class="no-data-row">
                                <td colspan="7" rowspan="2" align="center">
                                    <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.leave') }}!</p></div>
                                    <div class="invoice-btns">
                                        <a href="{{ route('leaves.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_leave') }}</a>
                                    </div>
                                </td>
                            </tr>
                            @endforelse                                
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
