@extends('layouts.dashboard')

@section('title', trans('labels.accounting_adjustments'))

@section('content')
<div class="dashboard-right cust-client-dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.accounting_adjustments') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating_an').' '.__('labels.accounting_adjustments') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('accounting.adjustments.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.journal_entry') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom accounting-adjustments-list-table products-table">
                        <table class="table table-striped" id="accounting-adjustments-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.sr_number') }}</th>
                                    <th>{{ __('labels.reference') }}</th>
                                    <th>{{ __('labels.date') }}</th>
                                    <th>{{ __('labels.observations') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                    <th class="to-show">{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($accountAdjustments as $key => $adjustment)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td class="descrip-notes"><span>{{ $adjustment->reference }}</span></td>
                                    <td>{{ dateFormat($adjustment->adjustment_date) }}</td>
                                    <td class="descrip-notes"><span>{{ $adjustment->observations }}</span></td>
                                    <td>{{ moneyFormat($adjustment->total, null, 2, true) }}</td>
                                    <td class="to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('accounting.adjustments.show', $adjustment)}}">{{ __('labels.view') }}</a></li>
                                                <li><a href="{{route('accounting.adjustments.edit', $adjustment)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-accounting-adjustments-form-' . $adjustment->id }}" href="{{route('accounting.adjustments.destroy', $adjustment)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['accounting.adjustments.destroy', $adjustment], 'id' => "destroy-accounting-adjustments-form-{$adjustment->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @empty
                                <tr class="no-data-row">
                                    <td colspan="6" rowspan="2" align="center">
                                        <div class="message"><p>{{ __('labels.you_have_not_yet_created_an').' '.__('labels.accounting_adjustments') }}!</p></div>
                                        <div class="invoice-btns">
                                            <a href="{{ route('accounting.adjustments.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new').' '.__('labels.journal_entry') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforelse 
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
