@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.journal_entry'))
@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
     <a class="arrow-back d-block" href="{{ route('accounting.adjustments.index') }}">{{ __('labels.back') }}</a>
        <h2>{{ __('labels.journal_entry').' '.__('labels.details') }}</h2>
        <div class="default-form view-form-detail account-adjustments-list-table-show-table">
            <div class="generate-invoice">
                <div class="add-form row">
                    <div class="form-group col-sm-4">
                        <label for="date"><b>{{ __('labels.date') }}: </b></label>
                        <span class="value-form">{{ dateFormat($accountAdjustment->adjustment_date) }}</span>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="reference"><b>{{ __('labels.reference') }} </b></label>
                        <div class="descrip-notes"> 
                            <span>{{ showIfAvailable($accountAdjustment->reference) }}</span>
                        </div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="observations"><b>{{ __('labels.observations') }} </b></label>
                        <div class="descrip-notes"> 
                            <span>{{ showIfAvailable($accountAdjustment->observations) }}</span>
                        </div>
                    </div>
                </div>
                @if ($accountAdjustment->documents->isNotEmpty())
                    <div class="form-group col-lg-12 documents-container">
                        <label for="name" class="p-0">{{ __('labels.documents') }}: </label>
                        <div class="wrap-document-elements">
                            <div class="documents mt-2" itemscope itemtype="http://schema.org/ImageGallery">
                                @foreach($accountAdjustment->documents->where('ext' ,'!=' , 'pdf') as $document)
                                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                    <a href="{{ asset('public/storage/documents/'.$document->name) }}" itemprop="contentUrl"
                                        data-size="1024x1024">
                                        <img src="{{ asset('public/storage/documents/'.$document->name) }}"
                                            itemprop="thumbnail" alt="Image description" />
                                    </a>
                                </figure>
    							@endforeach
                            </div>
							<div class="pdf-documents">
                                @foreach($accountAdjustment->documents->where('ext', 'pdf') as $document)
                                <a href="{{ asset('public/storage/documents/'.$document->name) }}" target="__blank"><img
                                        src="{{ asset('images/pdf.png') }}" itemprop="thumbnail" /></a>
                                @endforeach
                            </div>

                            <!-- Root element of PhotoSwipe. Must have class pswp. -->
                            <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                                <!-- Background of PhotoSwipe. 
    								It's a separate element, as animating opacity is faster than rgba(). -->
                                <div class="pswp__bg"></div>
                                <!-- Slides wrapper with overflow:hidden. -->
                                <div class="pswp__scroll-wrap">
                                    <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
                                    <!-- don't modify these 3 pswp__item elements, data is added later on. -->
                                    <div class="pswp__container">
                                        <div class="pswp__item"></div>
                                        <div class="pswp__item"></div>
                                        <div class="pswp__item"></div>
                                    </div>
                                    <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                                    <div class="pswp__ui pswp__ui--hidden">
                                        <div class="pswp__top-bar">
                                            <!--  Controls are self-explanatory. Order can be changed. -->
                                            <div class="pswp__counter"></div>
                                            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                                            <button class="pswp__button pswp__button--share" title="Share"></button>
                                            <button class="pswp__button pswp__button--fs"
                                                title="Toggle fullscreen"></button>
                                            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                                            <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                                            <!-- element will get class pswp__preloader--active when preloader is running -->
                                            <div class="pswp__preloader">
                                                <div class="pswp__preloader__icn">
                                                    <div class="pswp__preloader__cut">
                                                        <div class="pswp__preloader__donut"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                            <div class="pswp__share-tooltip"></div>
                                        </div>
                                        <button class="pswp__button pswp__button--arrow--left"
                                            title="Previous (arrow left)">
                                        </button>
                                        <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                                        </button>
                                        <div class="pswp__caption">
                                            <div class="pswp__caption__center"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="default-table add-invoice-table">
                    <div class="list-table-custom">
                        @if ($accountAdjustment->accountingEntries->isNotEmpty())
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.contact') }}</th>
                                    <th>{{ __('labels.description') }}</th>
                                    <th>{{ __('labels.account') }}</th>
                                    <th>{{ __('labels.debit') }}</th>
                                    <th>{{ __('labels.credit') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($accountAdjustment->accountingEntries as $accountingEntry)
                                <tr>
                                    <td>{{ $accountingEntry->contact ? $accountingEntry->contact->name : 'N/A' }}</td>
                                    <td>{{ showIfAvailable($accountingEntry->description) }}</td>
                                    <td>{{ $accountingEntry->userAccountingHead->name }}</td>
                                    <td>{{ $accountingEntry->nature == 'debit' ? moneyFormat($accountingEntry->amount,null,2,true) : '' }}</td>
                                    <td>{{ $accountingEntry->nature == 'credit' ? moneyFormat($accountingEntry->amount,null,2,true) : '' }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>{{ __('labels.total') }}</td>
                                    <td id="debit_total">{{ moneyFormat($accountAdjustment->debit_credit_amount['debit'], null, 2, true) }}</td>
                                    <td id="debit_total">{{ moneyFormat($accountAdjustment->debit_credit_amount['credit'], null, 2, true) }}</td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('before-body-scripts')
<script type="text/javascript" src="{{ asset('js/custom-photoswipe.js') }}"></script>
<script type="text/javascript">
initPhotoSwipeFromDOM('.documents');
</script>
@endpush