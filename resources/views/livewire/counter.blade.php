<div style="text-align: center">

	<div class="col-lg-2">
	    <div class="input-group">
		    <span class="input-group-btn">
		        <button wire:click="decrement" type="button" class="quantity-left-minus btn btn-danger btn-number"  data-type="minus" data-field="">
		        	-
		        </button>
		    </span>
		    <input type="text" id="quantity" name="quantity" class="form-control input-number" wire:model="count" value="10" min="1" max="100">
		    <span class="input-group-btn">
		        <button wire:click="increment" type="button" class="quantity-right-plus btn btn-success btn-number" data-type="plus" data-field="">
		            +
		        </button>
		    </span>
		</div>
	</div>
    <h1>{{ $count }}</h1>
</div>