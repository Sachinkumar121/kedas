@extends('layouts.dashboard')

@section('title', trans('labels.create').' '.trans('labels.membership'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.new_label').' '.__('labels.membership') }}</h2>
        <form method="POST" action="{{ route('memberships.store') }}" id="add-edit-membership-form">
            @csrf
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="name">{{ __('labels.name') }} <span>*</span></label>
                            <input class="form-control" type="text" name="name" placeholder="{{ __('labels.membership').' '.__('labels.name') }}" required value="{{ old('name') }}" autocomplete="off" />
                            @error('name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="created_on">{{ __('labels.created_on') }} <span>*</span></label>
                            <input id="membership_created_date" class="form-control" name="created_date"
                                required="required" value="{{ old('created_date', getTodayDate()) }}"
                                autocomplete="off" />
                            @error('created_date')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="discount">{{ __('labels.discount') }}</label>
                            <input class="form-control" type="number" min="0.1" max="99" step="any" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}" name="discount" placeholder="{{ __('labels.discount') }}(%)" value="{{ old('discount', null) }}" autocomplete="off" />
                            @error('discount')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="point_percentage">{{ __('labels.point_percentage') }}</label>
                            <input class="form-control" type="number" min="0.1" max="99" step="any" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.point_percentage')]) }}" name="point_percentage" placeholder="{{ __('labels.point_percentage') }}(%)" value="{{ old('point_percentage', null) }}" autocomplete="off" />
                            @error('point_percentage')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="description">{{ __('labels.description') }}</label>
                            <textarea class="form-control" name="description" placeholder="{{ __('labels.membership').' '.__('labels.description') }}" autocomplete="off">{{ old('description', null) }}</textarea>
                            @error('description')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="point_expire_date">{{ __('labels.point_expire_date') }}</label>
                            <input id="membership_point_expire_date" class="form-control" name="point_expire_date" value="{{ old('point_expire_date', getLastDayofMonth()) }}"
                                autocomplete="off" />
                            @error('point_expire_date')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('memberships.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.save') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
