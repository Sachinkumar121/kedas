@extends('layouts.dashboard')

@section('title', trans('labels.memberships'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.memberships') }}</h2>
        <div class="custom-tooltip"><span class="help-icon"><i class="fa fa-question-circle" aria-hidden="true"></i>
            </span>
            <span class="tooltiptext">{{ __('labels.if_you_need_help_creating').' '.__('labels.membership') }}.</span>
        </div>
        <div class="invoice-btns invoice-btns-position">
            <a href="{{ route('memberships.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label').' '.__('labels.membership') }}</a>
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-table-custom  products-table">
                        <table class="table table-striped" id="memberships-list-table">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.name') }}</th>
                                    <th>{{ __('labels.discount') }}</th>
                                    <th>{{ __('labels.point_percentage') }}</th>
                                    <th>{{ __('labels.created_on') }}</th>
                                    <th>{{ __('labels.point_expire_date') }}</th>
                                    <th>{{ __('labels.description') }}</th>
                                    <th>{{ __('labels.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($memberships as $membership)
                                <tr>
                                    <td>{{ $membership->name }}</td>
                                    <td>{{ showIfAvailable($membership->discount) }}</td>
                                    <td>{{ showIfAvailable($membership->point_percentage) }}</td>
                                    <td>{{ dateFormat($membership->created_date) }}</td>
                                    <td>{{ dateFormat($membership->point_expire_date) }}</td>
                                    <td>{{ showIfAvailable($membership->description) }}</td>
                                    <td class="to-show">
                                        <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li><a href="{{route('memberships.edit', $membership)}}">{{ __('labels.edit') }}</a></li>
                                                <li><a class="delete_resource" data-resource="{{ 'destroy-membership-form-' . $membership->id }}" href="{{route('memberships.destroy', $membership)}}">{{ __('labels.delete') }}</a></li>
                                                {{ Form::open(array('method'=>'DELETE','route' => ['memberships.destroy', $membership], 'id' => "destroy-membership-form-{$membership->id}", 'style' => 'display: none')) }}
                                                {!! Form::close() !!}
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                            <tr class="no-data-row">
                                <td colspan="6" rowspan="2" align="center">
                                    <div class="message"><p>{{ __('labels.you_have_not_yet_created').' '.__('labels.membership') }}!</p></div>
                                    <div class="invoice-btns">
                                        <a href="{{ route('memberships.create') }}" class="btn-custom"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('labels.new_label').' '.__('labels.membership') }}</a>
                                    </div>
                                </td>
                            </tr>
                            @endforelse                                
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
