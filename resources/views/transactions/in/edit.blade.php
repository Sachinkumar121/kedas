@extends('layouts.dashboard')
@section('title', trans('labels.edit').' '.trans('labels.payment_received'))
@section('content')
<input type="hidden" name="resource_type" value="{{ $transaction->type }}">
<div class="dashboard-right cust-dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        @if($transaction->receipt_number)
            <h2>{{ __('labels.receipt').' #'.$transaction->receipt_number }}</h2>
        @elseif($transaction->voucher_number)
            <h2>{{ __('labels.voucher').' #'.$transaction->voucher_number }}</h2>
        @endif
        <div class="default-form add-transaction-form">
            {{ Form::open(array('method'=>'PUT', 'route' => ['transactions.update', $transaction], 'id' => 'edit-payment-received-form', 'data-transaction-id' => $transaction->id, 'enctype' => 'multipart/form-data')) }}
                <div class="generate-invoice">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4 text-center company-data"></div>
                        <div class="col-sm-4">
                            <div class="consumption-box">
                                <div class="consumption-bottom">
                                    <span>{{ __('labels.no') }}.</span>
                                    <div class="consumption-number">{{ $next_number }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="add-form row">
                        <input type="hidden" name="transaction_with_invoice" value="1" />
                        <div class="form-group col-sm-6">
                            <label for="contact">{{ __('labels.contact') }} <span>*</span></label>
                            {!! Form::select('contact_id', $contact_options, old('contact_id', $transaction->contact_custom_id), ['class'=>"form-control single-search-selection", 'id'=>"contact_id"], $contact_attributes) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="payment_received_start_date">{{ __('labels.date') }} <span>*</span></label>
                            <input id="payment_received_start_date" class="form-control" name="start_date" required="required" value="{{old('start_date', $transaction->start_date)}}" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="bank_account">{{ __('labels.bank_account') }} <span>*</span></label>
                            <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="account-multilevelselect">
                                <button type="button" class="btn btn-secondary dropdown-toggle" id="account-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                <div class="dropdown-menu" aria-labelledby="account-multilevelselect-button">
                                    <div class="hs-searchbox">
                                        <input type="text" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="hs-menu-inner">
                                        <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                                        @foreach($bank_accounts_options as $bank_accounts_option)
                                            <a class="dropdown-item" data-value="{{$bank_accounts_option->id}}" data-level="{{$bank_accounts_option->level}}" href="javascript::void(0)">
                                                <span class="account-name">{{$bank_accounts_option->custom_name}}</span>
                                                @if($bank_accounts_option->level == 1)
                                                <span class="detail-type">{{ $bank_accounts_option->parent->name }}</span>
                                                @endif
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                                <input class="d-none" name="bank_account" readonly="readonly" aria-hidden="true" type="text" value="{{ old('bank_account', $transaction->bank_account) }}" />
                            </div>
                            @error('bank_account')
                            <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="waytopay">{{ __('labels.way_to_pay') }} <span>*</span></label>
                            {!! Form::select('payment_method', $payment_methods_options, old('payment_method', $transaction->method), ['class'=>"form-control", 'id'=>"payment_method", 'required' => 'required']) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="annotation">{{ __('labels.payment_received_annotation') }}</label>
                            <textarea class="form-control" id="annotation" name="annotation">{{old('annotation', $transaction->annotation)}}</textarea>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="image">
                                        {{ __('labels.documents') }} ({{ __('labels.pdf') }}, {{ __('labels.image') }})
                            </label>
                            <div class="form-control dropzone" id="document-dropzone">
                                
                            </div>
                            <span class="error" role="alert" id="document-error" style="display:none"></span>
                        </div>
                        <!-- <div class="form-group col-sm-12 text-center register-income">
                            <h4>Do you want to associate this income with an existing invoice?</h4>
                            <p>Remember that you can register an income without it being associated with a invoice</p>
                            <div class="radio">
                                <input type="radio" name="with_invoice" id="with_invoice_yes" value="1">
                                <label for="with_invoice_yes">Yes</label>
                                <input type="radio" name="with_invoice" id="with_invoice_no" value="0">
                                <label for="with_invoice_no">No</label>
                            </div>
                        </div> -->
                    </div>
                    <div class="default-table client-invoices-table">
                        <div class="list-table-custom">
                            <table class="table table-striped" id="client_invoices">
                                <thead>
                                    <tr>
                                        <th>{{ __('labels.number') }}</th>
                                        <th>{{ __('labels.total') }}</th>
                                        <th>{{ __('labels.paid') }}</th>
                                        <th>{{ __('labels.pending') }}</th>
                                        <th>{{ __('labels.value_received') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($transaction->invoices as $key => $invoice)
                                    <tr>
                                        <input type="hidden" name="payments[{{$key}}][invoice_id]" value="{{ $invoice->id }}">
                                        <td>{{ $invoice->internal_number }}</td>
                                        <td>{{ moneyFormat($invoice->calculation['total']) }}</td>
                                        <td>{{ moneyFormat($invoice->calculation['paidAmount']) }}</td>
                                        <td>{{ moneyFormat($invoice->calculation['pendingAmount']) }}</td>
                                        <td>
                                            <input type="number" name="payments[{{$key}}][amount]" id="amount" class="form-control" min="0.01" max="{{ $invoice->calculation['pendingAmount'] + $invoice->pivot->amount }}" value="{{ old('payments')[$key]['amount'] ?: $invoice->pivot->amount }}" step="any" />
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-group popup-btns">
                    <a href="{{ route('transactions.index', ['type' => $type]) }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom documents-submit-btn">{{ __('labels.update') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/custom-dropzone.js') }}"></script>
    <script type="text/javascript">
        var documents = {!! json_encode($transaction->documents) !!};
        addDropZone(documents, 'both', 10);
    </script>
    <script type="text/javascript" src="{{ asset('js/currency-exchange.js') }}"></script>
@endpush