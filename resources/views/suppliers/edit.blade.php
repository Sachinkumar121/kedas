@extends('layouts.dashboard')

@section('title', trans('labels.edit').' '.trans('labels.supplier'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.update') }} {{ titleCase($supplier->name) }} {{ __('labels.details') }}</h2>
        {{ Form::open(array('method'=>'PUT','route' => ['suppliers.update', $supplier], 'id' => 'add-edit-supplier-form')) }}
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="number">{{ __('labels.identification_type') }}</label>
                            {!! Form::select('identification_type', $identification_type_options, $supplier->identification_type_id, ['class'=>"form-control single-search-selection", 'id'=>"identification_type"]) !!}
                            @error('identification_type')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="taxid">{{ __('labels.tax_id') }}</label>
                            <input class="form-control" type="text" name="tax_id" placeholder="{{ __('labels.tax_id') }}" value="{{ old('tax_id', $supplier->tax_id) }}" autocomplete="off" />
                            @error('tax_id')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="name">{{ __('labels.name') }} <span>*</span></label>
                            <input class="form-control" type="text" name="name" placeholder="{{ __('labels.name') }}" required value="{{ old('name', $supplier->name) }}" autocomplete="off" />
                            @error('name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="email">{{ __('labels.email') }} <span>*</span></label>
                            <input class="form-control" type="email" name="email" placeholder="{{ __('labels.email_address') }}" required value="{{ old('email', $supplier->email) }}" autocomplete="email" />
                            @error('email')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="department">{{ __('labels.department') }}</label>
                            {!! Form::select('sector', $sector_options, $supplier->sector_id, ['class'=>"form-control single-search-selection", 'id'=>"sector"]) !!}
                            @error('sector')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="phone">{{ __('labels.phone') }}</label>
                            <input class="form-control" type="tel" name="phone" placeholder="{{ __('labels.phone') }}" value="{{ old('phone',  $supplier->phone) }}" autocomplete="off" />
                            @error('phone')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="address">{{ __('labels.address') }}</label>
                            <input class="form-control" type="text" name="address" placeholder="{{ __('labels.address') }}" value="{{ old('address', $supplier->address ? $supplier->address->line_1 : null) }}" autocomplete="off" />
                            @error('address')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="city">{{ __('labels.city') }}</label>
                            <input class="form-control" type="text" name="city" placeholder="{{ __('labels.city') }}" value="{{ old('city', $supplier->address ? $supplier->address->city : null) }}" autocomplete="off" />
                            @error('city')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="state">{{ __('labels.state') }}</label>
                            <input class="form-control" type="text" name="state" placeholder="{{ __('labels.state') }}" value="{{ old('state', $supplier->address ? $supplier->address->state : null) }}" autocomplete="off" />
                            @error('state')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="address">{{ __('labels.country') }}</label>
                            {!! Form::select('country', $country_options, $supplier->address ? $supplier->address->country_id : null, ['class'=>"form-control single-search-selection", 'id'=>"country"]) !!}
                            @error('country')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="zip">{{ __('labels.zip_code') }}</label>
                            <input class="form-control" type="text" name="zip" placeholder="{{ __('labels.zip') }}" value="{{ old('zip', $supplier->address ? $supplier->address->zip : null) }}" autocomplete="off" />
                            @error('zip')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="mobile">{{ __('labels.mobile') }}</label>
                            <input class="form-control" type="tel" name="mobile" placeholder="{{ __('labels.mobile') }}" value="{{ old('mobile', $supplier->mobile) }}" autocomplete="off" />
                            @error('mobile')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="mobile">{{ __('labels.contact_name') }}</label>
                            <input class="form-control" type="text" name="contact_name" placeholder="{{ __('labels.contact_name') }}" value="{{ old('contact_name', $supplier->contact_name) }}" autocomplete="off" />
                            @error('contact_name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="mobile">{{ __('labels.contact_phone_number') }}</label>
                            <input class="form-control" type="tel" name="contact_phone_number" placeholder="{{ __('labels.contact_phone_number') }}" value="{{ old('contact_phone_number', $supplier->contact_phone_number) }}" autocomplete="off" />
                            @error('contact_phone_number')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('suppliers.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                        <button type="submit" name="submit" class="btn-custom">{{ __('labels.update') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
