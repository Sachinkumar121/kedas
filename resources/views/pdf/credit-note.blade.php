<!DOCTYPE html>
<html>
      <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
            <title>{{ __('labels.credit_note').'-'.$creditNote->id }}</title>
            <!-- CSS Reset : BEGIN -->
            <style type="text/css">
                  * {
                  overflow: visible !important;
                  }
                  table tr.page-break{
                  page-break-after:always !important;
                  page-break-before:always !important;
                  } 
                  @page {
                  size: Legal landscape;
                  margin: 0.4in
                  }
                  @media print {
                  body,
                  table,
                  tr,
                  td,
                  th,
                  tbody,
                  thead,
                  tfoot {
                  /* page-break-inside: avoid !important; */
                  }
                  table {
                  /* page-break-inside: avoid !important; */
                  }
                  table tr:nth-child(10) {
                  /* page-break-inside: avoid !important; */
                  /* page-break-after: always !important; */
                  }
                  table#main-invoice-table tr{
                  /* page-break-after:always !important; */
                  /* page-break-before:always !important; */
                  page-break-inside: avoid;
                  display: table-header-group;
                  break-inside: avoid;
                  page-break-inside: avoid;
                  }
                  }
                  @media screen,
                  print {
                  /* table tr.page-break{
                  page-break-after:always !important;
                  page-break-before:always !important;
                  }  */
                  /* What it does: Remove spaces around the email design added by some email clients. */
                  /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
                  html,
                  body {
                  margin: 0 auto !important;
                  padding: 0 !important;
                  font-family: 'Raleway', sans-serif !important;
                  width: 100%;
                  height: 100%;
                  }
                  body {
                  position: relative;
                  margin: 0 auto;
                  color: #555555;
                  background: #FFFFFF;
                  font-size: 16px !important;
                  page-break-inside: avoid !important;
                  width: 100%;
                  height: 100%;
                  zoom: 100%;
                  }
                  /* What it does: Stops email clients resizing small text. */
                  * {
                  -ms-text-size-adjust: 100%;
                  -webkit-text-size-adjust: 100%;
                  }
                  /* What it does: Centers email on Android 4.4 */
                  div[style*="margin: 16px 0"] {
                  margin: 0 !important;
                  }
                  /* What it does: Stops Outlook from adding extra spacing to tables. */
                  table,
                  td {
                  mso-table-lspace: 0pt !important;
                  mso-table-rspace: 0pt !important;
                  }
                  /* What it does: Fixes webkit padding issue. */
                  table {
                  border-spacing: 0 !important;
                  border-collapse: collapse !important;
                  }
                  /* What it does: Uses a better rendering method when resizing images in IE. */
                  img {
                  -ms-interpolation-mode: bicubic;
                  }
                  /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
                  a {
                  text-decoration: none;
                  }
                  /* What it does: A work-around for email clients meddling in triggered links. */
                  a[x-apple-data-detectors],
                  /* iOS */
                  .unstyle-auto-detected-links a,
                  .aBn {
                  border-bottom: 0 !important;
                  cursor: default !important;
                  color: inherit !important;
                  text-decoration: none !important;
                  font-size: inherit !important;
                  font-family: inherit !important;
                  font-weight: inherit !important;
                  line-height: inherit !important;
                  }
                  /* What it does: Prevents Gmail from changing the text color in conversation threads. */
                  .im {
                  color: inherit !important;
                  }
                  /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
                  .a6S {
                  display: none !important;
                  opacity: 0.01 !important;
                  }
                  /* If the above doesn't work, add a .g-img class to any image in question. */
                  img.g-img+div {
                  display: none !important;
                  }
                  html,
                  table,
                  tr,
                  td {
                  margin: 0;
                  padding: 0;
                  }
                  h2 {
                  margin: 0;
                  padding: 0
                  }
                  .responsive-table {
                  width: calc(33.3% - 4px);
                  display: inline-block;
                  vertical-align: middle;
                  text-align: left;
                  }
                  img {
                  max-width: 100%
                  }
                  .conatiner-space {
                  padding-left: 30px;
                  padding-right: 30px;
                  }
                  .clearfix:after {
                  content: "";
                  display: table;
                  clear: both;
                  }
                  a {
                  color: #303030;
                  text-decoration: none;
                  }
                  table th,
                  table td {
                  background: transparent;
                  text-align: center;
                  border-bottom: 1px solid #FFFFFF;
                  color: #303030;
                  font-size: 15px;
                  font-weight: 400 !important;
                  }
                  table strong {
                  font-weight: 700 !important;
                  }
                  table th {
                  white-space: nowrap;
                  font-weight: normal;
                  font-weight: 700 !important;
                  }
                  table td {
                  text-align: right;
                  padding: 20px;
                  }
                  table tbody tr:last-child td {
                  border: none;
                  }
                  table tfoot td {
                  padding: 10px 20px;
                  background: #FFFFFF;
                  border-bottom: none;
                  font-size: 1.2em;
                  white-space: nowrap;
                  border-top: 1px solid #AAAAAA;
                  }
                  table tfoot tr:first-child td {
                  border-top: none;
                  }
                  table tfoot tr td:first-child {
                  border: none;
                  }
                  #pay-table thead th,
                  #main-invoice-table thead th {
                  font-size: 15px !important;
                  color: #ffffff;
                  font-weight: 700 !important;
                  background: #0e68b1;
                  padding: 6px;
                  border: 3px solid #0e68b1;
                  }
                  #main-invoice-table tbody td,
                  #pay-table tbody td {
                  border: 2px solid #0e68b1;
                  text-align: center;
                  padding: 8px;
                  font-weight: 400 !important;
                  }
                  #main-invoice-table tfoot>tr>td {
                  text-align: center;
                  font-weight: 700 !important;
                  font-size: 15px !important;
                  padding: 8px;
                  }
                  #divider td {
                  border: none !important;
                  }
                  #comapny-name td,
                  #client-name td {
                  font-size: 14px !important;
                  }
                  }
                  @media (max-width:1300px) {
                  /* What it does: Remove spaces around the email design added by some email clients. */
                  /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
                  html,
                  body {
                  margin: 0 auto !important;
                  padding: 0 !important;
                  font-family: 'Raleway', sans-serif !important;
                  width: 100%;
                  height: 100%;
                  }
                  body {
                  position: relative;
                  margin: 0 auto;
                  color: #555555;
                  background: #FFFFFF;
                  font-size: 16px !important;
                  page-break-inside: avoid !important;
                  width: 100%;
                  height: 100%;
                  zoom: 100%;
                  }
                  /* What it does: Stops email clients resizing small text. */
                  * {
                  -ms-text-size-adjust: 100%;
                  -webkit-text-size-adjust: 100%;
                  }
                  /* What it does: Centers email on Android 4.4 */
                  div[style*="margin: 16px 0"] {
                  margin: 0 !important;
                  }
                  /* What it does: Stops Outlook from adding extra spacing to tables. */
                  table,
                  td {
                  mso-table-lspace: 0pt !important;
                  mso-table-rspace: 0pt !important;
                  }
                  /* What it does: Fixes webkit padding issue. */
                  table {
                  border-spacing: 0 !important;
                  border-collapse: collapse !important;
                  }
                  /* What it does: Uses a better rendering method when resizing images in IE. */
                  img {
                  -ms-interpolation-mode: bicubic;
                  }
                  /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
                  a {
                  text-decoration: none;
                  }
                  /* What it does: A work-around for email clients meddling in triggered links. */
                  a[x-apple-data-detectors],
                  /* iOS */
                  .unstyle-auto-detected-links a,
                  .aBn {
                  border-bottom: 0 !important;
                  cursor: default !important;
                  color: inherit !important;
                  text-decoration: none !important;
                  font-size: inherit !important;
                  font-family: inherit !important;
                  font-weight: inherit !important;
                  line-height: inherit !important;
                  }
                  /* What it does: Prevents Gmail from changing the text color in conversation threads. */
                  .im {
                  color: inherit !important;
                  }
                  /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
                  .a6S {
                  display: none !important;
                  opacity: 0.01 !important;
                  }
                  /* If the above doesn't work, add a .g-img class to any image in question. */
                  img.g-img+div {
                  display: none !important;
                  }
                  html,
                  table,
                  tr,
                  td {
                  margin: 0;
                  padding: 0;
                  }
                  h2 {
                  margin: 0;
                  padding: 0
                  }
                  .responsive-table {
                  width: calc(33.3% - 4px);
                  display: inline-block;
                  vertical-align: middle;
                  text-align: left;
                  }
                  img {
                  max-width: 100%
                  }
                  .conatiner-space {
                  padding-left: 30px;
                  padding-right: 30px;
                  }
                  .clearfix:after {
                  content: "";
                  display: table;
                  clear: both;
                  }
                  a {
                  color: #303030;
                  text-decoration: none;
                  }
                  table th,
                  table td {
                  background: transparent;
                  text-align: center;
                  border-bottom: 1px solid #FFFFFF;
                  color: #303030;
                  font-size: 15px;
                  font-weight: 400 !important;
                  }
                  table strong {
                  font-weight: 700 !important;
                  }
                  table th {
                  white-space: nowrap;
                  font-weight: normal;
                  font-weight: 700 !important;
                  }
                  table td {
                  text-align: right;
                  padding: 20px;
                  }
                  table tbody tr:last-child td {
                  border: none;
                  }
                  table tfoot td {
                  padding: 10px 20px;
                  background: #FFFFFF;
                  border-bottom: none;
                  font-size: 1.2em;
                  white-space: nowrap;
                  border-top: 1px solid #AAAAAA;
                  }
                  table tfoot tr:first-child td {
                  border-top: none;
                  }
                  table tfoot tr td:first-child {
                  border: none;
                  }
                  #pay-table thead th,
                  #main-invoice-table thead th {
                  font-size: 15px !important;
                  color: #ffffff;
                  font-weight: 700 !important;
                  background: #0e68b1;
                  padding: 6px;
                  border: 3px solid #0e68b1;
                  }
                  #main-invoice-table tbody td,
                  #pay-table tbody td {
                  border: 2px solid #0e68b1;
                  text-align: center;
                  padding: 8px;
                  font-weight: 400 !important;
                  }
                  #main-invoice-table tfoot>tr>td {
                  text-align: center;
                  font-weight: 700 !important;
                  font-size: 15px !important;
                  padding: 8px;
                  }
                  #divider td {
                  border: none !important;
                  }
                  #comapny-name td,
                  #client-name td {
                  font-size: 14px !important;
                  }
                  body,
                  table,
                  tr,
                  td,
                  th,
                  tbody,
                  thead,
                  tfoot {
                  page-break-inside: avoid !important;
                  }
                  }
                  @media (max-width:600px) {
                  .responsive-table {
                  width: 100%;
                  min-width: 100% !important;
                  }
                  .conatiner-space {
                  padding-left: 30px;
                  padding-right: 30px;
                  }
                  }
            </style>
      </head>
      <body>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" role="presentation">
            <tbody>
                  <tr>
                        <td align="center" valign="top">
                              <!--[if (mso)|(IE)]>
                              <table width="600" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                          <td align="left" valign="top" width="100%">
                                                <![endif]-->
                                                <!--[if mso 16]>
                                                <table width="600" border="0" cellspacing="0" cellpadding="0">
                                                      <tr>
                                                            <td align="left" valign="top" width="100%">
                                                                  <![endif]-->
                                                                  <table width="100%" style="max-width:1100px; margin:0 auto;" border="0" cellspacing="0"
                                                                        cellpadding="0">
                                                                        <tr style="border-bottom: 3px solid #0e68b1;">
                                                                              <td>
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                          <tr>
                                                                                                <td style="text-align:left; padding:0;">
                                                                                                      @if($user->company->logo)
                                                                                                      <img id="image-preview" src="{{ asset($user->company->logo) }}"
                                                                                                            alt="{{ $user->name }}" style="width: 141px;" />
                                                                                                      @endif
                                                                                                </td>
                                                                                                <td style="text-align:right; padding:0; font-size: 34px; color: #0e68b1;  line-height: 41px;
                                                                                                      ">
                                                                                                      <h2>{{ __('labels.credit_note') }}</h2>
                                                                                                </td>
                                                                                          </tr>
                                                                                    </table>
                                                                              </td>
                                                                        </tr>
                                                                        <tr>
                                                                              <td style="padding:0;">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                          <tbody>
                                                                                                <tr>
                                                                                                      <td style="text-align:left; max-width: 37%; padding:0; vertical-align: text-top;"
                                                                                                            id="comapny-name">
                                                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                  <tbody>
                                                                                                                        <tr>
                                                                                                                              <td style="text-align:left; padding:0;">
                                                                                                                                    <h2
                                                                                                                                          style="font-size: 17px; color: #0e68b1;   line-height: 34px;">
                                                                                                                                          {{ $user->company->name }}
                                                                                                                                    </h2>
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                              <td style="text-align:left; padding:0;">
                                                                                                                                    @if(addressFormat(Auth::user()->company->address, 1)[0]
                                                                                                                                    ??
                                                                                                                                    null)
                                                                                                                                    <span>{{ addressFormat(Auth::user()->company->address, 1)[0] }}</span>
                                                                                                                                    @endif
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                              <td style="text-align:left; padding:0;">
                                                                                                                                    @if(addressFormat(Auth::user()->company->address, 1)[1]
                                                                                                                                    ??
                                                                                                                                    null)
                                                                                                                                    <span>{{ addressFormat(Auth::user()->company->address, 1)[1] }}</span>
                                                                                                                                    @endif
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                              <td style="text-align:left; padding:0;">
                                                                                                                                    <span>{{ __('labels.phone') }}:
                                                                                                                                    {{ Auth::user()->company->phone }}
                                                                                                                                    @if(Auth::user()->company->fax)
                                                                                                                                    {{ __('labels.fax') }}:
                                                                                                                                    {{ Auth::user()->company->fax }} @endif</span>
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                              <td style="text-align:left; padding:0;">
                                                                                                                                    @if(Auth::user()->company->support_email)
                                                                                                                                    <span>{{ __('labels.support_email') }}: <a
                                                                                                                                          href="mailto:{{Auth::user()->company->support_email}}">{{Auth::user()->company->support_email}}</a></span>
                                                                                                                                    @endif
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                              <td style="text-align:left; padding:0;">
                                                                                                                                    @if(Auth::user()->company->tax_id)
                                                                                                                                    <span>{{ __('labels.tax_id') }}:
                                                                                                                                    {{ Auth::user()->company->tax_id }}</span>
                                                                                                                                    @endif
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                  </tbody>
                                                                                                            </table>
                                                                                                      </td>
                                                                                                      <td style="text-align:left; max-width: 29%; padding:0; vertical-align: text-top;"
                                                                                                            id="client-name">
                                                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                  <tbody>
                                                                                                                        <tr>
                                                                                                                              <td style="text-align:left; padding:0;">
                                                                                                                                    <h2
                                                                                                                                          style="font-size: 17px; color: #303030; line-height: 34px;">
                                                                                                                                          {{ $creditNote->contact->name }}
                                                                                                                                    </h2>
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                              <td style="text-align:left; padding:0;">
                                                                                                                                    @if($creditNote->contact->address)
                                                                                                                                    <span>{{ addressFormat($creditNote->contact->address, 1)[0] }}</span>
                                                                                                                                    @endif
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                              <td style="text-align:left; padding:0;">
                                                                                                                                    @if($creditNote->contact->address &&
                                                                                                                                    addressFormat($creditNote->contact->address, 1)[1] ?? null)
                                                                                                                                    <span>{{ addressFormat($creditNote->contact->address, 1)[1] }}</span>
                                                                                                                                    @endif
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                              <td style="text-align:left; padding:0;">
                                                                                                                                    <span>@if($creditNote->contact->phone)
                                                                                                                                    {{ __('labels.phone') }}:
                                                                                                                                    {{ $creditNote->contact->phone }}
                                                                                                                                    @endif @if($creditNote->contact->mobile)
                                                                                                                                    {{ __('labels.mobile') }}: {{ $creditNote->contact->mobile }}
                                                                                                                                    @endif</span>
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                              <td style="text-align:left; padding:0;">
                                                                                                                                    @if($creditNote->contact->email)
                                                                                                                                    <span>{{ __('labels.email') }}: <a
                                                                                                                                          href="mailto:{{$creditNote->contact->email}}">{{$creditNote->contact->email}}</a></span>
                                                                                                                                    @endif
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                              <td style="text-align:left; padding:0;">
                                                                                                                                    @if($creditNote->contact->tax_id)
                                                                                                                                    <span>{{ __('labels.tax_id') }}:
                                                                                                                                    {{ $creditNote->contact->tax_id }}</span>
                                                                                                                                    @endif
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                  </tbody>
                                                                                                            </table>
                                                                                                      </td>
                                                                                                      <td style="padding:0; vertical-align: top;">
                                                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                  <tbody>
                                                                                                                        <tr>
                                                                                                                              <td
                                                                                                                                    style="text-align: center; padding: 0; font-size: 17px; color: #ffffff; font-weight: 700 !important; background: #0e68b1; padding: 6px; border: 2px solid #0e68b1;">
                                                                                                                                    {{ __('labels.credit_note') }} {{ __('labels.no') }}
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                              <td
                                                                                                                                    style="text-align: center; padding: 0; border: 2px solid #0e68b1; font-size: 26px; font-weight: 700;line-height: 73px; color: #303030;">
                                                                                                                                    {{ $creditNote->id }}
                                                                                                                              </td>
                                                                                                                        </tr>
                                                                                                                  </tbody>
                                                                                                            </table>
                                                                                                      </td>
                                                                                                </tr>
                                                                                          </tbody>
                                                                                    </table>
                                                                              </td>
                                                                        </tr>
                                                                        <tr>
                                                                              <td style="padding:20px 0;">
                                                                                    <table id="pay-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                          <thead>
                                                                                                <tr>
                                                                                                      <th>{{ __('labels.created_by') }}</th>
                                                                                                      <th>{{ __('labels.tax_id') }}</th>
                                                                                                      <th>{{ __('labels.invoice_with') }}</th>
                                                                                                      <th>{{ __('labels.ncf') }}</th>
                                                                                                      <th>{{ __('labels.date') }}</th>
                                                                                                      @if($creditNote->currency)
                                                                                                      <th>{{ __('labels.currency') }}</th>
                                                                                                      @endif
                                                                                                      <th>{{ __('labels.way_to_pay') }}</th>
                                                                                                      <th>{{ __('labels.exp_date') }}</th>
                                                                                                </tr>
                                                                                          </thead>
                                                                                          <tbody>
                                                                                                <tr>
                                                                                                      <td>{{ $creditNote->user->name }}</td>
                                                                                                      <td>{{ showIfAvailable($user->company->tax_id)}} </td>
                                                                                                      <td>{{ $creditNote->ncf ? showIfAvailable($creditNote->ncf->name) : 'N/A' }}</td>
                                                                                                      <td>{{ $creditNote->ncf ? showIfAvailable($creditNote->ncf_number) : 'N/A' }}</td>
                                                                                                      <td>{{ dateFormat($creditNote->start_date) }}</td>
                                                                                                      @if($creditNote->currency)
                                                                                                      <td>{{ $creditNote->currency->code }}</td>
                                                                                                      @endif
                                                                                                      <td>{{ __('N/A') }}</td>
                                                                                                      <td>{{ dateFormat($creditNote->expiration_date) }}</td>
                                                                                                </tr>
                                                                                          </tbody>
                                                                                    </table>
                                                                              </td>
                                                                        </tr>
                                                                        <tr>
                                                                              <td style="padding:0;">
                                                                                    <table id="main-invoice-table" width="100%" border="0" cellspacing="0" cellpadding="0"
                                                                                          class="page-break">
                                                                                          <thead>
                                                                                                <tr>
                                                                                                      <th>{{ __('labels.qty') }}</th>
                                                                                                      <th>{{ __('labels.ref') }}</th>
                                                                                                      <th>{{ __('labels.item') }}</th>
                                                                                                      <th>{{ __('labels.tax_amount') }}</th>
                                                                                                      <th>{{ __('labels.price') }}</th>
                                                                                                      <th>{{ __('labels.disc') }}%</th>
                                                                                                      <th>{{ __('labels.total') }}</th>
                                                                                                </tr>
                                                                                          </thead>
                                                                                          <tbody>
                                                                                                @foreach($creditNote->items as $key => $item)
                                                                                                <tr style="{{ $key == 4 ? 'page-break-before: always !important;' : '' }}" >
                                                                                                      <td>{{$item->quantity}}</td>
                                                                                                      <td>{{showIfAvailable($item->reference)}}</td>
                                                                                                      <td>{{$item->inventory->name}}</td>
                                                                                                      <td>{{moneyFormat($item->calculation['taxAmount'],null,2,true)}}</td>
                                                                                                      <td>{{moneyFormat($item->price,null,2,true)}}</td>
                                                                                                      <td>{{addZeros($item->discount,2,true)}}%</td>
                                                                                                      <td>{{moneyFormat($item->calculation['total'],null,2,true)}}</td>
                                                                                                </tr>
                                                                                                @endforeach
                                                                                          </tbody>
                                                                                          <tfoot>
                                                                                                <tr>
                                                                                                      <td colspan="4"></td>
                                                                                                      <td colspan="2" style="text-align:right; font-weight: 700;">
                                                                                                            {{ __('labels.total') }} {{ __('labels.discount') }}
                                                                                                      </td>
                                                                                                      <td style="border: 2px solid #0e68b1;">
                                                                                                            {{moneyFormat($creditNote->calculation['discountAmount'],null,2,true)}}
                                                                                                      </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                      <td colspan="4"></td>
                                                                                                      <td colspan="2" style="text-align:right; font-weight: 700;">
                                                                                                            {{ __('labels.sub_total') }}
                                                                                                      </td>
                                                                                                      <td style="border: 2px solid #0e68b1;">
                                                                                                            {{moneyFormat($creditNote->calculation['subtotal'],null,2,true)}}
                                                                                                      </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                      <td colspan="4"></td>
                                                                                                      <td colspan="2" style="text-align:right; font-weight: 700;">
                                                                                                            {{ __('labels.tax') }}
                                                                                                      </td>
                                                                                                      <td style="border: 2px solid #0e68b1;">
                                                                                                            {{moneyFormat($creditNote->calculation['taxAmount'],null,2,true)}}
                                                                                                      </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                      <td colspan="4">
                                                                                                      </td>
                                                                                                      <td colspan="2" style="text-align:right; font-weight: 700;">
                                                                                                            {{ __('labels.total') }}
                                                                                                      </td>
                                                                                                      <td style="border: 2px solid #0e68b1;">
                                                                                                            {{moneyFormat($creditNote->calculation['total'],null,2,true)}}
                                                                                                      </td>
                                                                                                </tr>
                                                                                                @if($creditNote->currency)
                                                                                                <tr>
                                                                                                      <td colspan="4">
                                                                                                      </td>
                                                                                                      <td colspan="2" style="text-align:right; font-weight: 700;">
                                                                                                            {{ __('labels.total') }} {{ $creditNote->currency->code }}
                                                                                                      </td>
                                                                                                      <td style="border: 2px solid #0e68b1;">
                                                                                                            {{ $creditNote->currency->symbol }}{{ addZeros($creditNote->exchange_rate_total,6,true) }}
                                                                                                      </td>
                                                                                                </tr>
                                                                                                @endif
                                                                                          </tfoot>
                                                                                    </table>
                                                                              </td>
                                                                        </tr>
                                                                        <tr>
                                                                              <td style="padding:0;">
                                                                                    <table width="300px" border="0" cellspacing="0" cellpadding="0" id="divider">
                                                                                          <tbody>
                                                                                                <tr>
                                                                                                      <td
                                                                                                            style="padding:0; font-size: 14px;  border-bottom: 2px solid #0e68b1 !important; text-align:center; padding:0; font-weight: 400;">
                                                                                                            @if($user->company->signature)
                                                                                                            <img style="width: 193px; display:block; margin: 0 auto;"
                                                                                                                  id="signature-preview" src="{{ asset($user->company->signature) }}"
                                                                                                                  alt="{{ $user->name }}" />@endif
                                                                                                      </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                      <td style="text-align:center; padding:0; text-transform: capitalize;">
                                                                                                            {{ __('labels.elaborated_by') }}
                                                                                                      </td>
                                                                                                </tr>
                                                                                          </tbody>
                                                                                    </table>
                                                                              </td>
                                                                        </tr>
                                                                        <tr>
                                                                              <td style="text-align:center; padding: 40px 0px 10px 0px; font-size: 13px;">
                                                                                    <strong style="font-weight: 700;">{{ __('labels.notes') }}:
                                                                                    </strong>{{ $creditNote->notes }}
                                                                              </td>
                                                                        </tr>
                                                                        <!--[if mso 16]>
                                                                        </td>
                                                                        </tr>
                                                                  </table>
                                                                  <![endif]-->
                                                                  <!--[if (mso)|(IE)]>
                                                            </td>
                                                      </tr>
                                                </table>
                                                <![endif]-->
                                          </td>
                                    </tr>
                                    </tbody>
                              </table>
      </body>
</html>