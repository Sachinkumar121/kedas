<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,300;0,400;0,600;0,700;0,800;0,900;1,500&display=swap">
    <title>{{ __('labels.'.$invoice->type) }} {{ __('labels.invoice').'-'.$invoice->internal_number }}</title>
    <!-- CSS Reset : BEGIN -->
    <style type="text/css">
    /* What it does: Remove spaces around the email design added by some email clients. */
    /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */


    @media print {
    
        @page {
            size: A4
        }

            *{
            margin: 0;
        }

       .invoice-template-body-wrap {
            font-family: 'Raleway', sans-serif !important;
            height: 297mm;
            width: 210mm;
            margin: 0 auto !important;
            padding: 0 !important;
        }

        page[size="A4"] {
            width: 21cm;
            height: 29.7cm;
        }

        .invoice-template-body-wrap tbody::after {
            content: '';
            display: block;
            page-break-after: always;
            page-break-inside: avoid;
            page-break-before: avoid;
        }

        .invoice-template-body-wrap body {
            position: relative;
            margin: 0 auto;
            color: #555555 !important;
            background: #FFFFFF !important;
            font-size: 16px !important;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        .invoice-template-body-wrap div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        .invoice-template-body-wrap table,
        .invoice-template-body-wrap td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
            font-family: 'Raleway', sans-serif !important;
        }

        /* What it does: Fixes webkit padding issue. */
        .invoice-template-body-wrap table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            font-family: 'Raleway', sans-serif !important;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        .invoice-template-body-wrap img {
            -ms-interpolation-mode: bicubic;
        }

        /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
        .invoice-template-body-wrap a {
            text-decoration: none;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */
        .invoice-template-body-wrap a[x-apple-data-detectors],
        /* iOS */
        .invoice-template-body-wrap .unstyle-auto-detected-links a,
        .invoice-template-body-wrap .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from changing the text color in conversation threads. */
        .invoice-template-body-wrap .im {
            color: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
        .invoice-template-body-wrap .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        /* If the above doesn't work, add a .g-img class to any image in question. */
        .invoice-template-body-wrap img.g-img+div {
            display: none !important;
        }

        .invoice-template-body-wrap html,
        .invoice-template-body-wrap table,
        .invoice-template-body-wrap tr,
        .invoice-template-body-wrap td {
            margin: 0;
            padding: 0;
            font-family: 'Raleway', sans-serif !important;
        }

        .invoice-template-body-wrap h2 {
            margin: 0;
            padding: 0
        }

        .invoice-template-body-wrap .responsive-table {
            width: calc(33.3% - 4px);
            display: inline-block;
            vertical-align: middle;
            text-align: left;
        }

        .invoice-template-body-wrap img {
            max-width: 100%
        }

        .invoice-template-body-wrap .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
        }

       .invoice-template-body-wrap {
            border: 0;
        }

        .invoice-template-body-wrap .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        .invoice-template-body-wrap a {
            color: #303030 !important;
            text-decoration: none;
        }

        /* .invoice-template-body-wrap table th,
        .invoice-template-body-wrap table td {
            background: transparent !important;
            text-align: center;
            border-bottom: 1px solid #FFFFFF !important;
            color: #303030 !important;
            font-size: 15px;
            font-weight: 400 !important;
        } */

        .invoice-template-body-wrap table strong {
            font-weight: 700 !important;
        }

        .invoice-template-body-wrap table th {
            white-space: nowrap;
            font-weight: normal;
            font-weight: 700 !important;
        }

        .invoice-template-body-wrap table td {
            text-align: right;
            padding: 20px;
        }

        .invoice-template-body-wrap table tbody tr:last-child td {
            border: none;
        }

        .invoice-template-body-wrap table tfoot td {
            padding: 10px 20px;
            background: #FFFFFF !important;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
        }

        .invoice-template-body-wrap table tfoot tr:first-child td {
            border-top: none;
        }

        .invoice-template-body-wrap table tfoot tr td:first-child {
            border: none;
        }

        .invoice-template-body-wrap #pay-table thead th,
        .invoice-template-body-wrap #main-invoice-table thead th {
            font-size: 15px !important;
            color: #ffffff !important;
            font-weight: 700 !important;
            background: #0e68b1 !important;
            padding: 6px;
            border: 3px solid #0e68b1 !important;
        }

        .invoice-template-body-wrap #main-invoice-table tbody td,
        .invoice-template-body-wrap #pay-table tbody td {
            border: 2px solid #0e68b1;
            text-align: center;
            padding: 8px;
            font-weight: 400 !important;
        }

        .invoice-template-body-wrap #main-invoice-table tfoot>tr>td {
            text-align: center;
            font-weight: 700 !important;
            font-size: 15px !important;
            padding: 8px;
        }

        .invoice-template-body-wrap #divider td {
            border: none !important;
        }

        .invoice-template-body-wrap #comapny-name td,
        .invoice-template-body-wrap #client-name td {
            font-size: 14px !important;
        }

        .invoice-template-body-wrap tfoot {
            position: relative;
            bottom: 0;
            display: table-footer-group;
        }

        .invoice-template-body-wrap table {
            -fs-table-paginate: paginate !important;
        }

        .invoice-template-body-wrap table thead {
            display: table-header-group;
            break-inside: avoid !important;
            page-break-inside: avoid !important;
        }

        .invoice-template-body-wrap #inner-main-table>td,
        .invoice-template-body-wrap #inner-main-table>tbody {
            border: 0px solid #0e68b1 !important;
        }

        .invoice-template-body-wrap #inner-main-table table,
        .invoice-template-body-wrap #inner-main-table table tbody,
        .invoice-template-body-wrap #main-invoice-table table tbody {
            border: none !important;
        }

        .invoice-template-body-wrap #inner-main-table tfoot td,
        .invoice-template-body-wrap #inner-main-table tfoot tr,
        .invoice-template-body-wrap #inner-main-table tfoot {
            border: none;
        }

        .invoice-template-body-wrap table,
        .invoice-template-body-wrap tr,
        .invoice-template-body-wrap td,
        .invoice-template-body-wrap div {
            page-break-inside: avoid !important;
        }

        .invoice-template-body-wrap #main-invoice-table {
            position: relative;
        }

        .invoice-template-body-wrap #main-invoice-table thead {
            display: table-row-group;
        }

        .invoice-template-body-wrap tfoot {
            display: table-row-group;
        }

        .invoice-template-body-wrap tr {
            page-break-inside: avoid;
        }
    }
    @page {
            size: A4
        }

           * {
            margin: 0;
        }

        .invoice-template-body-wrap {
            font-family: 'Raleway', sans-serif !important;
            height: 297mm;
            width: 210mm;
            margin: 0 auto !important;
            padding: 0 !important;
        }

        page[size="A4"] {
            width: 21cm;
            height: 29.7cm;
        }

        .invoice-template-body-wrap tbody::after {
            content: '';
            display: block;
            page-break-after: always;
            page-break-inside: avoid;
            page-break-before: avoid;
        }

        .invoice-template-body-wrap body {
            position: relative;
            margin: 0 auto;
            color: #555555 !important;
            background: #FFFFFF !important;
            font-size: 16px !important;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        .invoice-template-body-wrap div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        .invoice-template-body-wrap table,
        .invoice-template-body-wrap td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
            font-family: 'Raleway', sans-serif !important;
        }

        /* What it does: Fixes webkit padding issue. */
        .invoice-template-body-wrap table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            font-family: 'Raleway', sans-serif !important;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        .invoice-template-body-wrap img {
            -ms-interpolation-mode: bicubic;
        }

        /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
        .invoice-template-body-wrap a {
            text-decoration: none;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */
        .invoice-template-body-wrap a[x-apple-data-detectors],
        /* iOS */
        .invoice-template-body-wrap .unstyle-auto-detected-links a,
        .invoice-template-body-wrap .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from changing the text color in conversation threads. */
        .invoice-template-body-wrap .im {
            color: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
        .invoice-template-body-wrap .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        /* If the above doesn't work, add a .g-img class to any image in question. */
        .invoice-template-body-wrap img.g-img+div {
            display: none !important;
        }

        .invoice-template-body-wrap html,
        .invoice-template-body-wrap table,
        .invoice-template-body-wrap tr,
        .invoice-template-body-wrap td {
            margin: 0;
            padding: 0;
            font-family: 'Raleway', sans-serif !important;
        }

        .invoice-template-body-wrap h2 {
            margin: 0;
            padding: 0
        }

        .invoice-template-body-wrap .responsive-table {
            width: calc(33.3% - 4px);
            display: inline-block;
            vertical-align: middle;
            text-align: left;
        }

        .invoice-template-body-wrap img {
            max-width: 100%
        }

        .invoice-template-body-wrap .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
        }

        .invoice-template-body-wrap {
            border: 0;
        }

        .invoice-template-body-wrap .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        .invoice-template-body-wrap a {
            color: #303030;
            text-decoration: none;
        }

        .invoice-template-body-wrap table th,
        .invoice-template-body-wrap table td {
            /* background: transparent !important; */
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
            color: #303030 !important;
            font-size: 15px;
            font-weight: 400 !important;
        }

        .invoice-template-body-wrap table strong {
            font-weight: 700 !important;
        }

        .invoice-template-body-wrap table th {
            white-space: nowrap;
            font-weight: normal;
            font-weight: 700 !important;
        }

        .invoice-template-body-wrap table td {
            text-align: right;
            padding: 20px;
        }

        .invoice-template-body-wrap table tbody tr:last-child td {
            border: none;
        }

        .invoice-template-body-wrap table tfoot td {
            padding: 10px 20px;
            background: #FFFFFF !important;
            border-bottom: none;
            font-size: 1.2em !important;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
        }

        .invoice-template-body-wrap table tfoot tr:first-child td {
            border-top: none;
        }

        .invoice-template-body-wrap table tfoot tr td:first-child {
            border: none;
        }

        .invoice-template-body-wrap #pay-table thead th,
        .invoice-template-body-wrap #main-invoice-table thead th {
            font-size: 15px !important;
            color: #ffffff !important;
            font-weight: 700 !important;
            background: #0e68b1 !important;
            padding: 6px;
            border: 3px solid #0e68b1 !important;
        }

        .invoice-template-body-wrap #main-invoice-table tbody td,
        .invoice-template-body-wrap #pay-table tbody td {
            border: 2px solid #0e68b1;
            text-align: center;
            padding: 8px;
            font-weight: 400 !important;
        }

        .invoice-template-body-wrap #main-invoice-table tfoot>tr>td {
            text-align: center;
            font-weight: 700 !important;
            font-size: 15px !important;
            padding: 8px;
        }

        .invoice-template-body-wrap #divider td {
            border: none !important;
        }

        .invoice-template-body-wrap #comapny-name td,
        .invoice-template-body-wrap #client-name td {
            font-size: 14px !important;
        }

        .invoice-template-body-wrap tfoot {
            position: relative;
            bottom: 0;
            display: table-footer-group;
        }

        .invoice-template-body-wrap table {
            -fs-table-paginate: paginate !important;
        }

        .invoice-template-body-wrap table thead {
            display: table-header-group;
            break-inside: avoid !important;
            page-break-inside: avoid !important;
        }

        .invoice-template-body-wrap #inner-main-table>td,
        .invoice-template-body-wrap #inner-main-table>tbody {
            border: 0px solid #0e68b1 !important;
        }

        .invoice-template-body-wrap #inner-main-table table,
        .invoice-template-body-wrap #inner-main-table table tbody,
        .invoice-template-body-wrap #main-invoice-table table tbody {
            border: none !important;
        }

        .invoice-template-body-wrap #inner-main-table tfoot td,
        .invoice-template-body-wrap #inner-main-table tfoot tr,
        .invoice-template-body-wrap #inner-main-table tfoot {
            border: none;
        }

        .invoice-template-body-wrap table,
        .invoice-template-body-wrap tr,
        .invoice-template-body-wrap td,
        .invoice-template-body-wrap div {
            page-break-inside: avoid !important;
        }

        .invoice-template-body-wrap #main-invoice-table {
            position: relative;
        }

        .invoice-template-body-wrap #main-invoice-table thead {
            display: table-row-group;
        }

        .invoice-template-body-wrap tfoot {
            display: table-row-group;
        }

        .invoice-template-body-wrap tr {
            page-break-inside: avoid;
        }
    @media (max-width:1200px) { 
        @page {
            size: A4
        }

            {
            margin: 0;
        }

        .invoice-template-body-wrap {
            font-family: 'Raleway', sans-serif !important;
            height: 297mm;
            width: 210mm;
            margin: 0 auto !important;
            padding: 0 !important;
        }

        page[size="A4"] {
            width: 21cm;
            height: 29.7cm;
        }

        .invoice-template-body-wrap tbody::after {
            content: '';
            display: block;
            page-break-after: always;
            page-break-inside: avoid;
            page-break-before: avoid;
        }

        .invoice-template-body-wrap body {
            position: relative;
            margin: 0 auto;
            color: #555555 !important;
            background: #FFFFFF !important;
            font-size: 16px !important;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        .invoice-template-body-wrap div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        .invoice-template-body-wrap table,
        .invoice-template-body-wrap td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
            font-family: 'Raleway', sans-serif !important;
        }

        /* What it does: Fixes webkit padding issue. */
        .invoice-template-body-wrap table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            font-family: 'Raleway', sans-serif !important;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        .invoice-template-body-wrap img {
            -ms-interpolation-mode: bicubic;
        }

        /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
        .invoice-template-body-wrap a {
            text-decoration: none;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */
        .invoice-template-body-wrap a[x-apple-data-detectors],
        /* iOS */
        .invoice-template-body-wrap .unstyle-auto-detected-links a,
        .invoice-template-body-wrap .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from changing the text color in conversation threads. */
        .invoice-template-body-wrap .im {
            color: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
        .invoice-template-body-wrap .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        /* If the above doesn't work, add a .g-img class to any image in question. */
        .invoice-template-body-wrap img.g-img+div {
            display: none !important;
        }

        .invoice-template-body-wrap html,
        .invoice-template-body-wrap table,
        .invoice-template-body-wrap tr,
        .invoice-template-body-wrap td {
            margin: 0;
            padding: 0;
            font-family: 'Raleway', sans-serif !important;
        }

        .invoice-template-body-wrap h2 {
            margin: 0;
            padding: 0
        }

        .invoice-template-body-wrap .responsive-table {
            width: calc(33.3% - 4px);
            display: inline-block;
            vertical-align: middle;
            text-align: left;
        }

        .invoice-template-body-wrap img {
            max-width: 100%
        }

        .invoice-template-body-wrap .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
        }

       .invoice-template-body-wrap {
            border: 0;
        }

        .invoice-template-body-wrap .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        .invoice-template-body-wrap a {
            color: #303030 !important;
            text-decoration: none;
        }

        .invoice-template-body-wrap table th,
        .invoice-template-body-wrap table td {
            background: transparent !important;
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
            color: #303030 !important;
            font-size: 15px;
            font-weight: 400 !important;
        }

        .invoice-template-body-wrap table strong {
            font-weight: 700 !important;
        }

        .invoice-template-body-wrap table th {
            white-space: nowrap;
            font-weight: normal;
            font-weight: 700 !important;
        }

        .invoice-template-body-wrap table td {
            text-align: right;
            padding: 20px;
        }

        .invoice-template-body-wrap table tbody tr:last-child td {
            border: none;
        }

        .invoice-template-body-wrap table tfoot td {
            padding: 10px 20px;
            background: #FFFFF !important;
            border-bottom: none;
            font-size: 1.2em !important;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
        }

        .invoice-template-body-wrap table tfoot tr:first-child td {
            border-top: none;
        }

        .invoice-template-body-wrap table tfoot tr td:first-child {
            border: none;
        }

        .invoice-template-body-wrap #pay-table thead th,
        .invoice-template-body-wrap #main-invoice-table thead th {
            font-size: 15px !important;
            color: #ffffff;
            font-weight: 700 !important;
            background: #0e68b1 !important;
            padding: 6px;
            border: 3px solid #0e68b1 !important;
        }

        .invoice-template-body-wrap #main-invoice-table tbody td,
        .invoice-template-body-wrap #pay-table tbody td {
            border: 2px solid #0e68b1 !important;
            text-align: center;
            padding: 8px;
            font-weight: 400 !important;
        }

        .invoice-template-body-wrap #main-invoice-table tfoot>tr>td {
            text-align: center;
            font-weight: 700 !important;
            font-size: 15px !important;
            padding: 8px;
        }

        .invoice-template-body-wrap #divider td {
            border: none !important;
        }

        .invoice-template-body-wrap #comapny-name td,
        .invoice-template-body-wrap #client-name td {
            font-size: 14px !important;
        }

        .invoice-template-body-wrap tfoot {
            position: relative;
            bottom: 0;
            display: table-footer-group;
        }

        .invoice-template-body-wrap table {
            -fs-table-paginate: paginate !important;
        }

        .invoice-template-body-wrap table thead {
            display: table-header-group;
            break-inside: avoid !important;
            page-break-inside: avoid !important;
        }

        .invoice-template-body-wrap #inner-main-table>td,
        .invoice-template-body-wrap #inner-main-table>tbody {
            border: 0px solid #0e68b1 !important;
        }

        .invoice-template-body-wrap #inner-main-table table,
        .invoice-template-body-wrap #inner-main-table table tbody,
        .invoice-template-body-wrap #main-invoice-table table tbody {
            border: none !important;
        }

        .invoice-template-body-wrap #inner-main-table tfoot td,
        .invoice-template-body-wrap #inner-main-table tfoot tr,
        .invoice-template-body-wrap #inner-main-table tfoot {
            border: none;
        }

        .invoice-template-body-wrap table,
        .invoice-template-body-wrap tr,
        .invoice-template-body-wrap td,
        .invoice-template-body-wrap div {
            page-break-inside: avoid !important;
        }

        .invoice-template-body-wrap #main-invoice-table {
            position: relative;
        }

        .invoice-template-body-wrap #main-invoice-table thead {
            display: table-row-group;
        }

        .invoice-template-body-wrap tfoot {
            display: table-row-group;
        }

        .invoice-template-body-wrap tr {
            page-break-inside: avoid;
        }
    }

    @media (max-width:600px) {
        .invoice-template-body-wrap .responsive-table {
            width: 100%;
            min-width: 100% !important;
        }

        .invoice-template-body-wrap .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
        }
    }

    
    </style>
</head>

<body>
<div class="invoice-template-body-wrap">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" role="presentation">
        <tr>
            <td align="center" valign="top">
                <!--[if (mso)|(IE)]>
                <table width="600" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="left" valign="top" width="100%">
                            <![endif]-->
                <!--[if mso 16]>
                            <table width="600" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" valign="top" width="100%">
                                        <![endif]-->
                <table width="100%" style="max-width:1100px; margin:0 auto;" border="0" cellspacing="0" cellpadding="0">
                    <tr style="border-bottom: 3px solid #0e68b1;">
                        <td style="padding:20px 0;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align:left; padding:0;">
                                        @if($user->company->logo)
                                        <img id="image-preview" src="{{ asset($user->company->logo) }}"
                                            alt="{{ $user->company->name }}" style="width: 200px;" />
                                        @endif
                                    </td>
                                    <td style="text-align:right; padding:0; font-size: 34px; color: #0e68b1;  line-height: 41px;
                                                                ">
                                        <h2>{{ __('labels.invoice') }}</h2>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="border:none;">
                        <td style="padding:0; border:none !important;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:none;">
                                <tbody style="border:none !important;">
                                    <tr style="border:none;">
                                        <td style="text-align:left; max-width: 37%; padding:0; vertical-align: text-top;"
                                            id="comapny-name">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody style="border: none !important;">
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            <h2
                                                                style="font-size: 17px; color: #0e68b1;   line-height: 34px;">
                                                                {{ $user->company->name }}
                                                            </h2>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if(addressFormat($user->company->address, 1)[0]
                                                            ??
                                                            null)
                                                            <span>{{ addressFormat($user->company->address, 1)[0] }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if(addressFormat($user->company->address, 1)[1]
                                                            ??
                                                            null)
                                                            <span>{{ addressFormat($user->company->address, 1)[1] }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            <span>{{ __('labels.phone') }}:
                                                                {{ $user->company->phone }}
                                                                @if($user->company->fax)
                                                                {{ __('labels.fax') }}:
                                                                {{ $user->company->fax }} @endif</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($user->company->support_email)
                                                            <span>{{ __('labels.support_email') }}: <a
                                                                    href="mailto:{{$user->company->support_email}}">{{$user->company->support_email}}</a></span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($user->company->tax_id)
                                                            <span>{{ __('labels.tax_id') }}:
                                                                {{ $user->company->tax_id }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td style="text-align:left; max-width: 29%; padding:0; vertical-align: text-top;"
                                            id="client-name">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody style="border: none !important;">
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            <h2
                                                                style="font-size: 17px; color: #303030; line-height: 34px;">
                                                                {{ $invoice->contact->name }}
                                                            </h2>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($invoice->contact->address)
                                                            <span>{{ addressFormat($invoice->contact->address, 1)[0] }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($invoice->contact->address &&
                                                            addressFormat($invoice->contact->address, 1)[1] ?? null)
                                                            <span>{{ addressFormat($invoice->contact->address, 1)[1] }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            <span>@if($invoice->contact->phone)
                                                                {{ __('labels.phone') }}:
                                                                {{ $invoice->contact->phone }}
                                                                @endif @if($invoice->contact->mobile)
                                                                {{ __('labels.mobile') }}:
                                                                {{ $invoice->contact->mobile }}
                                                                @endif</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($invoice->contact->email)
                                                            <span>{{ __('labels.email') }}: <a
                                                                    href="mailto:{{$invoice->contact->email}}">{{$invoice->contact->email}}</a></span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left; padding:0;">
                                                            @if($invoice->contact->tax_id)
                                                            <span>{{ __('labels.tax_id') }}:
                                                                {{ $invoice->contact->tax_id }}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td style="padding:0; vertical-align: top;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody style="border: none !important;">
                                                    <tr>
                                                        <td
                                                            style="text-align: center; padding: 0; font-size: 17px !important; color: #ffffff !important; font-weight: 700 !important; background: #0e68b1 !important; padding: 6px; border: 2px solid #0e68b1 !important;">
                                                            {{ __('labels.invoice') }} {{ __('labels.no') }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td
                                                            style="text-align: center; padding: 0; border: 2px solid #0e68b1 !important; font-size: 26px; font-weight: 700;line-height: 73px; color: #303030 !important;">
                                                            {{ $invoice->internal_number }}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:20px 0;">
                            <table id="pay-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <thead>
                                    <tr>
                                        <th>{{ __('labels.seller') }}</th>
                                        @if(($viewType ?? null) != 'production-order')
                                        <th>{{ __('labels.tax_id') }}</th>
                                        <th>{{ __('labels.invoice_with') }}</th>
                                        <th>{{ __('labels.ncf') }}</th>
                                        @endif
                                        <th>{{ __('labels.date') }}</th>
                                        @if($invoice->currency && ($viewType ?? null) != 'production-order')
                                        <th>{{ __('labels.currency') }}</th>
                                        @endif
                                        @if(($viewType ?? null) != 'production-order')
                                        <th>{{ __('labels.way_to_pay') }}</th>
                                        @endif
                                        <th>{{ __('labels.exp_date') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $invoice->user->name }}</td>
                                        @if(($viewType ?? null) != 'production-order')
                                        <td>{{ showIfAvailable($user->company->tax_id)}} </td>
                                        <td>{{ $invoice->ncf ? showIfAvailable($invoice->ncf->name) : 'N/A' }}</td>
                                        <td>{{ $invoice->ncf ? showIfAvailable($invoice->ncf_number) : 'N/A' }}</td>
                                        @endif
                                        <td>{{ dateFormat($invoice->start_date) }}</td>
                                        @if($invoice->currency && ($viewType ?? null) != 'production-order')
                                        <td>{{ $invoice->currency->code }}</td>
                                        @endif
                                        @if(($viewType ?? null) != 'production-order')
                                        <td>{{ __('labels.cash') }}</td>
                                        @endif
                                        <td>{{ dateFormat($invoice->expiration_date) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:0;">
                            <table id="main-invoice-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <thead style="display: table-row-group !important;">
                                    <tr style="background: #000 !important;">
                                        <th>{{ __('labels.qty') }}</th>
                                        <th>{{ __('labels.ref') }}</th>
                                        <th>{{ __('labels.item') }} {{ __('labels.name') }}</th>
                                        @if(($viewType ?? null) != 'production-order')
                                        <th>{{ __('labels.tax_amount') }}</th>
                                        <th>{{ __('labels.price') }}</th>
                                        <th>{{ __('labels.disc') }}%</th>
                                        <th>{{ __('labels.total') }}</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody style="border: none !important;">

                                    @foreach($invoice->items as $key => $item)
                                    <tr>
                                        <td>{{$item->quantity}}</td>
                                        <td>{{showIfAvailable($item->reference)}}</td>
                                        <td>{{$item->inventory->name}}</td>
                                        @if(($viewType ?? null) != 'production-order')
                                        <td>{{moneyFormat($item->calculation['taxAmount'],null,2,true)}}</td>
                                        <td>{{moneyFormat($item->price,null,2,true)}}</td>
                                        <td>{{addZeros($item->discount,null,2,true)}}%</td>
                                        <td>{{moneyFormat($item->calculation['total'],null,2,true)}}</td>
                                        @endif
                                    </tr>
                                    @endforeach

                                </tbody>
                                @if(($viewType ?? null) != 'production-order')
                                <tfoot style="display: table-row-group">
                                    <tr>
                                        <td colspan="4"></td>
                                        <td colspan="2" style="text-align:right; font-weight: 700;">
                                            {{ __('labels.total') }} {{ __('labels.discount') }}
                                        </td>
                                        <td style="border: 2px solid #0e68b1;">
                                            {{moneyFormat($invoice->calculation['discountAmount'],null,2,true)}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"></td>
                                        <td colspan="2" style="text-align:right; font-weight: 700;">
                                            {{ __('labels.sub_total') }}
                                        </td>
                                        <td style="border: 2px solid #0e68b1;">
                                            {{moneyFormat($invoice->calculation['subtotal'],null,2,true)}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"></td>
                                        <td colspan="2" style="text-align:right; font-weight: 700;">
                                            {{ __('labels.tax') }}
                                        </td>
                                        <td style="border: 2px solid #0e68b1;">
                                            {{moneyFormat($invoice->calculation['taxAmount'],null,2,true)}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                        </td>
                                        <td colspan="2" style="text-align:right; font-weight: 700;">
                                            {{ __('labels.total') }}
                                        </td>
                                        <td style="border: 2px solid #0e68b1;">
                                            {{moneyFormat($invoice->calculation['total'],null,2,true)}}
                                        </td>
                                    </tr>
                                    @if($invoice->currency)
                                    <tr>
                                        <td colspan="4">
                                        </td>
                                        <td colspan="2" style="text-align:right; font-weight: 700;">
                                            {{ __('labels.total') }} {{ $invoice->currency->code }}
                                        </td>
                                        <td style="border: 2px solid #0e68b1;">
                                            {{ $invoice->currency->symbol }}{{ addZeros($invoice->exchange_rate_total,6,true) }}
                                        </td>
                                    </tr>
                                    @endif
                                </tfoot>
                                @endif
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:30px 0;">
                            <table width="300px" border="0" cellspacing="0" cellpadding="0" id="divider">
                                <tbody>
                                    <tr>
                                        <td
                                            style="padding:0; font-size: 14px;  border-bottom: 2px solid #0e68b1 !important; text-align:center; padding:0; font-weight: 400;">
                                            @if($user->company->signature)
                                            <img style="width: 193px; display:block; margin: 0 auto 10px;"
                                                id="signature-preview" src="{{ asset($user->company->signature) }}"
                                                alt="{{ $user->company->name }}" />@endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center; padding:0; text-transform: capitalize;">
                                            {{ __('labels.elaborated_by') }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <table width="auto" style="margin:0 auto;" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        @if($invoice->type == 'recurring')
                        <td style="text-align:left; padding: 40px 0px 5px 0px; font-size: 13px;">
                            <strong style="font-weight: 700;">{{ __('labels.observations') }}:
                            </strong>{{ $invoice->observation }}
                        </td>
                        @else
                        <td style="text-align:left; padding: 40px 0px 5px 0px; font-size: 13px;">
                            <strong style="font-weight: 700;">{{ __('labels.terms_and_conditions') }}:
                            </strong>{{ $invoice->tac }}
                        </td>
                        @endif
                    </tr>
                    <tr>
                        <td style="text-align:left; padding: 5px 0px 5px 0px; font-size: 13px;">
                            <strong style="font-weight: 700;">{{ __('labels.notes') }}:
                            </strong>{{ $invoice->notes }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left;  padding: 5px 0px 5px 0px; font-size: 13px;">
                            <strong style="font-weight: 700;">{{ __('labels.resolution_text') }}:
                            </strong>{{ $invoice->res_text }}
                        </td>
                    </tr>
                    </tbody>
                    </table>
                    <!--[if mso 16]>
                                            </td>
                                            </tr>
                                        </table>
                                        <![endif]-->
                    <!--[if (mso)|(IE)]>
                                    </td>
                                </tr>
                            </table>
                            <![endif]-->
            </td>
        </tr>
    </table>
    </div>
</body>

</html>