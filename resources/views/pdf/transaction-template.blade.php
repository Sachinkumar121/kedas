<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,300;0,400;0,600;0,700;0,800;0,900;1,500&display=swap">
        <title>{{ trans('labels.show').' '.trans('labels.payment') }}</title>
        <!-- CSS Reset : BEGIN -->
        <style type="text/css">
            /* What it does: Remove spaces around the email design added by some email clients. */
            /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
            @media print {
            @page {
            size: A4
            }
            * {
            margin: 0;
            }
            .invoice-template-body-wrap {
            font-family: 'Raleway', sans-serif !important;
            height: 297mm;
            width: 210mm;
            margin: 0 auto !important;
            padding: 0 !important;
            }
            page[size="A4"] {
            width: 21cm;
            height: 29.7cm;
            }
            .invoice-template-body-wrap tbody::after {
            content: '';
            display: block;
            page-break-after: always;
            page-break-inside: avoid;
            page-break-before: avoid;
            }
            .invoice-template-body-wrap body {
            position: relative;
            margin: 0 auto;
            color: #555555 !important;
            background: #FFFFFF !important;
            font-size: 16px !important;
            }
            /* What it does: Stops email clients resizing small text. */
            * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            }
            /* What it does: Centers email on Android 4.4 */
            .invoice-template-body-wrap div[style*="margin: 16px 0"] {
            margin: 0 !important;
            }
            /* What it does: Stops Outlook from adding extra spacing to tables. */
            .invoice-template-body-wrap table,
            .invoice-template-body-wrap td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
            font-family: 'Raleway', sans-serif !important;
            }
            /* What it does: Fixes webkit padding issue. */
            .invoice-template-body-wrap table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            font-family: 'Raleway', sans-serif !important;
            }
            /* What it does: Uses a better rendering method when resizing images in IE. */
            .invoice-template-body-wrap img {
            -ms-interpolation-mode: bicubic;
            }
            /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
            .invoice-template-body-wrap a {
            text-decoration: none;
            }
            /* What it does: A work-around for email clients meddling in triggered links. */
            .invoice-template-body-wrap a[x-apple-data-detectors],
            /* iOS */
            .invoice-template-body-wrap .unstyle-auto-detected-links a,
            .invoice-template-body-wrap .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
            }
            /* What it does: Prevents Gmail from changing the text color in conversation threads. */
            .invoice-template-body-wrap .im {
            color: inherit !important;
            }
            /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
            .invoice-template-body-wrap .a6S {
            display: none !important;
            opacity: 0.01 !important;
            }
            /* If the above doesn't work, add a .g-img class to any image in question. */
            .invoice-template-body-wrap img.g-img+div {
            display: none !important;
            }
            .invoice-template-body-wrap html,
            .invoice-template-body-wrap table,
            .invoice-template-body-wrap tr,
            .invoice-template-body-wrap td {
            margin: 0;
            padding: 0;
            font-family: 'Raleway', sans-serif !important;
            }
            .invoice-template-body-wrap h2 {
            margin: 0;
            padding: 0
            }
            .invoice-template-body-wrap .responsive-table {
            width: calc(33.3% - 4px);
            display: inline-block;
            vertical-align: middle;
            text-align: left;
            }
            .invoice-template-body-wrap img {
            max-width: 100%
            }
            .invoice-template-body-wrap .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
            }
            .invoice-template-body-wrap {
            border: 0;
            }
            .invoice-template-body-wrap .clearfix:after {
            content: "";
            display: table;
            clear: both;
            }
            .invoice-template-body-wrap a {
            color: #303030 !important;
            text-decoration: none;
            }
            /* .invoice-template-body-wrap table th,
            .invoice-template-body-wrap table td {
            background: transparent !important;
            text-align: center;
            border-bottom: 1px solid #FFFFFF !important;
            color: #303030 !important;
            font-size: 15px;
            font-weight: 400 !important;
            } */
            .invoice-template-body-wrap table strong {
            font-weight: 700 !important;
            }
            .invoice-template-body-wrap table th {
            white-space: nowrap;
            font-weight: normal;
            font-weight: 700 !important;
            }
            .invoice-template-body-wrap table td {
            text-align: right;
            padding: 20px;
            }
            .invoice-template-body-wrap table tbody tr:last-child td {
            border: none;
            }
            .invoice-template-body-wrap table tfoot td {
            padding: 10px 20px;
            background: #FFFFFF !important;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
            }
            .invoice-template-body-wrap table tfoot tr:first-child td {
            border-top: none;
            }
            .invoice-template-body-wrap table tfoot tr td:first-child {
            border: none;
            }
            .invoice-template-body-wrap #pay-table thead th,
            .invoice-template-body-wrap #main-invoice-table thead th {
            font-size: 15px !important;
            color: #ffffff !important;
            font-weight: 700 !important;
            background: #0e68b1 !important;
            padding: 6px;
            border: 3px solid #0e68b1 !important;
            }
            .invoice-template-body-wrap #main-invoice-table tbody td,
            .invoice-template-body-wrap #pay-table tbody td {
            border: 2px solid #0e68b1;
            text-align: center;
            padding: 8px;
            font-weight: 400 !important;
            }
            .invoice-template-body-wrap #main-invoice-table tfoot>tr>td {
            text-align: center;
            font-weight: 700 !important;
            font-size: 15px !important;
            padding: 8px;
            }
            .invoice-template-body-wrap #divider td {
            border: none !important;
            }
            .invoice-template-body-wrap #comapny-name td,
            .invoice-template-body-wrap #client-name td {
            font-size: 14px !important;
            }
            .invoice-template-body-wrap tfoot {
            position: relative;
            bottom: 0;
            display: table-footer-group;
            }
            .invoice-template-body-wrap table {
            -fs-table-paginate: paginate !important;
            }
            .invoice-template-body-wrap table thead {
            display: table-header-group;
            break-inside: avoid !important;
            page-break-inside: avoid !important;
            }
            .invoice-template-body-wrap #inner-main-table>td,
            .invoice-template-body-wrap #inner-main-table>tbody {
            border: 0px solid #0e68b1 !important;
            }
            .invoice-template-body-wrap #inner-main-table table,
            .invoice-template-body-wrap #inner-main-table table tbody,
            .invoice-template-body-wrap #main-invoice-table table tbody {
            border: none !important;
            }
            .invoice-template-body-wrap #inner-main-table tfoot td,
            .invoice-template-body-wrap #inner-main-table tfoot tr,
            .invoice-template-body-wrap #inner-main-table tfoot {
            border: none;
            }
            .invoice-template-body-wrap table,
            .invoice-template-body-wrap tr,
            .invoice-template-body-wrap td,
            .invoice-template-body-wrap div {
            page-break-inside: avoid !important;
            }
            .invoice-template-body-wrap #main-invoice-table {
            position: relative;
            }
            .invoice-template-body-wrap #main-invoice-table thead {
            display: table-row-group;
            }
            .invoice-template-body-wrap tfoot {
            display: table-row-group;
            }
            .invoice-template-body-wrap tr {
            page-break-inside: avoid;
            }

            }
            @page {
            size: A4
            }
            * {
            margin: 0;
            }
            .invoice-template-body-wrap {
            font-family: 'Raleway', sans-serif !important;
            height: 297mm;
            width: 210mm;
            margin: 0 auto !important;
            padding: 0 !important;
            }
            page[size="A4"] {
            width: 21cm;
            height: 29.7cm;
            }
            .invoice-template-body-wrap tbody::after {
            content: '';
            display: block;
            page-break-after: always;
            page-break-inside: avoid;
            page-break-before: avoid;
            }
            .invoice-template-body-wrap body {
            position: relative;
            margin: 0 auto;
            color: #555555 !important;
            background: #FFFFFF !important;
            font-size: 16px !important;
            }
            /* What it does: Stops email clients resizing small text. */
            * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            }
            /* What it does: Centers email on Android 4.4 */
            .invoice-template-body-wrap div[style*="margin: 16px 0"] {
            margin: 0 !important;
            }
            /* What it does: Stops Outlook from adding extra spacing to tables. */
            .invoice-template-body-wrap table,
            .invoice-template-body-wrap td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
            font-family: 'Raleway', sans-serif !important;
            }
            /* What it does: Fixes webkit padding issue. */
            .invoice-template-body-wrap table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            font-family: 'Raleway', sans-serif !important;
            }
            /* What it does: Uses a better rendering method when resizing images in IE. */
            .invoice-template-body-wrap img {
            -ms-interpolation-mode: bicubic;
            }
            /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
            .invoice-template-body-wrap a {
            text-decoration: none;
            }
            /* What it does: A work-around for email clients meddling in triggered links. */
            .invoice-template-body-wrap a[x-apple-data-detectors],
            /* iOS */
            .invoice-template-body-wrap .unstyle-auto-detected-links a,
            .invoice-template-body-wrap .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
            }
            /* What it does: Prevents Gmail from changing the text color in conversation threads. */
            .invoice-template-body-wrap .im {
            color: inherit !important;
            }
            /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
            .invoice-template-body-wrap .a6S {
            display: none !important;
            opacity: 0.01 !important;
            }
            /* If the above doesn't work, add a .g-img class to any image in question. */
            .invoice-template-body-wrap img.g-img+div {
            display: none !important;
            }
            .invoice-template-body-wrap html,
            .invoice-template-body-wrap table,
            .invoice-template-body-wrap tr,
            .invoice-template-body-wrap td {
            margin: 0;
            padding: 0;
            font-family: 'Raleway', sans-serif !important;
            }
            .invoice-template-body-wrap h2 {
            margin: 0;
            padding: 0
            }
            .invoice-template-body-wrap .responsive-table {
            width: calc(33.3% - 4px);
            display: inline-block;
            vertical-align: middle;
            text-align: left;
            }
            .invoice-template-body-wrap img {
            max-width: 100%
            }
            .invoice-template-body-wrap .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
            }
            .invoice-template-body-wrap {
            border: 0;
            }
            .invoice-template-body-wrap .clearfix:after {
            content: "";
            display: table;
            clear: both;
            }
            .invoice-template-body-wrap a {
            color: #303030;
            text-decoration: none;
            }
            .invoice-template-body-wrap table th,
            .invoice-template-body-wrap table td {
            /* background: transparent !important; */
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
            color: #303030 !important;
            font-size: 15px;
            font-weight: 400 !important;
            }
            .invoice-template-body-wrap table strong {
            font-weight: 700 !important;
            }
            .invoice-template-body-wrap table th {
            white-space: nowrap;
            font-weight: normal;
            font-weight: 700 !important;
            }
            .invoice-template-body-wrap table td {
            text-align: right;
            padding: 20px;
            }
            .invoice-template-body-wrap table tbody tr:last-child td {
            border: none;
            }
            .invoice-template-body-wrap table tfoot td {
            padding: 10px 20px;
            background: #FFFFFF !important;
            border-bottom: none;
            font-size: 1.2em !important;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
            }
            .invoice-template-body-wrap table tfoot tr:first-child td {
            border-top: none;
            }
            .invoice-template-body-wrap table tfoot tr td:first-child {
            border: none;
            }
            .invoice-template-body-wrap #pay-table thead th,
            .invoice-template-body-wrap #main-invoice-table thead th {
            font-size: 15px !important;
            color: #ffffff !important;
            font-weight: 700 !important;
            background: #0e68b1 !important;
            padding: 6px;
            border: 3px solid #0e68b1 !important;
            }
            .invoice-template-body-wrap #main-invoice-table tbody td,
            .invoice-template-body-wrap #pay-table tbody td {
            border: 2px solid #0e68b1;
            text-align: center;
            padding: 8px;
            font-weight: 400 !important;
            }
            .invoice-template-body-wrap #main-invoice-table tfoot>tr>td {
            text-align: center;
            font-weight: 700 !important;
            font-size: 15px !important;
            padding: 8px;
            }
            .invoice-template-body-wrap #divider td {
            border: none !important;
            }
            .invoice-template-body-wrap #comapny-name td,
            .invoice-template-body-wrap #client-name td {
            font-size: 14px !important;
            line-height: 24px !important;
            }
            .invoice-template-body-wrap tfoot {
            position: relative;
            bottom: 0;
            display: table-footer-group;
            }
            .invoice-template-body-wrap table {
            -fs-table-paginate: paginate !important;
            }
            .invoice-template-body-wrap table thead {
            display: table-header-group;
            break-inside: avoid !important;
            page-break-inside: avoid !important;
            }
            .invoice-template-body-wrap #inner-main-table>td,
            .invoice-template-body-wrap #inner-main-table>tbody {
            border: 0px solid #0e68b1 !important;
            }
            .invoice-template-body-wrap #inner-main-table table,
            .invoice-template-body-wrap #inner-main-table table tbody,
            .invoice-template-body-wrap #main-invoice-table table tbody {
            border: none !important;
            }
            .invoice-template-body-wrap #inner-main-table tfoot td,
            .invoice-template-body-wrap #inner-main-table tfoot tr,
            .invoice-template-body-wrap #inner-main-table tfoot {
            border: none;
            }
            .invoice-template-body-wrap table,
            .invoice-template-body-wrap tr,
            .invoice-template-body-wrap td,
            .invoice-template-body-wrap div {
            page-break-inside: avoid !important;
            }
            .invoice-template-body-wrap #main-invoice-table {
            position: relative;
            }
            .invoice-template-body-wrap #main-invoice-table thead {
            display: table-row-group;
            }
            .invoice-template-body-wrap tfoot {
            display: table-row-group;
            }
            .invoice-template-body-wrap tr {
            page-break-inside: avoid;
            }
             .invoice-template-body-wrap table td#comapny-name {
                padding-right: 40px !important;
            }
            .invoice-template-body-wrap table.contact-det {
                border-top:1px solid #000;
                 border-bottom:1px solid #000;
            }
             .invoice-template-body-wrap table.contact-det td {
                line-height: 26px !important;
                
             }
                 .invoice-template-body-wrap table.contact-det tr td:first-child  {
                    font-weight: bold !important;
                 }
            @media (max-width:1200px) {
            @page {
            size: A4
            }
            {
            margin: 0;
            }
            .invoice-template-body-wrap {
            font-family: 'Raleway', sans-serif !important;
            height: 297mm;
            width: 210mm;
            margin: 0 auto !important;
            padding: 0 !important;
            }
            page[size="A4"] {
            width: 21cm;
            height: 29.7cm;
            }
            .invoice-template-body-wrap tbody::after {
            content: '';
            display: block;
            page-break-after: always;
            page-break-inside: avoid;
            page-break-before: avoid;
            }
            .invoice-template-body-wrap body {
            position: relative;
            margin: 0 auto;
            color: #555555 !important;
            background: #FFFFFF !important;
            font-size: 16px !important;
            }
            /* What it does: Stops email clients resizing small text. */
            * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            }
            /* What it does: Centers email on Android 4.4 */
            .invoice-template-body-wrap div[style*="margin: 16px 0"] {
            margin: 0 !important;
            }
            /* What it does: Stops Outlook from adding extra spacing to tables. */
            .invoice-template-body-wrap table,
            .invoice-template-body-wrap td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
            font-family: 'Raleway', sans-serif !important;
            }
            /* What it does: Fixes webkit padding issue. */
            .invoice-template-body-wrap table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            font-family: 'Raleway', sans-serif !important;
            }
            /* What it does: Uses a better rendering method when resizing images in IE. */
            .invoice-template-body-wrap img {
            -ms-interpolation-mode: bicubic;
            }
            /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
            .invoice-template-body-wrap a {
            text-decoration: none;
            }
            /* What it does: A work-around for email clients meddling in triggered links. */
            .invoice-template-body-wrap a[x-apple-data-detectors],
            /* iOS */
            .invoice-template-body-wrap .unstyle-auto-detected-links a,
            .invoice-template-body-wrap .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
            }
            /* What it does: Prevents Gmail from changing the text color in conversation threads. */
            .invoice-template-body-wrap .im {
            color: inherit !important;
            }
            /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
            .invoice-template-body-wrap .a6S {
            display: none !important;
            opacity: 0.01 !important;
            }
            /* If the above doesn't work, add a .g-img class to any image in question. */
            .invoice-template-body-wrap img.g-img+div {
            display: none !important;
            }
            .invoice-template-body-wrap html,
            .invoice-template-body-wrap table,
            .invoice-template-body-wrap tr,
            .invoice-template-body-wrap td {
            margin: 0;
            padding: 0;
            font-family: 'Raleway', sans-serif !important;
            }
            .invoice-template-body-wrap h2 {
            margin: 0;
            padding: 0
            }
            .invoice-template-body-wrap .responsive-table {
            width: calc(33.3% - 4px);
            display: inline-block;
            vertical-align: middle;
            text-align: left;
            }
            .invoice-template-body-wrap img {
            max-width: 100%
            }
            .invoice-template-body-wrap .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
            }
            .invoice-template-body-wrap {
            border: 0;
            }
            .invoice-template-body-wrap .clearfix:after {
            content: "";
            display: table;
            clear: both;
            }
            .invoice-template-body-wrap a {
            color: #303030 !important;
            text-decoration: none;
            }
            .invoice-template-body-wrap table th,
            .invoice-template-body-wrap table td {
            background: transparent !important;
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
            color: #303030 !important;
            font-size: 15px;
            font-weight: 400 !important;
            }
            .invoice-template-body-wrap table strong {
            font-weight: 700 !important;
            }
            .invoice-template-body-wrap table th {
            white-space: nowrap;
            font-weight: normal;
            font-weight: 700 !important;
            }
            .invoice-template-body-wrap table td {
            text-align: right;
            padding: 20px;
            }
            .invoice-template-body-wrap table tbody tr:last-child td {
            border: none;
            }
            .invoice-template-body-wrap table tfoot td {
            padding: 10px 20px;
            background: #FFFFF !important;
            border-bottom: none;
            font-size: 1.2em !important;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
            }
            .invoice-template-body-wrap table tfoot tr:first-child td {
            border-top: none;
            }
            .invoice-template-body-wrap table tfoot tr td:first-child {
            border: none;
            }
            .invoice-template-body-wrap #pay-table thead th,
            .invoice-template-body-wrap #main-invoice-table thead th {
            font-size: 15px !important;
            color: #ffffff;
            font-weight: 700 !important;
            background: #0e68b1 !important;
            padding: 6px;
            border: 3px solid #0e68b1 !important;
            }
            .invoice-template-body-wrap #main-invoice-table tbody td,
            .invoice-template-body-wrap #pay-table tbody td {
            border: 2px solid #0e68b1 !important;
            text-align: center;
            padding: 8px;
            font-weight: 400 !important;
            }
            .invoice-template-body-wrap #main-invoice-table tfoot>tr>td {
            text-align: center;
            font-weight: 700 !important;
            font-size: 15px !important;
            padding: 8px;
            }
            .invoice-template-body-wrap #divider td {
            border: none !important;
            }
            .invoice-template-body-wrap #comapny-name td,
            .invoice-template-body-wrap #client-name td {
            font-size: 14px !important;
            }
            .invoice-template-body-wrap tfoot {
            position: relative;
            bottom: 0;
            display: table-footer-group;
            }
            .invoice-template-body-wrap table {
            -fs-table-paginate: paginate !important;
            }
            .invoice-template-body-wrap table thead {
            display: table-header-group;
            break-inside: avoid !important;
            page-break-inside: avoid !important;
            }
            .invoice-template-body-wrap #inner-main-table>td,
            .invoice-template-body-wrap #inner-main-table>tbody {
            border: 0px solid #0e68b1 !important;
            }
            .invoice-template-body-wrap #inner-main-table table,
            .invoice-template-body-wrap #inner-main-table table tbody,
            .invoice-template-body-wrap #main-invoice-table table tbody {
            border: none !important;
            }
            .invoice-template-body-wrap #inner-main-table tfoot td,
            .invoice-template-body-wrap #inner-main-table tfoot tr,
            .invoice-template-body-wrap #inner-main-table tfoot {
            border: none;
            }
            .invoice-template-body-wrap table,
            .invoice-template-body-wrap tr,
            .invoice-template-body-wrap td,
            .invoice-template-body-wrap div {
            page-break-inside: avoid !important;
            }
            .invoice-template-body-wrap #main-invoice-table {
            position: relative;
            }
            .invoice-template-body-wrap #main-invoice-table thead {
            display: table-row-group;
            }
            .invoice-template-body-wrap tfoot {
            display: table-row-group;
            }
            .invoice-template-body-wrap tr {
            page-break-inside: avoid;
            }
            }
            @media (max-width:600px) {
            .invoice-template-body-wrap .responsive-table {
            width: 100%;
            min-width: 100% !important;
            }
            .invoice-template-body-wrap .conatiner-space {
            padding-left: 30px;
            padding-right: 30px;
            }
            
            }
        </style>
    </head>
    <body>
        <div class="invoice-template-body-wrap">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" role="presentation">
            <tr>
                <td align="center" valign="top">
                    <!--[if (mso)|(IE)]>
                    <table width="600" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="left" valign="top" width="100%">
                    <![endif]-->
                    <!--[if mso 16]>
                    <table width="600" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="left" valign="top" width="100%">
                    <![endif]-->
                    <table width="100%" style="max-width:1100px; margin:0 auto;" border="0" cellspacing="0"
                        cellpadding="0">
                        <tr style="border-bottom: 3px solid #0e68b1;">
                            <td style="padding:20px 0;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="text-align:left; padding:0;">
                                            @if($user->company->logo)
                                            <img id="image-preview" src="{{ asset($user->company->logo) }}"
                                                alt="{{ $user->name }}" style="width: 200px;" />
                                            @endif
                                        </td>
                                        <td
                                            style="text-align:right; padding:0; font-size: 34px; color: #0e68b1;  line-height: 41px;">
                                            @if($transaction->receipt_number)
                                            <h2>{{ __('labels.receipt').' #'.$transaction->receipt_number }}</h2>
                                            @elseif($transaction->voucher_number)
                                            <h2>{{ 'Voucher'.' #'.$transaction->voucher_number }}</h2>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="border:none;">
                            <td style="padding:0; border:none !important;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:none;">
                                    <tbody style="border:none !important;">
                                        <tr style="border:none;">
                                            <td style="text-align:left; max-width: 37%; padding:0; vertical-align: text-top;"
                                                id="comapny-name">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody style="border: none !important;">
                                                        <tr>
                                                            <td style="text-align:left; padding:0;">
                                                                <h2
                                                                    style="font-size: 17px; color: #0e68b1;   line-height: 34px;">
                                                                    {{ $user->company->name }}
                                                                </h2>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left; padding:0;">
                                                                @if(addressFormat($user->company->address, 1)[0]
                                                                ??
                                                                null)
                                                                <span>{{ addressFormat($user->company->address, 1)[0] }}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left; padding:0;">
                                                                @if(addressFormat($user->company->address, 1)[1]
                                                                ??
                                                                null)
                                                                <span>{{ addressFormat($user->company->address, 1)[1] }}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left; padding:0;">
                                                                <span>{{ __('labels.phone') }}:
                                                                    {{ $user->company->phone }}
                                                                    @if($user->company->fax)
                                                                    {{ __('labels.fax') }}:
                                                                    {{ $user->company->fax }} @endif</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left; padding:0;">
                                                                @if($user->company->support_email)
                                                                <span>{{ __('labels.support_email') }}: <a
                                                                        href="mailto:{{$user->company->support_email}}">{{$user->company->support_email}}</a></span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left; padding:0;">
                                                                @if($user->company->tax_id)
                                                                <span>{{ __('labels.tax_id') }}:
                                                                    {{ $user->company->tax_id }}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td style="text-align:left; max-width: 29%; padding:0; vertical-align: text-top;"
                                                id="client-name">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody style="border: none !important;">
                                                        <tr>
                                                            <td style="text-align:left; padding:0;">
                                                                <h2
                                                                    style="font-size: 17px; color: #303030; line-height: 34px;">
                                                                    {{ $transaction->contact->name }}
                                                                </h2>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left; padding:0;">
                                                                @if($transaction->contact->address)
                                                                <span>{{ addressFormat($transaction->contact->address, 1)[0] }}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left; padding:0;">
                                                                @if($transaction->contact->address &&
                                                                addressFormat($transaction->contact->address, 1)[1] ?? null)
                                                                <span>{{ addressFormat($transaction->contact->address, 1)[1] }}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left; padding:0;">
                                                                <span>@if($transaction->contact->phone)
                                                                    {{ __('labels.phone') }}:
                                                                    {{ $transaction->contact->phone }}
                                                                    @endif @if($transaction->contact->mobile)
                                                                    {{ __('labels.mobile') }}:
                                                                    {{ $transaction->contact->mobile }}
                                                                    @endif</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left; padding:0;">
                                                                @if($transaction->contact->email)
                                                                <span>{{ __('labels.email') }}: <a
                                                                        href="mailto:{{$transaction->contact->email}}">{{$transaction->contact->email}}</a></span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left; padding:0;">
                                                                @if($transaction->contact->tax_id)
                                                                <span>{{ __('labels.tax_id') }}:
                                                                    {{ $transaction->contact->tax_id }}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td style="padding:0; vertical-align: top;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody style="border: none !important;">
                                                        <tr>
                                                            <td
                                                                style="text-align: center; padding: 0; font-size: 17px !important; color: #ffffff !important; font-weight: 700 !important; background: #0e68b1 !important; padding: 6px; border: 2px solid #0e68b1 !important;">
                                                                @if($transaction->receipt_number)
                                                                    {{ __('labels.receipt') }} {{ __('labels.no') }}
                                                                @elseif($transaction->voucher_number)
                                                                    {{ __('labels.voucher') }} {{ __('labels.no') }}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td
                                                                style="text-align: center; padding: 0; border: 2px solid #0e68b1 !important; font-size: 26px; font-weight: 700;line-height: 73px; color: #303030 !important;">
                                                                @if($transaction->receipt_number)
                                                                    <h2>{{ $transaction->receipt_number }}</h2>
                                                                @elseif($transaction->voucher_number)
                                                                    <h2>{{ $transaction->voucher_number }}</h2>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:20px 0;"  width="80%">
                                <table class="contact-det" width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td style="padding:10px 0;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding:0;  text-align:left;">{{ __('labels.contact') }}:</td>
                                                            <td style="padding:0;  text-align:left;"> {{ $transaction->contact ? $transaction->contact->name : 'N/A' }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0; text-align:left;">{{ __('labels.details') }}:</td>
                                                            <td style="padding:0; text-align:left;">{{ showIfAvailable($transaction->detail_line) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0; text-align:left;">{{ __('labels.account') }}:</td>
                                                            <td style="padding:0; text-align:left;">{{  $transaction->bankAccount ? $transaction->bankAccount->name : 'N/A' }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td style="padding:20px 0; text-align:right;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding:0;  text-align:left;">{{ __('labels.amount') }}:</td>
                                                            <td style="padding:0; text-align:left;">{{ moneyFormat($transaction->amount,null,2,true) }}</td>
                                                        </tr>
                                                        @if ($transaction->type == 'recurring')
                                                        <tr>
                                                            <td style="padding:0; text-align:left;">{{ __('labels.start_date') }}:</td>
                                                            <td style="padding:0; text-align:left;">{{ dateFormat($transaction->start_date) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0; text-align:left;">{{ __('labels.end_date') }}:</td>
                                                            <td style="padding:0; text-align:left;">{{ dateFormat($transaction->end_date) }}</td>
                                                        </tr>
                                                        @endif
                                                        @if ($transaction->type != 'recurring')
                                                        <tr>
                                                            <td style="padding:0; text-align:left;">{{ __('labels.annotation') }}:</td>
                                                            <td style="padding:0; text-align:left;"> {{ showIfAvailable($transaction->annotation) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0; text-align:left;">{{ __('labels.creation_date') }}</td>
                                                            <td style="padding:0; text-align:left;"> {{ dateFormat($transaction->start_date) }}</td>
                                                        </tr>
                                                        @else
                                                        <tr>
                                                            <td style="padding:0; text-align:left;">{{ __('labels.observations') }}:</td>
                                                            <td style="padding:0; text-align:left;">{{ showIfAvailable($transaction->observation) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0; text-align:left;">{{ __('labels.frequency').' '.__('labels.month') }}:</td>
                                                            <td style="padding:0; text-align:left;">{{ $transaction->frequency }}</td>
                                                        </tr>
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:20px 0;">
                                @if ($transaction->conceptItems->isNotEmpty())
                                <table id="pay-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <thead>
                                        <tr>
                                            <th>{{ __('labels.concept') }}</th>
                                            <th>{{ __('labels.price') }}</th>
                                            <th>{{ __('labels.disc') }} %</th>
                                            <th>{{ __('labels.tax') }}</th>
                                            <th>{{ __('labels.tax_amount') }}</th>
                                            <th>{{ __('labels.quantity') }}</th>
                                            <th>{{ __('labels.total') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($transaction->conceptItems as $conceptItem)
                                        <tr>
                                            <td>{{ $conceptItem->concept->title }}</td>
                                            <td>{{ moneyFormat($conceptItem->price,null,2,true) }}</td>
                                            <td>{{addZeros($conceptItem->discount,2,true)}}%</td>
                                            <td>{{$conceptItem->tax ? addZeros($conceptItem->tax->percentage).'%' : 'N/A'}}
                                            </td>
                                            <td>{{moneyFormat($conceptItem->calculation['taxAmount'],null,2,true)}}</td>
                                            <td>{{$conceptItem->quantity}}</td>
                                            <td>{{moneyFormat($conceptItem->calculation['baseTotal'] - $conceptItem->calculation['discountAmount'],null,2,true)}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <table id="pay-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <thead>
                                        <tr>
                                            <th>{{ __('labels.number') }}</th>
                                            <th>{{ __('labels.total') }}</th>
                                            <th>{{ __('labels.paid') }}</th>
                                            <th>{{ __('labels.pending') }}</th>
                                            <th>{{ __('labels.value_received') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($transaction->invoices as $key => $invoice)
                                        <tr>
                                            <input type="hidden" name="payments[{{$key}}][invoice_id]"
                                                value="{{ $invoice->id }}">
                                            <td>{{ $invoice->internal_number }}</td>
                                            <td>
                                                {{ moneyFormat($invoice->calculation['total'],null,2,true) }}
                                                @if($invoice->currency)
                                                <span
                                                    class="exe_currency_label">({{ moneyFormat($invoice->exchange_rate_total, $invoice->currency->symbol, 6) }})</span>
                                                @endif
                                            </td>
                                            <td>
                                                {{ moneyFormat($invoice->calculation['paidAmount'],null,2,true) }}
                                                @if($invoice->currency)
                                                <span
                                                    class="exe_currency_label">({{ moneyFormat($invoice->calculation['paidAmountInExe'], $invoice->currency->symbol, 6) }})</span>
                                                @endif
                                            </td>
                                            <td>
                                                {{ moneyFormat($invoice->calculation['pendingAmount'],null,2,true) }}
                                                @if($invoice->currency)
                                                <span
                                                    class="exe_currency_label">({{ moneyFormat($invoice->calculation['pendingAmountInExe'], $invoice->currency->symbol, 6) }})</span>
                                                @endif
                                            </td>
                                            <td>
                                                {{ moneyFormat($invoice->pivot->amount,null,2,true) }}
                                                @if($invoice->currency)
                                                <span class="exe_currency_label">({{  moneyFormat($invoice->pivot->amount / $invoice->exchange_rate_total, $invoice->currency->symbol, 6) }})</span>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot></tfoot>
                                </table>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:40px 0;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="divider">
                                    <tbody>
                                        <tr>
                                            <td style="width: 50%">
                                                <table border="0" cellspacing="0" cellpadding="0" id="divider">
                                                    <tbody>
                                                        <tr>
                                                  
                                                            <td
                                                                style="padding:0; font-size: 14px;  border-bottom: 2px solid #0e68b1 !important; text-align:center; padding:0; font-weight: 400;">
                                                                @if($user->company->signature)
                                                                <img style="width: 193px; display:block; margin: 0 auto 10px;"
                                                                    id="signature-preview" src="{{ asset($user->company->signature) }}"
                                                                    alt="{{ $user->name }}" />@endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:center; padding:0; text-transform: capitalize;">
                                                                {{ __('labels.elaborated_by') }}
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </td>
                                      


                                            <td style="width: 50% text-align:right; vertical-align: bottom;">

                                          <table border="0" cellspacing="0" cellpadding="0" id="divider" style=" float: right;">
                                              <tbody>
                                                  <tr>
                                            
                                                      <td
                                                          style="padding:0; font-size: 14px;  border-bottom: 2px solid #0e68b1 !important; text-align:center; padding:0; font-weight: 400;">
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td style="text-align:center; padding:0; text-transform: capitalize; width: 200px;">
                                                          {{ __('labels.client') }}
                                                      </td>
                                                  </tr>

                                              </tbody>
                                          </table>
                                          </td>

                                        </tr>
                                    </tbody>
                                </table>
                            </td>


                           <!--  <td style="padding:30px 0;">
                                <table border="0" cellspacing="0" cellpadding="0" id="divider">
                                    <tbody>
                                        <tr>
                                            <td
                                                style="padding:0; font-size: 14px;  border-bottom: 2px solid #0e68b1 !important; text-align:center; padding:0; font-weight: 400;">
                                                @if($user->company->signature)
                                                <img style="width: 193px; display:block; margin: 0 auto 10px;"
                                                    id="signature-preview" src="{{ asset($user->company->signature) }}"
                                                    alt="{{ $user->name }}" />@endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; padding:0; text-transform: capitalize;">
                                                {{ __('labels.elaborated_by') }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td> -->
                        </tr>
                        
                        <tr>
                            <td style="text-align:center; padding: 0px 0px 5px 0px; font-size: 13px;">
                                <strong style="font-weight: 700;">{{ __('labels.payment_received_annotation') }}:
                                </strong>{{ $transaction->annotation }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </div>
    </body>
</html>