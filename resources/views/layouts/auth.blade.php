<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}" />

    <title>{{ config('app.name', 'Laravel') }} @if(View::hasSection('title')) | @yield('title') @endif</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:wght@400;500;600;700&family=Roboto&display=swap" rel="stylesheet">

    <!-- Scripts -->
    <script type="text/javascript">
        const SITE_URL = "{{ URL::to('/') }}";
        const CURRENCY_SYMBOL = "{{ Auth::user() ? (Auth::user()->company ? Auth::user()->company->currency->symbol: '$') : '$'}}";
        const locale = "{{ app()->getLocale() }}";
    </script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script> 

    <script type="text/javascript" src="{{ asset('js/lang.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/lodash@4.17.20/lodash.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/validation.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/helpers.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/common-script.js') }}"></script>

    <!-- Scripts -->
    @stack('scripts')
</head>
<body>

    @yield('header')

    <section>
        <div class="main-wrapper">
            @yield('content')
        </div>
    </section>
</body>
</html>
<script type="text/javascript">
    @if (Session::has('success'))
        toastr.success("{!! session('success') !!}");
    @elseif (Session::has('error'))
        toastr.error("{!! session('error') !!}");
    @elseif (Session::has('warning'))
        toastr.warning("{!! session('warning') !!}");
    @elseif (Session::has('info'))
        toastr.info("{!! session('info') !!}");
    @endif
</script>