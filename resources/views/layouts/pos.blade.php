<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}"/>

    <title>{{ config('app.name', 'Laravel') }} @if(View::hasSection('title')) | @yield('title') @endif</title>

    <!-- Scripts -->
    <script type="text/javascript">
        const SITE_URL = "{{URL::to('/')}}";
        const CURRENCY_SYMBOL = "{{Auth::user()->company ? Auth::user()->company->currency->symbol : '$'}}";
        const locale = "{{ app()->getLocale() }}";
    </script>

    @stack('scripts')
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:wght@400;500;600;700&family=Roboto&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <!-- <link rel="stylesheet" href="{{ asset('css/style.css') }}" rel="stylesheet"> -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/datatable-custom.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/hierarchy-select.min.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" class="main-wrapper">

        @if(Auth::user())
            @include('layouts.navbar')
        @endif
        <main id="app">
            @yield('content')
        </main>
    </div>
    <!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>  -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>

    <script>
        let defaultDateFormat = "DD/MM/YYYY";
        if (locale == "en") {
            defaultDateFormat = "MM/DD/YYYY"
        }

        window.appConfig = {
            appUrl: "<?= Config::get('app.url') ?>",
            appLocale: locale,
            app_name: "<?= Config::get('app.name') ?>",
            publicPath: "<?= Config::get('app.url') ?>",
            dateFormat: defaultDateFormat,
            offline: true,
            appVersion: "1.1",
            currencyFormat: "left",
            currencySymbol: "{{Auth::user()->company ? Auth::user()->company->currency->symbol : '$'}}",
            thousandSeparator: ",",
            decimalSeparator: ".",
            numDec: "2",
            timeFormat: "12h",
            timezone : "UTC",
        }
    </script>

    <script type="text/javascript" src="{{ asset('js/lang.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/helpers.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/calculation.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/hierarchy-select.min.js') }}"></script>
</body>
</html>