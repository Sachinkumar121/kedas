@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.recurring_invoice'))
@section('content')
<input type="hidden" name="resource_type" value="{{ $invoice->type }}">
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block"
            href="{{ route('invoices.index', ['type' => $invoice->type]) }}">{{ __('labels.back') }}</a>

        <h2>{{ __('labels.invoice').' '.$invoice->internal_number }}</h2>
        <div class="action-btns-view">
            <a target="_blank" href="{{route('invoices.print', $invoice)}}" class="btn-custom"><i
                    class="fa fa-print"></i> {{ __('labels.print') }}</a>
            <a href="{{route('invoices.pdf', $invoice)}}" class="btn-custom"><i class="fa fa-download"></i>
                {{ __('labels.download') }}</a>
            <!-- <a href="{{ route('invoices.mail', ['email' => 'sachin@yopmail.com', 'invoice' => $invoice]) }}" class="btn-custom"><i class="fa fa-envelope"></i> Send by mail</a> -->
            <!-- <a href="#" class="btn-custom"><i class="fa fa-plus"></i> Add payment</a> -->
            <!-- <div class="more-actions">
                <i class="fa fa-sort-desc action-btn" class="dropdown-toggle" data-toggle="dropdown"></i>
                More Actions
                <div class="dropdown-menu">
                <ul>
                    <li><a href="#">Clone</a></li>
                    <li><a href="#">Make recurring</a></li>
                    <li><a href="#">Edit</a></li>
                    <li><a href="#">See versions</a></li>
                    <li><a href="#">Void</a></li>
                    <li><a href="#">Close without payment</a></li>
                    <li><a href="#">Apply advances</a></li>
                    <li><a href="#">Apply credit note</a></li>
                    <li><a href="#">Print as copy</a></li>
                    <li><a href="#">Download as copy</a></li>
                    <li><a href="#">Send as copy</a></li>
                    <li><a href="#">Attach a file</a></li>
                </ul>
                </div>
            </div> -->
        </div>
        <div class="invoice-detail-count recurring-invoice-details white-bg">
            <div class="invoice-detail-count-item">
                <span class="title">{{ __('labels.total_value') }}</span>
                <span class="value total-value">{{ moneyFormat($invoice->calculation['total'],null,2,true) }}</span>
            </div>
            <!-- <div class="invoice-detail-count-item">
                <span class="title">Retained</span>
                <span class="value">$0.00</span>
                </div>
                <div class="invoice-detail-count-item">
                <span class="title">Refunds</span>
                <span class="value">$0.00</span>
                </div> -->
            <div class="invoice-detail-count-item">
                <span class="title">{{ __('labels.paid') }}</span>
                <span class="value paid">{{ moneyFormat($invoice->calculation['paidAmount'],null,2,true) }}</span>
            </div>
            <div class="invoice-detail-count-item">
                <span class="title">{{ __('labels.pending') }}</span>
                <span class="value pending">{{ moneyFormat($invoice->calculation['pendingAmount'],null,2,true) }}</span>
            </div>
        </div>

        <div class="row adds-row recurring-company-details white-bg">

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group col-sm-12 file-input">
                            @if(Auth::user()->company->logo)
                            <img id="image-preview" src="{{ asset(Auth::user()->company->logo) }}"
                                alt="{{ Auth::user()->company->name }}" />
                            @else
                            <img id="image-preview" src="{{ asset('images/john-sec.png') }}"
                                alt="{{ Auth::user()->name }}" />
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 text-center company-data">
                        <div class="add-cell">

                            <div class="add-cell-center">
                                <h2>{{ Auth::user()->company->name }}</h2>
                                @if(addressFormat(Auth::user()->company->address, 1)[0] ?? null)
                                <span>
                                    <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[0] }}
                                    </p>
                                </span>
                                @endif
                                @if(addressFormat(Auth::user()->company->address, 1)[1] ?? null)
                                <span>
                                    <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[1] }}
                                    </p>
                                </span>
                                @endif
                                <span>
                                    <h3>{{ __('labels.phone') }}:</h3>
                                    <p>{{ Auth::user()->company->phone }}</p>
                                    @if(Auth::user()->company->fax)
                                    <h3>{{ __('labels.tax') }}:</h3>
                                    <p>{{ Auth::user()->company->fax }}</p>
                                    @endif
                                </span>
                                @if(Auth::user()->company->support_email)
                                <span>
                                    <h3>{{ __('labels.email') }}:</h3>
                                    <p><a href="mailto:{{Auth::user()->company->support_email}}">
                                            {{ Auth::user()->company->support_email }} </a></p>
                                </span>
                                @endif
                                @if(Auth::user()->company->tax_id)
                                <span>
                                    <h3>{{ __('labels.tax_id') }}:</h3>
                                    <p><span>{{ Auth::user()->company->tax_id }} </span></p>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                      <div class="consumption-box">
                        <div class="consumption-bottom">
                            <span>{{ __('labels.no') }}.</span>
                            <div class="consumption-number">{{ $invoice->internal_number }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="view-form-custom view-form-recurring white-bg sales-recurring-view">
            <table class="table">
                <tr>
                    <td><b>{{ __('labels.client') }}</b></td>
                    @if($invoice->contact instanceof App\Supplier)
                    <td><a
                            href="{{route('suppliers.show', $invoice->contact)}}">{{ $invoice->contact->name }}</a>
                    </td>
                    @else
                    <td><a href="{{route('clients.show', $invoice->contact)}}">{{ $invoice->contact->name }}</a>
                    </td>
                    @endif
                    <td><b>{{ __('labels.start_date') }}</b></td>
                    <td>{{ dateFormat($invoice->start_date) }}</td>
                </tr>
                <tr>
                    <td><b>{{ __('labels.tax_id') }}</b></td>
                    <td>{{ $invoice->contact->tax_id }}</td>
                    <td><b>{{ __('labels.end_date') }}</b></td>
                    <td>{{ dateFormat($invoice->end_date) }}</td>
                </tr>
                <tr>
                    <td><b>{{ __('labels.phone') }}</b></td>
                    <td>{{ $invoice->contact->phone }}</td>
                    <td><b>{{ __('labels.term') }}</b></td>
                    <td>{{ Config::get('constants.terms')[$invoice->term] }}
                            {{ $invoice->term == 'manual' ? '('.$invoice->manual_days.' '.__('labels.days').')' : '' }}</b>
                    </td>
                </tr>
                @if($invoice->ncf)
                <tr>
                    <td><b>{{ __('labels.invoice_with') }}</b></td>
                    <td>{{ showIfAvailable($invoice->ncf->name) }}</td>
                </tr>
                @endif
                @if($invoice->currency)
                <tr>
                    <td><b>{{ __('labels.currency') }}</b></td>
                    <td>{{ $invoice->currency->code }}</td>
                </tr>
                @endif
                <!-- <tr>
                            <td>{{ __('labels.observation') }}</td>
                            <td><b>{{ $invoice->observation }}</b></td>
                        </tr> -->
            </table>
        </div>

        <div class="default-form view-form">
            <div class="generate-invoice">

                <div class="default-table add-invoice-table">
                    <div class="list-table-custom">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.item') }}</th>
                                    <th>{{ __('labels.reference') }}</th>
                                    <th>{{ __('labels.price') }}</th>
                                    <th>{{ __('labels.disc') }} %</th>
                                    <th>{{ __('labels.tax') }}</th>
                                    <th>{{ __('labels.tax_amount') }}</th>
                                    <th>{{ __('labels.description') }}</th>
                                    <th>{{ __('labels.quantity') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($invoice->items as $item)
                                <tr>
                                    <td><a
                                            href="{{route('inventories.show', $item->inventory)}}">{{$item->inventory->name}}</a>
                                    </td>
                                    <td>{{$item->reference}}</td>
                                    <td>{{moneyFormat($item->price, null, 2, true)}}</td>
                                    <td>{{addZeros($item->discount)}}%</td>
                                    <td>{{$item->tax ? addZeros($item->tax->percentage).'%' : 'N/A'}}</td>
                                    <td>{{moneyFormat($item->calculation['taxAmount'], null, 2, true)}}</td>
                                    <td>{{showIfAvailable($item->inventory->description)}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{moneyFormat($item->calculation['total'],null,2,true)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group col-sm-12 file-input">
                            @if(Auth::user()->company->signature)
                            <img id="signature-preview" src="{{ asset(Auth::user()->company->signature) }}"
                                alt="{{ Auth::user()->company->name }}" />
                            @else
                            {{-- <img id="signature-preview" alt="{{ Auth::user()->company->name }}" /> --}}
                            @endif
                        </div>
                        <div class="elaborated">{{ __('labels.elaborated_by') }}</div>
                    </div>
                    <div class="col-sm-8 total-table">
                        <div class="total-table">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{moneyFormat($invoice->calculation['baseTotal'],null,2,true)}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">
                                            {{moneyFormat($invoice->calculation['discountAmount'],null,2,true)}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{moneyFormat($invoice->calculation['subtotal'],null,2,true)}}
                                        </td>
                                    </tr>
                                    <tr class="tax_row">
                                        <th>{{ __('labels.tax') }}</th>
                                        <td>{{moneyFormat($invoice->calculation['taxAmount'],null,2,true)}}</td>
                                    </tr>
                                    {{-- @foreach($invoice->calculation['taxInfo'] as $taxInfo)
                                    <tr class="tax_row">
                                        <th>{{$taxInfo['formatted_name']}}</th>
                                    <td>{{moneyFormat($taxInfo['tax'], null, 2, true)}}</td>
                                    </tr>
                                    @endforeach --}}
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{moneyFormat($invoice->calculation['total'],null,2,true)}}</td>
                                    </tr>
                                    @if($invoice->currency)
                                    <tr class="total_exchanged_rate">
                                        <th>{{ __('labels.total') }} <span class="total-ex-currency-label">{{ $invoice->currency->code }}</span></th>
                                        <td>
                                            <span class="total-ex-currency-label-symbol">{{ $invoice->currency->code }} {{ $invoice->currency->symbol }}</span><span id="total-ex-currency-rate">{{ addZeros($invoice->exchange_rate_total,6,true) }}</span>
                                        </td>
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                            <label class="total_currency_exchange_inner">1 
                                                <span class="exchange_currency_label">{{ $invoice->currency->code }}</span> 
                                                = {{ Auth::user()->company->currency->code }} ({{ Auth::user()->company->currency->symbol }}) 
                                                <span class="exchange_currency_rate">{{ addZeros($invoice->exchange_currency_rate,6,true) }}</span> 
                                            </label>
                                        </th>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="terms-conditions-form">
                    <div class="row">
                    <div class="form-group col-lg-4 col-md-6">
                            <label><b>{{ __('labels.observations') }}</b></label>
                            <p>{{showIfAvailable($invoice->observation)}}</p>
                        </div>
                        <div class="form-group col-lg-4 col-md-6">
                            <label><b>{{ __('labels.notes') }}</b></label>
                            <p>{{showIfAvailable($invoice->notes)}}</p>
                        </div>
                        <div class="form-group col-lg-4 col-md-6">
                            <label><b> {{ __('labels.resolution_text') }}</b></label>
                            <p>{{showIfAvailable($invoice->res_text)}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @if($invoice->transactions->isNotEmpty())
            <div class="tabs-custom default-form view-table-info">
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab"
                            href="#receivedpay">{{ __('labels.received_payments') }}</a></li>
                    <!-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#menu1">Accounting</a></li> -->
                </ul>
                <div class="tab-content">
                    <div id="receivedpay" class="tab-pane fade in active generate-invoice">
                        <div class="add-form">
                            <div class="default-table">
                                <div class="list-table-custom">
                                    <table class="table table-striped">
                                        <tr>
                                            <th>{{ __('labels.date') }}</th>
                                            <th>{{ __('labels.cash_receipt') }} #</th>
                                            <th>{{ __('labels.payment_method') }}</th>
                                            <th>{{ __('labels.amount') }}</th>
                                            <th>{{ __('labels.annotation') }}</th>
                                        </tr>
                                        @foreach($invoice->transactions as $transaction)
                                        <tr>
                                            <td>{{dateFormat($transaction->start_date)}}</td>
                                            <td>{{$transaction->receipt_number}}</td>
                                            <td>{{$transaction->method}}</td>
                                            <td>{{moneyFormat($transaction->pivot->amount, null, 2, true)}}</td>
                                            <td>{{showIfAvailable($transaction->annotation, null, 2, true)}}</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="menu1" class="tab-pane fade generate-invoice">
                        <div class="add-form">
                            <div class="default-table">
                                <div class="list-table-custom">
                                    <table class="table table-striped">
                                        <tr>
                                            <th>{{ __('labels.date') }}</th>
                                            <th>{{ __('labels.cash_receipt') }} #</th>
                                            <th>{{ __('labels.status') }}</th>
                                            <th>{{ __('labels.payment_method') }}</th>
                                            <th>{{ __('labels.amount') }}</th>
                                            <th>{{ __('labels.observations') }}</th>
                                        </tr>
                                        <tr>
                                            <td><a href="">20/08/2020</a></td>
                                            <td>1</td>
                                            <td>Pending</td>
                                            <td>Cash</td>
                                            <td>$120.00</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><a href="">20/08/2020</a></td>
                                            <td>1</td>
                                            <td>Pending</td>
                                            <td>Cash</td>
                                            <td>$120.00</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><a href="">20/08/2020</a></td>
                                            <td>1</td>
                                            <td>Pending</td>
                                            <td>Cash</td>
                                            <td>$120.00</td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>

    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
