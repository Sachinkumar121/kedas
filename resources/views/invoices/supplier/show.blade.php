@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.supplier').' '.trans('labels.invoice'))
@section('content')
<input type="hidden" name="resource_type" value="{{ $invoice->type }}">
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block" href="{{ route('invoices.index', ['type' => $invoice->type]) }}">{{ __('labels.back') }}</a>
        <h2>{{ __('labels.supplier').' '.__('labels.invoice').' '.$invoice->internal_number }}</h2>
        <div class="action-btns-view">
            <a target="_blank" href="{{route('invoices.print', $invoice)}}" class="btn-custom"><i
                    class="fa fa-print"></i> {{ __('labels.print') }}</a>
        </div>
        <div class="default-form view-form">
            <div class="generate-invoice">
                <div class="invoice-detail-count">
                    <div class="invoice-detail-count-item">
                        <span class="title">{{ __('labels.total_value') }}</span>
                        <span class="value total-value">{{ moneyFormat($invoice->calculation['total'],null,2,true) }}</span>
                    </div>
                    <div class="invoice-detail-count-item">
                        <span class="title">{{ __('labels.paid') }}</span>
                        <span class="value paid">{{ moneyFormat($invoice->calculation['paidAmount'],null,2,true) }}</span>
                    </div>
                    <div class="invoice-detail-count-item">
                        <span class="title">{{ __('labels.pending') }}</span>
                        <span class="value pending">{{ moneyFormat($invoice->calculation['supplierPendingAmount'],null,2,true) }}</span>
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col-sm-4"></div>
                    <div class="col-sm-4 text-center company-data">

                    </div> -->
                    <div class="col-sm-12">
                        <div class="consumption-box">
                            <div class="consumption-bottom">
                                <span>{{ __('labels.no') }}.</span>
                                <div class="consumption-number">{{ $invoice->internal_number }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="view-form-custom supplier-invoice-view">
                    <table class="table">
                        <tr>
                            <td><b>{{ __('labels.supplier') }}</b></td>
                            @if($invoice->contact instanceof App\Supplier)
                            <td style="width: 45%;"><a href="{{route('suppliers.show', $invoice->contact)}}">{{ $invoice->contact->name }}</a></td>
                            @else
                            <td><a href="{{route('clients.show', $invoice->contact)}}">{{ $invoice->contact->name }}</a></td>
                            @endif
                            <td><b>{{ __('labels.creation_date') }}</b></td>
                            <td>{{ dateFormat($invoice->start_date) }}</td>
                        </tr>
                        <tr>
                            <td><b>{{ __('labels.tax_id') }}</b></td>
                            <td>{{ $invoice->contact->tax_id }}</td>
                            <td><b>{{ __('labels.expiration_date') }}</td>
                            <td>{{ dateFormat($invoice->expiration_date) }}</td>
                        </tr>
                        <tr>
                            <td><b>{{ __('labels.phone') }}</b></td>
                            <td>{{ $invoice->contact->phone }}</td>
                        </tr>
                        <tr>
                            @if($invoice->ncf_number)
                            <td><b>{{ __('labels.ncf') }}</b></td>
                            <td>{{ showIfAvailable($invoice->ncf_number) }}</td>
                            @endif
                            @if($invoice->currency)
                            <td><b>{{ __('labels.currency') }}</b></td>
                            <td>{{ $invoice->currency->code }}</td>
                            @endif
                        </tr>
                        <tr>
                            <td><label><b>{{ __('labels.notes') }}</b></label></td>
                            <td class="descrip-notes">
                                <span>{{showIfAvailable($invoice->notes)}}</span>
                            </td>
                        </tr>
                    </table>
                </div>
                @if ($invoice->documents->isNotEmpty())
                    <div class="form-group col-sm-12 col-lg-12 documents-container">
						<label for="name"><b>{{ __('labels.documents') }}: </b></label>
                        <div class="wrap-document-elements">
    						<div class="documents mt-2" itemscope itemtype="http://schema.org/ImageGallery">
    						  @foreach($invoice->documents->where('ext' ,'!=' , 'pdf') as $document)
    								<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
    								<a href="{{ asset('public/storage/documents/'.$document->name) }}" itemprop="contentUrl" data-size="1024x1024">
        								<img src="{{ asset('public/storage/documents/'.$document->name) }}" itemprop="thumbnail" alt="Image description" />
        							</a>
        							</figure>
        						@endforeach
    						</div>
    						
    						<div class="pdf-documents">
    							@foreach($invoice->documents->where('ext', 'pdf') as $document)
    							<a href="{{ asset('public/storage/documents/'.$document->name) }}" target="__blank"><img src="{{ asset('images/pdf.png') }}" itemprop="thumbnail" /></a>
    							@endforeach
    						</div>
                        </div>
						
						<!-- Root element of PhotoSwipe. Must have class pswp. -->
						<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
							<!-- Background of PhotoSwipe. 
								It's a separate element, as animating opacity is faster than rgba(). -->
							<div class="pswp__bg"></div>
							<!-- Slides wrapper with overflow:hidden. -->
							<div class="pswp__scroll-wrap">
								<!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
								<!-- don't modify these 3 pswp__item elements, data is added later on. -->
								<div class="pswp__container">
									<div class="pswp__item"></div>
									<div class="pswp__item"></div>
									<div class="pswp__item"></div>
								</div>
								<!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
								<div class="pswp__ui pswp__ui--hidden">
									<div class="pswp__top-bar">
										<!--  Controls are self-explanatory. Order can be changed. -->
										<div class="pswp__counter"></div>
										<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
										<button class="pswp__button pswp__button--share" title="Share"></button>
										<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
										<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
										<!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
										<!-- element will get class pswp__preloader--active when preloader is running -->
										<div class="pswp__preloader">
											<div class="pswp__preloader__icn">
												<div class="pswp__preloader__cut">
													<div class="pswp__preloader__donut"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
										<div class="pswp__share-tooltip"></div>
									</div>
									<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
									</button>
									<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
									</button>
									<div class="pswp__caption">
										<div class="pswp__caption__center"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif
                <div class="default-table add-invoice-table">
                    <div class="list-table-custom">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.item') }}</th>
                                    <th>{{ __('labels.price') }}</th>
                                    <th>{{ __('labels.disc') }} %</th>
                                    <th>{{ __('labels.tax') }}</th>
                                    <th>{{ __('labels.tax_amount') }}</th>
                                    <th>{{ __('labels.quantity') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($invoice->conceptItems as $item)
                                <tr>
                                    <td>{{$item->concept ? $item->concept->title : $item->userAccountingHead->name}}</td>
                                    <td>{{moneyFormat($item->price,null,2,true)}}</td>
                                    <td>{{addZeros($item->discount)}}%</td>
                                    <td>{{$item->tax ? addZeros($item->tax->percentage).'%' : 'N/A'}}</td>
                                    <td>{{moneyFormat($item->calculation['taxAmount'],null,2,true)}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{moneyFormat($item->calculation['total'],null,2,true)}}</td>
                                </tr>
                                @endforeach
                                @foreach($invoice->items as $item)
                                <tr>
                                    <td><a href="{{route('inventories.show', $item->inventory)}}">{{$item->inventory->name}}</a></td>
                                    <td>{{moneyFormat($item->price,null,2,true)}}</td>
                                    <td>{{addZeros($item->discount)}}%</td>
                                    <td>{{$item->tax ? addZeros($item->tax->percentage).'%' : 'N/A'}}</td>
                                    <td>{{moneyFormat($item->calculation['taxAmount'],null,2,true)}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{moneyFormat($item->calculation['total'],null,2,true)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-8 total-table">
                        <div class="total-table">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{moneyFormat($invoice->calculation['baseTotal'],null,2,true)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{moneyFormat($invoice->calculation['discountAmount'],null,2,true)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{moneyFormat($invoice->calculation['subtotal'],null,2,true)}}</td>
                                    </tr>
                                    <tr class="tax_row">
                                        <th>{{ __('labels.tax') }}</th>
                                        <td id="invoice_tax">{{moneyFormat($invoice->calculation['taxAmount'],null,2,true)}}</td>
                                    </tr>
                                    {{-- @foreach($invoice->calculation['taxInfo'] as $taxInfo)
                                    <tr class="tax_row">
                                        <th>{{$taxInfo['formatted_name']}}</th>
                                        <td>{{moneyFormat($taxInfo['tax'], null, 2, true)}}</td>
                                    </tr>
                                    @endforeach --}}
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{moneyFormat($invoice->calculation['total'],null,2,true)}}</td>
                                    </tr>
                                    @if($invoice->currency)
                                    <tr class="total_exchanged_rate">
                                        <th>{{ __('labels.total') }} <span class="total-ex-currency-label">{{ $invoice->currency->code }}</span></th>
                                        <td>
                                            <span class="total-ex-currency-label-symbol">{{ $invoice->currency->code }} {{ $invoice->currency->symbol }}</span><span id="total-ex-currency-rate">{{ addZeros($invoice->exchange_rate_total,6,true) }}</span>
                                        </td>
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                            <label class="total_currency_exchange_inner">1 
                                                <span class="exchange_currency_label">{{ $invoice->currency->code }}</span> 
                                                = {{ Auth::user()->company->currency->code }} ({{ Auth::user()->company->currency->symbol }}) 
                                                <span class="exchange_currency_rate">{{ addZeros($invoice->exchange_currency_rate,6,true) }}</span> 
                                            </label>
                                        </th>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
               
            </div>

            {{-- Show accounting and payment section --}}
            <div class="tabs-custom default-form view-table-info">
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#invoice-received-payment">{{ __('labels.payments') }}</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#invoice-accounting">{{ __('labels.accounting') }}</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#invoice-credit-note-payment">{{ __('labels.debit_notes') }}</a></li>
                </ul>
                <div class="tab-content">
                    <div id="invoice-received-payment" class="tab-pane generate-invoice fade in active show">
                        <div class="add-form">                           
                            <div class="default-table">
                                <div class="list-table-custom">                           
                                    <table class="table table-striped">
                                        <thead>
                                            <th>{{ __('labels.date') }}</th>
                                            <th>{{ __('labels.voucher_number') }} #</th>
                                            <th>{{ __('labels.payment_method') }}</th>
                                            <th>{{ __('labels.amount') }}</th>
                                            <th>{{ __('labels.annotation') }}</th>
                                        </thead>
                                        @forelse($invoice->transactions as $transaction)
                                        <tr>
                                            <td>{{dateFormat($transaction->start_date)}}</td>
                                            <td>{{$transaction->voucher_number}}</td>
                                            <td>{{trans('constants'.'.'.$transaction->method)}}</td>
                                            <td>{{moneyFormat($transaction->pivot->amount, null, 2, true)}}</td>
                                            <td>{{showIfAvailable($transaction->annotation)}}</td>
                                        </tr>
                                        @empty
                                        <tr class="empty-row">
                                            <td colspan="5"><p>{{ __('messages.invoice_has_not_received_payment') }}</p></td>
                                        </tr>
                                        @endforelse
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="invoice-accounting" class="invoice-accounting-tab-pane tab-pane fade generate-invoice">
                        <div class="add-form">
                            <div class="default-table">
                                <div class="list-table-custom">
                                    <div id="accounting-head" class="accounting-head">
                                        <span>{{ __('labels.accounting_sheet') }}</span>
                                        <span>{{ __('labels.date') }} {{ dateFormat($invoice->start_date) }}</span>
                                    </div>
                                    @if($invoice->accountingEntries->isNotEmpty())
                                    <a href="#" id="to-print" class="to-print"><i class="fa fa-print" aria-hidden="true"></i>{{ __('labels.print') }}</a>
                                    @endif
                                    <!-- <span class="accounting-notification">Visualiza el movimiento contable de este comprobante. Puedes personalizar las cuentas contables y sus códigos aquí.</span> -->
                                    <div class="table-wrap">
                                        <table id="accounting-table" class="table table-striped">
                                            <thead>
                                                <th>{{ __('labels.code') }}</th>
                                                <th>{{ __('labels.account') }}</th>
                                                <th>{{ __('labels.debit') }}</th>
                                                <th>{{ __('labels.credit') }}</th>
                                            </thead>
                                            @forelse($invoice->accountingEntries as $accountingEntry)
                                            <tr>
                                                <td>{{ $accountingEntry->userAccountingHead->code ?: '-' }}</td>
                                                <td>{{ $accountingEntry->userAccountingHead->name }}</td>
                                                <td>{{ $accountingEntry->nature == 'debit' ? moneyFormat($accountingEntry->amount, null, 2, true) : '' }}</td>
                                                <td>{{ $accountingEntry->nature == 'credit' ? moneyFormat($accountingEntry->amount, null, 2, true) : '' }}</td>
                                            </tr>
                                            @empty
                                            <tr class="empty-row">
                                                <td colspan="4"><p>{{ __('messages.no_accounting_data') }}</p></td>
                                            </tr>
                                            @endforelse
                                            @if($invoice->accountingEntries->isNotEmpty() && count($invoice->debit_credit_amount))
                                                <tr>
                                                    <td colspan="2" style="text-align:right;"><strong>{{ __('labels.total') }}</strong></td>
                                                    <td><strong>{{ moneyFormat($invoice->debit_credit_amount['debit'], null, 2, true) }}</strong></td>
                                                    <td><strong>{{ moneyFormat($invoice->debit_credit_amount['credit'], null, 2, true) }}</strong></td>
                                                </tr>
                                            @endif
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="invoice-credit-note-payment" class="tab-pane generate-invoice fade">
                        <div class="add-form">                           
                            <div class="default-table">
                                <div class="list-table-custom">                      
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>{{ __('labels.sr_number') }}</th>
                                                <th>{{ __('labels.date') }}</th>
                                                <th>{{ __('labels.amount') }}</th>
                                                <th>{{ __('labels.remaining') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($invoice->debitNotes->isNotEmpty())
                                                @foreach($invoice->debitNotes as $index => $debitNote)
                                                <tr>
                                                    <td>{{ $index+1 }}</td>
                                                    <td>{{ dateFormat($debitNote->start_date) }}</td>
                                                    <td>{{ moneyFormat($debitNote->pivot->amount, null, 2, true) }}</td>
                                                    <td>{{ moneyFormat($debitNote->calculation['remaining_amount'], null, 2, true) }}</td>
                                                </tr>
                                                @endforeach
                                                @else
                                                <tr class="empty-row">
                                                    <td colspan="4"><p>{{ __('messages.invoice_has_not_debit_note') }}</p></td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('before-body-scripts')
<script type="text/javascript" src="{{ asset('js/custom-photoswipe.js') }}"></script>
<script type="text/javascript">
	initPhotoSwipeFromDOM('.documents');
</script>
@endpush