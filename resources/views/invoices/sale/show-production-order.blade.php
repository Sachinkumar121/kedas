@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.production_order'))
@section('content')
<input type="hidden" name="resource_type" value="{{ $invoice->type }}">
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block"
            href="{{ route('invoices.index', ['type' => $invoice->type]) }}">{{ __('labels.back') }}</a>

        <h2>{{ __('labels.invoice').' '.$invoice->internal_number }}</h2>
        <div class="action-btns-view">
            <a target="_blank" href="{{route('invoices.print', $invoice)}}" class="btn-custom"><i
                    class="fa fa-print"></i> {{ __('labels.print') }}</a>
            <a href="{{route('invoices.pdf', $invoice)}}" class="btn-custom"><i class="fa fa-download"></i>
                {{ __('labels.download') }}</a>
            <a role="button" data-toggle="modal" data-target="#invoiceEmailModal" href="javascript:void(0)"
                class="btn-custom"><i class="fa fa-envelope"></i> {{ __('labels.send_by_mail') }}</a>
        </div>
        <div class="default-form view-form">
            <div class="generate-invoice">
                <div class="row adds-row">
                    <div class="col-lg-4">
                        <div class="form-group col-sm-12 file-input">
                            @if(Auth::user()->company->logo)
                            <img id="image-preview" src="{{ asset(Auth::user()->company->logo) }}"
                                alt="{{ Auth::user()->name }}" />
                            @else
                            <img id="image-preview" src="{{ asset('images/john-sec.png') }}"
                                alt="{{ Auth::user()->name }}" />
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-8 text-center company-data">
                        <div class="add-cell">
                            <div class="consumption-box">
                                <div class="consumption-bottom">
                                    <span>{{ __('labels.no') }}.</span>
                                    <div class="consumption-number">{{ $invoice->internal_number }}</div>
                                </div>
                            </div>

                            <div class="add-cell-center">
                                <h2>{{ Auth::user()->company->name }}</h2>
                                @if(addressFormat(Auth::user()->company->address, 1)[0] ?? null)
                                <span>
                                    <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[0] }}
                                    </p>
                                </span>
                                @endif
                                @if(addressFormat(Auth::user()->company->address, 1)[1] ?? null)
                                <span>
                                    <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[1] }}
                                    </p>
                                </span>
                                @endif
                                <span>
                                    <h3>{{ __('labels.phone') }}:</h3>
                                    <p>{{ Auth::user()->company->phone }}</p>
                                    @if(Auth::user()->company->fax)
                                    <h3>{{ __('labels.fax') }}:</h3>
                                    <p>{{ Auth::user()->company->fax }}</p>
                                    @endif
                                </span>
                                @if(Auth::user()->company->support_email)
                                <span>
                                    <h3>{{ __('labels.email') }}:</h3>
                                    <p><a href="mailto:{{Auth::user()->company->support_email}}">
                                            {{ Auth::user()->company->support_email }} </a></p>
                                </span>
                                @endif
                                @if(Auth::user()->company->tax_id)
                                <span>
                                    <h3>{{ __('labels.tax_id') }}:</h3>
                                    <p><span>{{ Auth::user()->company->tax_id }} </span></p>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
                <div id="invoice-custom-wrap-block" class="view-form-custom custom-view-info-client">
                    <table class="table">
                        <tr>
                            <td class="cutom-td"><b>{{ __('labels.client') }}</b></td>
                            @if($invoice->contact instanceof App\Supplier)
                            <td><a href="{{route('suppliers.show', $invoice->contact)}}">{{ $invoice->contact->name }}</a>
                            </td>
                            @else
                            <td><a href="{{route('clients.show', $invoice->contact)}}">{{ $invoice->contact->name }}</a>
                            </td>
                            @endif
                            <td><b>{{ __('labels.creation_date') }}</b></td>
                            <td>{{ dateFormat($invoice->start_date) }}</td>
                        </tr>
                        <tr>
                             <td><b>{{ __('labels.phone') }}</b></td>
                            <td>{{ $invoice->contact->phone }}</td>
                            <td><b>{{ __('labels.expiration_date') }}</b></td>
                            <td>{{ dateFormat($invoice->expiration_date) }}</td>
                        </tr>
                        @if($invoice->ncf)
                        <tr>
                            <td><b>{{ __('labels.ncf') }}</b></td>
                            <td>{{ showIfAvailable($invoice->ncf_number) }}</td>
                            <td><b>{{ __('labels.invoice_with') }}</b></td>
                            <td>{{ showIfAvailable($invoice->ncf->name) }}</td>
                        </tr>
                        @endif
                    </table>
                </div>
                <div class="default-table add-invoice-table">
                    <div class="list-table-custom">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.item') }}</th>
                                    <th>{{ __('labels.reference') }}</th>
                                    <th>{{ __('labels.description') }}</th>
                                    <th>{{ __('labels.quantity') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($invoice->items as $item)
                                <tr>
                                    <td><a
                                            href="{{route('inventories.show', $item->inventory)}}">{{$item->inventory->name}}</a>
                                    </td>
                                    <td>{{$item->reference}}</td>
                                    <td>{{showIfAvailable($item->inventory->description)}}</td>
                                    <td>{{$item->quantity}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group col-sm-12 file-input">
                            @if(Auth::user()->company->signature)
                            <img id="signature-preview" src="{{ asset(Auth::user()->company->signature) }}"
                                alt="{{ Auth::user()->name }}" />
                            @else
                            {{-- <img id="signature-preview" alt="{{ Auth::user()->company->name }}" /> --}}
                            @endif
                        </div>
                        <div class="elaborated">{{ __('labels.elaborated_by') }}</div>
                    </div>
                </div>
                <div class="terms-conditions-form">
                    <div class="row">
                    <div class="form-group col-lg-4 col-md-6">
                            <label><b>{{ __('labels.terms_and_conditions') }}</b></label>
                            <p>{{showIfAvailable($invoice->tac)}}</p>
                        </div>
                        <div class="form-group col-lg-4 col-md-6">
                            <label><b>{{ __('labels.notes') }}</b></label>
                            <p>{{showIfAvailable($invoice->notes)}}</p>
                        </div>
                        <div class="form-group col-lg-4 col-md-6">
                            <label><b> {{ __('labels.resolution_text') }}</b></label>
                            <p>{{showIfAvailable($invoice->res_text)}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <a class="arrow-back" href="{{ route('invoices.index', ['type' => $invoice->type]) }}">{{ __('labels.back') }}</a> -->
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
<div class="modal fade" id="invoiceEmailModal" tabindex="-1" role="dialog" aria-labelledby="invoiceEmailModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="send-invoice-mail" action="{{ route('invoices.mail', ['invoice' => $invoice]) }}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="invoiceEmailModalLabel">{{ __('labels.send_invoice_to_emails') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" required="required" name="emails"
                        placeholder="{{ __('labels.separating_addresses_by_comma') }}"
                        value="{{ $invoice->contact->email ? $invoice->contact->email.',': null }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">{{ __('labels.cancel') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('labels.send') }}</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

