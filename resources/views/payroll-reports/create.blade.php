@extends('layouts.dashboard')

@section('title', 'Withholdings')

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>New Withholdings</h2>
        <form method="POST" action="#" id="add-edit-item-category-form" enctype="multipart/form-data">
            @csrf
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Employee <span>*</span></label>
                                <select class="form-control" name="identificationtype" id="identificationtype">
                                    <option>Select Employee</option>
                                    <option value="Mike Tower">Mike Tower</option>
                                    <option value="Joseph Para ">Joseph Para </option>
                                    <option value="Carlos Pricks">Carlos Pricks</option>
                                </select>
                                @error('employee')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Start Date <span>*</span></label>
                                 <input id="payroll_start_date" name="payroll_start_date" class="form-control"
                                required="required" value="{{old('payroll_start_date', getTodayDate())}}"
                                autocomplete="off" />
                                @error('payroll_start_date')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Types <span>*</span></label>
                                <select class="form-control" name="identificationtype" id="identificationtype">
                                    <option>Select Type</option>
                                    <option value="AFP">AFP </option>
                                    <option value="SDSS">SDSS</option>
                                    <option value="ISR">ISR</option>
                                    <option value="SS">SS</option>
                                    <option value="HI">HI</option>
                                    <option value="Others">Others</option>
                                </select>
                                @error('type')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">End Date <span>*</span></label>
                                <input id="payroll_end_date" name="payroll_end_date" class="form-control"
                                required="required" value="{{old('payroll_end_date', getTodayDate())}}"
                                autocomplete="off" />
                                @error('payroll_end_date')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Observations </label>
                                <textarea class="form-control" id="observations" name="observations"></textarea>
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group mt-2">
                                <input type="checkbox" name="recurring" id="recurring" checked>
                                <label for="recurring">Recurring</label>
                                <input type="checkbox" name="on_time" id="on_time" checked>
                                <label for="on_time">On Time</label>
                            </div> 
                            <div class="form-group">
                                <label for="name">Frequency <span>*</span></label>
                                <div>
                                    <select class="form-control col-5" name="identificationtype" id="identificationtype" style="display:inline-block">
                                        <option>Select Type</option>
                                        <option value="Monthly">Monthly </option>
                                        <option value="Biweekly">Biweekly  </option>
                                        <option value="Weekly">Weekly</option>
                                        <option value="Daily">Daily</option>
                                    </select>
                                    <input type="text" class="form-control col-3" name="contact" id="contact" style="display:inline-block">
                                </div>
                            </div>                       
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Amount </label>
                                <input type="text" class="form-control" name="contact" id="contact" value="$2500">
                            </div>                        
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="name">Incomes <span>*</span></label>
                                <select class="form-control" name="payroll_incomes" id="payroll_incomes">
                                    <option>Accounts Receivable</option>
                                </select>
                                @error('payroll_incomes')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                    <div class="form-group popup-btns">
                        <a href="{{ route('with-holdings.index') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                        <button type="submit" name="submit" class="btn-custom" disabled="disabled">{{ __('labels.save') }}</button>
                    </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
