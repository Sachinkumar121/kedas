@extends('layouts.livewire')

@section('title', "counter application")

@section('content')

	<h1>Basic blade part render by controller function</h1>

	{{-- The following is render through livewire component. --}}
	{{-- <livewire:counter :count="5" /> --}}
	@livewire('counter', ['count' => 5])


@endsection