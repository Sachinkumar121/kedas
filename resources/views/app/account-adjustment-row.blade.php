<tr>
    <input type="hidden" name="items[{{$row_number}}][id]" value="{{ old('items')[$row_number]['id'] ?: 0 }}">
    <td>
        {!! Form::select('items['.$row_number.'][contact_id]', $contact_options, old('items['.$row_number.'][contact_id]', null), ['class'=>"form-control single-search-selection", 'id'=>'items['.$row_number.'][contact_id]'], $contact_attributes) !!}
    </td>
    <td>
        <input type="text" placeholder="{{ __('labels.description') }}" name="items[{{$row_number}}][description]" id="items[{{$row_number}}][description]" class="form-control" value="{{ old('items')[$row_number]['description'] ?? null }}" />
    </td>
    <td class="td-concept">
        <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="concept-multilevelselect-{{$row_number}}">
            <button type="button" class="btn btn-secondary dropdown-toggle" id="concept-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
            <div class="dropdown-menu" aria-labelledby="concept-multilevelselect-button">
                <div class="hs-searchbox">
                    <input type="text" class="form-control" autocomplete="off">
                </div>
                <div class="hs-menu-inner">
                    <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_account') }}</a>
                    <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.concept') }}</strong>
                    @foreach($all_accounts_options as $all_account)
                        <a class="dropdown-item" data-value="{{$all_account->id}}" data-level="{{$all_account->level}}" href="javascript::void(0)">
                            <span class="account-name">{{$all_account->custom_name}}</span>
                            @if($all_account->level == 1)
                            <span class="detail-type">{{ $all_account->parent->name }}</span>
                            @endif
                        </a>
                    @endforeach
                </div>
            </div>
            @php
            // print_r(old('items')[$row_number]['concept-item']);
            @endphp
            <input class="d-none" name="items[{{$row_number}}][concept-item]" readonly="readonly" aria-hidden="true" type="text" value="{{ old('items')[$row_number]['concept-item'] ?: null }}"  required="required" />
        </div>
        @error('items[{{$row_number}}][concept-item]')
            <span class="error" role="alert">{{ $message }}</span>
        @enderror
    </td>
    <td style="text-align: center;">
        <input type="number" min="0.01" step="any" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.debit_amount')]) }}" placeholder="{{ __('labels.debit_amount') }}" name="items[{{$row_number}}][debit_amount]" class="form-control"  value="{{ old('items')[$row_number]['debit_amount'] ?? null }}" />
    </td>
    <td style="text-align: center;">
        <input type="number" min="0.01" step="any" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.credit_amount')]) }}" placeholder="{{ __('labels.credit_amount') }}" name="items[{{$row_number}}][credit_amount]" class="form-control"  value="{{ old('items')[$row_number]['credit_amount'] ?? null }}" />
    </td>
    <td>
        <a class="delete-icon remove-account-adjustment-row" data-row_number="{{$row_number}}"><i class="fa fa-times" aria-hidden="true"></i></a>
    </td>
</tr>
<script type="text/javascript">
    id = "{{$row_number}}";
    generateHierarchySelect('concept-multilevelselect-'+id, 'input[name="items['+id+'][concept-item]"]');
</script>