@extends(Auth::check() ? (Auth::user()->isSuperAdmin() ? 'layouts.admin.dashboard' : 'layouts.dashboard') : 'errors.layout')

@section('title', trans('labels.dashboard'))

@section('content')
<div class="dashboard-right error-dashboard-right comin-soon-wrap">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="dashboard-max-w">
        <div class="wrapper coming-soon">
            <p>{{ __('labels.stay_tuned') }}</p>
            <h1>{{ __('labels.launch_soon') }}<span class="dot">.</span></h1>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
