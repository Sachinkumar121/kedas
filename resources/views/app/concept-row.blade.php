<tr>
    <input type="hidden" name="concepts[{{$row_number}}][id]" value="{{ old('concepts')[$row_number]['id'] ?: 0 }}">
    <td class="td-concept">                                            
        <div class="dropdown hierarchy-select hierarchy-form-element hierarchy-select-group-by" id="concept-multilevelselect-{{$row_number}}">
            <button type="button" class="btn btn-secondary dropdown-toggle" id="concept-multilevelselect-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
            <div class="dropdown-menu" aria-labelledby="concept-multilevelselect-button">
                <div class="hs-searchbox">
                    <input type="text" class="form-control" autocomplete="off">
                </div>
                <div class="hs-menu-inner">
                    <a class="dropdown-item" data-value="" data-default-selected="" href="javascript::void(0)" disabled>{{ __('labels.select_concept') }}</a>
                    @if($concept_with_products && $trackedInventories->isNotEmpty())
                    <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.inventory') }}</strong>
                    @foreach($trackedInventories as $inventory_account)
                        <a class="dropdown-item" data-value="inventory-{{$inventory_account->id}}" data-level="1" href="javascript::void(0)">
                            <span class="account-name">{{$inventory_account->name}}</span>
                            @if($inventory_account->level == 1)
                            <span class="detail-type">{{ $inventory_account->parent->name }}</span>
                            @endif
                        </a>
                    @endforeach
                    @endif
                    <strong style="cursor: default;" data-level="0" href="javascript::void(0)">{{ __('labels.concept') }}</strong>
                    @foreach($all_accounts_options as $all_account)
                        <a class="dropdown-item" data-value="concept-{{$all_account->id}}" data-level="{{$all_account->level}}" href="javascript::void(0)">
                            <span class="account-name">{{$all_account->custom_name}}</span>
                            @if($all_account->level == 1)
                            <span class="detail-type">{{ $all_account->parent->name }}</span>
                            @endif
                        </a>
                    @endforeach
                </div>
            </div>
            <input class="d-none" name="concepts[{{$row_number}}][item]" readonly="readonly" aria-hidden="true" type="text" value="{{ old('concepts')[$row_number]['item'] ?: null }}" />
        </div>
        @error('concepts[{{$row_number}}][item]')
        <span class="error" role="alert">{{ $message }}</span>
        @enderror
    </td>
    <td>
        <input type="number" min="0.01" step="any" placeholder="{{ __('labels.price') }}" name="concepts[{{$row_number}}][price]" id="unitprice" class="form-control"  value="{{ old('concepts')[$row_number]['price'] ?: null }}" required />
    </td>
    <td style="text-align: center;">
        <input type="number" min="0" max="99" pattern="^\d*(\.\d{0,2})?$" data-msg="{{ __('validation.regex', ['attribute' => __('labels.discount')]) }}" placeholder="%" name="concepts[{{$row_number}}][discount]" id="discount" class="form-control" value="{{ old('concepts')[$row_number]['discount'] ?: 0 }}" />
    </td>
    <td>
        {!! Form::select('concepts['.$row_number.'][tax]', $tax_options, old('concepts['.$row_number.'][tax]', null), ['class'=>"form-control small-single-search-selection", 'id'=>'concepts['.$row_number.'][tax]'], $tax_attributes) !!}
    </td>
    <td>
        <input type="text" placeholder="{{ __('labels.tax_amount') }}" name="concepts[{{$row_number}}][tax_amount]" id="concepts[{{$row_number}}][tax_amount]" class="form-control" readonly value="{{ old('concepts')[$row_number]['tax_amount'] ?: null }}" />
    </td>
    <td>
        <input type="number" placeholder="1" name="concepts[{{$row_number}}][quantity]" id="quantity" class="form-control" min="1" value="{{ old('concepts')[$row_number]['quantity'] ?: 1 }}">
    </td>
    <td>
        <span id="concepts[{{$row_number}}][total]">{{Auth::user()->company->currency->symbol}}0.00</span>
    </td>
    <td>
        @if($row_number != 0)
            <a class="delete-icon remove-concept-row" data-row_number="{{$row_number}}"><i class="fa fa-times" aria-hidden="true"></i></a>
        @endif
    </td>
</tr>
<script type="text/javascript">
    id = "{{$row_number}}";
    generateHierarchySelect('concept-multilevelselect-'+id, 'input[name="concepts['+id+'][item]"]');
</script>