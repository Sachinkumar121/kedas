@extends('layouts.dashboard')

@section('title', trans('labels.edit').' '.trans('labels.inventory_adjustment'))

@section('content')
<div class="dashboard-right" id="inventory-adjustments">
    <update-adjustments-component cancel-route="{{ route('inventory-adjustments.index') }}" :adjustment="{{ $inventoryAdjustment }}" inventories="{{ $inventories }}"></update-adjustments-component>
</div>
<div class="clearfix"></div>
@endsection
@push('scripts')
    <script src="{{ asset('js/inventory-adjustments.js') }}" defer></script>
    <script type="text/javascript" src="{{ asset('js/custom-dropzone.js') }}"></script>
    <script type="text/javascript">
        var documents = {!! json_encode($inventoryAdjustment->documents) !!};
        addDropZone(documents, 'both', 10);
    </script>
@endpush