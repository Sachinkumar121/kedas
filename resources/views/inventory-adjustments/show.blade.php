@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.inventory_adjustment'))
@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
     <a class="arrow-back d-block" href="{{ route('inventory-adjustments.index') }}">{{ __('labels.back') }}</a>
        <h2>{{ __('labels.inventory_adjustment').' '.__('labels.details') }}</h2>
        <div class="default-form view-form-detail inventory-adjustments-list-table-show-table">
            <div class="generate-invoice">
                <div class="add-form row">
                    <div class="form-group col-sm-12">
                        <label for="name"><b>{{ __('labels.date') }}: </b></label>
                        <span class="value-form">{{ dateFormat($inventoryAdjustment->adjustment_date) }}</span>
                    </div>
                    <div class="form-group col-lg-12">
                        <label for="email"><b>{{ __('labels.observations') }} </b></label>
                        <div class="descrip-notes"> 
                            <span>{{ showIfAvailable($inventoryAdjustment->observations) }}</span>
                        </div>
                    </div>
                </div>
                @if ($inventoryAdjustment->documents->isNotEmpty())
                        <div class="form-group col-lg-12 documents-container">
                            <label for="name" class="p-0">{{ __('labels.documents') }}: </label>
                            <div class="wrap-document-elements">
                                <div class="documents mt-2" itemscope itemtype="http://schema.org/ImageGallery">
                                    @foreach($inventoryAdjustment->documents->where('ext' ,'!=' , 'pdf') as $document)
                                    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                        <a href="{{ asset('public/storage/documents/'.$document->name) }}" itemprop="contentUrl"
                                            data-size="1024x1024">
                                            <img src="{{ asset('public/storage/documents/'.$document->name) }}"
                                                itemprop="thumbnail" alt="Image description" />
                                        </a>
                                    </figure>
        							@endforeach
                                </div>
    							<div class="pdf-documents">
                                    @foreach($inventoryAdjustment->documents->where('ext', 'pdf') as $document)
                                    <a href="{{ asset('public/storage/documents/'.$document->name) }}" target="__blank"><img
                                            src="{{ asset('images/pdf.png') }}" itemprop="thumbnail" /></a>
                                    @endforeach
                                </div>

                                <!-- Root element of PhotoSwipe. Must have class pswp. -->
                                <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                                    <!-- Background of PhotoSwipe. 
        								It's a separate element, as animating opacity is faster than rgba(). -->
                                    <div class="pswp__bg"></div>
                                    <!-- Slides wrapper with overflow:hidden. -->
                                    <div class="pswp__scroll-wrap">
                                        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
                                        <!-- don't modify these 3 pswp__item elements, data is added later on. -->
                                        <div class="pswp__container">
                                            <div class="pswp__item"></div>
                                            <div class="pswp__item"></div>
                                            <div class="pswp__item"></div>
                                        </div>
                                        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                                        <div class="pswp__ui pswp__ui--hidden">
                                            <div class="pswp__top-bar">
                                                <!--  Controls are self-explanatory. Order can be changed. -->
                                                <div class="pswp__counter"></div>
                                                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                                                <button class="pswp__button pswp__button--share" title="Share"></button>
                                                <button class="pswp__button pswp__button--fs"
                                                    title="Toggle fullscreen"></button>
                                                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                                                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                                                <!-- element will get class pswp__preloader--active when preloader is running -->
                                                <div class="pswp__preloader">
                                                    <div class="pswp__preloader__icn">
                                                        <div class="pswp__preloader__cut">
                                                            <div class="pswp__preloader__donut"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                                <div class="pswp__share-tooltip"></div>
                                            </div>
                                            <button class="pswp__button pswp__button--arrow--left"
                                                title="Previous (arrow left)">
                                            </button>
                                            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                                            </button>
                                            <div class="pswp__caption">
                                                <div class="pswp__caption__center"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                <div class="default-table add-invoice-table">
                    <div class="list-table-custom">
                        @if ($inventoryAdjustment->items->isNotEmpty())
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.item') }}</th>
                                    <th>{{ __('labels.type_of_adjustment') }}</th>
                                    <th>{{ __('labels.quantity') }}</th>
                                    <th>{{ __('labels.unit_cost') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($inventoryAdjustment->items as $item)
                                <tr>
                                    <td>{{ $item->inventory->name }}</td>
                                    <td>{{ $item->inventory_adjustment_type }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ moneyFormat($item->unit_cost) }}</td>
                                    <td>{{ moneyFormat($item->unit_cost * $item->quantity) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
                <div class="inventory-value">
                    <p><b>{{ __('labels.total') }}</b>
                        <strong>{{ moneyFormat($inventoryAdjustment->total) }}</strong>
                    </p>
                </div>
            </div>
            <div class="tabs-custom default-form view-table-info">
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#accounting-section">{{ __('labels.accounting') }}</a></li>
                </ul>
                <div class="tab-content">
                    <div id="accounting-section" class="accounting-tab-pane tab-pane fade generate-invoice fade in active show">
                        <div class="add-form">
                            <div class="default-table">
                                <div class="list-table-custom">
                                    <div id="accounting-head" class="accounting-head">
                                        <span>{{ __('labels.accounting_sheet') }}</span>
                                        <span>{{ __('labels.date') }} {{ dateFormat($inventoryAdjustment->adjustment_date) }}</span>
                                    </div>
                                    @if($inventoryAdjustment->accountingEntries->isNotEmpty())
                                    <a href="#" id="to-print" class="to-print"><i class="fa fa-print" aria-hidden="true"></i>{{ __('labels.print') }}</a>
                                    @endif
                                    <!-- <span class="accounting-notification">Visualiza el movimiento contable de este comprobante. Puedes personalizar las cuentas contables y sus códigos aquí.</span> -->
                                    <div class="table-wrap">
                                        <table id="accounting-table" class="table table-striped">
                                            <thead>
                                                <th>{{ __('labels.code') }}</th>
                                                <th>{{ __('labels.account') }}</th>
                                                <th>{{ __('labels.debit') }}</th>
                                                <th>{{ __('labels.credit') }}</th>
                                            </thead>
                                            @forelse($inventoryAdjustment->accountingEntries as $accountingEntry)
                                            <tr>
                                                <td>{{ $accountingEntry->userAccountingHead->code ?: '-' }}</td>
                                                <td>{{ $accountingEntry->userAccountingHead->name }}</td>
                                                <td>{{ $accountingEntry->nature == 'debit' ? moneyFormat($accountingEntry->amount) : '' }}</td>
                                                <td>{{ $accountingEntry->nature == 'credit' ? moneyFormat($accountingEntry->amount) : '' }}</td>
                                            </tr>
                                            @empty
                                            <tr class="empty-row">
                                                <td colspan="4"><p>{{ __('messages.no_accounting_data') }}</p></td>
                                            </tr>
                                            @endforelse
                                            @if($inventoryAdjustment->accountingEntries->isNotEmpty() && count($inventoryAdjustment->debit_credit_amount))
                                                <tr>
                                                    <td colspan="2" style="text-align:right;"><strong>{{ __('labels.total') }}</strong></td>
                                                    <td><strong>{{ moneyFormat($inventoryAdjustment->debit_credit_amount['debit']) }}</strong></td>
                                                    <td><strong>{{ moneyFormat($inventoryAdjustment->debit_credit_amount['credit']) }}</strong></td>
                                                </tr>
                                            @endif
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
@push('before-body-scripts')
<script type="text/javascript" src="{{ asset('js/custom-photoswipe.js') }}"></script>
<script type="text/javascript">
initPhotoSwipeFromDOM('.documents');
</script>
@endpush