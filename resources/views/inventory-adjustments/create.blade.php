@extends('layouts.dashboard')

@section('title', trans('labels.inventory_adjustment'))

@section('content')
<div class="dashboard-right" id="inventory-adjustments">
    <adjustments-component cancel-route="{{ route('inventory-adjustments.index') }}" inventories="{{ $inventories }}"></adjustments-component>
</div>
<div class="clearfix"></div>
@endsection
@push('scripts')
    <script src="{{ asset('js/inventory-adjustments.js') }}" defer></script>
    <script type="text/javascript" src="{{ asset('js/custom-dropzone.js') }}"></script>
    <script type="text/javascript">
        addDropZone(null, 'both', 10);
    </script>
@endpush