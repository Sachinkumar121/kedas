{{ __('labels.hi') }} {{$inventory->user->name}}, 
{{ __('labels.new_inventory_named') }} <b>{{$inventory->name}}</b> {{ __('labels.has_been_created') }}.

{{ __('labels.copy_below_link_view').' '.__('labels.product') }}.
<a href="{{route('inventories.show', $inventory)}}" target="_blank" >{{route('inventories.show', $inventory)}}</a> 

{{ __('labels.regards') }},
{{ config('app.name', 'Kedas') }}

© {{date("Y")}} {{ config('app.name', 'Kedas') }}.{{ __('labels.all_rights_reserved') }}.