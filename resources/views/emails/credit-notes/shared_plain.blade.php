{{ __('labels.hi') }}, 
<b>{{ $creditNote->company->name }}</b> {{ __('labels.shared_an') }} <b>{{ __('credit_note ').$creditNote->id }}</b> {{ __('labels.with_you') }}.

{{ __('labels.find_attachment_for_credit_note_details') }}.

{{ __('labels.regards') }},
{{ config('app.name', 'Kedas') }}

© {{date("Y")}} {{ config('app.name', 'Kedas') }}. {{ __('labels.all_rights_reserved') }}.