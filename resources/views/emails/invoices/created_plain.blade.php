{{ __('labels.hi') }} {{$invoice->user->name}}, 
{{ __('labels.a_new') }} {{ __('labels.recurring_invoice') }} <b>{{ __('invoice '.$invoice->internal_number) }}</b> {{ __('labels.has_been_created') }}.

{{ __('labels.copy_below_link_view').' '.__('labels.invoice') }}.
<a href="{{route('invoices.show', $invoice)}}" target="_blank" >{{route('invoices.show', $invoice)}}</a> 
{{ __('labels.regards') }},
{{ config('app.name', 'Kedas') }}

© {{date("Y")}} {{ config('app.name', 'Kedas') }}. {{ __('labels.all_rights_reserved') }}.