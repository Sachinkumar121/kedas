{{ __('labels.hi') }}, 
<b>{{ $invoice->company->name }}</b> {{ __('labels.shared_an') }} <b>{{ __('invoice '.$invoice->internal_number) }}</b> {{ __('labels.with_you') }}.

{{ __('labels.find_attachment_for_invoice_details') }}.

{{ __('labels.regards') }},
{{ config('app.name', 'Kedas') }}

© {{date("Y")}} {{ config('app.name', 'Kedas') }}. {{ __('labels.all_rights_reserved') }}.