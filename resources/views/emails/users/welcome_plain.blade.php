{{ __('labels.hi') }} {{ $user->name }}, 
{{ __('labels.welcome_to') }} {{ config('app.name', 'Kedas') }}. 

{{ __('labels.copy_below_link_view').' '.__('labels.dashboard') }}.
<a href="{{ route('dashboard') }}" >{{ route('dashboard') }}</a> 

{{ __('labels.regards') }},
{{ config('app.name', 'Kedas') }}

© {{date("Y")}} {{ config('app.name', 'Kedas') }}. {{ __('labels.all_rights_reserved') }}.