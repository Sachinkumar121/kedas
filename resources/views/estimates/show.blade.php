@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.estimate'))
@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block" href="{{ route('estimates.index') }}">{{ __('labels.back') }}</a>
        <h2>{{ __('labels.estimate').' '.$estimate->internal_number }}</h2>
        <div class="action-btns-view">
            <a target="_blank" href="{{route('estimates.print', $estimate)}}" class="btn-custom"><i class="fa fa-print"></i> {{ __('labels.print') }}</a>
            <a href="{{route('estimates.pdf', $estimate)}}" class="btn-custom"><i class="fa fa-download"></i> {{ __('labels.download') }}</a>
            <a role="button" data-toggle="modal" data-target="#estimateEmailModal" href="javascript:void(0)" class="btn-custom"><i class="fa fa-envelope"></i> {{ __('labels.send_by_mail') }}</a>
            <a href="{{route('invoices.create', ['type' => $type, 'estimate_id' => $estimate->id ])}}" class="btn-custom"><i class="fa fa-pencil"></i> {{ __('labels.convert_to_invoice') }}</a>
        </div>
        <div class="default-form view-form estimate-show-view">
            <div class="generate-invoice">
                <div class="row adds-row">
                    <div class="col-lg-4">
                        <div class="form-group col-sm-12 file-input">
                            @if(Auth::user()->company->logo)
                            <img id="image-preview" src="{{ asset(Auth::user()->company->logo) }}" alt="{{ Auth::user()->name }}" />
                            @else
                            <img id="image-preview" src="{{ asset('images/john-sec.png') }}" alt="{{ Auth::user()->name }}" />
                            @endif 
                        </div>
                    </div>
                    <div class="col-lg-8 text-center company-data">
                        <div class="add-cell">
                                <div class="consumption-box">
                                    <div class="consumption-bottom">
                                        <span>{{ __('labels.no') }}.</span>
                                        <div class="consumption-number">{{ $estimate->internal_number }}</div>
                                    </div>
                                </div>

                                <div class="add-cell-center">
                                   <h2>{{ Auth::user()->company->name }}</h2>
                                   @if(addressFormat(Auth::user()->company->address, 1)[0] ?? null)
                                    <span>
                                        <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[0] }}</p>
                                    </span>
                                    @endif
                                   @if(addressFormat(Auth::user()->company->address, 1)[1] ?? null)
                                    <span>
                                        <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[1] }}</p>
                                    </span>
                                    @endif
                                    <span>
                                        <h3>{{ __('labels.phone') }}:</h3> <p>{{ Auth::user()->company->phone }}</p>
                                        @if(Auth::user()->company->fax)
                                            <h3>{{ __('labels.fax') }}:</h3> <p>{{ Auth::user()->company->fax }}</p>
                                        @endif
                                    </span>
                                    @if(Auth::user()->company->support_email)
                                        <span>
                                            <h3>{{ __('labels.email') }}:</h3> 
                                            <p><a href="mailto:{{Auth::user()->company->support_email}}"> {{ Auth::user()->company->support_email }} </a></p>
                                        </span>
                                    @endif
                                    @if(Auth::user()->company->tax_id)
                                        <span>
                                            <h3>{{ __('labels.tax_id') }}:</h3> 
                                            <p><span>{{ Auth::user()->company->tax_id }} </span></p>
                                        </span>
                                    @endif
                                 </div>
                        </div>
                    </div>
                </div>
                <div class="view-form-custom">
                    <table class="table">
                        <tr>
                            <td><b>{{ __('labels.client') }}</b></td>
                            @if($estimate->contact instanceof App\Supplier)
                            <td><a href="{{route('suppliers.show', $estimate->contact)}}">{{ $estimate->contact->name }}</a></td>
                            @else
                            <td><a href="{{route('clients.show', $estimate->contact)}}">{{ $estimate->contact->name }}</a></td>
                            @endif
                            <td><b>{{ __('labels.creation_date') }}</b></td>
                            <td>{{ dateFormat($estimate->start_date) }}</td>
                        </tr>
                        <tr>
                            <td><b>{{ __('labels.tax_id') }}</b></td>
                            <td>{{ $estimate->contact->tax_id }}</td>
                            <td><b>{{ __('labels.expiration_date') }}</b></td>
                            <td>{{ dateFormat($estimate->expiration_date) }}</td>
                        </tr>
                        <tr>
                            <td><b>{{ __('labels.phone') }}</b></td>
                            <td>{{ $estimate->contact->phone }}</td>
                        </tr>
                        @if($estimate->currency)
                        <tr>
                            <td><b>{{ __('labels.currency') }}</b></td>
                            <td>{{ $estimate->currency->code }}</td>
                        </tr>
                        @endif
                    </table>
                </div>
                <div class="terms-conditions-form">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label><b>{{ __('labels.notes') }}</b></label>
                            <div class="descrip-notes"><span>{{showIfAvailable($estimate->notes)}}</span></div>
                        </div>
                    </div>
                </div>
                <div class="default-table add-invoice-table">
                    <div class="list-table-custom">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.item') }}</th>
                                    <th>{{ __('labels.reference') }}</th>
                                    <th>{{ __('labels.price') }}</th>
                                    <th>{{ __('labels.disc') }} %</th>
                                    <th>{{ __('labels.tax') }}</th>
                                    <th>{{ __('labels.tax_amount') }}</th>
                                    <th>{{ __('labels.description') }}</th>
                                    <th>{{ __('labels.quantity') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($estimate->items as $item)
                                <tr>
                                    <td><a href="{{route('inventories.show', $item->inventory)}}">{{$item->inventory->name}}</a></td>
                                    <td>{{$item->reference}}</td>
                                    <td>{{moneyFormat($item->price,null,2,true)}}</td>
                                    <td>{{addZeros($item->discount)}}%</td>
                                    <td>{{$item->tax ? addZeros($item->tax->percentage).'%' : 'N/A'}}</td>
                                    <td>{{moneyFormat($item->calculation['taxAmount'],null,2,true)}}</td>
                                    <td>{{showIfAvailable($item->inventory->description)}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{moneyFormat($item->calculation['baseTotal'] - $item->calculation['discountAmount'],null,2,true)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group col-sm-12 file-input">
                            @if(Auth::user()->company->signature)
                            <img id="signature-preview" src="{{ asset(Auth::user()->company->signature) }}" alt="{{ Auth::user()->name }}" />
                            @else
                            {{-- <img id="signature-preview" alt="{{ Auth::user()->company->name }}" /> --}}
                            @endif 
                        </div>
                        <div class="elaborated">{{ __('labels.elaborated_by') }}</div>
                    </div>
                    <div class="col-sm-8 total-table">
                        <div class="total-table">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{moneyFormat($estimate->calculation['baseTotal'],null,2,true)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{moneyFormat($estimate->calculation['discountAmount'],null,2,true)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{moneyFormat($estimate->calculation['subtotal'],null,2,true)}}</td>
                                    </tr>
                                    <tr class="tax_row">
                                        <th>{{ __('labels.tax') }}</th>
                                        <td>{{moneyFormat($estimate->calculation['taxAmount'],null,2,true)}}</td>
                                    </tr>
                                    {{-- @foreach($estimate->calculation['taxInfo'] as $taxInfo)
                                    <tr class="tax_row">
                                        <th>{{$taxInfo['formatted_name']}}</th>
                                        <td>{{moneyFormat($taxInfo['tax'],null,2,true)}}</td>
                                    </tr>
                                    @endforeach --}}
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{moneyFormat($estimate->calculation['total'],null,2,true)}}</td>
                                    </tr>
                                    @if($estimate->currency)
                                    <tr class="total_exchanged_rate">
                                        <th>{{ __('labels.total') }} <span class="total-ex-currency-label">{{ $estimate->currency->code }}</span></th>
                                        <td>
                                            <span class="total-ex-currency-label-symbol">{{ $estimate->currency->code }} {{ $estimate->currency->symbol }}</span><span id="total-ex-currency-rate">{{ addZeros($estimate->exchange_rate_total,6,true) }}</span>
                                        </td>
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                            <label class="total_currency_exchange_inner">1 
                                                <span class="exchange_currency_label">{{ $estimate->currency->code }}</span> 
                                                = {{ Auth::user()->company->currency->code }} ({{ Auth::user()->company->currency->symbol }}) 
                                                <span class="exchange_currency_rate">{{ addZeros($estimate->exchange_currency_rate,6, true) }}</span> 
                                            </label>
                                        </th>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
              
            </div>
        </div>
        
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
<div class="modal fade" id="estimateEmailModal" tabindex="-1" role="dialog" aria-labelledby="estimateEmailModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form id="send-estimate-mail" action="{{ route('estimates.mail', ['estimate' => $estimate]) }}" method="POST">
        @csrf
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="estimateEmailModalLabel">{{ __('labels.send_estimate_to_emails') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <input type="text" class="form-control" required="required" name="emails" placeholder="{{ __('labels.separating_addresses_by_comma') }}" value="{{ $estimate->contact->email ? $estimate->contact->email.',': null }}">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('labels.cancel') }}</button>
            <button type="submit" class="btn btn-primary">{{ __('labels.send') }}</button>
          </div>
        </div>
    </form>
  </div>
</div>
@endsection
