@extends('layouts.dashboard')

@section('title', trans('labels.company'))

@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.edit').' '.__('labels.company').' '.__('labels.information') }}</h2>
        {{ Form::open(array('method'=>'POST', 'route' => ['company.update'], 'id' => 'edit-company-form', 'enctype' => 'multipart/form-data')) }}
            <div class="default-form add-new-contact-form">
                <div class="generate-invoice">
                    <div class="add-form row">
                        <div class="form-group col-sm-6">
                            <label for="name">{{ __('labels.name') }} <span>*</span></label>
                            <input class="form-control" type="text" name="name" placeholder="{{ __('labels.name_of_business') }}" value="{{ old('name', $company->name) }}" autocomplete="off" />
                            @error('name')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="role">{{ __('labels.role') }} <span>*</span></label>
                            <input class="form-control" type="text" name="role" placeholder="{{ __('labels.role_in_company') }}" value="{{ old('role', $company->role) }}" autocomplete="off" />
                            @error('role')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="address">{{ __('labels.address') }} <span>*</span></label>
                            <input class="form-control" type="text" name="address" placeholder="{{ __('labels.address') }}" autocomplete="off" value="{{ old('address', $company->address ? $company->address->line_1 : '') }}" />
                            @error('address')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>                        
                        <div class="form-group col-sm-6">
                            <label for="city">{{ __('labels.city') }} </label>
                            <input class="form-control" type="text" name="city" placeholder="{{ __('labels.city') }}" value="{{ old('city', $company->address ? $company->address->city : '') }}" autocomplete="off" />
                            @error('city')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="zip">{{ __('labels.zip_code') }}</label>
                            <input class="form-control" type="text" name="zip" placeholder="{{ __('labels.zip') }}" value="{{ old('zip', $company->address ? $company->address->zip : '') }}" autocomplete="off" />
                            @error('zip')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="state">{{ __('labels.state') }} </label>
                            <input class="form-control" type="text" name="state" placeholder="{{ __('labels.state') }}" value="{{ old('state', $company->address ? $company->address->state : '') }}" autocomplete="off" />
                            @error('state')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="country">{{ __('labels.country') }} <span>*</span></label>
                            {!! Form::select('country', $country_options, old('country', $company->address ? $company->address->country_id : null), ['class'=>"form-control single-search-selection", 'id'=>"country", 'required' => 'required']) !!}
                            @error('country')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="phone">{{ __('labels.phone') }} <span>*</span></label>
                            <input class="form-control" type="tel" name="phone" placeholder="{{ __('labels.telephone') }}" value="{{ old('phone', $company->phone) }}" autocomplete="off" />
                            @error('phone')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="fax">{{ __('labels.mobile') }} </label>
                            <input class="form-control" type="text" name="mobile" placeholder="{{ __('labels.mobile') }}" value="{{ old('mobile', $company->mobile) }}" autocomplete="off" />
                            @error('fax')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="tax_id">{{ __('labels.tax_id') }} <span>*</span></label>
                            <input class="form-control" type="text" name="tax_id" placeholder="{{ __('labels.tax_id') }}" value="{{ old('tax_id', $company->tax_id) }}" autocomplete="off" />
                            @error('tax_id')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="support_email">{{ __('labels.support').' '.__('labels.email') }} <span>*</span></label>
                            <input class="form-control" type="text" name="support_email" placeholder="{{ __('labels.support').' '.__('labels.email') }}" value="{{ old('support_email', $company->support_email) }}" autocomplete="off" />
                            @error('support_email')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="currency">{{ __('labels.currency') }} <span>*</span></label>
                            @if($resource_associated)
                            {!! Form::select('currency', $currency_options, $company->currency_id, ['class'=>"form-control single-search-selection", 'id'=>"currency", 'required' => 'required', 'disabled' => true]) !!}
                            @else
                            {!! Form::select('currency', $currency_options, $company->currency_id, ['class'=>"form-control single-search-selection", 'id'=>"currency", 'required' => 'required']) !!}
                            @endif
                            @error('currency')
                                <span class="error" role="alert">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-sm-6">
                            <div class="form-group file-input file-input-under-form">
                                @if(!$company->logo)
                                    <label for="image" id="image-preview-label">
                                        <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                        {{ __('labels.logo') }}
                                    </label>
                                @endif                                
                                @if($company->logo)
                                  <img id="image-preview" src="{{ asset($company->logo) }}" alt="{{ $company->name }}" />
                                @else
                                  <img id="image-preview" src="" />
                                @endif                           
                                <input type="file" id="company-logo" name="logo" class="file-upload-preview" accept="image/*" data-img_id="image-preview" data-msg-accept="{{ __('messages.invalid_image_file_error') }}" data-label_id="image-preview-label"/>
                                <a class="upload-button" data-preview_section="company-logo"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                @error('logo')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <div class="form-group file-input file-input-under-form">
                                @if(!$company->signature)
                                    <label for="signature" id="signature-preview-label">
                                        <i class="fa fa-file-image-o image-icon" aria-hidden="true"></i>
                                        {{ __('labels.signature') }}
                                    </label>
                                @endif
                                @if($company->signature)
                                  <img id="signature-preview" src="{{ asset($company->signature) }}" alt="{{ $user->name }}" />
                                @else
                                  <img id="signature-preview" src="" />
                                @endif 
                                <input type="file" id="company-sign" name="signature" class="file-upload-preview" accept="image/*" data-img_id="signature-preview" data-msg-accept="{{ __('messages.invalid_image_file_error') }}." data-label_id="signature-preview-label"/>
                                <a class="upload-button" data-preview_section="company-sign"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                @error('signature')
                                    <span class="error" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comments-section">
                <div class="form-group popup-btns">
                    <a href="{{ route('dashboard') }}"><button type="button" name="cancel" class="btn-custom cancel-btn">{{ __('labels.cancel') }}</button></a>
                    <button type="submit" name="submit" class="btn-custom">{{ __('labels.update') }}</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
</div>
@endsection
