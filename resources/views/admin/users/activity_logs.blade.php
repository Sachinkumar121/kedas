@inject('controller', 'App\Http\Controllers\Controller')

@extends('layouts.admin.dashboard')

@section('title', trans('labels.activity_logs'))

@section('content')
<div class="dashboard-right cust-dashboard-md-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <h2>{{ __('labels.activity_logs') }}</h2>
        <div class="invoice-btns invoice-btns-position">
        </div>
        <div class="tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="list-group">
                        @forelse($activityLogs as $activityLog)
                            <div class="list-group-item list-group-item-action flex-column align-items-start">
                                <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">{{ __('labels.user_logged_in') }}</h5>
                                <small>{{ $activityLog->created_at->diffForHumans() }}</small>
                                </div>
                                <p class="mb-1">{{ $activityLog->description }}</p>
                                <small>{{ __('labels.details') }}:- {{ __('labels.latitude') }}: <b>{{ showIfAvailable($activityLog->getExtraProperty('data')['lat']) }}</b>, {{ __('labels.longitude') }}: <b>{{ showIfAvailable($activityLog->getExtraProperty('data')['lon']) }}</b></small>
                            </div>
                        @empty
                            <div class="list-group-item list-group-item-action list-group-item-light">{{ __('labels.no_records_found') }}</div>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection
