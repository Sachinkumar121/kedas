@extends('layouts.dashboard')
@section('title', trans('labels.show').' '.trans('labels.remission'))
@section('content')
<div class="dashboard-right">
    <div class="menu-icon">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="invoice-container">
        <a class="arrow-back d-block" href="{{ route('remissions.index') }}">{{ __('labels.back') }}</a>
        <h2>{{ __('labels.remission').' '.$remission->internal_number }}</h2>
        <div class="action-btns-view">
            <a href="{{ route('invoices.create', ['type' => $type, 'remission_id' => $remission->id ]) }}" class="btn-custom {{ $remission->invoice ? 'btn-disabled' : ''}}"><i class="fa fa-pencil"></i> {{ __('labels.convert_to_invoice') }}</a>
        </div>
        <div class="default-form view-form remission-show-view">
            <div class="generate-invoice">
                <div class="row adds-row">
                    <div class="col-lg-4">
                        <div class="form-group col-sm-12 file-input">
                            @if(Auth::user()->company->logo)
                            <img id="image-preview" src="{{ asset(Auth::user()->company->logo) }}" alt="{{ Auth::user()->name }}" />
                            @else
                            <img id="image-preview" src="{{ asset('images/john-sec.png') }}" alt="{{ Auth::user()->name }}" />
                            @endif 
                        </div>
                    </div>
                    <div class="col-lg-8 text-center company-data">
                        <div class="add-cell">
                                <div class="consumption-box">
                                    <div class="consumption-bottom">
                                        <span>{{ __('labels.no') }}.</span>
                                        <div class="consumption-number">{{ $remission->internal_number }}</div>
                                    </div>
                                </div>

                                <div class="add-cell-center">
                                   <h2>{{ Auth::user()->company->name }}</h2>
                                   @if(addressFormat(Auth::user()->company->address, 1)[0] ?? null)
                                    <span>
                                        <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[0] }}</p>
                                    </span>
                                    @endif
                                   @if(addressFormat(Auth::user()->company->address, 1)[1] ?? null)
                                    <span>
                                        <p class="address-block">{{ addressFormat(Auth::user()->company->address, 1)[1] }}</p>
                                    </span>
                                    @endif
                                    <span>
                                        <h3>{{ __('labels.phone') }}:</h3> <p>{{ Auth::user()->company->phone }}</p>
                                        @if(Auth::user()->company->mobile)
                                            <h3>{{ __('labels.mobile') }}:</h3> <p>{{ Auth::user()->company->mobile }}</p>
                                        @endif
                                    </span>
                                    @if(Auth::user()->company->support_email)
                                        <span>
                                            <h3>{{ __('labels.email') }}:</h3> 
                                            <p><a href="mailto:{{Auth::user()->company->support_email}}"> {{ Auth::user()->company->support_email }} </a></p>
                                        </span>
                                    @endif
                                    @if(Auth::user()->company->tax_id)
                                        <span>
                                            <h3>{{ __('labels.tax_id') }}:</h3> 
                                            <p><span>{{ Auth::user()->company->tax_id }} </span></p>
                                        </span>
                                    @endif
                                 </div>
                        </div>
                    </div>
                </div>
                <div class="view-form-custom">
                    <table class="table">
                        <tr>
                            <td><b>{{ __('labels.client') }}</b></td>
                            @if($remission->contact instanceof App\Supplier)
                            <td><a href="{{route('suppliers.show', $remission->contact)}}">{{ $remission->contact->name }}</a></td>
                            @else
                            <td><a href="{{route('clients.show', $remission->contact)}}">{{ $remission->contact->name }}</a></td>
                            @endif
                            <td><b>{{ __('labels.creation_date') }}</b></td>
                            <td>{{ dateFormat($remission->start_date) }}</td>
                        </tr>
                        <tr>
                            <td><b>{{ __('labels.tax_id') }}</b></td>
                            <td>{{ $remission->contact->tax_id }}</td>
                            <td><b>{{ __('labels.expiration_date') }}</b></td>
                            <td>{{ dateFormat($remission->expiration_date) }}</td>
                        </tr>
                        <tr>
                            <td><b>{{ __('labels.phone') }}</b></td>
                            <td>{{ $remission->contact->phone }}</td>
                        </tr>
                        @if($remission->currency)
                        <tr>
                            <td><b>{{ __('labels.currency') }}</b></td>
                            <td>{{ $remission->currency->code }}</td>
                        </tr>
                        @endif
                    </table>
                </div>
                <div class="terms-conditions-form">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label><b>{{ __('labels.notes') }}</b></label>
                            <div class="descrip-notes"><span>{{showIfAvailable($remission->notes)}}</span></div>
                        </div>
                    </div>
                </div>
                <div class="default-table add-invoice-table">
                    <div class="list-table-custom">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{ __('labels.item') }}</th>
                                    <th>{{ __('labels.reference') }}</th>
                                    <th>{{ __('labels.price') }}</th>
                                    <th>{{ __('labels.disc') }} %</th>
                                    <th>{{ __('labels.tax') }}</th>
                                    <th>{{ __('labels.tax_amount') }}</th>
                                    <th>{{ __('labels.description') }}</th>
                                    <th>{{ __('labels.quantity') }}</th>
                                    <th>{{ __('labels.total') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($remission->items as $item)
                                <tr>
                                    <td><a href="{{route('inventories.show', $item->inventory)}}">{{$item->inventory->name}}</a></td>
                                    <td>{{$item->reference}}</td>
                                    <td>{{moneyFormat($item->price,null,2,true)}}</td>
                                    <td>{{addZeros($item->discount)}}%</td>
                                    <td>{{$item->tax ? addZeros($item->tax->percentage).'%' : 'N/A'}}</td>
                                    <td>{{moneyFormat($item->calculation['taxAmount'],null,2,true)}}</td>
                                    <td>{{showIfAvailable($item->inventory->description)}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{moneyFormat($item->calculation['baseTotal'] - $item->calculation['discountAmount'],null,2,true)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group col-sm-12 file-input">
                            @if(Auth::user()->company->signature)
                            <img id="signature-preview" src="{{ asset(Auth::user()->company->signature) }}" alt="{{ Auth::user()->name }}" />
                            @else
                            {{-- <img id="signature-preview" alt="{{ Auth::user()->company->name }}" /> --}}
                            @endif 
                        </div>
                        <div class="elaborated">{{ __('labels.elaborated_by') }}</div>
                    </div>
                    <div class="col-sm-8 total-table">
                        <div class="total-table">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>{{ __('labels.base_total') }}</th>
                                        <td id="invoice_basetotal">{{moneyFormat($remission->calculation['baseTotal'],null,2,true)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.discount') }}</th>
                                        <td id="invoice_discount">{{moneyFormat($remission->calculation['discountAmount'],null,2,true)}}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('labels.sub_total') }}</th>
                                        <td id="invoice_subtotal">{{moneyFormat($remission->calculation['subtotal'],null,2,true)}}</td>
                                    </tr>
                                    <tr class="tax_row">
                                        <th>{{ __('labels.tax') }}</th>
                                        <td>{{moneyFormat($remission->calculation['taxAmount'],null,2,true)}}</td>
                                    </tr>
                                    {{-- @foreach($remission->calculation['taxInfo'] as $taxInfo)
                                    <tr class="tax_row">
                                        <th>{{$taxInfo['formatted_name']}}</th>
                                        <td>{{moneyFormat($taxInfo['tax'],null,2,true)}}</td>
                                    </tr>
                                    @endforeach --}}
                                    <tr>
                                        <th>{{ __('labels.total') }}</th>
                                        <td id="invoice_total">{{moneyFormat($remission->calculation['total'],null,2,true)}}</td>
                                    </tr>
                                    @if($remission->currency)
                                    <tr class="total_exchanged_rate">
                                        <th>{{ __('labels.total') }} <span class="total-ex-currency-label">{{ $remission->currency->code }}</span></th>
                                        <td>
                                            <span class="total-ex-currency-label-symbol">{{ $remission->currency->code }} {{ $remission->currency->symbol }}</span><span id="total-ex-currency-rate">{{ addZeros($remission->exchange_rate_total,6,true) }}</span>
                                        </td>
                                    </tr>
                                    <tr class="total_currency_exchange-tr">
                                        <th>
                                            <label class="total_currency_exchange_inner">1 
                                                <span class="exchange_currency_label">{{ $remission->currency->code }}</span> 
                                                = {{ Auth::user()->company->currency->code }} ({{ Auth::user()->company->currency->symbol }}) 
                                                <span class="exchange_currency_rate">{{ addZeros($remission->exchange_currency_rate,6, true) }}</span> 
                                            </label>
                                        </th>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
              
            </div>
        </div>
        
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
@endsection
