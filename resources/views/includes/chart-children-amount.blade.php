<div id="collapse-{{$accountingHead->id}}" class="collapse" data-parent="#{{$parent}}" aria-labelledby="heading-{{$accountingHead->id}}">
	<div class="wrap-level-{{$level}}">
		<div id="accordion-{{$accountingHead->id}}">
		@foreach($accountingHeads as $acc)
			@if($acc->children->isEmpty())
			<div class="wrap-inside-list-content">
				<span class="list-heading">{{ $acc->custom_name }}</span>
                <span class="list-amount">{{ moneyFormat($mapIdToAmount[$acc->id] ?? 0, null, 2, true) }}</span>
            </div>
			@else
    	    <div class="card">
                <div class="card-header" id="heading-{{$acc->id}}">
      
                        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse-{{$acc->id}}" aria-expanded="false" aria-controls="collapse-{{$acc->id}}">
                            <span class="list-heading">{{ $acc->custom_name }}</span>
                            <span class="list-amount">{{ moneyFormat($mapIdToAmount[$acc->id] ?? 0, null, 2, true) }}</span>
                        </a>
                  
                </div>
                @if($acc->hasChildren())
                    @include('includes.chart-children-amount', ['accountingHeads' => $acc->children, 'accountingHead' => $acc, 'level' => $level + 1, 'parent' => 'accordion-'.$acc->id])
                @endif
            </div>
			@endif
		@endforeach
		</div>
	</div>
</div>