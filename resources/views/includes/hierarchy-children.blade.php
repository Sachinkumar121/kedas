{{-- Not using at all --}}
@foreach($account_control_options as $account_control_option)
	<a class="dropdown-item" data-value="{{$account_control_option['value']}}" data-level="{{$account_control_option['level']}}" href="javascript::void(0)">{{$account_control_option['title']}}</a>
	@if($account_control_option['children'] ?? [])
		@include('includes.hierarchy-children', ['account_control_options' => $account_control_option['children']])
	@endif
@endforeach