{{-- Language selector drop down --}}

@inject('controller', 'App\Http\Controllers\Controller')

@auth
	@env(['development', 'production', 'staging'])
    	<li>{!! Form::select('locale_selector',  $controller::mapOptionsBasedOnLocale(Config::get('constants.languages')), old('locale_selector', Auth::user()->language ?: App::getLocale()), ['class'=>"", 'id'=>"locale_selector"]) !!}</li>
    @endenv
@endauth

@guest
	<li>{!! Form::select('locale_selector',  $controller::mapOptionsBasedOnLocale(Config::get('constants.languages')), old('locale_selector', App::getLocale()), ['class'=>"", 'id'=>"locale_selector"]) !!}</li>
@endguest