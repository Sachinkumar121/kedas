export const subTotalAmount = function (total, tax = {}, subTotal, cart) {
    total = 0;
    subTotal = 0;
    let taxInformation = {};
    let taxTotal = 0;
    let discountTotal = 0;

    cart.forEach(function (cartItem) {
        let calculatedPriceForSub = 0;
        let totalPrice = 0;
        let discount = 0;

        if (cartItem.quantity > 0) {
            calculatedPriceForSub = cartItem.productPrice * cartItem.quantity;
            totalPrice = cartItem.totalPrice * cartItem.quantity;

            if (cartItem.discount) {
                discount = calculatedPriceForSub * cartItem.discount / 100;
                discountTotal += discount
                totalPrice -= discount;
            }

            cartItem.calculatedPrice = totalPrice;

            if (taxInformation.hasOwnProperty(cartItem.taxID)) {
                taxInformation[cartItem.taxID]['amount'] = Number(taxInformation[cartItem.taxID]['amount']) + (Number(cartItem.taxAmount) * cartItem.quantity);
                taxInformation[cartItem.taxID]['name'] = cartItem.taxName;
            } else {
                if (cartItem.taxAmount > 0) {
                    taxInformation[cartItem.taxID] = {};
                    taxInformation[cartItem.taxID]['amount'] = Number(cartItem.taxAmount) * cartItem.quantity;
                    taxInformation[cartItem.taxID]['name'] = cartItem.taxName;
                }
            }
        }

        subTotal += calculatedPriceForSub;
        total += totalPrice;
    });

    for (let taxId in taxInformation) {
        taxTotal += taxInformation[taxId].amount;
    }

    return {
        subTotal,
        total,
        taxInformation,
        taxTotal,
        discountTotal
    }
}


/*
* setCartItemsToCookieOrDB
* */

export const cartItemsToCookie = function (flag = 0, object, appVersion) {
    let cookieName = appVersion + "-user-" + object.user.id + "-sales-cart",
        cookieObject = {
            'cart': object.cart,
            'customer': object.selectedCustomer,
            'discount': object.discount,
            'orderID': object.orderID
        };
    if (!window.$cookies.isKey(cookieName)) {
        window.$cookies.set(cookieName, cookieObject, "4m");
    } else {
        if (flag == 0) {
            let cookieValue = window.$cookies.get(cookieName);
            let cookieCart = cookieValue.cart;

            cookieCart.forEach(function (cookieCartItem, index, array) {
                if (cookieCartItem.showItemCollapse) {
                    array[index].showItemCollapse = false;
                }
            }),
                object.cart = cookieCart;
            object.selectedCustomer = cookieValue.customer;
            object.orderID = cookieValue.orderID;
            object.discount = cookieValue.discount;
            object.newDiscount = cookieValue.discount;
            if (object.selectedCustomer.length == 1) {
                object.customerNotAdded = false;
            }
        } else {
            window.$cookies.set(cookieName, cookieObject, "4m");
        }
    }
    return {
        selectedCustomer: object.selectedCustomer,
        orderID: object.orderID,
        customerNotAdded: object.customerNotAdded,
        discount: object.discount,
        cart: object.cart
    }

}


/*
* Delete Cart From cookie
* */
export const deleteCartItemsFromCookieOrDB = function (user, order_type, appVersion) {
    window.$cookies.remove(appVersion + "-user-" + user.id + "-sales-cart");
}
