<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'manage sales',
                'custom_name' => '{"en":"manage sales", "es":"administrar ventas"}',
                'guard_name' => 'web',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'manage expenses',
                'custom_name' => '{"en":"manage expenses", "es":"administrar gastos"}',
                'guard_name' => 'web',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'manage inventories',
                'custom_name' => '{"en":"manage inventories", "es":"administrar inventarios"}',
                'guard_name' => 'web',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'manage contacts',
                'custom_name' => '{"en":"manage contacts", "es":"gestionar contactos"}',
                'guard_name' => 'web',
                'created_at' => Carbon::now(), 
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
