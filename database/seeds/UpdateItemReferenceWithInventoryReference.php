<?php

use Illuminate\Database\Seeder;

class UpdateItemReferenceWithInventoryReference extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->join('inventories', 'items.inventory_id', '=', 'inventories.id')->update(['items.reference' => DB::raw('inventories.	reference')]);
    }
}
