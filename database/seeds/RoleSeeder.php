<?php

use Illuminate\Database\Seeder;
use App\Company;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = Company::all();
        foreach ($companies as $company) {
        	$company->roles()->createMany([
                [
                    'name' => 'admin',
                    'custom_name' => ["en" => "admin", "es" => "administración"],
                    'company_id' => $company->id
                ],
                [
                    'name' => 'manager',
                    'custom_name' => ["en" => "manager", "es" => "gerente"],
                    'company_id' => $company->id
                ],
                [
                    'name' => 'employee',
                    'custom_name' => ["en" => "employee", "es" => "empleada"],
                    'company_id' => $company->id
                ]
            ]);
        }
    }
}
