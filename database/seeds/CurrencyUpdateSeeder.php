<?php

use Illuminate\Database\Seeder;

class CurrencyUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->where('code', 'dop')->update([
        	'symbol' => 'DOP$'
        ]);
    }
}
