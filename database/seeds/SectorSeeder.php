<?php

use Illuminate\Database\Seeder;

class SectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sectors')->insert([
			['name' => '{"en":"Agriculture / Livestock", "es":"Agricultura / Ganadería"}'],
			['name' => '{"en":"Food / Gastronomy", "es":"Comida / Gastronomía"}'],
			['name' => '{"en":"Architecture / Construction", "es":"Arquitectura / Construcción"}'],
			['name' => '{"en":"Arts / Crafts", "es":"Artes / manualidades"}'],
			['name' => '{"en":"Motors and parts", "es":"Motores y piezas"}'],
			['name' => '{"en":"Retail", "es":"Minorista"}'],
			['name' => '{"en":"E-commerce", "es":"Comercio electrónico"}'],
			['name' => '{"en":"Consulting", "es":"Consultoría"}'],
			['name' => '{"en":"Personal care / Aesthetic / Health", "es":"Cuidado personal / Estética / Salud"}'],
			['name' => '{"en":"Education", "es":"Educación"}'],
			['name' => '{"en":"Entertainment", "es":"Entretenimiento"}'],
			['name' => '{"en":"Hotel / Tourism", "es":"Hotel / Turismo"}'],
			['name' => '{"en":"Manufacturing", "es":"Fabricación"}'],
			['name' => '{"en":"Non-governmental organization", "es":"Organización no gubernamental"}'],
			['name' => '{"en":"Advertising / Digital media", "es":"Publicidad / Medios digitales"}'],
			['name' => '{"en":"Accounting or financial services", "es":"Servicios contables o financieros"}'],
			['name' => '{"en":"Specialized services", "es":"Servicios especializados"}'],
			['name' => '{"en":"Technology / Telecommunication", "es":"Tecnología / Telecomunicaciones"}'],
			['name' => '{"en":"Transport / Logistics", "es":"Transporte / Logística"}'],
			['name' => '{"en":"Others", "es":"Otros"}'],
        ]);
    }
}
