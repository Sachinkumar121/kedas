<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class IdentificationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('identification_types')->insert([
            ['name' => '{"en":"NIT - Tax identification number", "es":"NIT - Número de identificación fiscal"}'],
            ['name' => '{"en":"CC - Citizenship card", "es":"CC - Tarjeta de ciudadanía"}'],
            ['name' => '{"en":"DIE - Foreign Identification Document", "es":"DIE - Documento de identidad extranjero"}'],
            ['name' => '{"en":"PP - Passport", "es":"PP - Pasaporte"}'],
            ['name' => '{"en":"CE - Aliens ID", "es":"CE - ID de extranjero"}'],
            ['name' => '{"en":"TE - Immigration card", "es":"TE - Tarjeta de inmigración"}'],
            ['name' => '{"en":"IT - Identity card", "es":"IT - Tarjeta de identidad"}'],
            ['name' => '{"en":"RC - Civil Registry", "es":"RC - Registro civil"}'],
            ['name' => '{"en":"NIT from another country - NIT from another country", "es":"NIT de otro país - NIT de otro país"}'],
            ['name' => '{"en":"NUIP - NUIP", "es":"NUIP - NUIP"}'],
        ]);
    }
}
