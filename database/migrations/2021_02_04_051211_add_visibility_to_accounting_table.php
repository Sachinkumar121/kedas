<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVisibilityToAccountingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_heads', function (Blueprint $table) {
            $table->boolean('visibility')->default(false)->after('code');
            $table->boolean('can_be_parent')->default(true)->after('visibility');
        });

        Schema::table('user_accounting_heads', function (Blueprint $table) {
            $table->boolean('visibility')->default(false)->after('code');
            $table->boolean('can_be_parent')->default(true)->after('visibility');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_heads', function (Blueprint $table) {
            $table->dropColumn('visibility');
            $table->dropColumn('can_be_parent');
        });

        Schema::table('user_accounting_heads', function (Blueprint $table) {
            $table->dropColumn('visibility');
            $table->dropColumn('can_be_parent');
        });
    }
}
