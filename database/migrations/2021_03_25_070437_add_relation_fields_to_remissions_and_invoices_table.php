<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationFieldsToRemissionsAndInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('remissions', function (Blueprint $table) {
            $table->foreignId('invoice_id')->nullable()->after('internal_number');
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('invoices', function (Blueprint $table) {
            $table->foreignId('remission_id')->nullable()->after('recurring_invoice_id');
            $table->foreign('remission_id')->references('id')->on('remissions')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('remissions', function (Blueprint $table) {
            $table->dropForeign(['invoice_id']);
            $table->dropColumn('invoice_id');
        });
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropForeign(['remission_id']);
            $table->dropColumn('remission_id');
        });
    }
}
