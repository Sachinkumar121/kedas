<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeExtraHoursEnumToExtraHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('extra_hours', function (Blueprint $table) {

        });
        DB::statement("ALTER TABLE `extra_hours` CHANGE `incomes` `incomes` ENUM('accounts_receivable') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        DB::statement("ALTER TABLE `discounts` CHANGE `type` `type` ENUM('cash_advance','damages','uniform','cashier_missing','misc','others') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('extra_hours', function (Blueprint $table) {
            
        });
        DB::statement("ALTER TABLE `extra_hours` CHANGE `incomes` `incomes` ENUM('payroll_expenses') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        DB::statement("ALTER TABLE `discounts` CHANGE `type` `type` ENUM('cash, 'advance','damages','uniform','cashier_missing','misc','others') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
    }
}
