<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCascadeOnUpdateForForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('model_has_permissions', function (Blueprint $table) {

            $table->dropForeign(['permission_id']);
            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('model_has_roles', function (Blueprint $table) {

            $table->dropForeign(['role_id']);
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('role_has_permissions', function (Blueprint $table) {

            $table->dropForeign(['permission_id']);
            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['role_id']);
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('addresses', function (Blueprint $table) {

            $table->dropForeign(['country_id']);
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade')->onUpdate('cascade');

        });   


        Schema::table('suppliers', function (Blueprint $table) {

            /*$table->dropForeign('suppliers_sector_id_foreign');
            $table->foreign('sector_id')->references('id')->on('sectors')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign('suppliers_identification_type_id_foreign');
            $table->foreign('identification_type_id')->references('id')->on('identification_types')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign('suppliers_company_id_foreign');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign('suppliers_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');*/

        });

        Schema::table('clients', function (Blueprint $table) {

            /*$table->dropForeign('clients_sector_id_foreign');
            $table->foreign('sector_id')->references('id')->on('sectors')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign('clients_identification_type_id_foreign');
            $table->foreign('identification_type_id')->references('id')->on('identification_types')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign('clients_company_id_foreign');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign('clients_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');*/

        });

        Schema::table('taxes', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('item_categories', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('inventories', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['tax_id']);
            $table->foreign('tax_id')->references('id')->on('taxes')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['item_category_id']);
            $table->foreign('item_category_id')->references('id')->on('item_categories')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('invoices', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('items', function (Blueprint $table) {

            $table->dropForeign(['invoice_id']);
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['tax_id']);
            $table->foreign('tax_id')->references('id')->on('taxes')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['inventory_id']);
            $table->foreign('inventory_id')->references('id')->on('inventories')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('transactions', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('invoice_transaction', function (Blueprint $table) {

            $table->dropForeign(['invoice_id']);
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['transaction_id']);
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('credit_notes', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('estimates', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('inventory_adjustments', function (Blueprint $table) {

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('concepts', function (Blueprint $table) {

            $table->dropForeign(['parent_id']);
            $table->foreign('parent_id')->references('id')->on('concepts')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('debit_notes', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('purchase_orders', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('concept_items', function (Blueprint $table) {

            $table->dropForeign(['invoice_id']);
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['transaction_id']);
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['tax_id']);
            $table->foreign('tax_id')->references('id')->on('taxes')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['debit_note_id']);
            $table->foreign('debit_note_id')->references('id')->on('debit_notes')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['purchase_order_id']);
            $table->foreign('purchase_order_id')->references('id')->on('purchase_orders')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['concept_id']);
            $table->foreign('concept_id')->references('id')->on('concepts')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('credit_note_invoice', function (Blueprint $table) {

            $table->dropForeign(['invoice_id']);
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign(['credit_note_id']);
            $table->foreign('credit_note_id')->references('id')->on('credit_notes')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('users', function (Blueprint $table) {

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('roles', function (Blueprint $table) {

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

        });

       Schema::table('companies', function (Blueprint $table) {

            $table->dropForeign('user_profile_sector_id_foreign');
            $table->foreign('sector_id')->references('id')->on('sectors')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign('user_profile_currency_id_foreign');
            $table->foreign('currency_id')->references('id')->on('currencies')->onDelete('cascade')->onUpdate('cascade');

            $table->dropForeign('user_profile_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('model_has_permissions', function (Blueprint $table) {

            $table->dropForeign(['permission_id']);
            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');

        });

        Schema::table('model_has_roles', function (Blueprint $table) {

            $table->dropForeign(['role_id']);
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');

        });

        Schema::table('role_has_permissions', function (Blueprint $table) {

            $table->dropForeign(['permission_id']);
            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');

            $table->dropForeign(['role_id']);
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');

        });

        Schema::table('addresses', function (Blueprint $table) {

            $table->dropForeign(['country_id']);
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');

        });

        Schema::table('suppliers', function (Blueprint $table) {

            $table->foreign('sector_id')->references('id')->on('sectors')->onDelete('cascade');

            $table->foreign('identification_type_id')->references('id')->on('identification_types')->onDelete('cascade');

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });

        Schema::table('clients', function (Blueprint $table) {

            $table->foreign('sector_id')->references('id')->on('sectors')->onDelete('cascade');

            $table->foreign('identification_type_id')->references('id')->on('identification_types')->onDelete('cascade');

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });


        Schema::table('taxes', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

        });

        Schema::table('item_categories', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

        });

        Schema::table('inventories', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->dropForeign(['tax_id']);
            $table->foreign('tax_id')->references('id')->on('taxes')->onDelete('cascade');

            $table->dropForeign(['item_category_id']);
            $table->foreign('item_category_id')->references('id')->on('item_categories')->onDelete('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

        });

        Schema::table('invoices', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

        });

        Schema::table('items', function (Blueprint $table) {

            $table->dropForeign(['invoice_id']);
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');

            $table->dropForeign(['tax_id']);
            $table->foreign('tax_id')->references('id')->on('taxes')->onDelete('cascade');

            $table->dropForeign(['inventory_id']);
            $table->foreign('inventory_id')->references('id')->on('inventories')->onDelete('cascade');

        });

        Schema::table('transactions', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

        });

        Schema::table('invoice_transaction', function (Blueprint $table) {

            $table->dropForeign(['invoice_id']);
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');

            $table->dropForeign(['transaction_id']);
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');

        });

        Schema::table('credit_notes', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

        });

        Schema::table('estimates', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

        });

        Schema::table('inventory_adjustments', function (Blueprint $table) {

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

        });

        Schema::table('concepts', function (Blueprint $table) {

            $table->dropForeign(['parent_id']);
            $table->foreign('parent_id')->references('id')->on('concepts')->onDelete('cascade');

        });

        Schema::table('debit_notes', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

        });

        Schema::table('purchase_orders', function (Blueprint $table) {

            $table->dropForeign(['user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

        });

        Schema::table('concept_items', function (Blueprint $table) {

            $table->dropForeign(['invoice_id']);
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');

            $table->dropForeign(['transaction_id']);
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');

            $table->dropForeign(['tax_id']);
            $table->foreign('tax_id')->references('id')->on('taxes')->onDelete('cascade');

            $table->dropForeign(['debit_note_id']);
            $table->foreign('debit_note_id')->references('id')->on('debit_notes')->onDelete('cascade');

            $table->dropForeign(['purchase_order_id']);
            $table->foreign('purchase_order_id')->references('id')->on('purchase_orders')->onDelete('cascade');

            $table->dropForeign(['concept_id']);
            $table->foreign('concept_id')->references('id')->on('concepts')->onDelete('cascade');

        });

        Schema::table('credit_note_invoice', function (Blueprint $table) {

            $table->dropForeign(['invoice_id']);
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');

            $table->dropForeign(['credit_note_id']);
            $table->foreign('credit_note_id')->references('id')->on('credit_notes')->onDelete('cascade');

        });

        Schema::table('users', function (Blueprint $table) {

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

        });

        Schema::table('roles', function (Blueprint $table) {

            $table->dropForeign(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

        });

        Schema::table('companies', function (Blueprint $table) {

            $table->dropForeign(['sector_id']);
            $table->foreign('sector_id', 'user_profile_sector_id_foreign')->references('id')->on('sectors')->onDelete('cascade');

            $table->dropForeign(['currency_id']);
            $table->foreign('currency_id', 'user_profile_currency_id_foreign')->references('id')->on('currencies')->onDelete('cascade');

            $table->dropForeign(['user_id']);
            $table->foreign('user_id', 'user_profile_user_id_foreign')->references('id')->on('users')->onDelete('cascade');

        });
    }
}
