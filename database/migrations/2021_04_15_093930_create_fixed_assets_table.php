<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFixedAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixed_assets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('company_id');

            $table->string('name');
            $table->string('serial_number')->nullable();
            $table->decimal('price', 15, 2);
            $table->foreignId('item_category_id')->nullable();
            $table->text('description')->nullable();
            $table->foreignId('account')->nullable();
            $table->foreignId('expense_account')->nullable();

            $table->enum('cal_type', ['straight_line', 'sum_of_digits', 'units_produced']);
            $table->string('image')->nullable();

            $table->enum('legal_category', ['cat_I', 'cat_II', 'cat_III'])->nullable();
            $table->float('percentage', 5, 2)->nullable();
            $table->enum('location', ['main_office'])->nullable();
            $table->decimal('book_value', 15, 2)->nullable();
            $table->date('date_of_purchase')->nullable();
            $table->date('final_date')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('item_category_id')->references('id')->on('item_categories')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('expense_account')->references('id')->on('user_accounting_heads')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixed_assets');
    }
}
