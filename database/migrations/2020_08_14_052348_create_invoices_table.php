<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->integer('contact_id');
            $table->string('contact_type');
            $table->foreignId('user_id');
            $table->unsignedBigInteger('internal_number');
            
            $table->enum('type', ['sale', 'supplier', 'recurring'])->default('sale');
            $table->enum('term', ['0', '8', '15', '30', '60', 'manual'])->nullable();
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->date('expiration_date');
            $table->unsignedSmallInteger('frequency')->nullable();
            $table->text('tac')->nullable();
            $table->text('notes')->nullable();
            $table->text('res_text')->nullable();
            $table->text('observation')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
