<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeRecurringFieldsToBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('benefits', function (Blueprint $table) {
            $table->dropColumn('is_recurring');
            $table->dropColumn('is_on_time');
            $table->enum('recurring_type', ['recurring','on_time'])->default('recurring')->after('frequency');
        });

        Schema::table('with_holdings', function (Blueprint $table) {
            $table->dropColumn('is_recurring');
            $table->dropColumn('is_on_time');
            $table->enum('recurring_type', ['recurring','on_time'])->default('recurring')->after('frequency');
        });

        Schema::table('discounts', function (Blueprint $table) {
            $table->dropColumn('is_recurring');
            $table->dropColumn('is_on_time');
            $table->enum('recurring_type', ['recurring','on_time'])->default('recurring')->after('frequency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('benefits', function (Blueprint $table) {
            $table->boolean('is_recurring')->default('0');
            $table->boolean('is_on_time')->default('0');
            $table->dropColumn('recurring_type');
        });

         Schema::table('with_holdings', function (Blueprint $table) {
            $table->boolean('is_recurring')->default('0');
            $table->boolean('is_on_time')->default('0');
            $table->dropColumn('recurring_type');
        });

         Schema::table('discounts', function (Blueprint $table) {
            $table->boolean('is_recurring')->default('0');
            $table->boolean('is_on_time')->default('0');
            $table->dropColumn('recurring_type');
        });
    }
}
