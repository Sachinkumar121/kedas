<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCurrencyExchangeToInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->foreignId('exchange_currency_id')->nullable()->after('observation');
            $table->decimal('exchange_currency_rate', 15, 2)->nullable()->after('exchange_currency_id');
            $table->foreign('exchange_currency_id')->references('id')->on('currencies')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropForeign(['exchange_currency_id']);
            
            $table->dropColumn('exchange_currency_id');
            $table->dropColumn('exchange_currency_rate');
        });
    }
}
