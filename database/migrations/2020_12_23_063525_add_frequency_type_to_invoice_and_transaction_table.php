<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFrequencyTypeToInvoiceAndTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->enum('frequency_type', ['month', 'day'])->nullable()->after('frequency');
        });
        Schema::table('transactions', function (Blueprint $table) {
            $table->enum('frequency_type', ['month', 'day'])->nullable()->after('frequency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn('frequency_type');
        });
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('frequency_type');
        });
    }
}
