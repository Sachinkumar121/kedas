<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_applications', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('company_id');
            $table->foreignId('employee_id')->nullable();
            $table->foreignId('leave_type_id');
            $table->date('application_from_date');
            $table->date('application_to_date');
            $table->unsignedSmallInteger('number_of_days');
            $table->date('approve_date')->nullable();
            $table->date('reject_date')->nullable();
            $table->foreignId('approve_by')->nullable();
            $table->foreignId('reject_by')->nullable();
            $table->text('purpose');
            $table->text('remarks')->nullable();
            $table->string('status')->default('1')->comment = "status(1,2,3) = Pending,Approve,Reject";

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('leave_type_id')->references('id')->on('leave_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('approve_by')->references('id')->on('employees')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('reject_by')->references('id')->on('employees')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_applications');
    }
}
