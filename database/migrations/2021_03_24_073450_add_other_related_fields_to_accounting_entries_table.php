<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOtherRelatedFieldsToAccountingEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_entries', function (Blueprint $table) {

            $table->foreignId('credit_note_id')->nullable()->after('transaction_id');
            $table->foreignId('estimate_id')->nullable()->after('credit_note_id');
            $table->foreignId('debit_note_id')->nullable()->after('estimate_id');
            $table->foreignId('purchase_order_id')->nullable()->after('debit_note_id');
            $table->foreignId('inventory_adjustment_id')->nullable()->after('purchase_order_id');
            $table->foreignId('remission_id')->nullable()->after('inventory_adjustment_id');

            $table->foreign('credit_note_id')->references('id')->on('credit_notes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('estimate_id')->references('id')->on('estimates')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('debit_note_id')->references('id')->on('debit_notes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('purchase_order_id')->references('id')->on('purchase_orders')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('inventory_adjustment_id')->references('id')->on('inventory_adjustments')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('remission_id')->references('id')->on('remissions')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_entries', function (Blueprint $table) {
            $table->dropForeign(['credit_note_id']);
            $table->dropForeign(['estimate_id']);
            $table->dropForeign(['debit_note_id']);
            $table->dropForeign(['purchase_order_id']);
            $table->dropForeign(['inventory_adjustment_id']);
            $table->dropForeign(['remission_id']);
            
            $table->dropColumn('credit_note_id');
            $table->dropColumn('estimate_id');
            $table->dropColumn('debit_note_id');
            $table->dropColumn('purchase_order_id');
            $table->dropColumn('inventory_adjustment_id');
            $table->dropColumn('remission_id');
        });
    }
}
