<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFieldsInFixedAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // un-comment after move to prod.
        Schema::table('fixed_assets', function (Blueprint $table) {
            $table->float('years', 5, 2)->after('percentage')->nullable();
            $table->float('scrap_percentage', 5, 2)->after('percentage')->nullable();

        });

        Schema::table('users', function (Blueprint $table) {
            $table->boolean('is_company_emp')->default('0')->after('employee_id');
        });

        Schema::table('employees', function (Blueprint $table) {
            // database statement to make the fields null in employee table.
            DB::statement("ALTER TABLE `employees` CHANGE 
            `start_time` `start_time` TIME NULL");

            DB::statement("ALTER TABLE `employees` CHANGE 
            `end_time` `end_time` TIME NULL");

            DB::statement("ALTER TABLE `employees` CHANGE `id_number` `id_number` VARCHAR(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

            DB::statement("ALTER TABLE `employees` CHANGE `type_of_id` `type_of_id` VARCHAR(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

            DB::statement("ALTER TABLE `employees` CHANGE `hiring_date` `hiring_date` DATE NULL");

            DB::statement("ALTER TABLE `employees` CHANGE `role` `role` VARCHAR(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

            DB::statement("ALTER TABLE `employees` CHANGE `birth_date` `birth_date` DATE NULL");

            DB::statement("ALTER TABLE `employees` CHANGE `status` `status` ENUM('active','inactive','vacation') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");

            DB::statement("ALTER TABLE `employees` CHANGE `department_id` `department_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL");

            $table->boolean('is_company_emp')->default('1')->after('related_user_id');
        });

        Schema::table('fixed_assets', function (Blueprint $table) {
            $table->unsignedMediumInteger('total_unit_produced')->nullable()->after('cal_type');
            $table->unsignedMediumInteger('unit_produced_per_year')->nullable()->after('total_unit_produced');
            $table->boolean('same_for_year')->nullable()->after('unit_produced_per_year');
            $table->text('per_year_prod')->nullable()->after('same_for_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fixed_assets', function (Blueprint $table) {
            $table->dropColumn('years');
            $table->dropColumn('scrap_percentage');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('is_company_emp');
        });

        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn('is_company_emp');
        });

        Schema::table('fixed_assets', function (Blueprint $table) {
            $table->dropColumn('total_unit_produced');
            $table->dropColumn('unit_produced_per_year');
            $table->dropColumn('same_for_year');
            $table->dropColumn('per_year_prod');
        });
    }
}
