<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCurrencyExchangeRateToCreditNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credit_notes', function (Blueprint $table) {
            $table->decimal('exchange_currency_rate', 15, 6)->change();
        });

        Schema::table('estimates', function (Blueprint $table) {
            $table->decimal('exchange_currency_rate', 15, 6)->change();
        });

        Schema::table('debit_notes', function (Blueprint $table) {
            $table->decimal('exchange_currency_rate', 15, 6)->change();
        });

        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->decimal('exchange_currency_rate', 15, 6)->change();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->decimal('exchange_currency_rate', 15, 6)->change();
        });

        Schema::table('user_currency_rates', function (Blueprint $table) {
            $table->decimal('rate', 15, 6)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credit_notes', function (Blueprint $table) {
            $table->decimal('exchange_currency_rate', 15, 2)->change();
        });

        Schema::table('estimates', function (Blueprint $table) {
            $table->decimal('exchange_currency_rate', 15, 2)->change();
        });

        Schema::table('debit_notes', function (Blueprint $table) {
            $table->decimal('exchange_currency_rate', 15, 2)->change();
        });

        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->decimal('exchange_currency_rate', 15, 2)->change();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->decimal('exchange_currency_rate', 15, 2)->change();
        });

        Schema::table('user_currency_rates', function (Blueprint $table) {
            $table->decimal('rate', 15, 2)->change();
        });
    }
}
