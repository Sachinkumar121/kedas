<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            Schema::table('employees', function (Blueprint $table) {
                $table->enum('period', ['weekly', 'monthly', 'daily', 'biweekly', 'yearly', 'custom'])->after('birth_date');
                $table->enum('contract_type', ['permanent', 'daily'])->after('period');
                $table->unsignedBigInteger('bank_account')->after('contract_type')->nullable();
                $table->string('bank_name')->after('bank_account')->nullable();
                $table->enum('type_of_account', ['checking', 'saving'])->after('bank_name');
                $table->text('note')->nullable()->after('type_of_account')->nullable();
                $table->string('role')->after('hiring_date')->nullable();
                $table->decimal('hours_price', 15, 2)->after('insurance_brand_id')->nullable();
                $table->enum('salary_type', ['hourly', 'fixed'])->after('contract_type');
                $table->string('driver_license')->after('insurance_brand_id')->nullable();
                $table->string('blood_type')->after('driver_license')->nullable();
                $table->string('break_time')->after('blood_type');
                $table->string('image')->nullable()->after('break_time')->nullable();
                $table->boolean('monday')->default('0')->after('image');
                $table->boolean('tuesday')->default('0')->after('monday');
                $table->boolean('wednesday')->default('0')->after('tuesday');
                $table->boolean('thursday')->default('0')->after('wednesday');
                $table->boolean('friday')->default('0')->after('thursday');
                $table->boolean('saturday')->default('0')->after('friday');
                $table->boolean('sunday')->default('0')->after('saturday');
                $table->dropColumn('insurance_cost');
                $table->dropColumn('insurance_discount');
                $table->dropColumn('final_date');
                $table->dropColumn('status');
                $table->dropColumn('afp');
                $table->dropForeign(['designation_id']);
                $table->dropColumn('designation_id');
                $table->dropForeign(['supervisor_id']);
                $table->dropColumn('supervisor_id');
                $table->time('start_time')->after('image');
                $table->time('end_time')->after('image');

            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn('period');
            $table->dropColumn('contract_type');
            $table->dropColumn('bank_account');
            $table->dropColumn('bank_name');
            $table->dropColumn('type_of_account');
            $table->dropColumn('note');
            $table->dropColumn('hours_price');
            $table->dropColumn('salary_type');
            $table->dropColumn('driver_license');
            $table->dropColumn('blood_type');
            $table->dropColumn('break_time');
            $table->dropColumn('image');
            $table->dropColumn('monday');
            $table->dropColumn('tuesday');
            $table->dropColumn('wednesday');
            $table->dropColumn('thursday');
            $table->dropColumn('friday');
            $table->dropColumn('saturday');
            $table->dropColumn('sunday');
            $table->decimal('insurance_cost', 15, 2)->nullable();
            $table->float('insurance_discount', 5, 2)->nullable();
            $table->date('final_date');
            $table->enum('status', ['working', 'not_working']);
            $table->decimal('afp', 15, 2)->nullable();
            $table->foreignId('designation_id');
            $table->foreignId('supervisor_id')->nullable();
            $table->foreign('designation_id')->references('id')->on('designations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('supervisor_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->dropColumn('start_time');
            $table->dropColumn('end_time');
        });
    }
}
