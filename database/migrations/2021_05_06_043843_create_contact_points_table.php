<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_points', function (Blueprint $table) {
            $table->id();
            $table->integer('contact_id');
            $table->string('contact_type');
            $table->foreignId('membership_id')->nullable();
            $table->foreignId('invoice_id')->nullable();

            $table->enum('type', ['membership'])->default('membership')->nullable();
            $table->float('point_percentage', 5, 2);
            $table->decimal('points', 15, 2);

            $table->foreign('membership_id')->references('id')->on('memberships')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('invoice_id')->references('id')->on('invoices')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_points');
    }
}
