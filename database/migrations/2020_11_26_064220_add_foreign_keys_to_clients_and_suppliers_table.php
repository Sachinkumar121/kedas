<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToClientsAndSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (App::environment('local')) {
            Schema::table('suppliers', function (Blueprint $table) {
                $table->foreign('sector_id')->references('id')->on('sectors')->onDelete('cascade')->onUpdate('cascade');

                $table->foreign('identification_type_id')->references('id')->on('identification_types')->onDelete('cascade')->onUpdate('cascade');

                $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            });

            Schema::table('clients', function (Blueprint $table) {
                $table->foreign('sector_id')->references('id')->on('sectors')->onDelete('cascade')->onUpdate('cascade');

                $table->foreign('identification_type_id')->references('id')->on('identification_types')->onDelete('cascade')->onUpdate('cascade');

                $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (App::environment('local')) {
            Schema::table('suppliers', function (Blueprint $table) {
                $table->dropForeign('suppliers_sector_id_foreign');
                $table->dropForeign('suppliers_identification_type_id_foreign');
                $table->dropForeign('suppliers_company_id_foreign');
                $table->dropForeign('suppliers_user_id_foreign');
            });

            Schema::table('clients', function (Blueprint $table) {
                $table->dropForeign('clients_sector_id_foreign');
                $table->dropForeign('clients_identification_type_id_foreign');
                $table->dropForeign('clients_company_id_foreign');
                $table->dropForeign('clients_user_id_foreign');
            });
        }
    }
}
