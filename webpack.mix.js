const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/inventory-adjustments.js', 'public/js')

// build
/*mix.js([
    'public/js/helpers.js',
    'public/js/calculation.js',
    'public/js/custom.js',
    'public/js/common-script.js',
    'public/js/sidebar.js',
    'public/js/datepickers.js',
    'public/js/validation.js',
    'public/js/data-tables.js',
    'public/js/charts.js',
    'public/js/hierarchy-select.min.js',
], 'public/js/app-build.min.js').version();*/
