<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

use App\InventoryAdjustment;

class InventoryAdjustmentCreatedOrUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    /**
     * The name of the connection the job should be sent to.
     *
     * @var string|null
     */
    public $connection = 'sync';

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(InventoryAdjustment $inventoryAdjustment)
    {
        $this->inventoryAdjustment = $inventoryAdjustment;
        $this->type = 'inventory-adjustment';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
