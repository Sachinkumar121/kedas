<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExtraHour extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'extra_hours';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['total_extra_hours', 'extra_hours_total'];


    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        /* static::deleted(function($tax) {
            $tax->inventories()->each(function($inventory) {
                $inventory->tax()->dissociate();
                $inventory->save();
            });
        }); */
    }

    /**
     * Get the company associated with the tax.
     */
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public function getTotalExtraHoursAttribute() {
       	$time1 = strtotime($this->attributes['start_time']);
		$time2 = strtotime($this->attributes['end_time']);
		$difference = round(abs($time2 - $time1) / 3600,2);
		return $difference;
    }

    public function getExtraHoursTotalAttribute() {
       return $this->getTotalExtraHoursAttribute() * $this->attributes['hours_price'];
    }

   /*  public function getTypeAttribute()
    {
        return ucwords(str_replace('_', ' ', $this->attributes['type'])); 
    }

    public function getFrequencyTypeAttribute()
    {
        return ucwords(str_replace('_', ' ', $this->attributes['frequency_type'])); 
    }

    public function getExpensesAttribute()
    {
        return ucwords(str_replace('_', ' ', $this->attributes['expenses'])); 
    } */

    /**
     * Get the company information associated with the inventory.
     */
    public function incomeAccount() {
        return $this->belongsTo('App\UserAccountingHead', 'income_account');
    }

    /**
     * Get the company information associated with the inventory.
     */
    public function typeAccount() {
        return $this->belongsTo('App\UserAccountingHead', 'type_account');
    }
}
