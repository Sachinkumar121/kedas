<?php

namespace App\Exports;

use Auth;
use Cache;
use Config;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class InventoriesExport implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting
{
	public function __construct()
    {
        $this->identification_types = Cache::get('identification_types');
        $this->countries = Cache::get('countries');
        $this->sectors = Cache::get('sectors');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	$user = Auth::user();
        return $user->company->inventories;
    }

    public function headings(): array
    {
        return [
            'Name',
            'Sale Price',
            'Barcode',
            'Tax',
            'Type of item',
            'Reference',
            'Item category',
            'Account',
            'Description',
            'Date'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'B' => '0.00',
            'J' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    /**
    * @var Inventory $inventory
    */
    public function map($inventory): array
    {
        return [            
            $inventory->name,
            $inventory->sale_price,
            $inventory->barcode,
            $inventory->tax_id ? Auth::user()->taxes->pluck('id', 'name')->search($inventory->tax_id) : null,
            $inventory->type_of_item ? slugToStr($inventory->type_of_item) : null,
            $inventory->reference ?: '',
            $inventory->item_category_id ? Auth::user()->itemCategories->pluck('id', 'name')->search($inventory->item_category_id) : null,
            $inventory->account ? Config::get('constants.accounts')[$inventory->account] : null,
            $inventory->description,
            Date::dateTimeToExcel($inventory->created_at)
        ];
    }
}
