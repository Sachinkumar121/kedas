<?php

namespace App\Exports;

use Auth;
use Config;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ItemCategoriesExport implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting
{
	public function __construct()
    {

    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	$user = Auth::user();
        return $user->company->itemCategories;
    }

    public function headings(): array
    {
        return [
            'Name',
            'Description',
            'Date'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    /**
    * @var ItemCategory $itemCategory
    */
    public function map($itemCategory): array
    {
        return [            
            $itemCategory->name,
            $itemCategory->description,
            Date::dateTimeToExcel($itemCategory->created_at)
        ];
    }
}
