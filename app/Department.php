<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Department extends Model
{
    use SoftDeletes;
    use HasTranslations;
    public $translatable = ['custom_name'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'departments';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::deleted(function($department) {
            $department->employees()->each(function($employee) {
                $employee->department()->dissociate();
                $employee->save();
            });
        });
    }

    /**
     * Get the employees associated with the department.
     */
    public function employees()
    {
        return $this->hasMany('App\Employee');
    }
}
