<?php
namespace App\Repository;

use App\Ncf;
use App\Company;
use Illuminate\Support\Collection;

interface NcfRepositoryInterface
{
   	public function all(): Collection;

   	public function create(Company $company, array $attributes): Ncf;

	/*public function update(array $data, $id);

	public function delete($id);

	public function show($id);*/
}