<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logs:clear {--file=laravel}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will clear the logs of the application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $file_regex = $this->option('file');
        $files = glob('storage/logs/'.$file_regex.'*.log');

        foreach ($files as $file) {
            if (file_exists($file)) {
                file_put_contents($file, "");
                // unlink($file);
            }
        }
        $this->info('The '.$file_regex.' logs has been cleared.');
    }
}
