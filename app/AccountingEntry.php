<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class AccountingEntry extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($accountingEntry) {
            $accountingEntry->entry_date = $accountingEntry->entry_date == null ? Carbon::now()->format("Y-m-d") : $accountingEntry->entry_date;
        });
    }

    /**
     * Get the accounting head of the entry.
     */
    public function userAccountingHead()
    {
        return $this->belongsTo('App\UserAccountingHead');
    }

    /**
     * Get the owning contact model.
     */
    public function contact()
    {
        return $this->morphTo();
    }
}
