<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Kalnoy\Nestedset\NodeTrait;

class AccountingHead extends Model
{
    use HasTranslations;
    use NodeTrait;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', '_lft', '_rgt', 'parent_id'
    ];

    protected $guarded = ['id'];

    public $translatable = ['name'];
}
