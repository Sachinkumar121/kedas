<?php

namespace App\Mail;

use App\Remission;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RemissionShared extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The remission instance.
     *
     * @var Invoice
     */
    public $remission;

    /**
     * The pdf as attachement.
     *
     * @var stream $pdf
     */
    protected $pdf;

    /**
     * Create a new message instance.
     *
     * @param  \App\Invoice  $remission
     * @return void
     */
    public function __construct(Remission $remission, $pdf)
    {
        $this->remission = $remission;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $filename = 'remission-'.$this->remission->internal_number.'.pdf';
        return $this->subject(trans('labels.remission').' #'.$this->remission->internal_number)
                    ->from(env('MAIL_FROM_ADDRESS'), $this->remission->company->name)
                    ->view('emails.remissions.shared')
                    ->text('emails.remissions.shared_plain')
                    ->attachData($this->pdf, $filename, [
                        'mime' => 'application/pdf',
                    ]);
    }
}
