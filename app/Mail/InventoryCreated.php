<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Inventory;

class InventoryCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The inventory instance.
     *
     * @var Inventory
     */
    public $inventory;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Inventory $inventory)
    {
        $this->inventory = $inventory;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('labels.product_service_created').' | '.config('app.name', 'Kedas'))
                    ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
                    ->view('emails.inventories.created')
                    ->text('emails.inventories.created_plain');
    }
}
