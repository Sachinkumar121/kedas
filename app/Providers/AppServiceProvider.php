<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Collection;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Schema;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Sector;
use App\Country;
use App\Currency;
use App\IdentificationType;
use App\Concept;
use App\AccountingHead;
use App\Permission;

use Config;
use Cache;
use App;
use Validator;
use Auth;
use Illuminate\Database\Eloquent\Builder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // The following make an issue for large data like invoices and transactions listing.
        if ($this->app->environment('locals')) {
            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }
    }

    private function getItems($resource = 'items', $parentId = null) {
        $items = collect([]);
        $data = Cache::get($resource);

        if ($data) {
            foreach ($data->where('parent_id', $parentId)->values() as $item) {
                $tmp = collect([
                    'id' => $item->id,
                    'title' => $item->title,
                    'children' => $this->getitems($resource, $item->id)
                ]);
                $items->push($tmp);
            }
        }
        return $items;
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        JsonResource::withoutWrapping();
        
        if (Schema::hasTable('sectors') && Schema::hasTable('countries') && Schema::hasTable('currencies') && Schema::hasTable('identification_types') && Schema::hasTable('concepts')) {
            $sectors = Cache::rememberForever('sectors', function () {
                return Sector::orderBy('name')->get();
            });
            $countries = Cache::rememberForever('countries', function () {
                return Country::select('id', 'name', 'code')->orderBy('id')->get();
            });
            $currencies = Cache::rememberForever('currencies', function () {
                return Currency::orderBy('name')->get();
            });
            $identification_types = Cache::rememberForever('identification_types', function () {
                return IdentificationType::orderBy('name')->get();
            });

            $concepts = Cache::rememberForever('concepts', function () {
                return Concept::orderBy('title')->get();
            });

            $permissions = Cache::rememberForever('permissions', function () {
                return Permission::all();
            });

            // if (App::Environment(['staging']) && Schema::hasTable('accounting_heads')) {
            if (Schema::hasTable('accounting_heads')) {
                $accounting_heads = Cache::rememberForever('accounting_heads', function () {
                    return AccountingHead::all();
                });
            }

            // $concepts = collect($this->getItems('concepts'));

            view()->composer('*', function(View $view) use($sectors, $countries, $currencies, $identification_types, $concepts) {
                /*Cache::rememberForever('user_accounting_heads_'.Auth::user()->company->id, function () {
                    return Auth::user()->company->accountingHeads;
                });*/
                // if (App::Environment(['staging']) && Auth::check() && Auth::user()->company) {
                /*if (Auth::check() && Auth::user()->company) {
                    if (Schema::hasTable('user_accounting_heads')) {
                        Cache::rememberForever('user_accounting_heads_'.Auth::user()->company->id, function () {
                            return Auth::user()->company->accountingHeads;
                        });
                    }
                }*/

                $need_cached_options = $view->getData()['need_cached_options'] ?? true;

                if (!$need_cached_options) {
                    $view->getData();
                } else {
                    $combinedData = array_merge([
                        'sector_options' => ['' => trans('labels.business_sector')] + $sectors->pluck('name', 'id')->toArray(),
                        'country_options' => ['' => trans('labels.select_country')] + $countries->pluck('name', 'id')->toArray(),
                        'currency_options' => ['' => trans('labels.select_currency')] + $currencies->pluck('formatted_name', 'id')->toArray(),
                        'identification_type_options' => ['' => trans('labels.select_type')] + $identification_types->pluck('name', 'id')->toArray(),
                        'concept_options' => ['' => trans('labels.select_concept')] + $concepts->whereNotNull('parent_id')->pluck('title', 'id')->toArray(),
                    ], $view->getData()); // $view->getData() is an array of existing data in the view
                    $view->with($combinedData);
                }
            });

            // todo:- need to verify the following content is required or not. 
            /*Config::set('constants.identification_types', $identification_types);
            Config::set('static.countries', $countries);
            Config::set('static.sectors', $sectors);
            Config::set('static.currencies', $currencies);
            Config::set('static.concepts', $concepts);*/
        }

        Collection::macro('sortByDate', function ($column = 'created_at', $order = SORT_DESC) {
            /* @var $this Collection */
            return $this->sortBy(function ($datum) use ($column) {
                return strtotime($datum->$column);
            }, SORT_REGULAR, $order == SORT_DESC);
        });
        
        Collection::macro('recursive', function () {
            return $this->map(function ($value) {
                if (is_array($value) || is_object($value)) {
                    return collect($value)->recursive();
                }
                return $value;
            });
        });

        Validator::extend('extensions', function ($attribute, $value, $parameters) {
            return in_array($value->getClientOriginalExtension(), $parameters);
        });

        Validator::replacer('extensions', function($message, $attribute, $rule, $parameters) {
            return str_replace([':attribute', ':values'], [$attribute, implode(',', $parameters)], $message);
        });

        Builder::macro('orderByTrans', function ($field, $order = 'asc', $locale = null) {
            if (
                in_array(HasTranslations::class, class_uses($this->model))
                && in_array($field, $this->model->translatable)
            ) {
                $locale = $locale ?? app()->getLocale();
                $this->query->orderByRaw("$field->>'$locale' $order");
            } else {
                $this->query->orderBy($field, $order);
            }
            return $this;
        });

        /**
         * Paginate a standard Laravel Collection.
         *
         * @param int $perPage
         * @param int $total
         * @param int $page
         * @param string $pageName
         * @return array
         */
        Collection::macro('paginate', function($perPage, $total = null, $page = null, $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);

            return new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });
    }
}
