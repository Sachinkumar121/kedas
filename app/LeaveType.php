<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;
use Storage;

class LeaveType extends Model
{
    use SoftDeletes;
    use HasTranslations;
    public $translatable = ['custom_name'];


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'leave_types';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
   /*  protected static function booted()
    {
        static::deleted(function($itemCategory) {
            $itemCategory->inventories()->each(function($inventory) {
                $inventory->itemCategory()->dissociate();
                $inventory->save();
            });
        });
    } */

    /**
     * Get all of the leave type's applications.
     */
    public function leaveApplications()
    {
        return $this->hasMany('App\LeaveApplications');
    }
}
