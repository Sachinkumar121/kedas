<?php

namespace App\Transformers;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use App\Transformers\StoreTransformer;

class EstimateTransformer
{
	public function transform($estimates)
	{
		// Create a top level instance somewhere
		$fractal = new Manager();

		$resource = new Collection($estimates, function($estimates) {
			return [
				'id' => $estimates->id,
				'number' => $estimates->internal_number,
				'store' => (new StoreTransformer())->singleTransform($estimates->company),
				'contact_id' => $estimates->contact_id,
				'type' => $estimates->type,
				'notes' => $estimates->notes,
				'internal_number' => $estimates->internal_number,
				'calculation' => $estimates->calculation,
				'created_at' => $estimates->created_at,
				'updated_at' => $estimates->updated_at,
				'expiration_date' => $estimates->expiration_date,
				'frequency' => $estimates->frequency_type,
				'frequency_type' => $estimates->frequency_type,
				'currency' => $estimates->currency,
				'exchange_rate_total' => $estimates->exchange_rate_total,
			];
		});
		$resource = $fractal->createData($resource)->toArray();
		return $resource["data"];
	}

	public function singleTransform($estimates)
	{	
		return [
			'id' => $estimates->id,
			'number' => $estimates->internal_number,
			'contact_id' => $estimates->contact_id,
			'notes' => $estimates->notes,
			'internal_number' => $estimates->internal_number,
			'type' => $estimates->type,
			'calculation' => $estimates->calculation,
			'created_at' => $estimates->created_at,
			'updated_at' => $estimates->updated_at,
			'expiration_date' => $estimates->expiration_date,
			'frequency' => $estimates->frequency_type,
			'frequency_type' => $estimates->frequency_type,
			'currency' => $estimates->currency,
			'exchange_rate_total' => $estimates->exchange_rate_total,
		];
	}
}