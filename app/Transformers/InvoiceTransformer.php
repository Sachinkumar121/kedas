<?php

namespace App\Transformers;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use App\Transformers\StoreTransformer;

class InvoiceTransformer
{
	public function transform($invoices)
	{
		// Create a top level instance somewhere
		$fractal = new Manager();

		$resource = new Collection($invoices, function($invoices) {
			return [
				'id' => $invoices->id,
				'number' => $invoices->internal_number,
				'currency' => $invoices->currency,

				'store' => (new StoreTransformer())->singleTransform($invoices->company),

				'calculation' => $invoices->calculation,

				'status' => $invoices->calculation['status'],
				'start_date' => $invoices->start_date,
				'start_date' => $invoices->start_date,
				'expiration_date' => $invoices->expiration_date,
				'created_at' => $invoices->created_at,
			];
		});
		$resource = $fractal->createData($resource)->toArray();
		return $resource["data"];
	}

	public function singleTransform($invoices)
	{	
		return [
			'id' => $invoices->id,
			'number' => $invoices->internal_number,
			'currency' => $invoices->currency,
			
			'store' => (new StoreTransformer())->singleTransform($invoices->company),

			'calculation' => $invoices->calculation,

			'status' => $invoices->calculation['status'],
			'start_date' => $invoices->start_date,
			'start_date' => $invoices->start_date,
			'expiration_date' => $invoices->expiration_date,
			'created_at' => $invoices->created_at,
			'frequency_type' => @$invoices->frequency_type,
			'frequency' => @$invoices->frequency,
			'term' => @$invoices->term,
			'manual_days' => @$invoices->manual_days

		];
	}
}