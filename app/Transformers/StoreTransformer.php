<?php

namespace App\Transformers;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use App\Transformers\AddressTransformer;

class StoreTransformer
{
	public function transform($stores)
	{
		// Create a top level instance somewhere
		$fractal = new Manager();

		$resource = new Collection($stores, function($store) {
			return [
				'id' => $store->id,
				'name' => $store->name,
				'logo' => $store->logo,
				'phone' => $store->phone,
				'fax' => $store->fax,
				'currency' => $store->currency,
				'support_email' => $store->support_email,
				'address' => @ (new AddressTransformer())->singleTransform($store->address),
				'primary' => @$store->pivot->primary,
			];
		});
		$resource = $fractal->createData($resource)->toArray();
		return $resource["data"];
	}

	public function singleTransform($store)
	{	
		return [
			'id' => $store->id,
			'name' => $store->name,
			'logo' => $store->logo,
			'phone' => $store->phone,
			'fax' => $store->fax,
			'currency' => $store->currency,
			'support_email' => $store->support_email,
			'address' => @ (new AddressTransformer())->singleTransform($store->address),
			'primary' => @ $store->pivot->primary,
		];
	}
}