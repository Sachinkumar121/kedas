<?php

namespace App\Transformers;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use App\Transformers\AddressTransformer;

class UserTransformer
{
	public function singleTransform($user)
	{
		return [
			'name' => $user->name,
			'email' => $user->email,
			'phone' => $user->phone,
			'language' => $user->language,
			'address' => @ (new AddressTransformer())->singleTransform($user->address()),
			'is_profile_updated' => (bool)$user->addresses()->count(),
			'logo' => @$user->image ? asset($user->image): null,
		];
	}
}