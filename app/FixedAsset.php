<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;

class FixedAsset extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fixed_assets';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image_url'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::deleted(function($fixedAsset) {});
    }

    public function getImageUrlAttribute() {
    	return $this->attributes['image'] ? Storage::url('items/'.$this->attributes['image']) : null;
    }

    /**
     * Get the depreciation of the company
     */
    public function depreciation()
    {
        return $this->morphMany('App\Depreciation', 'resource');
    }

    /**
     * Get the company information associated with the inventory.
     */
    public function legalCategory() {
        return $this->belongsTo('App\UserAccountingHead', 'legal_category');
    }

    /**
     * Get the tax information associated with the inventory.
     */
    public function itemCategory() {
        return $this->belongsTo('App\ItemCategory');
    }

    /**
     * Get the accounting entries of the fixed assets.
     */
    public function accountingEntries()
    {
        return $this->hasMany('App\AccountingEntry');
    }
}
