<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use Auth;

class LeaveApplication extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'leave_applications';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

     /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['approved_leaves', 'status_label'];

    public function getApprovedLeavesAttribute() {
        $user = Auth::user();
        $approveLeaves = $user->leaveApplications()->where('status', 2)->sum('number_of_days');
        return $approveLeaves;
    }

    public function getStatusLabelAttribute() {
        if ($this->status == 1) {
            return trans('labels.pending');
        } else if ($this->status == 2) {
            return trans('labels.approved');
        } else {
             return trans('labels.rejected');
        }
    }

    /**
     * Get the leaveType associated with the leave application.
    */
    public function leaveType()
    {
        return $this->belongsTo('App\LeaveType');
    }

    /**
     * Get the employee associated with the leave application.
    */
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    /**
     * Get the user associated with the leave application.
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
