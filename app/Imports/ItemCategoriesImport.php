<?php

namespace App\Imports;

use App\ItemCategory;
use App\Validators\ItemCategoryValidator;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

use Auth;

class ItemCategoriesImport implements ToModel, WithValidation, WithHeadingRow, WithMapping, WithChunkReading
{
    use Importable;

    /**
     * Rules for client import
     *
     * @var array
     */
    protected $rules;

    public function __construct()
    {}

    /*public function batchSize(): int
    {
        return 1000;
    }*/

    public function chunkSize(): int
    {
        return 1000;
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        $row = array_map('trim', $row);
        $row = array_map(function($field) {
            return !empty($field) ? $field : null;
        }, $row);
        return $row;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $itemCategory = new ItemCategory([
            'user_id'  => Auth::id(),
            'company_id'  => Auth::user()->company->id,
            'name'  => $row['name'],
            'description' => $row['description']
        ]);
        $itemCategory->save();

        return $itemCategory; 
    }

    public function rules(): array
    {
        $itemCategoryValidator = new ItemCategoryValidator;
        return $itemCategoryValidator->getRules();
    }
}
