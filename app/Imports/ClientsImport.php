<?php

namespace App\Imports;

use App\Client;
use App\Validators\ClientValidator;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Auth;
use Cache;

class ClientsImport implements ToModel, WithValidation, WithHeadingRow, WithMapping, WithChunkReading
{
    use Importable;

    /**
     * Rules for client import
     *
     * @var array
     */
    protected $rules;

    public function __construct()
    {
        $this->identification_types = Cache::get('identification_types')->map(function($row) {
            $row->name = mb_strtolower($row->name);
            return $row;
        });
        $this->countries = Cache::get('countries')->map(function($row) {
            $row->name = mb_strtolower($row->name);
            return $row;
        });
        $this->sectors = Cache::get('sectors')->map(function($row) {
            $row->name = mb_strtolower($row->name);
            return $row;
        });
    }

    /*public function batchSize(): int
    {
        return 1000;
    }*/

    public function chunkSize(): int
    {
        return 1000;
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        $row = array_map('trim', $row);
        $row = array_map(function($field) {
            return !empty($field) ? $field : null;
        }, $row);

        $manipulateKeys = ['identification_type', 'country', 'department'];
        return collect($row)->map(function($field, $key) use ($manipulateKeys) {
            return in_array($key, $manipulateKeys) ? mb_strtolower($field) : $field;
        }, $row)->toArray();
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $identification_type_id = (isset($row['identification_type']) && $row['identification_type']) ? $this->identification_types->pluck('name', 'id')->search($row['identification_type']) : null;
        $sector_id = (isset($row['department']) && $row['department']) ? $this->sectors->pluck('name', 'id')->search($row['department']) : null;
        $country_id = (isset($row['country']) && $row['country']) ? $this->countries->pluck('name', 'id')->search($row['country']) : null;

        $client = new Client([
            'user_id'  => Auth::id(),
            'company_id'  => Auth::user()->company->id,
            'name'  => $row['name'],
            'email' => $row['email'],
            'identification_type_id' => $identification_type_id,
            'tax_id' => @$row['tax_id'],
            'sector_id' => $sector_id,
            'phone' => @$row['phone'],
            'mobile' => @$row['mobile'],
            'fax' => @$row['fax']
        ]);
        $client->save();

        if ($row['address'] || $row['zip'] || $row['country']) {
            $addressData = [
                'line_1' => $row['address'],
                'zip' => $row['zip'],
                'country_id' => $country_id
            ];
            $client->address()->create($addressData);
        }

        return $client; 
    }

    public function rules(): array
    {
        $clientValidator = new ClientValidator;
        $additional_rules = [
            'identification_type' => 'nullable|in:'.$this->identification_types->implode('name', ','),
            'country' => 'nullable|in:'.$this->countries->implode('name', ','),
            'department' => 'nullable|in:'.$this->sectors->implode('name', ',')
        ];
        $this->rules = array_merge($clientValidator->getRules(), $additional_rules);

        return $this->rules;
    }

    /**
     * @return array
     */
    public function customValidationMessages()
    {
        return [
            'identification_type.in' => trans('validation.in', ['attribute' => trans('labels.identification_type')]),
            'country.in' => trans('validation.in', ['attribute' => trans('labels.country')]),
            'department.in' => trans('validation.in', ['attribute' => trans('labels.department')]),
        ];
    }
}
