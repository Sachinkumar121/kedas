<?php

namespace App\Exceptions;

use Exception;

class ServiceException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request, $exception) {
    	echo "ere";
    	die;

	    if (!$request->ajax()) {
	        // view('error_handler', compact('exception'));
	    }

	    return response()->json([
			'code' => $exception->getCode(),
			'status' => 'error',
			'message' => $exception->getMessage(),
			'data' => 'sample data'
      	]);

	}
}
