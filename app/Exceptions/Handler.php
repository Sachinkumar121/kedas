<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Exception;
use Request;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Response;

use App\Traits\ApiResponse;
use App\Exceptions\ServiceException;

class Handler extends ExceptionHandler
{
    use ApiResponse;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        // echo get_class($exception);
        if ($exception instanceof AuthorizationException && $request->wantsJson())
        {
            return $this->failResponse([
                "message" => trans('messages.account_access')
            ], 403);
        }
        $locale = getAppLocale($request);
        app()->setLocale($locale);


        if ($exception instanceof ModelNotFoundException && $request->wantsJson()) {
            return $this->failResponse([
                "message" => trans('messages.resource_not_found'),
            ], 404);
        }
        
        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception) 
    {
        if ($request->wantsJson()) {
            return $this->failResponse([
                "message" => trans('messages.unauthenticated'),
            ], 401);
        }

        return redirect()->guest('login');
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        // $this->renderable(function (ServiceException $e, $request) {
        //     return response()->view('errors.custom', [], 500);
        // });
    }

}
