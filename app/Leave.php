<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use Auth;

class Leave extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'leaves';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Get the employee associated with the leave application.
    */
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    /**
     * Get the user associated with the leave application.
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
