<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

// use App\Events\PaymentCreatedOrUpdated;

use Throwable;
use Log;
use DB;

use Carbon\Carbon;
use App\FixedAsset;

class GenerateAccountEntriesFromFixedAssets implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 20;

    /**
     * The number of seconds to wait before retrying the job.
     *
     * @var int
     */
    public $retryAfter = 9;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $current_date = Carbon::now();
        // $current_date = Carbon::now()->lastOfMonth();
        // $current_date = Carbon::parse("2021-03-27")->lastOfMonth();
        $logData = [];

        try {
            DB::beginTransaction();
            DB::enableQueryLog();

            // $fixedAssets = FixedAsset::doesntHave('accountingEntries')->whereMonth('date_of_purchase', '=', $current_date->month)->get();
            $fixedAssets = FixedAsset::where('depr_should_cal', true)->get();

            foreach ($fixedAssets as $fixedAsset) {
                $maxUnitsProduced = null;
                $unitsProducedPerYear = [];

                if ($fixedAsset->cal_type == 'units_produced') {
                    $maxUnitsProduced = $fixedAsset->total_unit_produced;
                    if ($fixedAsset->same_for_year == "1") {
                        $unitsProducedPerYear = array_fill(0, $fixedAsset->years, $fixedAsset->unit_produced_per_year);
                    } else {
                        $unitsProducedPerYear = explode(',', $fixedAsset->per_year_prod);
                    }
                    $unitsProducedPerYear = array_map('trim', $unitsProducedPerYear);
                }

                $monthWiseDepr = collect(mapMonthWithDepreciation($fixedAsset->cal_type, $fixedAsset->price, $fixedAsset->scrap_percentage, $fixedAsset->years, $fixedAsset->date_of_purchase, $maxUnitsProduced, $unitsProducedPerYear));

                $thisMonthDepr = $monthWiseDepr->where('date', $current_date->format('Y-m-d'))->first();
                if ($thisMonthDepr) {
                    $accountingEntries = collect([
                        ['amount' => abs($thisMonthDepr['amount']), 'order' => 1, 'user_accounting_head_id' => $fixedAsset->expense_account, 'nature' => 'debit', 'entry_date' => $current_date->format('Y-m-d')],
                        ['amount' => abs($thisMonthDepr['amount']), 'order' => 2, 'user_accounting_head_id' => $fixedAsset->account, 'nature' => 'credit', 'entry_date' => $current_date->format('Y-m-d')]
                    ]);
                    $fixedAsset->accountingEntries()->createMany($accountingEntries);
                    array_push($logData, ["fixed_asset_id" => $fixedAsset->id, "accountingEntries" => $accountingEntries]);
                }
            }
            DB::commit();
            if ($logData) {
                Log::channel('cronlog')->info("New account entry added: ".json_encode($logData));
                return "New account entry added: ".json_encode($logData);
            }
        } catch(Exception $e) {
            DB::rollback();
            $this->failed($e);
        }
    }

    /**
     * Handle a job failure.
     *
     * @param  \Throwable  $exception
     * @return void
     */
    public function failed(Throwable $exception)
    {
        Log::channel('cronlog')->error($exception->getMessage());
        // Send user notification of failure, etc...
        // Log::error($exception->getMessage());
        // die;
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        // return now()->addSeconds(5);
    }
}
