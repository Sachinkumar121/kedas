<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Events\PaymentCreatedOrUpdated;

use Throwable;
use Log;
use DB;

use App\Transaction;
use Carbon\Carbon;

class GeneratePaymentsFromRecurringPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 20;

    /**
     * The number of seconds to wait before retrying the job.
     *
     * @var int
     */
    public $retryAfter = 9;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $current_date = Carbon::now()->format('Y-m-d');

        try {
            DB::beginTransaction();
            DB::enableQueryLog();
            $current_date = Carbon::now()->format('Y-m-d');
            $recurringTransactionsIds = DB::table('transactions')->from('transactions as t')->select('id')->where(['type' => 'recurring'])->where(function($query) use ($current_date) {
                return $query->where('start_date', '=', $current_date)
                    ->orWhere(function($q) use ($current_date) {
                        // CONVERT_TZ(NOW(), @@session.time_zone, '+00:00') // UTC alternative.
                        return $q->whereRaw('"'.$current_date.'" <= end_date AND "'.$current_date.'" in (WITH RECURSIVE
                        cte AS ( SELECT id, start_date, end_date, frequency, frequency_type
                             FROM transactions
                             UNION ALL
                             SELECT id, CASE frequency_type WHEN "day" THEN start_date + INTERVAL frequency DAY WHEN "month" THEN start_date + INTERVAL frequency MONTH END, end_date, frequency, frequency_type
                             FROM cte
                             WHERE start_date <= end_date )
                        SELECT start_date date
                        FROM cte
                        WHERE start_date <= end_date and id = t.id
                        ORDER BY 1)');
                    });
            })->whereNull('deleted_at')->orderBy('t.id')->get()->pluck('id')->toArray();
            // Log::channel('cronlog')->info(DB::getQueryLog());
            // Log::channel('cronlog')->info($recurringTransactionsIds);

            $logData = [];
            if ($recurringTransactionsIds) {
                foreach ($recurringTransactionsIds as $recurringTransactionId) {
                    $recurringTransaction = Transaction::find($recurringTransactionId);
                    if ($recurringTransaction) {
                        $recurringTransactionItems = $recurringTransaction->conceptItems;

                        $itemsToInsert = $recurringTransactionItems->map(function($item, $key) {
                            return $item->only(['quantity', 'tax_id', 'user_accounting_head_id', 'discount', 'price']);
                        })->toArray();
                        $transaction = $recurringTransaction->replicate();
                        $transaction->type = "out";
                        $transaction->start_date = $current_date;
                        $transaction->end_date = null;
                        $transaction->frequency = null;
                        $transaction->frequency_type = null;
                        $transaction->recurring_transaction_id = $recurringTransaction->id;
                        $transaction->save();

                        $transaction->conceptItems()->createMany($itemsToInsert);

                        event(new PaymentCreatedOrUpdated($transaction));
                        
                        if (array_key_exists($recurringTransactionId, $logData)) {
                            array_push($logData[$recurringTransactionId][], $transaction->id);
                        } else {
                            $logData[$recurringTransactionId][] = $transaction->id;
                        }
                    }
                }
            }
            DB::commit();
            if ($logData) {
                Log::channel('cronlog')->info("New payments has been generated: ".json_encode($logData));
                return "New payments has been generated: ".json_encode($logData);
            }
        } catch(Exception $e) {
            DB::rollback();
            $this->failed($e);
        }
    }

    /**
     * Handle a job failure.
     *
     * @param  \Throwable  $exception
     * @return void
     */
    public function failed(Throwable $exception)
    {
        Log::channel('cronlog')->error($exception->getMessage());
        // Send user notification of failure, etc...
        // Log::error($exception->getMessage());
        // die;
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        // return now()->addSeconds(5);
    }
}
