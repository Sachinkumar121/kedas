<?php
namespace App\Services;

use App\Services\Service;

use Illuminate\Http\Request;

use App\Invoice;
use App\Inventory;
use App\Item;
use App\ConceptItem;

use App\Validators\InvoiceValidator;

use Image;
use File;

use App\Exceptions\ServiceException;

use App\Events\SaleInvoiceCreatedOrUpdated;
use App\Events\SupplierInvoiceCreatedOrUpdated;

class InvoiceService extends Service
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $type = self::SALE)
    {
    	try {
		    $input = $request->all();
		    $user = $request->user();

		    list($contact_type, $contact_id) = explode('-', $input['contact_id']);
		    $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

		    if (!$contact) {
		    	throw new ServiceException(trans('validation.exists', ['attribute' => trans('labels.contact')]), 400);
		    }

		    if ($type != self::SUPPLIER) {
		        $invoiceItems = collect($input['products'])->filter(function($row) {
		            return $row['quantity'] && $row['item'];
		            })->map(function($item) {
		                $item['inventory_id'] = $item['item'];
		                $item['tax_id'] = $item['tax'] ?: null;
		                unset($item['item']);
		                unset($item['tax']);
		                //unset($item['reference']);
		                //unset($item['price']);
		                unset($item['description']);
		                unset($item['tax_amount']);
		                return $item;
		        })->values()->toArray();
		    } else {
		        $conceptItems = collect($input['concepts'])->filter(function ($concept) {
		            return substr($concept['item'], 0, strlen(self::CONCEPT)) === self::CONCEPT;
		        })->filter(function($row) {
		            return $row['quantity'] && $row['price'] && $row['item'];
		        })->map(function($item) {
		            list($item_type, $item_id) = explode('-', $item['item']);
		            $item['user_accounting_head_id'] = $item_id;
		            $item['tax_id'] = $item['tax'] ?: null;
		            unset($item['item']);
		            unset($item['tax']);
		            unset($item['tax_amount']);
		            return $item;
		        })->values()->toArray();

		        $inventoryItems = collect($input['concepts'])->filter(function ($concept) {
		            return substr($concept['item'], 0, strlen(self::INVENTORY)) === self::INVENTORY;
		        })->filter(function($row) {
		            return $row['quantity'] && $row['price'] && $row['item'];
		        })->map(function($item) {
		            list($item_type, $item_id) = explode('-', $item['item']);
		            $item['inventory_id'] = $item_id;
		            $item['tax_id'] = $item['tax'] ?: null;
		            unset($item['item']);
		            unset($item['tax']);
		            //unset($item['reference']);
		            unset($item['description']);
		            unset($item['tax_amount']);
		            return $item;
		        })->values()->toArray();

		        $invoiceItems = collect([])->concat($conceptItems)->concat($inventoryItems);

		    }

		    if (!$invoiceItems) {
		    	throw new ServiceException(trans('messages.invoice_without_items'), 400);
		    }

		    $invoice = $contact->invoices()->create([
		        'user_id' => $user->id,
		        'company_id' => $user->company->id,
		        'term' => @$input['term'],
		        'manual_days' => @$input['term'] == 'manual' ? @$input['manual_days'] : null,
		        'start_date' => $input['invoice_start_date'],
		        'expiration_date' => $type == self::SALE ? (date('Y-m-d', strtotime($input['invoice_start_date']. ' + '.($input['term'] == 'manual' ? ($input['manual_days'] ?? 1) : ($input['term'] ?? 0) ).' days'))) : ($input['invoice_expiration_date'] ?? null),
		        'end_date' => @$input['invoice_end_date'],
		        'type' => $type,
		        'tac' => $input['tac'] ?? null,
		        'ncf_id' => $type != self::SUPPLIER ? ($input['ncf_id'] ?? null) : null,
		        'notes' => $input['notes'] ?? null,
		        'res_text' => $input['res_text'] ?? null,
		        'observation' => $input['observation'] ?? null,
		        'frequency' => $input['frequency'] ?? null,
		        'frequency_type' => $input['frequency_type'] ?? null,
		        'privacy_policy' => $input['privacy_policy'] ?? null,
		        'ncf_number' => $input['ncf_number'] ?? null,
		        'exchange_currency_rate' => $input['exchange_rate'] ?? null,
		        'exchange_currency_id' => $input['exchange_currency'] ?? null,
		    ]);

		    if ($type != self::SUPPLIER) {
		        $invoice->items()->createMany($invoiceItems);

		        // decrease the quantity of each item's inventory while creating the invoice.
		        if ($type == self::SALE) {
		            $invoice->items->each(function($item) {
		                $item->inventory->quantity = $item->inventory->quantity - $item->quantity;
		                $item->inventory->save();
		            });
		        }
		    } else {
		        if ($inventoryItems) {
		            $invoice->items()->createMany($inventoryItems);
		            // increase the quantity of each item's inventory while creating the invoice.
		            $invoice->items->each(function($item) {
		                $item->inventory->quantity = $item->inventory->quantity + $item->quantity;
		                $item->inventory->save();
		            });
		        }
		        $invoice->conceptItems()->createMany($conceptItems);
		    }

		    if ($type == self::SALE && $invoice->ncf_number) {
		        $invoice->ncf->current_number = $invoice->ncf_number;
		        $invoice->ncf->save();
		    }

		    // set the contact points based on the membership
		    if ($type == self::SALE && $contact->membership && $contact->membership->point_percentage && Carbon::now()->format('Y-m-d') <= $contact->membership->point_expire_date) {
		        $points = ($invoice->calculation['total'] / 100) * $contact->membership->point_percentage;

		        $contactPoints = $contact->points()->create([
		            'invoice_id' => $invoice->id,
		            'membership_id' => $contact->membership->id,
		            'point_percentage' => $contact->membership->point_percentage,
		            'points' => $points
		        ]);
		    }

		    if (isset($input['logo']) && $input['logo']) {
		        $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
		        $dir = public_path("storage/profile-pics");
		        $path = $dir."/".$filename;

		        if( !\File::isDirectory($dir) ) { 
		            \File::makeDirectory($dir, 493, true);
		        }
		        
		        $logoInstance = Image::make($input['logo']);
		        if ($logoInstance->height() > 300) {
		            $logoInstance->fit(300)->save($path);
		        } else {
		            $logoInstance->orientate()->save($path);
		        }

		        $user->company->logo = $filename;
		        $user->company->save();
		    }

		    if (isset($input['signature']) && $input['signature']) {
		        $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
		        $dir = public_path("storage/signatures");
		        $path = $dir."/".$filename;

		        if( !\File::isDirectory($dir) ) { 
		            \File::makeDirectory($dir, 493, true);
		        }
		        
		        $signatureInstance = Image::make($input['signature']);

		        if ($signatureInstance->height() > 150) {
		            $signatureInstance->fit(300, 150)->save($path);
		        } else {
		            $signatureInstance->orientate()->save($path);
		        }

		        $user->company->signature = $filename;
		        $user->company->save();
		    }

		    if ($invoice->type == self::RECURRING) {
		        Mail::to($user->email)->send(new InvoiceCreated($invoice));
		    }

		    /* upload documents */
		    if (isset($input['documents']) && $input['documents']) {
		        $files = $input['documents'];

		        $dir = public_path("storage/documents");
		        if ( !\File::isDirectory($dir) ) { 
		            \File::makeDirectory($dir, 493, true);
		        }

		        foreach ($files as $file) {
			        // For apis
		        	if ($this->isApiCall) {
			            $name = 'invoice-' . uniqid() . '.' . $file->extension();
			            $file->move($dir, $name);
			            $documents_data[] = [
			                'name' => $name,
			            ];
		        	} else {
		        		// For web
			        	if (File::exists(storage_path('tmp/'.$file))) {
			                File::move(storage_path('tmp/'.$file), public_path('storage/documents/invoice-'.$file));
			                $documents_data[] = [
			                    'name' => 'invoice-'.$file,
			                ];
			            }
		        	}
		        }

		        $documents = $invoice->documents()->createMany($documents_data);
		    }

		    /* Save exchange rate */
		    if (isset($input['exchange_rate']) && $input['exchange_rate'] && isset($input['exchange_currency']) && $input['exchange_currency']) {
		        $user->company->currencyRates()->updateOrCreate(
		            ['currency_id' => $input['exchange_currency'], 'user_id' => $user->id],
		            ['rate' => $input['exchange_rate']]
		        );
		    }

		    $remission_id = $input['remission_id'] ?? null;

		    if ($remission_id) {
		        $remission = $user->company->remissions()->where('id', $remission_id)->whereNull('invoice_id')->first();
		        if ($remission) {
		            $remission->invoice_id = $invoice->id;
		            $invoice->remission_id = $remission_id;
		            $remission->save();
		            $invoice->save();
		        }
		    }
		    if ($type == self::SALE) {
		        event(new SaleInvoiceCreatedOrUpdated($invoice));
		    }
		    if ($type == self::SUPPLIER) {
		        event(new SupplierInvoiceCreatedOrUpdated($invoice));
		    }		    
    	} catch (ServiceException | Exception $e) {
		    throw $e;
    	}

	    return $invoice;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        try {
	        $type = $invoice->type;
	        $input = $request->all();
	        $user = $request->user();

	        // currently invoice is only associated with transactions.
	        $resource_associated = $invoice->transactions->isNotEmpty();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
		    	throw new ServiceException(trans('validation.exists', ['attribute' => trans('labels.contact')]), 400);
		    }

            if ($type != self::SUPPLIER) {
                $invoiceItems = collect($input['products'])->filter(function($row) {
                    return $row['quantity'] && $row['item'];
                })->map(function($item) {
                    $item['inventory_id'] = $item['item'];
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    //unset($item['reference']);
                    //unset($item['price']);
                    unset($item['description']);
                    unset($item['tax_amount']);
                    return $item;
                })->values();
            } else {
                $conceptItems = collect($input['concepts'])->filter(function ($concept) {
                    return substr($concept['item'], 0, strlen(self::CONCEPT)) === self::CONCEPT;
                })->filter(function($row) {
                    return $row['quantity'] && $row['price'] && $row['item'];
                })->map(function($item) {
                    list($item_type, $item_id) = explode('-', $item['item']);
                    $item['user_accounting_head_id'] = $item_id;
                    $item['tax_id'] = $item['tax'] ?: null;

                    if ($item['id']) {
                        list($row_type, $row_id) = explode('-', $item['id']);
                        $item['id'] = ($row_type === self::CONCEPT) ? $row_id : 0;
                    }

                    unset($item['item']);
                    unset($item['tax']);
                    unset($item['tax_amount']);
                    return $item;
                })->values();

                $inventoryItems = collect($input['concepts'])->filter(function ($concept) {
                    return substr($concept['item'], 0, strlen(self::INVENTORY)) === self::INVENTORY;
                })->filter(function($row) {
                    return $row['quantity'] && $row['price'] && $row['item'];
                })->map(function($item) {
                    list($item_type, $item_id) = explode('-', $item['item']);
                    $item['inventory_id'] = $item_id;
                    $item['tax_id'] = $item['tax'] ?: null;

                    if ($item['id']) {
                        list($row_type, $row_id) = explode('-', $item['id']);
                        $item['id'] = ($row_type === self::INVENTORY) ? $row_id : 0;
                    }

                    unset($item['item']);
                    unset($item['tax']);
                    unset($item['reference']);
                    unset($item['description']);
                    unset($item['tax_amount']);
                    return $item;
                })->values();


                $invoiceItems = collect([])->concat($conceptItems)->concat($inventoryItems);
            }

            // Redirect if items are empty. 
            if (!$invoiceItems) {
		    	throw new ServiceException(trans('messages.invoice_without_items'), 400);
		    }

            if ($type != self::SUPPLIER) {
                // map inventory id as key and quantity as value from submit form value.
                $invoice_inventory_quantity = $invoiceItems->groupBy('inventory_id')->map(function($it) {
                    return $it->sum('quantity');
                });
                // map inventory id as key and quantity as value from existing invoice's item value.
                $existing_inventory_quantity = $invoice->items->groupBy('inventory_id')->map(function($it) {
                    return $it->sum('quantity');
                });

                // map inventory id as key and quantity as value as new quantity for eack inventory.
                $inventory_quantity = $invoice_inventory_quantity->map(function($iiq, $inventoryId) use($existing_inventory_quantity){
                    return (($existing_inventory_quantity[$inventoryId] ?? 0) - $iiq);
                })->union($existing_inventory_quantity->diffKeys($invoice_inventory_quantity));

                $updateInvoiceItems = $invoiceItems->filter(function($row) {
                    return $row['id'];
                })->values();

                $insertInvoiceItems = $invoiceItems->filter(function($row) {
                    return !$row['id'];
                })->map(function($item) {
                    unset($item['id']);
                    return $item;
                })->values()->toArray();

                $deleteInvoiceItemsExcept = $updateInvoiceItems->pluck('id')->toArray();
            } else {
                // The following is for invoice's items
                // map inventory id as key and quantity as value from submit form value.
                $invoice_inventory_quantity = $inventoryItems->groupBy('inventory_id')->map(function($it) {
                    return $it->sum('quantity');
                });
                // map inventory id as key and quantity as value from existing invoice's item value.
                $existing_inventory_quantity = $invoice->items->groupBy('inventory_id')->map(function($it) {
                    return $it->sum('quantity');
                });

                if ($existing_inventory_quantity->isNotEmpty()) {
                    // map inventory id as key and quantity as value as new quantity for eack inventory.
                    $inventory_quantity = $existing_inventory_quantity->map(function($iiq, $inventoryId) use($invoice_inventory_quantity){
                        return ($iiq - ($invoice_inventory_quantity[$inventoryId] ?? 0));
                    });
                } else {
                    // map inventory id as key and quantity as value as new quantity for eack inventory.
                    $inventory_quantity = $invoice_inventory_quantity->map(function($iiq) {
                        return (0 - $iiq);
                    });
                }

                $updateInvoiceItems = $inventoryItems->filter(function($row) {
                    return $row['id'];
                })->values();

                $insertInvoiceItems = $inventoryItems->filter(function($row) {
                    return !$row['id'];
                })->map(function($item) {
                    unset($item['id']);
                    return $item;
                })->values()->toArray();

                $deleteInvoiceItemsExcept = $updateInvoiceItems->pluck('id')->toArray();

                // The following is for invoice's concept items
                $updateInvoiceConceptItems = $conceptItems->filter(function($row) {
                    return $row['id'];
                })->values();

                $insertInvoiceConceptItems = $conceptItems->filter(function($row) {
                    return !$row['id'];
                })->map(function($item) {
                    unset($item['id']);
                    return $item;
                })->values()->toArray();

                $deleteInvoiceConceptItemsExcept = $updateInvoiceConceptItems->pluck('id')->toArray();
            }

            $invoice_total = $input['invoice_total_hidden'] ?? $invoice->calculation['total'];

            // Redirect if the user enter invalid amount for the invoice.
            if ((float)$invoice_total < $invoice->calculation['paidAmount']) {
            	throw new ServiceException(trans('messages.invoice_underflow_total'), 400);
            }

            // Change the contact association only if there is no transaction associated with invoice.
            if (!$resource_associated) {
                $invoice->contact()->associate($contact);
            }

            // update the contact information for each transaction against the invoice.
            /*if ($resource_associated) {
                $invoice->transactions()->each(function($transaction) use($contact) {
                    $transaction->contact()->associate($contact);
                    $transaction->save();
                });
            }*/

            // Update the basic information.

            $invoice->update([
                'term' => @$input['term'],
                'manual_days' => @$input['term'] == 'manual' ? @$input['manual_days'] : null,
                'start_date' => $input['invoice_start_date'],
                'expiration_date' => $type == self::SALE ? (date('Y-m-d', strtotime($input['invoice_start_date']. ' + '.($input['term'] == 'manual' ? ($input['manual_days'] ?? 1) : ($input['term'] ?? 0) ).' days'))) : ($input['invoice_expiration_date'] ?? null),
                'end_date' => @$input['invoice_end_date'],
                'tac' => $input['tac'] ?? null,
                'notes' => $input['notes'] ?? null,
                'res_text' => $input['res_text'] ?? null,
                'frequency' => $input['frequency'] ?? null,
                'frequency_type' => $input['frequency_type'] ?? null,
                'observation' => $input['observation'] ?? null,
                'privacy_policy' => $input['privacy_policy'] ?? null
            ]);

            if ($type == self::SUPPLIER) {
                $invoice->ncf_number = $input['ncf_number'] ?? null;
                $invoice->save();
            }

            if ($deleteInvoiceItemsExcept) {
                $invoiceItemsToDelete = $invoice->items()->whereNotIn('id', $deleteInvoiceItemsExcept);
                // if ($type == self::SALE) {
                //     // increase the quantity of each item's inventory while delete the items of the invoice.
                //     $invoiceItemsToDelete->get()->each(function($iitd) {
                //         $iitd->inventory->quantity = $iitd->inventory->quantity + $iitd->quantity;
                //         $iitd->inventory->save();
                //     });
                // }
                // if ($type == self::SUPPLIER) {
                //     // decrease the quantity of each item's inventory while delete the items of the invoice.
                //     $invoiceItemsToDelete->get()->each(function($iitd) {
                //         $iitd->inventory->quantity = $iitd->inventory->quantity - $iitd->quantity;
                //         $iitd->inventory->save();
                //     });
                // }
                $invoiceItemsToDelete->delete();
            }

            if ($insertInvoiceItems) {
                $invoice->items()->createMany($insertInvoiceItems);
            }

            if ($updateInvoiceItems) {
                foreach ($updateInvoiceItems as $updateInvoiceItem) {
                    $updatedData = $updateInvoiceItem;
                    unset($updatedData['id']);
                    Item::findOrFail($updateInvoiceItem['id'])->update($updatedData);
                }
            }

            // In case of supplier invoice perform CRUD on concepts
            if ($type == self::SUPPLIER) {
                if ($deleteInvoiceConceptItemsExcept) {
                    $invoice->conceptItems()->whereNotIn('id', $deleteInvoiceConceptItemsExcept)->delete();
                }

                if ($insertInvoiceConceptItems) {
                    $invoice->conceptItems()->createMany($insertInvoiceConceptItems);
                }

                if ($updateInvoiceConceptItems) {
                    foreach ($updateInvoiceConceptItems as $updateInvoiceConceptItem) {
                        $updatedData = $updateInvoiceConceptItem;
                        unset($updatedData['id']);
                        ConceptItem::findOrFail($updateInvoiceConceptItem['id'])->update($updatedData);
                    }
                }

                if ($inventoryItems->isEmpty()) {
                    $invoice->items()->delete();
                }

                if ($conceptItems->isEmpty()) {
                    $invoice->conceptItems()->delete();
                }
            }

            // update the quantity of each item's inventory according to mapped data.
            if ($type == self::SALE) {
                $inventory_quantity->each(function($quantity, $inventoryId) {
                    $inventory = Inventory::findOrFail($inventoryId);
                    $inventory->quantity = $inventory->quantity + $quantity;
                    $inventory->save();
                });
            }

            if ($type == self::SUPPLIER) {
                $inventory_quantity->each(function($quantity, $inventoryId) {
                    $inventory = Inventory::findOrFail($inventoryId);
                    $inventory->quantity = $inventory->quantity - $quantity;
                    $inventory->save();
                });
            }

            // update the contact points based on the membership
            if ($type == self::SALE) {
                if ($contact->membership && $contact->membership->point_percentage && Carbon::now()->format('Y-m-d') <= $contact->membership->point_expire_date) {
                    $points = ($invoice->calculation['total'] / 100) * $contact->membership->point_percentage;

                    $contact->points()->updateOrCreate([
                        'invoice_id' => $invoice->id,
                    ], [
                        'membership_id' => $contact->membership->id,
                        'point_percentage' => $contact->membership->point_percentage,
                        'points' => $points
                    ]);
                } else {
                    if ($invoice->contactPoint) {
                        $invoice->contactPoint->delete();
                    }
                }
            }

            if (isset($input['logo']) && $input['logo']) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/profile-pics");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                
                $logoInstance = Image::make($input['logo']);
                if ($logoInstance->height() > 300) {
                    $logoInstance->fit(300)->save($path);
                } else {
                    $logoInstance->orientate()->save($path);
                }

                $user->company->logo = $filename;
                $user->company->save();
            }

            if (isset($input['signature']) && $input['signature']) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/signatures");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $signatureInstance = Image::make($input['signature']);

                if ($signatureInstance->height() > 150) {
                    $signatureInstance->fit(300, 150)->save($path);
                } else {
                    $signatureInstance->orientate()->save($path);
                }

                $user->company->signature = $filename;
                $user->company->save();
            }

            /* update documents start */
            $documents = $input['documents'] ?? [];
            $deleted_documents_ids = $input['deleted_documents_ids'] ?? [];

            if ($this->isApiCall) {

	            if ($deleted_documents_ids) {
	                $invoice->documents()->whereIn('id', $deleted_documents_ids)->delete();
	            }
	        
	            if ($documents) {
	                $dir = public_path("storage/documents");
	                if ( !\File::isDirectory($dir) ) { 
	                    \File::makeDirectory($dir, 493, true);
	                }

	                foreach ($documents as $file) {
	                    $name = 'invoice-' . uniqid() . '.' . $file->extension();
	                    $file->move($dir, $name);
	                    $documents_data[] = [
	                        'name' => $name,
	                    ];
	                }
	                $invoice->documents()->createMany($documents_data);
	            }

            } else {
            	if ($invoice->documents->isNotEmpty()) {
	                foreach ($invoice->documents as $document) {
	                    if (!in_array($document->name, $documents)) {
	                        $invoice->documents()->where('id', '=', $document->id)->delete();
	                    }
	                }
	            }
	         
	            $alreadyExistDocuments = $invoice->documents->pluck('name')->toArray();

	            $dir = public_path("storage/documents");

	            foreach ($request->input('documents', []) as $file) {
	                if ($alreadyExistDocuments || !in_array($file, $alreadyExistDocuments)) {
	                    if (File::exists(storage_path('tmp/'.$file))) {
	                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/invoice-'.$file));
	                        $document_data = array('name' => 'invoice-'.$file);
	                        $invoice->documents()->create($document_data);
	                    }
	                }
	            }
            }            
            /* update documents end */

            if ($type == self::SALE) {
                event(new SaleInvoiceCreatedOrUpdated($invoice));
            }
            if ($invoice->type == self::SUPPLIER) {
                event(new SupplierInvoiceCreatedOrUpdated($invoice));
            }
        } catch (ServiceException | Exception $e) {
		    throw $e;
    	}
    	return $invoice;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Invoice $invoice)
    {
        $type = $invoice->type;

        try {
            if ($invoice->transactions->isEmpty() && $invoice->creditNotes->isEmpty()) {
                if ($type == self::SUPPLIER) {
                    // decrease the quantity of each item's inventory while delete the items of the invoice.
                    $invoice->items()->each(function($invoiceItem) {
                        $invoiceItem->inventory->quantity = $invoiceItem->inventory->quantity - $invoiceItem->quantity;
                        $invoiceItem->inventory->save();
                    });
                    $invoice->conceptItems()->delete();
                }
                if ($type == self::SALE) {
                    // increase the quantity of each item's inventory while delete the items of the invoice.
                    $invoice->items()->each(function($invoiceItem) {
                        $invoiceItem->inventory->quantity = $invoiceItem->inventory->quantity + $invoiceItem->quantity;
                        $invoiceItem->inventory->save();
                    });
                    $invoice->items()->delete();            
                }
                $invoice->delete();
            } else {
            	throw new ServiceException(trans('messages.delete_prohibited'), 403);
            }
        } catch (ServiceException | Exception $e) {
		    throw $e;
    	}
    }

    /**
     * Void a created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function void(Request $request, Invoice $invoice)
    {
        try {
            if (!$invoice->is_void) {
                if ($invoice->transactions->isEmpty() && $invoice->creditNotes->isEmpty()) {
                    $invoice->is_void = 1;
                    // increase the quantity of each item's inventory while delete the items of the invoice.
                    // todo: do the same for recurring type invoice.
                    $invoice->items()->each(function($invoiceItem) {
                        $invoiceItem->inventory->quantity = $invoiceItem->inventory->quantity + $invoiceItem->quantity;
                        $invoiceItem->inventory->save();
                    });
                    $invoice->save();
                    $invoice->accountingEntries()->delete();
                } else {
                    if ($invoice->type == self::SUPPLIER) {
                    	throw new ServiceException(trans('messages.supplier_invoice_void_prohibited'), 403);
                    } else {
                    	throw new ServiceException(trans('messages.invoice_void_prohibited'), 403);
                    }
                }
            } else {
            	throw new ServiceException(trans('messages.invoice_already_void'), 400);
            }
        } catch (ServiceException | Exception $e) {
		    throw $e;
    	}
    }
}