<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discount extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'discounts';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        /* static::deleted(function($tax) {
            $tax->inventories()->each(function($inventory) {
                $inventory->tax()->dissociate();
                $inventory->save();
            });
        }); */
    }

    /**
     * Get the company associated with the tax.
     */
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    /* public function getTypeAttribute()
    {
        return ucwords(str_replace('_', ' ', $this->attributes['type'])); 
    }

    public function getFrequencyTypeAttribute()
    {
        return ucwords(str_replace('_', ' ', $this->attributes['frequency_type'])); 
    }

    public function getIncomesAttribute()
    {
        return ucwords(str_replace('_', ' ', $this->attributes['incomes'])); 
    } */
}
