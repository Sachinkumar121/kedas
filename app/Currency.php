<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Currency extends Model
{
    use HasTranslations;

    public $translatable = ['name'];

    public $timestamps = false;
    
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['formatted_name'];
    
    public function getFormattedNameAttribute() {
        $name = $this->getTranslation('name', app()->getLocale());
        return $name.' - '.$this->attributes['code'].' ('.$this->attributes['symbol'].')';
    }

    /**
     * Set the Currency's code.
     *
     * @param  string  $value
     * @return void
     */
    public function setCodeAttribute($value) {
        $this->attributes['code'] = strtoupper($value);
    }

}
