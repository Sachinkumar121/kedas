<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Address as AddressResource;
use App\Http\Resources\Sector as SectorResource;
use App\Http\Resources\IdentificationType as IdentificationTypeResource;

class Supplier extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'mobile' => $this->mobile,
            // 'fax' => $this->fax,
            'company_name' => $this->company_name,
            'tax_id' => $this->tax_id,
            'contact_custom_id' => $this->contact_custom_id,

            'contact_name' => $this->contact_name,
            'contact_phone_number' => $this->contact_phone_number,

            'sector' => new SectorResource($this->sector),
            'identification_type' => new IdentificationType($this->identificationInfo),
            'address' => new AddressResource($this->address),

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
