<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class Document extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'uploaded_by' => $this->uploaded_by ? new UserResource($this->owner) : null,
            'type' => $this->type,
            'url' => $this->url ? asset($this->url): null,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
