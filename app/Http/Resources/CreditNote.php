<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Document as DocumentResource;
use App\Http\Resources\Item as ItemResource;
use App\Http\Resources\Client as ClientResource;
use App\Http\Resources\Supplier as SupplierResource;


class CreditNote extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'contact' => $this->contact,
            // 'contact' => $this->contact instanceof Supplier ? new SupplierResource($this->contact) : new ClientResource($this->contact),
            'start_date' => dateFormat($this->start_date),
            'notes' => $this->notes,

            'ncf' => $this->ncf,
            'ncf_number' => $this->ncf_number,

            'calculation' => $this->calculation,
            'currency' => $this->currency,
            'exchange_currency_rate' => $this->exchange_currency_rate,

            'documents' => DocumentResource::collection($this->documents),
            'items' => ItemResource::collection($this->items),

            'invoices' => $this->invoices,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
