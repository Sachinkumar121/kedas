<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Inventory as InventoryResource;
use App\Http\Resources\Tax as TaxResource;

class Item extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'inventory' => new InventoryResource($this->inventory),
            'discount' => $this->discount,
            'quantity' => $this->quantity,
            'current_quantity' => $this->inventory_adjustment_type == "increase" ? ($this->inventory->quantity - $this->quantity) : ($this->inventory->quantity + $this->quantity),
            'reference' => $this->reference,
            'description' => $this->description,
            'tax' => new TaxResource($this->tax),
            'price' => $this->price,
            'unit_cost' => $this->unit_cost,
            'inventory_adjustment_type' => $this->inventory_adjustment_type,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
