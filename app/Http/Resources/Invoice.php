<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Document as DocumentResource;
use App\Http\Resources\Item as ItemResource;
use App\Http\Resources\ConceptItem as ConceptItemResource;
use App\Http\Resources\Client as ClientResource;
use App\Http\Resources\Supplier as SupplierResource;
use Config;

class Invoice extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'internal_number' => $this->internal_number,
            'contact' => $this->contact,
            // 'contact' => $this->contact instanceof Supplier ? new SupplierResource($this->contact) : new ClientResource($this->contact),

            'start_date' => $this->start_date,
            'term' => $this->term ? (Config::get('constants.terms')[$this->term].($this->term == 'manual' ? '('.$this->manual_days.' '.trans('labels.days').')' : '')) : null,
            'end_date' => $this->end_date,
            'frequency' => $this->frequency,
            'frequency_type' => $this->frequency_type,
            'expiration_date' => $this->expiration_date,
            'calculation' => $this->calculation,
            'term_in_locale' => $this->term_in_locale,
            'notes' => $this->notes,
            'tac' => $this->tac,
            'privacy_policy' => $this->privacy_policy,

            'documents' => DocumentResource::collection($this->documents),
            'items' => ItemResource::collection($this->items),
            'concept_items' => ConceptItemResource::collection($this->conceptItems),

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
