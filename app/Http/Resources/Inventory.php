<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ItemCategory as ItemCategoryResource;
use App\Http\Resources\Tax as TaxResource;

class Inventory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'reference' => $this->reference,
            'description' => $this->description,
            'barcode' => $this->barcode,
            'sku' => $this->sku,
            'quantity' => $this->quantity,
            'init_quantity_date' => $this->init_quantity_date,
            'init_quantity' => $this->init_quantity,
            'memberships' => $this->memberships ? $this->memberships->pluck('id') : null,

            // 'item_category_id' => $this->item_category_id,
            'item_category' => new ItemCategoryResource($this->itemCategory),
            // 'tax_id' => $this->tax_id,
            'tax' => new TaxResource($this->tax),
            'type_of_item' => $this->type_of_item,
            // 'image' => $this->image,
            'image_url' => $this->image ? asset($this->image_url): null,
            'sale_price' => $this->sale_price,
            'unit_cost' => $this->unit_cost,
            'cost' => $this->cost,
            'track_inventory' => (bool)$this->track_inventory,
            'unit_cost' => $this->unit_cost,

            'asset_account' => $this->asset_account,
            'income_account' => $this->income_account,
            'expense_account' => $this->expense_account,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
