<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Address as AddressResource;
use App\Http\Resources\Sector as SectorResource;
use App\Http\Resources\Currency as CurrencyResource;

class Company extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'role' => $this->role,
            'tax_id' => $this->tax_id,

            'logo' => $this->logo ? asset($this->logo): null,
            'signature' => $this->signature ? asset($this->signature): null,

            'support_email' => $this->support_email,
            'phone' => $this->phone,
            'mobile' => $this->mobile,
            'fax' => $this->fax,

            'sector_id' => $this->sector_id,
            'currency_id' => $this->currency_id,

            'currency' => new CurrencyResource($this->currency),
            'sector' => new SectorResource($this->sector),
            'address' => new AddressResource($this->address),
        ];
    }
}
