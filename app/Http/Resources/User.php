<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Company as CompanyResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'user_type' => $this->user_type,
            'language' => $this->language,

            'is_profile_completed' => (bool)$this->company,

            // 'company' => new CompanyResource($this->company),

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
