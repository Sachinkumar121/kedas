<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponse;
use Closure;
use Auth;

class VerifyEmailToken
{
    use ApiResponse;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->email_token !== NULL && Auth::user()->email_verified_at == NULL) {
            return $this->failResponse([
                "message" => trans('messages.account_not_verified'),
            ], 422);
        }
        return $next($request);
    }
}
