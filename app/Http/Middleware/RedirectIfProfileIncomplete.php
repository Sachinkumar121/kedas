<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfProfileIncomplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->isSuperAdmin()) {
            return redirect('/admin/dashboard');
        }

        if (Auth::check() && !$request->user()->company) {
            $data = [
                'status' => false,
                'message' => trans('messages.profile_not_completed'),
                'messages' => []
            ];
            return $request->expectsJson()
                    ? response()->json($data, 403)
                    // ? abort(403, trans('messages.profile_not_completed'))
                    : redirect(RouteServiceProvider::DASHBOARD);
        }
        return $next($request);
    }
}
