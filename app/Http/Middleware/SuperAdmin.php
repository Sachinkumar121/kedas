<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Auth;

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->isSuperAdmin()) {
            return $next($request);
        } else {
            $data = [
                'status' => false,
                'message' => trans('messages.something_went_wrong'),
                'messages' => []
            ];
            return $request->expectsJson()
                    ? response()->json($data, 403)
                    : redirect(RouteServiceProvider::DASHBOARD);
        }

    }
}
