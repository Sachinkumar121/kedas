<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponse;
use Closure;
use Auth;

class VerifyProfileUpdate
{
    use ApiResponse;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // preprint(Auth::user()->addresses()->count());
        if (Auth::user()->addresses()->count() <= 0) {
            return $this->failResponse([
                "message" => trans('messages.update_profile'),
            ], 422);
        }
        return $next($request);
    }
}
