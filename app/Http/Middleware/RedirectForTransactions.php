<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Auth;

class RedirectForTransactions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $transaction_type = $request->route('type') ?? 'in';

        if (Auth::user()->isSuperAdmin() || Auth::user()->isCompanyOwner()
            || ($transaction_type == 'in' && Auth::user()->can('manage sales'))
            || (($transaction_type == 'out' || $transaction_type == 'recurring') && Auth::user()->can('manage expenses'))
        ) {
            return $next($request);
        } else {
            return redirect(RouteServiceProvider::DASHBOARD);
        }

    }
}
