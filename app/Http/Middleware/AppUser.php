<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponse;
use Closure;
use Auth;

class AppUser
{
    use ApiResponse;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->user_role == 'app_user') {
            return $next($request);
        } else {
            return $this->failResponse([
                "message" => trans('messages.account_access'),
            ], 403);
        }
    }
}
