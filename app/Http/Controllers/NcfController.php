<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\NcfValidator;

use App\Ncf;

use DB;
use Auth;
use Config;

use App\Repository\NcfRepositoryInterface;

class NcfController extends Controller
{
    public function __construct(NcfRepositoryInterface $ncfRepository)
    {
        $this->ncfRepository = $ncfRepository;
        $this->authorizeResource(Ncf::class, 'ncf');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $company = $user->company;

        $ncfs = $this->ncfRepository->findforCompany($company);

        if ($request->wantsJson() || $request->ajax()) {
            return response()->json(['data' => $ncfs]);
        }
        return view('ncfs.index', compact('ncfs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('ncfs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, NcfValidator $ncfValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$ncfValidator->with($input)->passes()) {
                $request->session()->flash('error', $ncfValidator->getErrors()[0]);
                return back()
                    ->withErrors($ncfValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();
            $company = $user->company;

            $data = [
                'name' => $input['name'],
                'initial_number' => $input['ncf_number'],
                'description' => $input['description'],
                'user_id' => $user->id
            ];

            // $ncf = $user->company->ncfs()->create($data);
            $ncf = $this->ncfRepository->create($company, $data);

            DB::commit();

            $request->session()->flash('success', trans('messages.ncf_created'));
            return redirect()->route('ncfs.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ncf  $ncf
     * @return \Illuminate\Http\Response
     */
    public function show(Ncf $ncf)
    { 
        return view('ncfs.show', compact('ncf'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ncf  $ncf
     * @return \Illuminate\Http\Response
     */
    public function edit(Ncf $ncf)
    {
        $resource_associated = $ncf->invoices->isNotEmpty();
        return view('ncfs.edit', compact('ncf', 'resource_associated'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ncf  $ncf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ncf $ncf, NcfValidator $ncfValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $resource_associated = $ncf->invoices->isNotEmpty();

            if (!$ncfValidator->with($input)->passes()) {
                $request->session()->flash('error', $ncfValidator->getErrors()[0]);
                return back()
                    ->withErrors($ncfValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'initial_number' => $resource_associated ? $ncf->initial_number : $input['ncf_number'],
                'description' => $input['description']
            ];

            $this->ncfRepository->update($ncf, $data);

            DB::commit();

            $request->session()->flash('success', trans('messages.ncf_updated'));
            return redirect()->route('ncfs.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ncf  $ncf
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Ncf $ncf)
    {
        try {
            if ($ncf->invoices->isEmpty()) {
                $ncf->delete();
                $request->session()->flash('success', trans('messages.ncf_deleted'));
            } else {
                $request->session()->flash('error', trans('messages.delete_prohibited'));
            }
            return redirect()->route('ncfs.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
