<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\LeaveApplication;
use App\Validators\LeaveApplicationValidator;

use DB;

class LeaveApplicationController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(LeaveApplication::class, 'leave_application');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $leaveApplications = LeaveApplication::all();
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $leaveApplications->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'leave_type' => $item->leaveType->name,
                    'request_duration' => dateFormat($item->application_from_date)." ".trans('labels.to')." ". dateFormat($item->application_to_date)."<br/><span class='text-muted'>".trans('labels.application_date').": ".dateFormat($item->created_at)."</span>",
                    'to_date' => $item->application_to_date,
                    'number_of_days' => $item->number_of_days,
                    'status' => $item->status_label,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('leave-applications.index', compact('leaveApplications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $leave_type_options = ['' => trans('labels.select_leave_type')] + $user->company->leaveTypes->pluck('name', 'id')->toArray();
        return view('leave-applications.create', compact('leave_type_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, LeaveApplicationValidator $leaveApplicationValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$leaveApplicationValidator->with($input)->passes()) {
                $request->session()->flash('error', $leaveApplicationValidator->getErrors()[0]);
                return back()
                    ->withErrors($leaveApplicationValidator->getValidator())
                    ->withInput();
            }

            if ($input['number_of_days'] > $input['current_balance']) {
                $request->session()->flash('error', trans('messages.invalid_number_of_days'));
                return back()->withInput();
            }

            $user = $request->user();

            $data = [
                'leave_type_id' => $input['leave_type_id'],
                'application_from_date' => $input['from_date'],
                'application_to_date' => $input['to_date'],
                'number_of_days' => $input['number_of_days'],
                'employee_id' => $user->employee_id,
                'purpose' => $input['purpose'],
                'user_id' => $user->id,
            ];

            $leaveApplication = $user->company->leaveApplications()->create($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.leave_application_created'));
            return redirect()->route('leave-applications.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LeaveApplication  $leaveApplication
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, LeaveApplication $leaveApplication)
    {
        try {
            $leaveApplication->delete();
            $request->session()->flash('success', trans('messages.item_category_deleted'));
            return redirect()->route('leave-applications.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function requested_applications(Request $request)
    {
        $user = $request->user();
        $leaveApplications = $user->company->leaveApplications;
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $leaveApplications->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'leave_type' => $item->leaveType->name,
                    'request_duration' => dateFormat($item->application_from_date)." ".trans('labels.to')." ". dateFormat($item->application_to_date)."<br/><span class='text-muted'>".trans('labels.application_date').": ".dateFormat($item->created_at)."</span>",
                    'to_date' => $item->application_to_date,
                    'number_of_days' => $item->number_of_days,
                    'status' => $item->status_label,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('leave-applications.requested', compact('leaveApplications'));
    }
}
