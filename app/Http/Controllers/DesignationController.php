<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Designation;
use App\Validators\DesignationValidator;

use DB;

class DesignationController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Designation::class, 'designation');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $designations = $user->company->designations;

        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $designations->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'name' => $item->name,
                    'custom_name' => $item->custom_name,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('designations.index', compact('designations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('designations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, DesignationValidator $designationValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$designationValidator->with($input)->passes()) {
                $request->session()->flash('error', $designationValidator->getErrors()[0]);
                return back()
                    ->withErrors($designationValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $designation = new designation(['name' => $input['name'], 'user_id' => $user->id]);
            $designation->setTranslation('custom_name', 'en', $input['name'])->setTranslation('custom_name', 'es', $input['name']);
            $user->company->designations()->save($designation);

            DB::commit();

            $request->session()->flash('success', trans('messages.designation_created'));
            return redirect()->route('designations.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function edit(Designation $designation)
    {
        return view('designations.edit', compact('designation'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Designation  $tax
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Designation $designation)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $locale = app()->getLocale();
            
            $designationValidator = new DesignationValidator('update', $designation);
            if (!$designationValidator->with($input)->passes()) {
                $request->session()->flash('error', $designationValidator->getErrors()[0]);
                return back()
                    ->withErrors($designationValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $designation->setTranslation('custom_name', $locale, $input['name']);
            $designation->save();

            DB::commit();

            $request->session()->flash('success', trans('messages.designation_updated'));
            return redirect()->route('designations.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Designation $designation)
    {
        try {
            if ($designation->employees->isEmpty()) {
                $designation->delete();
                $request->session()->flash('success', trans('messages.designation_deleted'));
            } else {
                $request->session()->flash('error', trans('messages.delete_prohibited'));
            }
            return redirect()->route('designations.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
