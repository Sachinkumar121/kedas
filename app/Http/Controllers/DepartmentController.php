<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Department;
use App\Validators\DepartmentValidator;

use DB;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Department::class, 'department');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $departments = $user->company->departments;
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $departments->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'name' => $item->name,
                    'custom_name' => $item->custom_name,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('departments.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, DepartmentValidator $departmentValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$departmentValidator->with($input)->passes()) {
                $request->session()->flash('error', $departmentValidator->getErrors()[0]);
                return back()
                    ->withErrors($departmentValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $department = new Department(['name' => $input['name'], 'user_id' => $user->id]);
            $department->setTranslation('custom_name', 'en', $input['name'])->setTranslation('custom_name', 'es', $input['name']);
            $user->company->departments()->save($department);

            DB::commit();

            $request->session()->flash('success', trans('messages.department_created'));
            return redirect()->route('departments.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        return view('departments.edit', compact('department'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $tax
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $locale = app()->getLocale();
            
            $departmentValidator = new DepartmentValidator('update', $department);
            if (!$departmentValidator->with($input)->passes()) {
                $request->session()->flash('error', $departmentValidator->getErrors()[0]);
                return back()
                    ->withErrors($departmentValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $department->setTranslation('custom_name', $locale, $input['name']);
            $department->save();

            DB::commit();

            $request->session()->flash('success', trans('messages.department_updated'));
            return redirect()->route('departments.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Department $department)
    {
        try {
            if ($department->employees->isEmpty()) {
                $department->delete();
                $request->session()->flash('success', trans('messages.department_deleted'));
            } else {
                $request->session()->flash('error', trans('messages.delete_prohibited'));
            }
            return redirect()->route('departments.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
