<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\Discount;
use App\Validators\DiscountValidator;

use Config;

class DiscountController extends Controller
{
    public function __construct()
    {
        //$this->authorizeResource(Tax::class, 'tax');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $discounts = $user->company->discounts;
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $discounts->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'sr_no' => $key + 1,
                    'employee' => $item->employee,
                    'type' => $item->type,
                    'start_date' => $item->start_date,
                    'end_date' => $item->end_date,
                    'frequency' => $item->frequency,
                    'frequency_type' => $item->frequency_type,
                    'observation' => $item->observation,
                    'amount' => $item->amount,
                    'id' => $item->id,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('discounts.index', compact('discounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $employee_options = ['' => trans('labels.select_employee')] + $user->company->employees->pluck('name', 'id')->toArray();
        $type_option = ['' => trans('labels.select_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.discount_type_option'));
        $frequency_type_option = ['' => trans('labels.select_frequency_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_frequency_type_option'));
        $incomes_option = ['' => trans('labels.select_incomes_option')] + $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_incomes_option'));
        return view('discounts.create', compact('employee_options', 'type_option', 'frequency_type_option', 'incomes_option'));
        return view('discounts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, DiscountValidator $discountValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$discountValidator->with($input)->passes()) {
                $request->session()->flash('error', $discountValidator->getErrors()[0]);
                return back()
                    ->withErrors($discountValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'employee_id' => $input['employee_id'],
                'type' => $input['type'],
                'start_date' => $input['start_date'],
                'end_date' => $input['end_date'],
                'observation' => $input['observation'],
                'recurring_type' => $input['recurring_type'],
                'frequency_type' => $input['frequency_type'],
                'frequency' => $input['frequency'],
                'amount' => $input['amount'],
                'incomes' => $input['incomes'],
                'user_id' => $user->id
            ];

            $discounts = $user->company->discounts()->create($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.discount_created'));
            return redirect()->route('discounts.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Benefit  $benefit
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Discount $discount)
    {
        $user = $request->user();
        $employee_options = ['' => trans('labels.select_employee')] + $user->company->employees->pluck('name', 'id')->toArray();
        $type_option = ['' => trans('labels.select_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.discount_type_option'));
        $frequency_type_option = ['' => trans('labels.select_frequency_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_frequency_type_option'));
        $incomes_option = ['' => trans('labels.select_incomes_option')] + $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_incomes_option'));
        return view('discounts.edit', compact('discount', 'employee_options', 'type_option', 'frequency_type_option', 'incomes_option'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Discount $discount, DiscountValidator $discountValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            
            if (!$discountValidator->with($input)->passes()) {
                $request->session()->flash('error', $discountValidator->getErrors()[0]);
                return back()
                    ->withErrors($discountValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'employee_id' => $input['employee_id'],
                'type' => $input['type'],
                'start_date' => $input['start_date'],
                'end_date' => $input['end_date'],
                'observation' => $input['observation'],
                'recurring_type' => $input['recurring_type'],
                'frequency_type' => $input['frequency_type'],
                'frequency' => $input['frequency'],
                'amount' => $input['amount'],
                'incomes' => $input['incomes'],
                'user_id' => $user->id
            ];

            $discount->update($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.discount_updated'));
            return redirect()->route('discounts.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,  Discount $discount)
    {
        try {
            $discount->delete();
            $request->session()->flash('success', trans('message.discount_deleted'));
            return redirect()->route('discounts.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
