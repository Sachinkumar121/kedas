<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\Membership;
use App\Validators\MembershipValidator;

class MembershipController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Membership::class, 'membership');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $memberships = $user->company->memberships;

        if ($request->wantsJson() || $request->ajax()) {
            return response()->json(['data' => $memberships]);
        }
        return view('memberships.index', compact('memberships'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('memberships.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, MembershipValidator $membershipValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$membershipValidator->with($input)->passes()) {
                $request->session()->flash('error', $membershipValidator->getErrors()[0]);
                return back()
                    ->withErrors($membershipValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'description' => $input['description'] ?? null,
                'discount' => $input['discount'] ?? null,
                'point_percentage' => $input['point_percentage'] ?? null,
                'created_date' => $input['created_date'],
                'point_expire_date' => $input['point_expire_date'] ?? null,
                'user_id' => $user->id
            ];

            $membership = $user->company->memberships()->create($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.membership_created'));
            return redirect()->route('memberships.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Membership $membership)
    {        
        return view('memberships.edit', compact('membership'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Membership $membership, MembershipValidator $membershipValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$membershipValidator->with($input)->passes()) {
                $request->session()->flash('error', $membershipValidator->getErrors()[0]);
                return back()
                    ->withErrors($membershipValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'description' => $input['description'] ?? null,
                'discount' => $input['discount'] ?? null,
                'point_percentage' => $input['point_percentage'] ?? null,
                'created_date' => $input['created_date'],
                'point_expire_date' => $input['point_expire_date'],
                'user_id' => $user->id
            ];

            $membership->update($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.membership_updated'));
            return redirect()->route('memberships.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Membership $membership)
    {
        try {
            $membership->delete();
            $request->session()->flash('success', trans('messages.membership_deleted'));
            return redirect()->route('memberships.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
