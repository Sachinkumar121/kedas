<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Interfaces\CommonConstants;


class Controller extends BaseController implements CommonConstants
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function mapOptionsBasedOnLocale($options = [], $fileName = 'constants') {
		return collect($options)->map(function($option, $key) use ($fileName) {
            return trans($fileName.'.'.$key);
        })->toArray();
	}

	/**
     * Recursive function to set level for each account head.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generateAccountsLevels($child, &$account_control_options, $level = 1, $include_me = false) {
    	if ($include_me) {
    		$child->level = $level;
            $account_control_options->push($child);
            $level += 1;
            $this->generateAccountsLevels($child, $account_control_options, $level);
    	} else {
	        if ($child->children->isNotEmpty()) {
	            $child->children->each(function($subChild) use ($account_control_options, $level) {
	                $subChild->level = $level;
	                $account_control_options->push($subChild);
	                $level += 1;
	                $this->generateAccountsLevels($subChild, $account_control_options, $level);
	            });
	        }
    	}
    }
}
