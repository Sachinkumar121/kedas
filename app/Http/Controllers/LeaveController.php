<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\Leave;
use App\Validators\LeaveValidator;

use Config;

class LeaveController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(LeaveType::class, 'leave');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $leaves = $user->company->leave;
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $leaves->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'leave_type' => $item->leave_type,
                    'employee' => $item->employee->name,
                    'from_date' => $item->application_from_date,
                    'to_date' => $item->application_to_date,
                    'number_of_days' => $item->number_of_days,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('leaves.index', compact('leaves'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $leave_type_options =  ['' => trans('labels.select_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.leave_type_option'));
        $employee_options = ['' => trans('labels.select_employee')] + $user->company->employees->pluck('name', 'id')->toArray();
        return view('leaves.create', compact('leave_type_options', 'employee_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, LeaveValidator $leaveValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$leaveValidator->with($input)->passes()) {
                $request->session()->flash('error', $leaveValidator->getErrors()[0]);
                return back()
                    ->withErrors($leaveValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'employee_id' => $input['employee_id'],
                'leave_type' => $input['leave_type'],
                'application_from_date' => $input['from_date'],
                'application_to_date' => $input['to_date'],
                'number_of_days' => $input['number_of_days'],
                'note' => $input['note'],
                'is_paid' => $input['is_paid'],
                'user_id' => $user->id,
            ];

            $leave = $user->company->leave()->create($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.leave_created'));
            return redirect()->route('leaves.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */

    public function edit(Request $request, Leave $leave)
    {
        $user = $request->user();
        $employee_options = ['' => trans('labels.select_employee')] + $user->company->employees->pluck('name', 'id')->toArray();
        $leave_type_options =  ['' => trans('labels.select_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.leave_type_option'));
        return view('leaves.edit', compact('leave', 'employee_options', 'leave_type_options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LeaveType  $leaveType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Leave $leave, LeaveValidator $leaveValidator)
    {
       try {
            DB::beginTransaction();
            $input = $request->all();
            
            if (!$leaveValidator->with($input)->passes()) {
                $request->session()->flash('error', $leaveValidator->getErrors()[0]);
                return back()
                    ->withErrors($leaveValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'employee_id' => $input['employee_id'],
                'leave_type' => $input['leave_type'],
                'application_from_date' => $input['from_date'],
                'application_to_date' => $input['to_date'],
                'number_of_days' => $input['number_of_days'],
                'note' => $input['note'],
                'is_paid' => $input['is_paid'],
                'user_id' => $user->id,
            ];

            $leave->update($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.leave_updated'));
            return redirect()->route('leaves.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Benefit  $benefit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Leave $leave)
    {
        try {
            $leave->delete();
            $request->session()->flash('success', trans('messages.leave_deleted'));
            return redirect()->route('leaves.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
