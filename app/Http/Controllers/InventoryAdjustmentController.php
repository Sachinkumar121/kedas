<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\InventoryAdjustment;
use App\Item;
use App\Inventory;
use App\Validators\InventoryAdjustmentValidator;

use App\Events\InventoryAdjustmentCreatedOrUpdated;

use DB;
use File;

class InventoryAdjustmentController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(InventoryAdjustment::class, 'inventory_adjustment');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $user = $request->user();
        $inventoryAdjustments = $user->company->inventoryAdjustments;
        
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $inventoryAdjustments->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'sr_no' => $key + 1,
                    'id' => $item->id,
                    'adjustment_date' => $item->adjustment_date,
                    'observations' => $item->observations,
                    'total' => $item->total,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('inventory-adjustments.index', compact('inventoryAdjustments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();

        $inventories = $user->company->inventories->where('track_inventory', 1)->values();

        if ($inventories->isEmpty()) {
            $request->session()->flash('error', trans('messages.inventory_adjustment_without_inventory'));
            return redirect('dashboard');
        }

        return view('inventory-adjustments.create', compact('inventories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, InventoryAdjustmentValidator $inventoryAdjustmentValidator)
    {
        try {
            DB::beginTransaction();
            $allData = $request->all();
            $input = json_decode($allData['data'],true);
            
            if (!$inventoryAdjustmentValidator->with($input)->passes()) {
                return response()->json([
                    'error' => $inventoryAdjustmentValidator->getErrors()[0],
                    'errors' => $inventoryAdjustmentValidator->getMessageBag()->toArray(),
                    'success' => false
                ], 422);
            }

            $user = $request->user();

            $inventoryAdjustmentItems = collect($input['adjustment_items'])->filter(function($row) {
                return $row['qty'] && $row['item_id'] && $row['unit_price'] && $row['unit_price'] > 0;
                })->map(function($item) {
                    $item['inventory_id'] = $item['item_id'];
                    $item['quantity'] = $item['qty'];
                    $item['inventory_adjustment_type'] = $item['adj_type'];
                    $item['unit_cost'] = $item['unit_price'];
                    unset($item['current_qty']);
                    unset($item['final_qty']);
                    unset($item['adj_total']);
                    unset($item['adj_type']);
                    unset($item['item_id']);
                    unset($item['qty']);
                    unset($item['unit_price']);
                    return $item;
            })->values()->toArray(); 

            if (!$inventoryAdjustmentItems) {
                return response()->json([
                    'error' => trans('messages.inventory_adjustment_without_items'),
                    'errors' => [trans('messages.inventory_adjustment_without_items')],
                    'success' => false
                ], 422);
            }

            $inventoryAdjustment = $user->inventoryAdjustments()->create([
                'company_id' => $user->company->id,
                'adjustment_date' => $input['adjustment_date'],
                'observations' => $input['observations'] ?? null
            ]);


            $inventoryAdjustment->items()->createMany($inventoryAdjustmentItems);

            // Update Inventory items //
            /*foreach ($inventoryAdjustment->items as $single_item) {
                if ($single_item->inventory_adjustment_type == 'increase') {
                    $single_item->inventory->increment('quantity', $single_item->quantity);
                } else{
                    $single_item->inventory->decrement('quantity', $single_item->quantity);
                }
            }*/

            foreach ($inventoryAdjustment->items as $item) {
                $item->inventory->quantity = $item->inventory->quantity + ($item->inventory_adjustment_type == 'increase' ? $item->quantity : (-$item->quantity));
                $item->inventory->save();
            }

            /* upload documents */
            if (isset($allData['documents']) && $allData['documents']) {
                $files = $allData['documents'];

                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($files as $file) {
                    if(File::exists(storage_path('tmp/'.$file))) {
                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/adjustment-'.$file));
                        $documents_data[] = [
                            'name' => 'adjustment-'.$file,
                        ];
                    }
                }

                $documents = $inventoryAdjustment->documents()->createMany($documents_data);
            }
            
            event(new InventoryAdjustmentCreatedOrUpdated($inventoryAdjustment));

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => trans('messages.inventory_adjustment_created'),
                'redirect_url' => 'inventory-adjustments'
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => $e->getMessage(),
                'errors' => [$e->getMessage()],
                'success' => false
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(InventoryAdjustment $inventoryAdjustment)
    {
        return view('inventory-adjustments.show', compact('inventoryAdjustment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, InventoryAdjustment $inventoryAdjustment)
    {
        $user = $request->user();
        $inventories = $user->company->inventories->where('track_inventory', 1)->values();
        return view('inventory-adjustments.edit', compact('inventoryAdjustment', 'inventories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InventoryAdjustment $inventoryAdjustment)
    {
        $inventoryAdjustmentValidator = new InventoryAdjustmentValidator('update');
        
        try {
            DB::beginTransaction();
            $allData = $request->all();
            $input = json_decode($allData['data'],true);
            
            if (!$inventoryAdjustmentValidator->with($input)->passes()) {
                return response()->json([
                    'error' => $inventoryAdjustmentValidator->getErrors()[0],
                    'errors' => $inventoryAdjustmentValidator->getMessageBag()->toArray(),
                    'success' => false
                ], 422);
            }
            
            $user = $request->user();

            $inventoryAdjustmentItems = collect($input['adjustment_items'])->filter(function($row) {
                    return $row['qty'] && $row['item_id'] && $row['unit_price'] && $row['unit_price'] > 0;
                })->map(function($item) {
                    $item['inventory_id'] = $item['item_id'];
                    $item['quantity'] = $item['qty'];
                    $item['inventory_adjustment_type'] = $item['adj_type'];
                    $item['unit_cost'] = $item['unit_price'];
                    unset($item['current_qty']);
                    unset($item['final_qty']);
                    unset($item['adj_total']);
                    unset($item['adj_type']);
                    unset($item['item_id']);
                    unset($item['qty']);
                    unset($item['unit_price']);
                    return $item;
            })->values(); 


            if ($inventoryAdjustmentItems->isEmpty()) {
                return response()->json([
                    'error' => trans('messages.inventory_adjustment_without_items'),
                    'errors' => [trans('messages.inventory_adjustment_without_items')],
                    'success' => false
                ], 422);
            }


            $updateInventoryItems = $inventoryAdjustmentItems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertInventoryItems = $inventoryAdjustmentItems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deleteInventoryItemsExcept = $updateInventoryItems->pluck('id')->toArray();
            
            $inventoryAdjustment->update([
                'adjustment_date' => $input['adjustment_date'],
                'observations' => $input['observations'] ?? null
            ]);

            if ($deleteInventoryItemsExcept) {
                $deleteAdjustmentItems =  $inventoryAdjustment->items()->whereNotIn('id', $deleteInventoryItemsExcept)->get();

                // Update inventory quantity on delete items //
                foreach ($deleteAdjustmentItems as $single_item) {
                    if ($single_item->inventory_adjustment_type == 'increase') {
                        $single_item->inventory->decrement('quantity', $single_item->quantity);
                    } else{
                        $single_item->inventory->increment('quantity', $single_item->quantity);
                    }
                }

                $inventoryAdjustment->items()->whereNotIn('id', $deleteInventoryItemsExcept)->delete();
            }

            if ($insertInventoryItems) {
                $inventoryAdjustment->items()->createMany($insertInventoryItems);

                // Update inventory quantity on add new items //
                foreach ($inventoryAdjustment->items as $item) {
                    $item->inventory->quantity = $item->inventory->quantity + ($item->inventory_adjustment_type == 'increase' ? $item->quantity : (-$item->quantity));
                    $item->inventory->save();
                }

            }

            // Manage inventory quantity //
            $updateInventoryQuantityItems = collect($input['adjustment_items'])->map(function($inventroy) {
                $inventroy['id'] = $inventroy['item_id'];
                $inventroy['quantity'] = $inventroy['adj_type']=="increase" ? $inventroy['current_qty'] + $inventroy['qty']  : $inventroy['current_qty'] - $inventroy['qty'];
                return $inventroy;
            })->values();

            if ($updateInventoryItems) {
                foreach ($updateInventoryItems as $updateInventoryItems) {
                    $updatedData = $updateInventoryItems;
                    unset($updatedData['id']);
                    Item::findOrFail($updateInventoryItems['id'])->update($updatedData);
                }
            }

            if ($updateInventoryQuantityItems) {
                // Update Current Quantity //
                foreach ($updateInventoryQuantityItems as $updateInventoryQuantityItems) {
                    $updatedData = $updateInventoryQuantityItems;
                    Inventory::findOrFail($updateInventoryQuantityItems['id'])->update($updatedData);
                }
            }

            /* update documents start */
            $documents = $request->input('documents', []);

            if ($inventoryAdjustment->documents->isNotEmpty()) {
                foreach ($inventoryAdjustment->documents as $document) {
                    if (!in_array($document->name, $documents)) {
                        $inventoryAdjustment->documents()->where('id', '=', $document->id)->delete();
                    }
                }
            }
        
            $alreadyExistDocuments = $inventoryAdjustment->documents->pluck('name')->toArray();

            $dir = public_path("storage/documents");

            foreach ($request->input('documents', []) as $file) {
            
                if ($alreadyExistDocuments || !in_array($file, $alreadyExistDocuments)) {
                    if (File::exists(storage_path('tmp/'.$file))) {
                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/adjustment-'.$file));
                        $document_data = array('name' => 'adjustment-'.$file);
                        $inventoryAdjustment->documents()->create($document_data);
                    }
                }
            }
            /* update documents end */

            DB::commit();
            
            event(new InventoryAdjustmentCreatedOrUpdated($inventoryAdjustment));

            return response()->json([
                'success' => true,
                'message' => trans('messages.inventory_adjustment_updated'),
                'redirect_url' => 'inventory-adjustments'
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, inventoryAdjustment $inventoryAdjustment)
    {
        try {
            // Update Inventory items while delete //
            $inventoryAdjustmentItems = $inventoryAdjustment->items;

            foreach ($inventoryAdjustment->items as $item) {
                $item->inventory->quantity = $item->inventory->quantity + ($item->inventory_adjustment_type == 'increase' ? (-$item->quantity) : ($item->quantity));
                $item->inventory->save();
            }
            $inventoryAdjustment->delete();
            $request->session()->flash('success', trans('messages.inventory_adjustment_deleted'));
            return redirect()->route('inventory-adjustments.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
