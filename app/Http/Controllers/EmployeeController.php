<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\Employee;
use App\Address;
use App\Validators\EmployeeValidator;
use Illuminate\Support\Facades\Hash;

use Config;
use App\User;
use Carbon\Carbon;
use Image;
use PDF;
use File;

class EmployeeController extends Controller
{
    public function __construct()
    {
        //$this->authorizeResource(Employee::class, 'employee');
        // $this->middleware('preventBackHistory');
        // $this->authorizeResource(Client::class, 'client', [
        //     'except' => [ 'index', 'show' ],
        // ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $company = $user->company;
        $employees = $company->employees;
      
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $employees->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'sr_no' => $key + 1,
                    'name' => $item->name,
                    'id_number' => $item->id_number,
                    'hiring_date' => $item->hiring_date,
                    'salary' => $item->salary,
                    'phone' => $item->phone,
                    'role' => $item->role,
                    'id' => $item->id,
                    'status' => $item->status,
                    'schedules' =>  timeFormat($item->start_time)." - ".timeFormat($item->end_time),
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $emp_status_options = ['' => trans('labels.select_status')] + $this->mapOptionsBasedOnLocale(Config::get('constants.emp_status'));
        $department_options = ['' => trans('labels.select_department')] + $user->company->departments->pluck('name', 'id')->toArray();
        $designation_options = ['' => trans('labels.select_designation')] + $user->company->designations->pluck('name', 'id')->toArray();
        $insurance_options = ['' => trans('labels.select_insurance_brand')] + $user->company->insuranceBrands->pluck('name', 'id')->toArray();
        $supervisor_options = ['' => trans('labels.select_supervisor')] + $user->company->employees->pluck('name', 'id')->toArray();
        $period_options = ['' => trans('labels.select_period')] + $this->mapOptionsBasedOnLocale(Config::get('constants.employee_period'));
        $contract_type_options = ['' => trans('labels.select_contract')] + $this->mapOptionsBasedOnLocale(Config::get('constants.contract_type'));
        $type_of_account_options = ['' => trans('labels.select_type_of_account')] + $this->mapOptionsBasedOnLocale(Config::get('constants.type_of_account'));
        $salary_type_options = ['' => trans('labels.select_salary_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.salary_type'));
        $roles = $user->company->roles;
        $role_options = ['' => trans('labels.select_role')] + $roles->pluck('custom_name', 'id')->toArray();
        return view('employees.create',compact('emp_status_options', 'department_options', 'designation_options', 'insurance_options', 'supervisor_options', 'role_options', 'period_options', 'contract_type_options', 'type_of_account_options', 'salary_type_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, EmployeeValidator $employeeValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $user = $request->user();

            if (!$employeeValidator->with($input)->passes()) {
                $request->session()->flash('error', $employeeValidator->getErrors()[0]);
                return back()
                    ->withErrors($employeeValidator->getValidator())
                    ->withInput();
            }
            $data = [
                'name' => $input['name'],
                'id_number' => $input['id_number'],
                'type_of_id' => $input['type_of_id'],
                'hiring_date' => $input['hiring_date'],
                'email' => $input['email'],
                'phone' => $input['phone'],
                'mobile' => $input['mobile'],
                'salary' => $input['salary'],
                'salary_type' => $input['salary_type'],
                'birth_date' => $input['birth_date'],
                'username' => $input['username'],
                // 'role' => $input['role'],
                'password' => Hash::make($input['password']),
                'user_id' => $user->id,
                'company_id' => $user->company->id,
                'department_id' => $input['department_id'],
                'insurance_brand_id' => $input['insurance_brand_id'],
                'period' => $input['period'],
                'contract_type' => $input['contract_type'],
                'bank_account' => $input['bank_account'],
                'bank_name' => $input['bank_name'],
                'type_of_account' => $input['type_of_account'],
                'note' => $input['note'],
                'hours_price' => $input['hours_price'],
                'driver_license' => $input['driver_license'],
                'blood_type' => $input['blood_type'],
                'break_time' => $input['break_time'],
                'start_time' => $input['start_time'],
                'end_time' => $input['end_time'],
                'status' => $input['status'],
                'monday' => (!empty($input['monday'])) ?: 0,
                'tuesday' => (!empty($input['tuesday'])) ?: 0,
                'wednesday' => (!empty($input['wednesday'])) ?: 0,
                'thursday' => (!empty($input['thursday'])) ?: 0,
                'friday' => (!empty($input['friday'])) ?: 0,
                'saturday' => (!empty($input['saturday'])) ?: 0,
                'sunday' => (!empty($input['sunday'])) ?: 0,
            ];

            $employee = Employee::create($data);

            // Store Employee Image //
            if (isset($input['image'])) {
                $filename = $employee->id.'-'.substr( md5( $employee->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/profile-pics");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $imageInstance = Image::make($input['image']);
                if ($imageInstance->height() > 300) {
                    $imageInstance->fit(300)->save($path);
                } else {
                    $imageInstance->orientate()->save($path);
                }
                $employee->image=$filename;
                $employee->save();
            }

            if ($input['address'] || $input['zip'] || $input['country'] || $input['state'] || $input['city']) {
                $addressData = [
                    'line_1' => $input['address'],
                    'zip' => $input['zip'],
                    'country_id' => $input['country'],
                    'state' => $input['state'],
                    'city' => $input['city'],
                ];
                $address = $employee->address()->create($addressData);
            }

            // Save Data in User Table //
            $role = $input['role'] ?? null;

            $companyUser = User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password'])
            ]);

            $companyUser->is_company_emp = true;
            $companyUser->email_verified_at = Carbon::now(); // as of now the email is verified after user is created.
            $companyUser->company_id = $user->company->id;
            $companyUser->user_role = self::COMPANY_USER; // set the new created user as company user.
            $companyUser->employee_id = $employee->id;
            $companyUser->save();

            $employee->related_user_id = $companyUser->id;
            $employee->save();

            if ($role) {
                $companyUser->assignRole($role);
            }

            /* upload documents */
            if (isset($input['documents']) && $input['documents']) {
                $files = $input['documents'];

                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($files as $file) {
                    if(File::exists(storage_path('tmp/'.$file))) {
                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/employee-'.$file));
                        $documents_data[] = [
                            'name' => 'employee-'.$file,
                        ];
                    }
                }

                $documents = $employee->documents()->createMany($documents_data);
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.employee_created'));
            return redirect()->route('employees.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('employees.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Employee $employee)
    {
        $user = $request->user();
        $emp_status_options = ['' => trans('labels.select_status')] + $this->mapOptionsBasedOnLocale(Config::get('constants.emp_status'));
        $department_options = ['' => trans('labels.select_department')] + $user->company->departments->pluck('name', 'id')->toArray();
        $designation_options = ['' => trans('labels.select_designation')] + $user->company->designations->pluck('name', 'id')->toArray();
        $insurance_options = ['' => trans('labels.select_insurance_brand')] + $user->company->insuranceBrands->pluck('name', 'id')->toArray();
        $supervisor_options = ['' => trans('labels.select_supervisor')] + $user->company->employees->pluck('name', 'id')->toArray();
        $period_options = ['' => trans('labels.select_period')] + $this->mapOptionsBasedOnLocale(Config::get('constants.employee_period'));
        $contract_type_options = ['' => trans('labels.select_contract')] + $this->mapOptionsBasedOnLocale(Config::get('constants.contract_type'));
        $type_of_account_options = ['' => trans('labels.select_type_of_account')] + $this->mapOptionsBasedOnLocale(Config::get('constants.type_of_account'));
        $salary_type_options = ['' => trans('labels.select_salary_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.salary_type'));
        $roles = $user->company->roles;
        $role_options = ['' => trans('labels.select_role')] + $roles->pluck('custom_name', 'id')->toArray();
        $assignedRole = $employee->user->role();

        return view('employees.edit',compact('emp_status_options', 'department_options', 'designation_options', 'insurance_options', 'supervisor_options', 'role_options', 'period_options', 'contract_type_options', 'type_of_account_options', 'salary_type_options', 'employee', 'assignedRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            $employeeValidator = new EmployeeValidator('update', $employee);

            if (!$employeeValidator->with($input)->passes()) {
                $request->session()->flash('error', $employeeValidator->getErrors()[0]);
                return back()
                    ->withErrors($employeeValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'id_number' => $input['id_number'],
                'type_of_id' => $input['type_of_id'],
                'hiring_date' => $input['hiring_date'],
                'email' => $input['email'],
                'phone' => $input['phone'],
                'mobile' => $input['mobile'],
                'salary' => $input['salary'],
                // 'role' => $input['role'],
                'salary_type' => $input['salary_type'],
                'birth_date' => $input['birth_date'],
                'username' => $input['username'],
                'password' => Hash::make($input['password']),
                'department_id' => $input['department_id'],
                'insurance_brand_id' => $input['insurance_brand_id'],
                'period' => $input['period'],
                'contract_type' => $input['contract_type'],
                'bank_account' => $input['bank_account'],
                'bank_name' => $input['bank_name'],
                'type_of_account' => $input['type_of_account'],
                'note' => $input['note'],
                'hours_price' => $input['hours_price'],
                'driver_license' => $input['driver_license'],
                'blood_type' => $input['blood_type'],
                'break_time' => $input['break_time'],
                'start_time' => $input['start_time'],
                'end_time' => $input['end_time'],
                'status' => $input['status'],
                'monday' => (!empty($input['monday'])) ?: 0,
                'tuesday' => (!empty($input['tuesday'])) ?: 0,
                'wednesday' => (!empty($input['wednesday'])) ?: 0,
                'thursday' => (!empty($input['thursday'])) ?: 0,
                'friday' => (!empty($input['friday'])) ?: 0,
                'saturday' => (!empty($input['saturday'])) ?: 0,
                'sunday' => (!empty($input['sunday'])) ?: 0,
            ];

            if (isset($input['password']) && $input['password']) {
                $data['password'] = Hash::make($input['password']);
            }

            if (isset($input['image'])) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/profile-pics");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $imageInstance = Image::make($input['image']);
                if ($imageInstance->height() > 300) {
                    $imageInstance->fit(300)->save($path);
                } else {
                    $imageInstance->orientate()->save($path);
                }
                $employee->image = $filename;
            }

            $employee->update($data);

            $companyUser = $employee->user;
            $companyUser->update([
                'name' => $input['name'],
                'email' => $input['email']
            ]);

            if (isset($input['password']) && $input['password']) {
                $companyUser->password = Hash::make($input['password']);
            }

            $companyUser->save();
            
            $companyUser->syncRoles($input['role']);

            if ($input['address'] || $input['zip'] || $input['country'] || $input['state'] || $input['city']) {
                $addressData = [
                    'line_1' => $input['address'],
                    'zip' => $input['zip'],
                    'country_id' => $input['country'],
                    'state' => $input['state'],
                    'city' => $input['city'],
                ];

                $employee->address()->updateOrCreate([], $addressData);
            }

             /* update documents start */
            $documents = $request->input('documents', []);

            if ($employee->documents->isNotEmpty()) {
                foreach ($employee->documents as $document) {
                    if (!in_array($document->name, $documents)) {
                        $employee->documents()->where('id', '=', $document->id)->delete();
                    }
                }
            }
        
            $alreadyExistDocuments = $employee->documents->pluck('name')->toArray();

            $dir = public_path("storage/documents");

            foreach ($request->input('documents', []) as $file) {
            
                if ($alreadyExistDocuments || !in_array($file, $alreadyExistDocuments)) {
                    if (File::exists(storage_path('tmp/'.$file))) {
                        File::move(storage_path('tmp/'.$file), public_path('storage/documents/employee-'.$file));
                        $document_data = array('name' => 'employee-'.$file);
                        $employee->documents()->create($document_data);
                    }
                }
            }
            /* update documents end */

            DB::commit();

            $request->session()->flash('success', trans('messages.employee_updated'));
            return redirect()->route('employees.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Employee $employee)
    {
         try {
            $employee->delete();
            $request->session()->flash('success', trans('messages.employee_deleted'));
            return redirect()->route('employees.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
