<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use App\WithHolding;
use App\Validators\WithHoldingValidator;

use Config;

class WithHoldingController extends Controller
{
    public function __construct()
    {
        //$this->authorizeResource(Tax::class, 'tax');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $withHoldings = $user->company->withHoldings;

        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $withHoldings->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'sr_no' => $key + 1,
                    'employee' => $item->employee,
                    'type' => $item->typeAccount->name,
                    'start_date' => $item->start_date,
                    'end_date' => $item->end_date,
                    'frequency' => $item->frequency,
                    'frequency_type' => $item->frequency_type,
                    'observation' => $item->observation,
                    'amount' => $item->amount,
                    'id' => $item->id,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('with-holdings.index', compact('withHoldings'));
    }

   /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $accounting_heads = $user->company->accountingHeads;

        $employee_options = ['' => trans('labels.select_employee')] + $user->company->employees->pluck('name', 'id')->toArray();
        // $type_option = ['' => trans('labels.select_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.with_holding_type_option'));
        $frequency_type_option = ['' => trans('labels.select_frequency_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_frequency_type_option'));
        // $incomes_option = ['' => trans('labels.select_incomes_option')] + $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_incomes_option'));

        $type_accounts_options = collect([]);
        $type_accounts = $accounting_heads->where('code', self::LIABILITIESCODE)->pluck('id')->toArray();
        $type_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $type_accounts)->pluck('id')->toArray();
        $type_accounts = $accounting_heads->whereIn('parent_id', $type_account_types)->values();

        $type_accounts->each(function($incomes_account) use($type_accounts_options) {
            $this->generateAccountsLevels($incomes_account, $type_accounts_options, 1, true);
        });

        $income_accounts_options = collect([]);
        $income_accounts = $accounting_heads->where('code', self::ASSETSCODE)->pluck('id')->toArray();
        $incomes_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $income_accounts)->pluck('id')->toArray();
        $income_accounts = $accounting_heads->whereIn('parent_id', $incomes_account_types)->values();

        $income_accounts->each(function($incomes_account) use($income_accounts_options) {
            $this->generateAccountsLevels($incomes_account, $income_accounts_options, 1, true);
        });

        return view('with-holdings.create', compact('employee_options', 'frequency_type_option', 'income_accounts_options', 'type_accounts_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, WithHoldingValidator $withHoldingValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$withHoldingValidator->with($input)->passes()) {
                $request->session()->flash('error', $withHoldingValidator->getErrors()[0]);
                return back()
                    ->withErrors($withHoldingValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'employee_id' => $input['employee_id'],
                'type_account' => $input['type_account'],
                'start_date' => $input['start_date'],
                'end_date' => $input['end_date'],
                'observation' => $input['observation'],
                'recurring_type' => $input['recurring_type'],
                'frequency_type' => $input['frequency_type'],
                'frequency' => $input['frequency'],
                'amount' => $input['amount'],
                'income_account' => $input['income_account'],
                'user_id' => $user->id
            ];

            $withHoldings = $user->company->withHoldings()->create($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.with_holding_created'));
            return redirect()->route('with-holdings.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Benefit  $benefit
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, WithHolding $withHolding)
    {
        $user = $request->user();
        $accounting_heads = $user->company->accountingHeads;

        $employee_options = ['' => trans('labels.select_employee')] + $user->company->employees->pluck('name', 'id')->toArray();
        // $type_option = ['' => trans('labels.select_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.with_holding_type_option'));
        $frequency_type_option = ['' => trans('labels.select_frequency_type')] + $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_frequency_type_option'));
        // $incomes_option = ['' => trans('labels.select_incomes_option')] + $this->mapOptionsBasedOnLocale(Config::get('constants.payroll_incomes_option'));

        $type_accounts_options = collect([]);
        $type_accounts = $accounting_heads->where('code', self::LIABILITIESCODE)->pluck('id')->toArray();
        $type_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $type_accounts)->pluck('id')->toArray();
        $type_accounts = $accounting_heads->whereIn('parent_id', $type_account_types)->values();

        $type_accounts->each(function($incomes_account) use($type_accounts_options) {
            $this->generateAccountsLevels($incomes_account, $type_accounts_options, 1, true);
        });

        $income_accounts_options = collect([]);
        $income_accounts = $accounting_heads->where('code', self::ASSETSCODE)->pluck('id')->toArray();
        $incomes_account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $income_accounts)->pluck('id')->toArray();
        $income_accounts = $accounting_heads->whereIn('parent_id', $incomes_account_types)->values();

        $income_accounts->each(function($incomes_account) use($income_accounts_options) {
            $this->generateAccountsLevels($incomes_account, $income_accounts_options, 1, true);
        });

        return view('with-holdings.edit', compact('withHolding', 'employee_options', 'frequency_type_option', 'income_accounts_options', 'type_accounts_options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WithHolding  $WithHolding
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WithHolding $WithHolding, WithHoldingValidator $withHoldingValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            
            if (!$withHoldingValidator->with($input)->passes()) {
                $request->session()->flash('error', $withHoldingValidator->getErrors()[0]);
                return back()
                    ->withErrors($withHoldingValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $data = [
                'employee_id' => $input['employee_id'],
                'type_account' => $input['type_account'],
                'start_date' => $input['start_date'],
                'end_date' => $input['end_date'],
                'observation' => $input['observation'],
                'recurring_type' => $input['recurring_type'],
                'frequency_type' => $input['frequency_type'],
                'frequency' => $input['frequency'],
                'amount' => $input['amount'],
                'income_account' => $input['income_account'],
            ];

            $WithHolding->update($data);

            DB::commit();

            $request->session()->flash('success', trans('messages.with_holding_updated'));
            return redirect()->route('with-holdings.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\withHolding  $withHolding
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, withHolding $withHolding)
    {
        try {
            $withHolding->delete();
            $request->session()->flash('success', trans('messages.with_holding_deleted'));
            return redirect()->route('with-holdings.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
