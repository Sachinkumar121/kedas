<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Exports\ClientsExport;
use App\Exports\SuppliersExport;
use App\Exports\InventoriesExport;
use App\Exports\ItemCategoriesExport;

use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class ExportController extends Controller
{

    public function __construct()
    {

    }

    /**
     * Generate Excel data from resource and export.
     *
     * @return \Illuminate\Http\Response
     */
    public function export($resource, Request $request) 
    {
        $resource = strToCamelCase($resource);
        try {
            $user = $request->user();
            if ($user->company->{$resource}->isEmpty()) {
                $request->session()->flash('error', trans('messages.no_data_import'));
                return back();
            }
            switch ($resource) {
                case 'clients':
                    $exportInstance = new ClientsExport;
                    $redirectTo = 'clients.index';
                    $fileName = config('app.name')." - $resource ".Carbon::now()->format('d-m-Y').".xlsx";
                    break;
                case 'suppliers':
                    $exportInstance = new SuppliersExport;
                    $redirectTo = 'suppliers.index';
                    $fileName = config('app.name')." - $resource ".Carbon::now()->format('d-m-Y').".xlsx";
                    break;
                case 'inventories':
                    $exportInstance = new InventoriesExport;
                    $redirectTo = 'inventories.index';
                    $fileName = config('app.name')." - $resource ".Carbon::now()->format('d-m-Y').".xlsx";
                    break;
                case 'itemCategories':
                    $exportInstance = new ItemCategoriesExport;
                    $redirectTo = 'item-categories.index';
                    $fileName = config('app.name')." - $resource ".Carbon::now()->format('d-m-Y').".xlsx";
                    break;
                
                default:
                    $exportInstance = null;
                    $redirectTo = 'dashboard';
                    $fileName = config('app.name')." - ".Carbon::now()->format('d-m-Y').".xlsx";
                    break;
            }
            return Excel::download($exportInstance, $fileName);
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            $error = reset($failures);
            $validationError = $error->errors()[0];
            $request->session()->flash('error', $validationError." ".trans('labels.at_row')." ".$error->row());
            return back();
        }
    }
}
