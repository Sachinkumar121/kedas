<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Traits\ApiResponse;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->successResponse(["i" => 1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255'
            ]);
            if ($validator->fails()) {
                $data = [
                    'error'     => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'message'  => $validator->errors()->all()[0],
                    'messages'  => $validator->errors()->all()
                ];
                return response()->json($data);
            }
            $permission = Permission::create(['name' => $input['name']]);
            return $this->successResponse(["permission" => $permission]);
        } catch (Exception $e) {
            return $this->failResponse([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        $deleted = $permission->delete();
        return $this->successResponse(["success" => $deleted]);
    }
}
