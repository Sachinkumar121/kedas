<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\User;
use App\UserProfile;
use App\Company;
use App\Role;
use App\Activity;

use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Validators\User\ProfileValidator;

use Lang;
use DB;

class UserController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $user = $request->user();        
        $language_options = config('constants.languages');
        return view('admin.edit', compact('user', 'language_options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = $request->user();

        try {
            DB::beginTransaction();
            $input = $request->all();
            $userUpdateValidator = new ProfileValidator('update');

            if (!$userUpdateValidator->with($input)->passes()) {
                if ($request->wantsJson() || $request->ajax()) {
                    $response = [
                        'error'     => Response::HTTP_UNPROCESSABLE_ENTITY,
                        'message'   => $userUpdateValidator->getErrors()[0],
                        'messages'  => $userUpdateValidator->getErrors()
                    ];
                    return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
                }
                $request->session()->flash('error', $userUpdateValidator->getErrors()[0]);
                return back()
                    ->withErrors($userUpdateValidator->getValidator())
                    ->with([
                        'message'   => $userUpdateValidator->getErrors()[0],
                        'alert-type' => 'error'
                    ])
                    ->withInput();
            }

            $data = $input;

            if (isset($data['password']) && $data['password']) {
                if (Hash::check($data['current_password'], $user->password)) {
                    $user->password = Hash::make($data['password']);
                } else {
                    return redirect()->back()
                        ->withInput()
                        ->withErrors(['current_password' => trans('messages.old_password_error')]);
                    // throw new \Exception(trans('messages.old_password_error'), 1);
                }
            }

            $user->name = $data['name'];
            $user->language = $data['language'];
            $user->save();

            app()->setlocale($data['language']);
            DB::commit();

            $response = [
                'success' => Lang::get('messages.user_profile_update')
            ];

            if ($request->wantsJson() || $request->ajax()) {
                return response()->json($response);
            }
            return redirect()->route('admin-dashboard')->with($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'error'     => $e->getMessage()
            ];
            if ($request->wantsJson() || $request->ajax()) {
                return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            return back()->with($response)->withInput();
        }
    }

    /**
     * Show the listing for each user.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $users = User::where([
            'user_role' => self::ADMIN
        ])->get();

        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $users->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'name' => $item->name,
                    'email' => $item->email,
                    'account_verified' => $item->hasVerifiedEmail() ? trans('labels.yes') : trans('labels.no')
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the activity logs for each user.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function activityLogs(Request $request)
    {
        $activityLogs = Activity::where('log_name', 'login-log')->latest()->get();
        
        return view('admin.users.activity_logs', compact('activityLogs'));
    }
}