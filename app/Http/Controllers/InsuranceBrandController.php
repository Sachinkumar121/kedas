<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\InsuranceBrand;
use App\Validators\InsuranceBrandValidator;

use DB;

class InsuranceBrandController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(InsuranceBrand::class, 'insurance_brand');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $insuranceBrands = $user->company->insuranceBrands;

        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect();
            $insuranceBrands->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'name' => $item->name,
                    'custom_name' => $item->custom_name,
                ]);
            });
            return response()->json(['data' => $jsonCollection]);
        }
        return view('insurance-brands.index', compact('insuranceBrands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('insurance-brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, InsuranceBrandValidator $insuranceBrandValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$insuranceBrandValidator->with($input)->passes()) {
                $request->session()->flash('error', $insuranceBrandValidator->getErrors()[0]);
                return back()
                    ->withErrors($insuranceBrandValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();
            $insuranceBrand = new insuranceBrand(['name' => $input['name'], 'user_id' => $user->id]);
            $insuranceBrand->setTranslation('custom_name', 'en', $input['name'])->setTranslation('custom_name', 'es', $input['name']);
            $user->company->insuranceBrands()->save($insuranceBrand);

            DB::commit();

            $request->session()->flash('success', trans('messages.insurance_brand_created'));
            return redirect()->route('insurance-brands.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsuranceBrand  $insuranceBrand
     * @return \Illuminate\Http\Response
     */
    public function edit(InsuranceBrand $insuranceBrand)
    {
        return view('insurance-brands.edit', compact('insuranceBrand'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsuranceBrand  $tax
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsuranceBrand $insuranceBrand)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $locale = app()->getLocale();
            
            $insuranceBrandValidator = new InsuranceBrandValidator('update', $insuranceBrand);
            if (!$insuranceBrandValidator->with($input)->passes()) {
                $request->session()->flash('error', $insuranceBrandValidator->getErrors()[0]);
                return back()
                    ->withErrors($insuranceBrandValidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            $insuranceBrand->setTranslation('custom_name', $locale, $input['name']);
            $insuranceBrand->save();

            DB::commit();

            $request->session()->flash('success', trans('messages.insurance_brand_updated'));
            return redirect()->route('insurance-brands.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsuranceBrand  $insuranceBrand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, InsuranceBrand $insuranceBrand)
    {
        try {
            if ($insuranceBrand->employees->isEmpty()) {
                $insuranceBrand->delete();
                $request->session()->flash('success', trans('messages.insurance_brand_deleted'));
            } else {
                $request->session()->flash('error', trans('messages.delete_prohibited'));
            }
            return redirect()->route('insurance-brands.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
