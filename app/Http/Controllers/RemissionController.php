<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Remission;
use App\Supplier;
use App\Client;
use App\Item;
use App\Invoice;
use App\Inventory;

use App\Validators\RemissionValidator;
use App\Mail\RemissionShared;

use DB;
use Config;
use Image;
use PDF;
use Cache;

class RemissionController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Remission::class, 'remission');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $remissions = $user->company->remissions;
        if ($request->wantsJson() || $request->ajax()) {
            return response()->json(['data' => $remissions]);
        }
        return view('remissions.index', compact('remissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();
        $next_remission_number = $user->company->remissions()->withTrashed() ? $user->company->remissions()->withTrashed()->count() + 1 : 1;

        $currencies = Cache::get('currencies');
        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $inventories = $user->company->inventories;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.remission_without_contacts'));
            return redirect('dashboard');
        }

        if ($inventories->isEmpty()) {
            $request->session()->flash('error', trans('messages.remission_without_items'));
            return redirect('dashboard');
        }

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        $inventory_attributes = $inventories->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-reference' => $item->reference ?: '',
                'data-description' => $item->description ?: '',
                'data-sale_price' => $item->sale_price ?: '',
                'data-tax_id' => $item->tax_id ?: ''
            ]];
        })->toArray();

        $inventory_options = ['' => trans('labels.select_item')] + $inventories->pluck('name', 'id')->toArray();

        $taxes = $user->company->taxes;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();

        $currency_rates = $user->currencyRates->pluck('rate', 'currency_id')->toArray();

        $currency_attributes = $currencies->mapWithKeys(function ($currency) use($currency_rates) {
            $currency_rate = isset($currency_rates[$currency->id]) ? $currency_rates[$currency->id] : '';
            return [$currency->id => [
                'data-code' => $currency->code ?: '',
                'data-symbol' => $currency->symbol ?: '',
                'data-rate' => $currency_rate
            ]];
        })->toArray();

        $user_currency = $user->company->currency;
        $user_currency_id = $user_currency->id;
        $currency_options = ['' => trans('labels.select_currency')] + $currencies->where('id', '<>', $user_currency_id)->pluck('formatted_name', 'id')->toArray();

        return view('remissions.create', compact('next_remission_number', 'inventories', 'tax_options', 'contact_options', 'contact_attributes', 'inventory_options', 'inventory_attributes', 'tax_attributes',  'currency_attributes', 'currency_options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $remissionvalidator = new RemissionValidator();
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$remissionvalidator->with($input)->passes()) {
                $request->session()->flash('error', $remissionvalidator->getErrors()[0]);
                return back()
                    ->withErrors($remissionvalidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
                return back()->withInput();
            };

            $remissionitems = collect($input['products'])->filter(function($row) {
                return $row['quantity'] && $row['item'];
                })->map(function($item) {
                    $item['inventory_id'] = $item['item'];
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    //unset($item['reference']);
                    //unset($item['price']);
                    unset($item['description']);
                    return $item;
            })->values()->toArray();

            if (!$remissionitems) {
                $request->session()->flash('error', trans('messages.remission_without_items'));
                return back()
                    ->withInput();
            }

            $remission = $contact->remissions()->create([
                'user_id' => $user->id,
                'company_id' => $user->company->id,
                'start_date' => $input['remission_start_date'],
                'expiration_date' => $input['remission_expiration_date'],
                'notes' => $input['notes'] ?? null,
                'exchange_currency_rate' => $input['exchange_rate'] ?? null,
                'exchange_currency_id' => $input['exchange_currency'] ?? null,
            ]);
           
            $remission->items()->createMany($remissionitems);

            if (isset($input['logo']) && $input['logo']) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/profile-pics");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $logoInstance = Image::make($input['logo']);
                if ($logoInstance->height() > 300) {
                    $logoInstance->fit(300)->save($path);
                } else {
                    $logoInstance->orientate()->save($path);
                }
                $user->company->logo = $filename;
                $user->company->save();
            }

            if (isset($input['signature']) && $input['signature']) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/signatures");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $signatureInstance = Image::make($input['signature']);
                if ($signatureInstance->height() > 150) {
                    $signatureInstance->fit(300, 150)->save($path);
                } else {
                    $signatureInstance->orientate()->save($path);
                }
                $user->company->signature = $filename;
                $user->company->save();
            }

            /* Save exchange rate */
            if (isset($input['exchange_rate']) && $input['exchange_rate'] && isset($input['exchange_currency']) && $input['exchange_currency']) {
                $user->company->currencyRates()->updateOrCreate(
                    ['currency_id' => $input['exchange_currency'], 'user_id' => $user->id],
                    ['rate' => $input['exchange_rate']]
                );
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.remission_created'));
            return redirect()->route('remissions.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Remission  $remission
     * @return \Illuminate\Http\Response
     */
    public function show(Remission $remission, $type = self::SALE)
    {
        return view('remissions.show', compact('remission', 'type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Remission  $remission
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Remission $remission)
    {
        if ($remission->invoice) {
            $request->session()->flash('error', trans('messages.edit_prohibited'));
            return redirect('remissions');
        }

        $remission->contact_custom_id = ($remission->contact instanceof Supplier ? 'supplier' : 'client') .'-'. $remission->contact->id;

        $user = $request->user();
        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $inventories = $user->company->inventories;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.remission_without_contacts'));
            return redirect('dashboard');
        }

        if ($inventories->isEmpty()) {
            $request->session()->flash('error', trans('messages.remission_without_items'));
            return redirect('dashboard');
        }

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        $inventory_attributes = $inventories->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-reference' => $item->reference ?: '',
                'data-description' => $item->description ?: '',
                'data-sale_price' => $item->sale_price ?: '',
                'data-tax_id' => $item->tax_id ?: ''
            ]];
        })->toArray();

        $inventory_options = ['' => trans('labels.select_item')] + $inventories->pluck('name', 'id')->toArray();

        $taxes = $user->company->taxes;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();

        return view('remissions.edit', compact('remission', 'inventories', 'tax_options', 'contact_options', 'contact_attributes', 'inventory_options', 'inventory_attributes', 'tax_attributes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Remission  $remission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Remission $remission)
    {
        if ($remission->invoice) {
            $request->session()->flash('error', trans('messages.edit_prohibited'));
            return redirect('remissions');
        }

        $remissionvalidator = new RemissionValidator();
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$remissionvalidator->with($input)->passes()) {
                $request->session()->flash('error', $remissionvalidator->getErrors()[0]);
                return back()
                    ->withErrors($remissionvalidator->getValidator())
                    ->withInput();
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
                return back()->withInput();
            };

            $remissionitems = collect($input['products'])->filter(function($row) {
                return $row['quantity'] && $row['item'];
            })->map(function($item) {
                $item['inventory_id'] = $item['item'];
                $item['tax_id'] = $item['tax'] ?: null;
                unset($item['item']);
                unset($item['tax']);
                //unset($item['reference']);
                //unset($item['price']);
                unset($item['description']);
                return $item;
            })->values();

            $updateRemissionItems = $remissionitems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertRemissionItems = $remissionitems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deleteRemissionItemsExcept = $updateRemissionItems->pluck('id')->toArray();

            if ($remissionitems->isEmpty()) {
                $request->session()->flash('error', trans('messages.remission_without_items'));
                return back()
                    ->withInput();
            }

            $remission->contact()->associate($contact);

            $remission->update([
                'start_date' => $input['remission_start_date'],
                'expiration_date' => $input['remission_expiration_date'],
                'notes' => $input['notes'] ?? null
            ]);

            if ($deleteRemissionItemsExcept) {
                $remission->items()->whereNotIn('id', $deleteRemissionItemsExcept)->delete();
            }

            if ($insertRemissionItems) {
                $remission->items()->createMany($insertRemissionItems);
            }

            if ($updateRemissionItems) {
                foreach ($updateRemissionItems as $updateRemissionItem) {
                    $updatedDate = $updateRemissionItem;
                    unset($updatedDate['id']);
                    Item::findOrFail($updateRemissionItem['id'])->update($updatedDate);
                }
            }

            if (isset($input['logo']) && $input['logo'])  {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/profile-pics");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                $logoInstance = Image::make($input['logo']);
                if ($logoInstance->height() > 300) {
                    $logoInstance->fit(300)->save($path);
                } else {
                    $logoInstance->orientate()->save($path);
                }
                
                $user->company->logo = $filename;
                $user->company->save();
            }

            if (isset($input['signature']) && $input['signature'])  {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/signatures");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $signatureInstance = Image::make($input['signature']);
                if ($signatureInstance->height() > 150) {
                    $signatureInstance->fit(300, 150)->save($path);
                } else {
                    $signatureInstance->orientate()->save($path);
                }

                $user->company->signature = $filename;
                $user->company->save();
            }

            DB::commit();

            $request->session()->flash('success', trans('messages.remission_updated'));
            return redirect()->route('remissions.index');
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Remission  $remission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Remission $remission)
    {
        if ($remission->invoice) {
            $request->session()->flash('error', trans('messages.delete_prohibited'));
            return redirect('remissions');
        }
        
        try {
            $remission->delete();
            $request->session()->flash('success', trans('messages.remission_deleted'));
            return redirect()->route('remissions.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
