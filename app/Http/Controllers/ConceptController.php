<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use App;
use App\Concept;
use App\ConceptItem;
use App\User;
use App\Tax;
use App\Inventory;
use App\AccountingHead;
use App\UserAccountingHead;
use App\InventoryAdjustment;
use App\Events\ProfileCompleted;
use App\Events\InventoryCreatedOrUpdated;
use App\Events\InventoryAdjustmentCreatedOrUpdated;

use Carbon\Carbon;
use App\Role;
use App\Permission;
use App\Company;

class ConceptController extends Controller
{
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	// $concept = new Concept;
    	// $concept->setTranslation('title', 'en', 'Parent en')
    	//    ->setTranslation('title', 'es', 'Parent es')
    	//    ->save(); // Saved as root

    	// $parent = Concept::find(97);
    	// $childNode = new Concept;
    	// $childNode->setTranslation('title', 'en', 'Child en')
    	//    ->setTranslation('title', 'es', 'Child es')
    	//    ->save(); // Saved as child of root
     	//    $childNode->appendToNode($parent)->save();

    	// get all children


    	// get whole tree
    	// $tree = Concept::get()->toTree();
    	// prePrint($tree);
    	// return response()->json(['data' => $tree]);

    	// Display the whole tree
    	$nodes = Concept::get()->toTree();

        $traverse = function ($categories, $prefix = '-') use (&$traverse) {
        	// prePrint($categories);
            foreach ($categories as $category) {
                echo PHP_EOL.$prefix.' '.$category->title;

                $traverse($category->children, $prefix.'-');
            }
        };

        $traverse($nodes);


        die;
    }

    public function fixTree(Request $request) {
        die("Done");
        // fix the tree from old data.
        Concept::fixTree();
        die("Done");
    }

    public function fixUsers()
    {
        die("Done");
        // remove old user accounts
        if (App::Environment(['production'])) {
            User::whereIn('id', [7, 11, 16, 21, 55, 56])->delete();
        }
    }

    public function createChartOfAccountsForAllUsers()
    {
        die("Done");
        // create chart of accounts for all users.
        foreach (User::all() as $user) {
            if ($user->isCompanyOwner()) {
                event(new ProfileCompleted($user));
            }
        }
        die("Done");
    }

    public function mapConceptToAccounts(Request $request) {
        die("Done");
        try {
            DB::beginTransaction();

            // $concept_ids = ConceptItem::all()->pluck('concept.title', 'concept_id')->sortKeys()->unique()->toArray();
            // $concept_titles = ConceptItem::all()->pluck('concept.title')->sort()->unique()->values()->toArray();
            // prePrint($concept_ids);

            /*$concept_to_account_head_code_map = [
                "64" => "2001",
                "85" => "1101",
            ];*/

            $concept_to_account_head_code_map = [
                "6" => "5006",
                "7" => "6016",
                "9" => "9009",
                "12" => "8004",
                "16" => "3015",
                "21" => "5003",
                "23" => "6016",
                "26" => "6012",
                "28" => "9009", 
                "31" => "4004",
                "33" => "8002",

                "54" => "9009",
                "55" => "9014",
                "58" => "9009",
                "59" => "6001",
                "62" => "9004",
                "63" => "9009",
                
                "64" => "2001",
                "66" => "2001",
                "68" => "2101",
                "73" => "1101",
                "84" => "2505",
                "85" => "1101",
                "89" => "2505",
            ];

            // $conceptItems = ConceptItem::whereIn('concept_id', ["64", "85"])->get();
            $conceptItems = ConceptItem::all();
            foreach ($conceptItems as $key => $conceptItem) {
                $company = null;
                if ($conceptItem->invoice) {
                    $company = $conceptItem->invoice->company;
                } else if ($conceptItem->transaction) {
                    $company = $conceptItem->transaction->company;
                } else if ($conceptItem->debitNote) {
                    $company = $conceptItem->debitNote->company;
                } else if ($conceptItem->purchaseOrder) {
                    $company = $conceptItem->purchaseOrder->company;
                }
                if ($company) {
                    $code_id = $company->accountingHeads->where('code', $concept_to_account_head_code_map[$conceptItem->concept_id])->first()->id;
                    $conceptItem->user_accounting_head_id = $code_id;
                    $conceptItem->save();
                } 
            }
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
        die("Done");
    }

    public function addMoreAccounts(Request $request) {
        try {
            // add new accounting heads in the main table first.
            DB::beginTransaction();
            AccountingHead::where('code', 1002)->first()->setTranslation('name', 'es', 'Comprobacion')->save();
            AccountingHead::where('code', 3)->first()->setTranslation('name', 'es', 'Patrimonio')->save();
            AccountingHead::where('code', 3000)->first()->setTranslation('name', 'es', 'Patrimonio')->save();
            AccountingHead::where('code', 1002)->first()->setTranslation('name', 'es', 'Cuentas corrientes')->setTranslation('name', 'en', 'Checking Accounts')->save();
            AccountingHead::where('code', 1005)->first()->setTranslation('name', 'es', 'Cuenta de Ahorros')->setTranslation('name', 'en', 'Savings Account')->save();
            AccountingHead::where('code', 1001)->first()->setTranslation('name', 'es', 'Caja')->save();
            
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
        die("Done");
    }

    public function updateUserAccounts(Request $request) {
        try {
            // add new accounting heads in the main table first.
            DB::beginTransaction();

            foreach (UserAccountingHead::whereIn('code', [3, 3000])->get() as $userAccountingHead) {
                $userAccountingHead->setTranslation('name', 'es', 'Patrimonio');
                $userAccountingHead->save();
            }

            foreach (UserAccountingHead::where('code', 1002)->get() as $userAccountingHead) {
                $userAccountingHead->setTranslation('name', 'es', 'Cuentas corrientes')->setTranslation('name', 'en', 'Checking Accounts')->save();
            }

            foreach (UserAccountingHead::where('code', 1005)->get() as $userAccountingHead) {
                $userAccountingHead->setTranslation('name', 'es', 'Cuenta de Ahorros')->setTranslation('name', 'en', 'Savings Account')->save();
            }

            foreach (UserAccountingHead::where('code', 1001)->get() as $userAccountingHead) {
                $userAccountingHead->setTranslation('name', 'es', 'Caja')->save();
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            prePrint($e->getMessage());
        }
        die("Done");
    }

    public function addMoreAccountsOld(Request $request) {
        try {
            // add new accounting heads in the main table first.
            DB::beginTransaction();

            $currentLab = AccountingHead::where('code', 2505)->first();
            $otherCurrentAs = AccountingHead::where('code', 1200)->first();
            $cogs = AccountingHead::where('code', 5006)->first();

            $currentLabChildren = AccountingHead::create([
                'name' =>  [
                    'en' => 'Taxes to pay',
                    'es' => 'Impuestos por pagar'
                ],
                'incr_nature' => 'credit',
                'decr_nature' => 'debit',
                'slug' => strToSlug('Taxes to pay', '-'),
                'description' => null,
                'code' => 2601,
                'visibility' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'children' => [
                    [
                        'name' =>  [
                            'en' => 'Sales tax payable',
                            'es' => 'impuesto sobre las ventas a pagar'
                        ],
                        'incr_nature' => 'credit',
                        'decr_nature' => 'debit',
                        'slug' => strToSlug('Sales tax payable', '-'),
                        'description' => null,
                        'code' => 2701,
                        'visibility' => true,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ],[
                        'name' =>  [
                            'en' => 'ITBIS payable',
                            'es' => 'ITBIS por pagar'
                        ],
                        'incr_nature' => 'credit',
                        'decr_nature' => 'debit',
                        'slug' => strToSlug('ITBIS payable', '-'),
                        'description' => null,
                        'code' => 2702,
                        'visibility' => true,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ],[
                        'name' =>  [
                            'en' => 'ISC payable',
                            'es' => 'ISC por pagar'
                        ],
                        'incr_nature' => 'credit',
                        'decr_nature' => 'debit',
                        'slug' => strToSlug('ISC payable', '-'),
                        'description' => null,
                        'code' => 2703,
                        'visibility' => true,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ],[
                        'name' =>  [
                            'en' => 'Tips payable',
                            'es' => 'Propinas por pagar'
                        ],
                        'incr_nature' => 'credit',
                        'decr_nature' => 'debit',
                        'slug' => strToSlug('Tips payable', '-'),
                        'description' => null,
                        'code' => 2704,
                        'visibility' => true,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ],[
                        'name' =>  [
                            'en' => 'Other type of tax payable',
                            'es' => 'Otro tipo de impuesto por pagar'
                        ],
                        'incr_nature' => 'credit',
                        'decr_nature' => 'debit',
                        'slug' => strToSlug('Other type of tax payable', '-'),
                        'description' => null,
                        'code' => 2705,
                        'visibility' => true,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ],
                ]
            ]);

            $currentLab->appendNode($currentLabChildren);

            $otherCurrentAsChildren = AccountingHead::create([
                'name' =>  [
                    'en' => 'Current tax assets',
                    'es' => 'Activos por impuestos corrientes'
                ],
                'incr_nature' => 'debit',
                'decr_nature' => 'credit',
                'slug' => strToSlug('Current tax assets', '-'),
                'description' => null,
                'code' => 1216,
                'visibility' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'children' => [
                    [
                        'name' =>  [
                            'en' => 'Taxes in favor',
                            'es' => 'Retenciones por pagar'
                        ],
                        'incr_nature' => 'debit',
                        'decr_nature' => 'credit',
                        'slug' => strToSlug('Taxes in favor', '-'),
                        'description' => null,
                        'code' => 1300,
                        'visibility' => true,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'children' => [
                            [
                                'name' =>  [
                                    'en' => 'Sales tax input',
                                    'es' => 'Entrada de impuesto sobre las ventas'
                                ],
                                'incr_nature' => 'debit',
                                'decr_nature' => 'credit',
                                'slug' => strToSlug('Sales tax input', '-'),
                                'description' => null,
                                'code' => 1301,
                                'visibility' => true,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ],[
                                'name' =>  [
                                    'en' => 'ITBIS in favor',
                                    'es' => 'Retención de ITBIS por pagar'
                                ],
                                'incr_nature' => 'debit',
                                'decr_nature' => 'credit',
                                'slug' => strToSlug('ITBIS in favor', '-'),
                                'description' => null,
                                'code' => 1302,
                                'visibility' => true,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ],[
                                'name' =>  [
                                    'en' => 'ISC a favor',
                                    'es' => 'Retención de ISR por pagar'
                                ],
                                'incr_nature' => 'debit',
                                'decr_nature' => 'credit',
                                'slug' => strToSlug('ISC a favor', '-'),
                                'description' => null,
                                'code' => 1303,
                                'visibility' => true,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ],[
                                'name' =>  [
                                    'en' => 'Other type of tax in favor',
                                    'es' => 'Otro tipo de retención por pagar'
                                ],
                                'incr_nature' => 'debit',
                                'decr_nature' => 'credit',
                                'slug' => strToSlug('Other type of tax in favor', '-'),
                                'description' => null,
                                'code' => 1304,
                                'visibility' => true,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ],
                        ]
                    ]
                ]
            ]);

            $otherCurrentAs->appendNode($otherCurrentAsChildren);

            $cogsChildren = AccountingHead::create([
                'name' =>  [
                    'en' => 'Inventory adjustments',
                    'es' => 'Ajustes de inventario'
                ],
                'incr_nature' => 'debit',
                'decr_nature' => 'credit',
                'slug' => strToSlug('Inventory adjustments', '-'),
                'description' => null,
                'code' => 5011,
                'visibility' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            $cogs->appendNode($cogsChildren);

            $obe = AccountingHead::where('code', 3006)->first();

            $obeChildren = AccountingHead::create([
                'name' =>  [
                    'en' => 'Initial adjustment in inventory',
                    'es' => 'Ajuste inicial en inventario'
                ],
                'incr_nature' => 'credit',
                'decr_nature' => 'debit',
                'slug' => strToSlug('Initial adjustment in inventory', '-'),
                'description' => null,
                'code' => 3501,
                'visibility' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            $obe->appendNode($obeChildren);

            $invAdj = AccountingHead::where('code', 5011)->first();
            $invAdj->code = 3502;
            $invAdj->incr_nature = 'credit';
            $invAdj->decr_nature = 'debit';
            $invAdj->appendToNode($obe)->save();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
        die("Done");
    }

    public function updateUserAccountsOld(Request $request) {
        try {
            // add new accounting heads in the main table first.
            DB::beginTransaction();

            $newHeadToId = AccountingHead::where('id', '>', 174)->pluck('id', 'code')->toArray();

            foreach (User::all() as $user) {
                if ($user->isCompanyOwner()) {

                    $currentLab = $user->company->accountingHeads->where('code', 2505)->first();
                    $otherCurrentAs = $user->company->accountingHeads->where('code', 1200)->first();
                    $cogs = $user->company->accountingHeads->where('code', 5006)->first();

                    $currentLabChildren = UserAccountingHead::create([
                        'name' =>  [
                            'en' => 'Taxes to pay',
                            'es' => 'Impuestos por pagar'
                        ],
                        'incr_nature' => 'credit',
                        'decr_nature' => 'debit',
                        'slug' => strToSlug('Taxes to pay', '-'),
                        'description' => null,
                        'code' => 2601,
                        'visibility' => true,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'custom_code' => 2601,
                        'company_id' => $user->company->id,
                        'user_id' => $user->id,
                        'accounting_head_id' => $newHeadToId[2601],
                        'children' => [
                            [
                                'name' =>  [
                                    'en' => 'Sales tax payable',
                                    'es' => 'impuesto sobre las ventas a pagar'
                                ],
                                'incr_nature' => 'credit',
                                'decr_nature' => 'debit',
                                'slug' => strToSlug('Sales tax payable', '-'),
                                'description' => null,
                                'code' => 2701,
                                'visibility' => true,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                                'custom_code' => 2701,
                                'company_id' => $user->company->id,
                                'user_id' => $user->id,
                                'accounting_head_id' => $newHeadToId[2701],
                            ],[
                                'name' =>  [
                                    'en' => 'ITBIS payable',
                                    'es' => 'ITBIS por pagar'
                                ],
                                'incr_nature' => 'credit',
                                'decr_nature' => 'debit',
                                'slug' => strToSlug('ITBIS payable', '-'),
                                'description' => null,
                                'code' => 2702,
                                'visibility' => true,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                                'custom_code' => 2702,
                                'company_id' => $user->company->id,
                                'user_id' => $user->id,
                                'accounting_head_id' => $newHeadToId[2702],
                            ],[
                                'name' =>  [
                                    'en' => 'ISC payable',
                                    'es' => 'ISC por pagar'
                                ],
                                'incr_nature' => 'credit',
                                'decr_nature' => 'debit',
                                'slug' => strToSlug('ISC payable', '-'),
                                'description' => null,
                                'code' => 2703,
                                'visibility' => true,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                                'custom_code' => 2703,
                                'company_id' => $user->company->id,
                                'user_id' => $user->id,
                                'accounting_head_id' => $newHeadToId[2703],
                            ],[
                                'name' =>  [
                                    'en' => 'Tips payable',
                                    'es' => 'Propinas por pagar'
                                ],
                                'incr_nature' => 'credit',
                                'decr_nature' => 'debit',
                                'slug' => strToSlug('Tips payable', '-'),
                                'description' => null,
                                'code' => 2704,
                                'visibility' => true,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                                'custom_code' => 2704,
                                'company_id' => $user->company->id,
                                'user_id' => $user->id,
                                'accounting_head_id' => $newHeadToId[2704],
                            ],[
                                'name' =>  [
                                    'en' => 'Other type of tax payable',
                                    'es' => 'Otro tipo de impuesto por pagar'
                                ],
                                'incr_nature' => 'credit',
                                'decr_nature' => 'debit',
                                'slug' => strToSlug('Other type of tax payable', '-'),
                                'description' => null,
                                'code' => 2705,
                                'visibility' => true,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                                'custom_code' => 2705,
                                'company_id' => $user->company->id,
                                'user_id' => $user->id,
                                'accounting_head_id' => $newHeadToId[2705],
                            ],
                        ]
                    ]);

                    $currentLab->appendNode($currentLabChildren);

                    $otherCurrentAsChildren = UserAccountingHead::create([
                        'name' =>  [
                            'en' => 'Current tax assets',
                            'es' => 'Activos por impuestos corrientes'
                        ],
                        'incr_nature' => 'debit',
                        'decr_nature' => 'credit',
                        'slug' => strToSlug('Current tax assets', '-'),
                        'description' => null,
                        'code' => 1216,
                        'visibility' => true,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'custom_code' => 1216,
                        'company_id' => $user->company->id,
                        'user_id' => $user->id,
                        'accounting_head_id' => $newHeadToId[1216],
                        'children' => [
                            [
                                'name' =>  [
                                    'en' => 'Taxes in favor',
                                    'es' => 'Retenciones por pagar'
                                ],
                                'incr_nature' => 'debit',
                                'decr_nature' => 'credit',
                                'slug' => strToSlug('Taxes in favor', '-'),
                                'description' => null,
                                'code' => 1300,
                                'visibility' => true,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                                'custom_code' => 1300,
                                        'company_id' => $user->company->id,
                                        'user_id' => $user->id,
                                        'accounting_head_id' => $newHeadToId[1300],
                                'children' => [
                                    [
                                        'name' =>  [
                                            'en' => 'Sales tax input',
                                            'es' => 'Entrada de impuesto sobre las ventas'
                                        ],
                                        'incr_nature' => 'debit',
                                        'decr_nature' => 'credit',
                                        'slug' => strToSlug('Sales tax input', '-'),
                                        'description' => null,
                                        'code' => 1301,
                                        'visibility' => true,
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now(),
                                        'custom_code' => 1301,
                                        'company_id' => $user->company->id,
                                        'user_id' => $user->id,
                                        'accounting_head_id' => $newHeadToId[1301],
                                    ],[
                                        'name' =>  [
                                            'en' => 'ITBIS in favor',
                                            'es' => 'Retención de ITBIS por pagar'
                                        ],
                                        'incr_nature' => 'debit',
                                        'decr_nature' => 'credit',
                                        'slug' => strToSlug('ITBIS in favor', '-'),
                                        'description' => null,
                                        'code' => 1302,
                                        'visibility' => true,
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now(),
                                        'custom_code' => 1302,
                                        'company_id' => $user->company->id,
                                        'user_id' => $user->id,
                                        'accounting_head_id' => $newHeadToId[1302],
                                    ],[
                                        'name' =>  [
                                            'en' => 'ISC a favor',
                                            'es' => 'Retención de ISR por pagar'
                                        ],
                                        'incr_nature' => 'debit',
                                        'decr_nature' => 'credit',
                                        'slug' => strToSlug('ISC a favor', '-'),
                                        'description' => null,
                                        'code' => 1303,
                                        'visibility' => true,
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now(),
                                        'custom_code' => 1303,
                                        'company_id' => $user->company->id,
                                        'user_id' => $user->id,
                                        'accounting_head_id' => $newHeadToId[1303],
                                    ],[
                                        'name' =>  [
                                            'en' => 'Other type of tax in favor',
                                            'es' => 'Otro tipo de retención por pagar'
                                        ],
                                        'incr_nature' => 'debit',
                                        'decr_nature' => 'credit',
                                        'slug' => strToSlug('Other type of tax in favor', '-'),
                                        'description' => null,
                                        'code' => 1304,
                                        'visibility' => true,
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now(),
                                        'custom_code' => 1304,
                                        'company_id' => $user->company->id,
                                        'user_id' => $user->id,
                                        'accounting_head_id' => $newHeadToId[1304],
                                    ],
                                ]
                            ]
                        ]
                    ]);

                    $otherCurrentAs->appendNode($otherCurrentAsChildren);

                    $cogsChildren = UserAccountingHead::create([
                        'name' =>  [
                            'en' => 'Inventory adjustments',
                            'es' => 'Ajustes de inventario'
                        ],
                        'incr_nature' => 'debit',
                        'decr_nature' => 'credit',
                        'slug' => strToSlug('Inventory adjustments', '-'),
                        'description' => null,
                        'code' => 5011,                        
                        'visibility' => true,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'custom_code' => 5011,
                        'company_id' => $user->company->id,
                        'user_id' => $user->id,
                        'accounting_head_id' => $newHeadToId[5011],
                    ]);

                    $cogs->appendNode($cogsChildren);
                }
            }

            $companies = Company::all();
            foreach ($companies as $key => $company) {
                if ($company->owner) {
                    $obe = $company->accountingHeads->where('code', 3006)->first();
                    $obeChildren = UserAccountingHead::create([
                        'name' =>  [
                            'en' => 'Initial adjustment in inventory',
                            'es' => 'Ajuste inicial en inventario'
                        ],
                        'incr_nature' => 'credit',
                        'decr_nature' => 'debit',
                        'slug' => strToSlug('Initial adjustment in inventory', '-'),
                        'description' => null,
                        'code' => 3501,
                        'visibility' => true,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'custom_code' => 3501,
                        'company_id' => $company->id,
                        'user_id' => $company->owner->id,
                        'accounting_head_id' => 188,
                    ]);

                    $obe->appendNode($obeChildren);

                    $invAdj = $company->accountingHeads->where('code', 5011)->first();
                    $invAdj->code = 3502;
                    $invAdj->incr_nature = 'credit';
                    $invAdj->decr_nature = 'debit';
                    $invAdj->appendToNode($obe)->save();
                }
            }

            foreach (UserAccountingHead::where('code', 1002)->get() as $userAccountingHead) {
                $userAccountingHead->setTranslation('name', 'es', 'Comprobacion');
                $userAccountingHead->save();
            }
                
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
        die("Done");
    }

    public function fixTax(Request $request) {
        die("Done");
        $taxes = Tax::all();
        $purchases_account_code = 1301;
        $sales_account_code = 2701;

        foreach ($taxes as $tax) {
            $codeToIdMap = UserAccountingHead::where('company_id', $tax->company_id)->where(function($q) use ($purchases_account_code, $sales_account_code) {
                return $q->where('code', $purchases_account_code)->orWhere('code', $sales_account_code);
            })->get()->pluck('id', 'code')->toArray();
            $tax->purchases_account = $codeToIdMap[$purchases_account_code] ?? null;
            $tax->sales_account = $codeToIdMap[$sales_account_code] ?? null;
            $tax->save();
        }
    }

    public function fixInventory(Request $request) {
        die("Done");
        // set the type to inventory where track_inventory = 1
        // set assets, income and expenses account for track inventries where all of them all null. 1204(Inventory), 4004(Sales of product income), 5005(Supplies & materials - COGS)
        // set income account for service inventries(non-track) where income account is null. 1101(Accounts receivable)
        // set expense account for service inventries(non-track) where expense account is null. 2001(Accounts payable)

        try {

            DB::beginTransaction();
            
            $inventories = Inventory::all();

            $inventory_account_code = 1204;
            $sopi_account_code = 4004;
            $cogs_account_code = 5005;
            $ar_code = 1101;
            $ap_code = 2001;

            foreach ($inventories as $inventory) {
                if ($inventory->track_inventory) {
                    $inventory->type = self::INVENTORY;
                    if (!$inventory->asset_account && !$inventory->income_account && !$inventory->expense_account) {
                        $codeToIdMap = UserAccountingHead::where('company_id', $inventory->company_id)->where(function($q) use ($inventory_account_code, $sopi_account_code, $cogs_account_code) {
                            return $q->where('code', $inventory_account_code)->orWhere('code', $sopi_account_code)->orWhere('code', $cogs_account_code);
                        })->get()->pluck('id', 'code')->toArray();

                        $inventory->asset_account = $codeToIdMap[$inventory_account_code] ?? null;
                        $inventory->income_account = $codeToIdMap[$sopi_account_code] ?? null;
                        $inventory->expense_account = $codeToIdMap[$cogs_account_code] ?? null;
                    }
                } else {
                    $inventory->type = self::SERVICES;
                    if (!$inventory->income_account) {
                        $codeToIdMap = UserAccountingHead::where('company_id', $inventory->company_id)->where(function($q) use ($ar_code) {
                            return $q->where('code', $ar_code);
                        })->get()->pluck('id', 'code')->toArray();

                        $inventory->income_account = $codeToIdMap[$ar_code] ?? null;
                    }

                    /*if (!$inventory->expense_account) {
                        $codeToIdMap = UserAccountingHead::where('company_id', $inventory->company_id)->where(function($q) use ($ap_code) {
                            return $q->where('code', $ap_code);
                        })->get()->pluck('id', 'code')->toArray();

                        $inventory->expense_account = $codeToIdMap[$ap_code] ?? null;
                    }*/
                }
                // prePrint($inventory);
                $inventory->save();
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back();
        }
        die("Done");

    }

    public function fixRoles(Request $request) {
        die("Done");
        try {
            // echo "<pre>";
            DB::beginTransaction();
            $collect = collect([]);
            $collect = $collect->concat(DB::table('model_has_roles')->pluck('role_id')->toArray());
            $collect = $collect->concat(Role::whereHas('permissions')->get()->pluck('id')->toArray());
            $collect->push(Role::where('company_id', 14)->where('name', 'manager')->first()->id);

            Role::where('name', 'admin')->orWhere(function ($query) use ($collect){
                  $query->whereNotIn('id', $collect->toArray());
              })->delete();

            foreach (DB::table('model_has_permissions')->get()->groupBy('model_id') as $key => $value) {
                $model_has_roles = DB::table('model_has_roles')->where('model_id', $value->first()->model_id)->first();
                if ($model_has_roles) {

                    Role::find($model_has_roles->role_id)->syncPermissions($value->pluck('permission_id'));
                } else {
                    // prePrint(User::find($value->first()->model_id)->company->roles);
                    User::find($value->first()->model_id)->company->roles->where('name', 'manager')->first()->syncPermissions($value->pluck('permission_id'));
                    User::find($value->first()->model_id)->assignRole(User::find($value->first()->model_id)->company->roles->where('name', 'manager')->first());

                }
             } 
            Role::find(132)->syncPermissions(Permission::all()->pluck('id'));
            DB::table('model_has_permissions')->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
        die("Done");
        // Delete role where no permission or no models.
    }

    public function fixInventoryEntries(Request $request) {
        $inventories = Inventory::where([
            'type' => 'inventory',
            'track_inventory' => 1
        ])->whereNotNull('asset_account')
        ->whereNotNull('unit_cost')
        ->get();
        foreach ($inventories as $inventory) {
            $inventory->init_quantity_date = $inventory->init_quantity_date ?: Carbon::createFromFormat('Y-m-d H:i:s', $inventory->created_at)->format('Y-m-d');
            $inventory->init_quantity = is_null($inventory->init_quantity) ? 0 : $inventory->init_quantity;

            $inventory->save();
            event(new InventoryCreatedOrUpdated($inventory));
        }
    }

    public function fixInventoryAdjEntries(Request $request) {
        $inventoryAdjustments = InventoryAdjustment::all();
        foreach ($inventoryAdjustments as $inventoryAdjustment) {
            // prePrint($inventoryAdjustment);
            event(new InventoryAdjustmentCreatedOrUpdated($inventoryAdjustment));
        }
        die("done");
    }
}