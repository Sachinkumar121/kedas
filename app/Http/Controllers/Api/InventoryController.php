<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Mail;
use ProVision\Searchable\SearchableModes;

use App\Inventory;
use App\Validators\InventoryValidator;
use App\Mail\InventoryCreated;

use App\Http\Resources\Inventory as InventoryResource;
use App\Http\Resources\InventoryCollection;

use DB;
use Auth;
use Config;
use Image;
use Cache;

use App\Traits\ApiResponse;

class InventoryController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(Inventory::class, 'inventory');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        $inventories = $user->company->inventories;

        if ($term) {
            $inventories = $inventories->reject(function ($element) use ($term) {
                return mb_strpos(strtolower($element->name), strtolower($term)) === false 
                && mb_strpos(strtolower($element->reference), strtolower($term)) === false
                && mb_strpos(strtolower($element->sale_price), strtolower($term)) === false
                && mb_strpos(strtolower($element->cost), strtolower($term)) === false
                && mb_strpos(strtolower($element->quantity), strtolower($term)) === false
                && mb_strpos(strtolower($element->description), strtolower($term)) === false
                && mb_strpos(strtolower(($element->itemCategory ? $element->itemCategory->name : null)), strtolower($term)) === false;
            })->values();
        }

        $paginateData = $inventories->paginate($perPage);
        $collection = new InventoryCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => escapeResponse($collection), 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $type = $input['type'] ?? self::SERVICES;

            $inventoryValidator = new InventoryValidator('add', null, $type);

            if ($type != self::INVENTORY) {
                $input['income_account'] = ($input['is_product_sale'] ?? false) ? ($input['income_account'] ?? null) : null;
                $input['expense_account'] = ($input['is_product_purchase'] ?? false) ? ($input['expense_account'] ?? null) : null;
                $input['asset_account'] = null;
            }

            if (!$inventoryValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $inventoryValidator->getErrors()[0],
                    "messages" => $inventoryValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            $data = [
                'type' => $input['type'],

                'name' => $input['name'],
                'reference' => $input['reference'],
                'description' => $input['description'],
                'barcode' => $input['barcode'],
                'sku' => $input['sku'] ?? null,
                'item_category_id' => $input['item_category_id'],
                'tax_id' => (int)$input['tax_id'] ? $input['tax_id'] : null,
                'sale_price' => $input['sale_price'],
                'cost' => $input['cost'] ?? null,

                // 'type_of_item' => $input['type_of_item'],
                'account' => $input['account'] ?? null,

                'track_inventory' => $type == self::INVENTORY ? 1 : 0,
                'quantity' => $input['quantity'] ?? null,
                'init_quantity' => $input['quantity'] ?? null,
                'init_quantity_date' => $input['init_quantity_date'] ?? null,
                'unit_cost' => $input['unit_cost'] ?? null,

                'asset_account' => $input['asset_account'] ?? null,
                'income_account' => $input['income_account'] ?? null,
                'expense_account' => $input['expense_account'] ?? null,

                'user_id' => $user->id
            ];

            if (isset($input['image']) && $input['image'])  {
                $filename = $user->id.'-item-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/items");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                
                $itemImgInstance = Image::make($input['image']);
                /*if ($itemImgInstance->height() > 300) {
                    $itemImgInstance->fit(300)->save($path);
                } else {
                }*/
                $itemImgInstance->orientate()->save($path);

                $data['image'] = $filename;
            }

            $inventory = $user->company->inventories()->create($data);

            $memberships = $input['memberships'] ?? [];

            if ($memberships) {
                $inventory->memberships()->attach($memberships);
            }

            Mail::to($user->email)->send(new InventoryCreated($inventory));

            DB::commit();

            return $this->successResponse([
                'data' => new InventoryResource($inventory),
                'message' => trans('messages.inventory_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(Inventory $inventory)
    {
        return $this->successResponse(['data' => new InventoryResource($inventory)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventory $inventory)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $resource_associated = $inventory->items->isNotEmpty();

            $inventoryValidator = new InventoryValidator('update', $inventory, $inventory->type);

            $input['type'] = $inventory->type;

            if ($inventory->type != self::INVENTORY) {
                $input['income_account'] = ($input['is_product_sale'] ?? false) ? ($input['income_account'] ?? null) : null;
                $input['expense_account'] = ($input['is_product_purchase'] ?? false) ? ($input['expense_account'] ?? null) : null;
                $input['asset_account'] = null;
            }

            if (!$inventoryValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $inventoryValidator->getErrors()[0],
                    "messages" => $inventoryValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'reference' => $input['reference'],
                'description' => $input['description'],
                'barcode' => $input['barcode'],
                'sku' => $input['sku'] ?? null,
                'item_category_id' => $input['item_category_id'],
                'tax_id' => (int)$input['tax_id'] ? $input['tax_id'] : null,
                'sale_price' => $input['sale_price'],
                'cost' => $input['cost'] ?? null,

                // 'type_of_item' => $input['type_of_item'],
                'account' => $input['account'] ?? null,

                'asset_account' => $input['asset_account'] ?? null,
                'income_account' => $input['income_account'] ?? null,
                'expense_account' => $input['expense_account'] ?? null
            ];

            if(!$inventory->track && $inventory->quantity == null) {
                $data += [
                    'track_inventory' => $input['track_inventory'] ?? 0,
                    'quantity' => $input['quantity'] ?? null,
                    'init_quantity' => $input['quantity'] ?? null,
                    'unit_cost' => $input['unit_cost'] ?? null,
                ];
            }
            
            if (isset($input['image']) && $input['image'])  {
                $filename = $user->id.'-item-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/items");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                
                $itemImgInstance = Image::make($input['image']);
                /*if ($itemImgInstance->height() > 300) {
                    $itemImgInstance->fit(300)->save($path);
                } else {
                }*/
                $itemImgInstance->orientate()->save($path);
                
                $data['image'] = $filename;
            }

            $inventory->update($data);

            $memberships = $input['memberships'] ?? [];
            $inventory->memberships()->sync($memberships);

            DB::commit();

            return $this->successResponse([
                'data' => new InventoryResource($inventory),
                'message' => trans('messages.inventory_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Inventory $inventory)
    {
        try {
            if ($inventory->items->isEmpty()) {
                $inventory->delete();
                return $this->successResponse([
                    'message' => trans('messages.inventory_deleted')
                ]);
            } else {
                return $this->failResponse([
                    'message' => trans('messages.delete_prohibited')
                ], 403);
            }
            return redirect()->route('inventories.index');
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }

    /**
     * Searching of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        try {
            $user = $request->user();
            $input = $request->all();
            $limit = $input['limit'] ?? 28;
            $offset = $input['offset'] ?? 0;
            $searchValue = $input['searchValue'] ?? null;

            if ($searchValue) {
                $inventories = Inventory::search($searchValue)->where('company_id', $user->company->id)->get();
            } else {
                $inventories = $user->company->inventories;
            }
            $products = $inventories->slice($offset, $limit)->values();

            $data = [
                'products' => $products,
                'count' => $products->count(),
                'total_products' => $inventories->count()
            ];
            return $this->successResponse([
                'data' => $data
            ]);

        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }
}