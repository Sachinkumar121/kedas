<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\EstimateValidator;

use App\Http\Resources\Estimate as EstimateResource;
use App\Http\Resources\EstimateCollection;

use App\Estimate;
use App\Item;

use Illuminate\Support\Facades\Mail;
use App\Mail\EstimateShared;

use DB;
use Auth;
use Config;
use Image;
use PDF;
use Cache;

use App\Traits\ApiResponse;

class EstimateController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(Estimate::class, 'estimate');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $estimates = $user->company->estimates;

        // Why do this because it loads appends attribute.
        // which make recursive call.
        $estimates->each(function ($estimate, $key) {
            $estimate->setAppends([]);
        });

        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        if ($term) {
            $estimates = $estimates->reject(function ($element) use ($term) {
                // search according to the data-tables.
                return 
                    mb_strpos(strtolower($element->contact->name), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->phone), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->tax_id), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->calculation['total']), strtolower($term)) === false &&
                    mb_strpos(strtolower(dateFormat($element->internal_number)), strtolower($term)) === false &&
                    mb_strpos(strtolower(dateFormat($element->start_date)), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->notes), strtolower($term)) === false;
            })->values();
        }

        $paginateData = $estimates->paginate($perPage);
        $collection = new EstimateCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, EstimateValidator $estimateValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$estimateValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $estimateValidator->getErrors()[0],
                    "messages" => $estimateValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                return $this->failResponse([
                    "message" => trans('validation.exists', ['attribute' => trans('labels.contact')]),
                    "messages" => [trans('validation.exists', ['attribute' => trans('labels.contact')])],
                ], 400);
            }

            $estimateitems = collect($input['products'])->filter(function($row) {
                return $row['quantity'] && $row['item'];
                })->map(function($item) {
                    $item['inventory_id'] = $item['item'];
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    //unset($item['reference']);
                    //unset($item['price']);
                    unset($item['description']);
                    return $item;
            })->values()->toArray();

            if (!$estimateitems) {
                return $this->failResponse([
                    "message" => trans('messages.estimate_without_items'),
                    "messages" => [trans('messages.estimate_without_items')],
                ], 400);
            }

            $estimate = $contact->estimates()->create([
                'user_id' => $user->id,
                'company_id' => $user->company->id,
                'start_date' => $input['estimate_start_date'],
                'expiration_date' => $input['estimate_expiration_date'],
                'notes' => @$input['notes'],
                'exchange_currency_rate' => $input['exchange_rate'] ?? null,
                'exchange_currency_id' => $input['exchange_currency'] ?? null,
            ]);

            $estimate->items()->createMany($estimateitems);
            
            /* upload logo and signature */
            if (isset($input['logo']) && $input['logo']) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/profile-pics");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $logoInstance = Image::make($input['logo']);
                if ($logoInstance->height() > 300) {
                    $logoInstance->fit(300)->save($path);
                } else {
                    $logoInstance->orientate()->save($path);
                }
                $user->company->logo = $filename;
                $user->company->save();
            }

            if (isset($input['signature']) && $input['signature']) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/signatures");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $signatureInstance = Image::make($input['signature']);
                if ($signatureInstance->height() > 150) {
                    $signatureInstance->fit(300, 150)->save($path);
                } else {
                    $signatureInstance->orientate()->save($path);
                }
                $user->company->signature = $filename;
                $user->company->save();
            }

            /* Save exchange rate */
            if (isset($input['exchange_rate']) && $input['exchange_rate'] && isset($input['exchange_currency']) && $input['exchange_currency']) {
                $user->company->currencyRates()->updateOrCreate(
                    ['currency_id' => $input['exchange_currency'], 'user_id' => $user->id],
                    ['rate' => $input['exchange_rate']]
                );
            }

            DB::commit();

            return $this->successResponse([
                'data' => new EstimateResource($estimate),
                'message' => trans('messages.estimate_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function show(Estimate $estimate)
    {
        return $this->successResponse(['data' => new EstimateResource($estimate)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estimate $estimate)
    {
        $estimateValidator = new EstimateValidator('update');
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$estimateValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $estimateValidator->getErrors()[0],
                    "messages" => $estimateValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                return $this->failResponse([
                    "message" => trans('validation.exists', ['attribute' => trans('labels.contact')]),
                    "messages" => [trans('validation.exists', ['attribute' => trans('labels.contact')])],
                ], 400);
            }

            $estimateitems = collect($input['products'])->filter(function($row) {
                return $row['quantity'] && $row['item'];
                })->map(function($item) {
                    $item['inventory_id'] = $item['item'];
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    //unset($item['reference']);
                    //unset($item['price']);
                    unset($item['description']);
                    return $item;
                })->values();
            $updateEstimateItems = $estimateitems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertEstimateItems = $estimateitems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deleteEstimateItemsExcept = $updateEstimateItems->pluck('id')->toArray();

            if (!$estimateitems) {
                $request->session()->flash('error', trans('messages.estimate_without_items'));
                return back()
                    ->withInput();
            }

            $estimate->contact()->associate($contact);

            $estimate->update([
                'start_date' => $input['estimate_start_date'],
                'expiration_date' => $input['estimate_expiration_date'],
                'notes' => $input['notes'] ?? null
            ]);

            if ($deleteEstimateItemsExcept) {
                $estimate->items()->whereNotIn('id', $deleteEstimateItemsExcept)->delete();
            }

            if ($insertEstimateItems) {
                $estimate->items()->createMany($insertEstimateItems);
            }

            if ($updateEstimateItems) {
                foreach ($updateEstimateItems as $updateEstimateItem) {
                    $updatedDate = $updateEstimateItem;
                    unset($updatedDate['id']);
                    Item::findOrFail($updateEstimateItem['id'])->update($updatedDate);
                }
            }

            if (isset($input['logo']) && $input['logo'])  {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/profile-pics");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }
                $logoInstance = Image::make($input['logo']);
                if ($logoInstance->height() > 300) {
                    $logoInstance->fit(300)->save($path);
                } else {
                    $logoInstance->orientate()->save($path);
                }
                
                $user->company->logo = $filename;
                $user->company->save();
            }

            if (isset($input['signature']) && $input['signature'])  {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/signatures");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $signatureInstance = Image::make($input['signature']);
                if ($signatureInstance->height() > 150) {
                    $signatureInstance->fit(300, 150)->save($path);
                } else {
                    $signatureInstance->orientate()->save($path);
                }

                $user->company->signature = $filename;
                $user->company->save();
            }

            DB::commit();

            return $this->successResponse([
                'data' => new EstimateResource($estimate),
                'message' => trans('messages.estimate_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Estimate $estimate)
    {
        try {
            $estimate->delete();
            return $this->successResponse([
                'message' => trans('messages.estimate_deleted')
            ]);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }
}