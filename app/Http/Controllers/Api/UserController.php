<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

use App\Validators\User\ProfileValidator;
use App\Validators\ClientValidator;
use App\Traits\ApiResponse;

use App\User;
use App\Company;

use App\Transformers\UserTransformer;
use App\Events\ProfileCompleted;

use App\Http\Resources\User as UserResource;
use App\Http\Resources\Company as CompanyResource;
use App\Http\Resources\CurrencyExchangeRate as CurrencyExchangeRateResource;

use DB;
use Auth;
use Config;
use Image;
use Cache;
use Session;
use Validator;
use Lang;

class UserController extends Controller
{
    use ApiResponse;

    public function __construct()
    {

    }

    /**
     * User profile from kedas
     * 
     * @OA\Get(
     *     path="/api/v1/profile",
     *     tags={"Users"},
     *     operationId="profile",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     * )
     */
    public function index(Request $request)
    {
        try {
            $data['user'] = (new UserTransformer())->singleTransform($request->user());
            $response = ['message' => trans('messages.login_user_detail'), 'data' => $data];
            return $this->successResponse($response);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Update user to the kedas
     * 
     * @OA\Post(
     *     path="/api/v1/profile",
     *     tags={"Users"},
     *     operationId="updateProfile",
     *        security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         required=true,
     *     ),
     *      @OA\Parameter(
     *         name="address",
     *         in="query",
     *         required=true,
     *     ),
     *      @OA\Parameter(
     *         name="zip",
     *         in="query",
     *         required=true,
     *     ),
     *      @OA\Parameter(
     *         name="city",
     *         in="query",
     *         required=true,
     *     ),
     *      @OA\Parameter(
     *         name="state",
     *         in="query",
     *         required=true,
     *     ),
     *      @OA\Parameter(
     *         name="country",
     *         in="query",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         description="The response code",
     *                         example="john smith"
     *                     ),
     *                     @OA\Property(
     *                         property="address",
     *                         type="string",
     *                         description="The response message",
     *                         example="#12 ab street"
     *                     ),
     *                     @OA\Property(
     *                         property="city",
     *                         type="integer",
     *                         description="The response data",
     *                         example="london"
     *                     ),
     *                      @OA\Property(
     *                         property="state",
     *                         type="string",
     *                         description="The response data",
     *                         example="Scotland"
     *                     ),
     *                      @OA\Property(
     *                         property="country",
     *                         type="string",
     *                         description="227 = US",
     *                         example="227"
     *                     )
     *                 )
     *             )
     *         }
     *     )
     * )
     */

    public function updateProfile(Request $request)
    {
        $user = $request->user();
        try {
            DB::beginTransaction();
            $input = $request->all();

            $userUpdateValidator = new ProfileValidator('api_user_update');

            if (!$userUpdateValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $userUpdateValidator->getErrors()[0],
                    "messages" => $userUpdateValidator->getErrors()
                ], 422);
            }

            if (isset($input['logo'])) {
                $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                $dir = public_path("storage/profile-pics");
                $path = $dir."/".$filename;

                if( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                $logoInstance = Image::make($input['logo']);
                if ($logoInstance->height() > 300) {
                    $logoInstance->fit(300)->save($path);
                } else {
                    $logoInstance->orientate()->save($path);
                }
                $user->image = $filename;
            }

            $user->name = @$input['firstname']. ' ' . @$input['lastname'];

            $user->save();

            if ($user->addresses()->count()) {
                $user->addresses()->update([
                    'line_1' => $input['address'],
                    'city' => $input['city'],
                    'state' => $input['state'],
                    'country_id' => $input['country'],
                    'zip' => $input['zip'],
                ]);
            } else {
                $user->addresses()->create([
                    'line_1' => $input['address'],
                    'city' => $input['city'],
                    'state' => $input['state'],
                    'country_id' => $input['country'],
                    'zip' => $input['zip'],
                ]);                
            }

            DB::commit();

            return $this->successResponse([
                "message" => trans('messages.user_profile_update'),
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * set the locale for the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function setLocale(Request $request)
    {
        try {
            $input = $request->all();
            $user = auth('api')->user();

            $validator = Validator::make($request->all(), [
                'locale' => 'required|in:en,es'
            ]);
            if ($validator->fails()) {
                return $this->failResponse([
                    "message" => $validator->errors()->all()[0],
                ], 422);
            }
            if ($user) {
                $user->language = $input['locale'];
                $user->save();
            } else {
                app()->setLocale($input['locale']);
            }

            return $this->successResponse([
                "message" => trans('messages.user_profile_update'),
            ]);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }


    /**
     * complete the user profile.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function completeProfile(Request $request)
    {
        $user = $request->user();

        try {
            if ($user->company) {

                $response = [
                    'status' => false,
                    'message' => trans('messages.profile_already_completed'),
                    'messages' => []
                ];
                return $this->failResponse($response, 403);
            }
            DB::beginTransaction();
            $input = $request->all();
            $userUpdateValidator = new ProfileValidator;

            if (!$userUpdateValidator->with($input)->passes()) {
                $response = [
                    'message'   => $userUpdateValidator->getErrors()[0],
                    'messages'  => $userUpdateValidator->getErrors()
                ];
                return $this->failResponse($response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $data = $input;
            $data['sector_id'] = $input['sector'];
            $data['currency_id'] = $input['currency'];

            unset($data['sector']);
            unset($data['country']);
            unset($data['currency']);
            unset($data['address']);
            unset($data['city']);
            unset($data['user_type']);

            $data['user_id'] = $user->id;

            $company = new Company($data);
            $company->save();

            $user->user_type = $input['user_type'];
            $user->company_id = $company->id;
            $user->save();

            $address = $company->address()->create([
                'line_1' => $input['address'],
                'city' => $input['city'],
                'state' => $input['state'],
                'country_id' => $input['country']
            ]);
            
            event(new ProfileCompleted($user));

            DB::commit();

            $response = [
                'message' => Lang::get('messages.user_profile_update'),
            ];

            return $this->successResponse($response);

        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'message'   => $e->getMessage(),
                'messages'  => $e->getMessage()
            ];
            return $this->failResponse($response);
        }
    }

    /**
     * Get or update the user profile.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        $user = $request->user();

        try {
            if ($request->isMethod('put')) {
                DB::beginTransaction();
                $input = $request->all();
                $userUpdateValidator = new ProfileValidator('update');

                if (!$userUpdateValidator->with($input)->passes()) {
                    $response = [
                        'message'   => $userUpdateValidator->getErrors()[0],
                        'messages'  => $userUpdateValidator->getErrors()
                    ];
                    return $this->failResponse($response, Response::HTTP_UNPROCESSABLE_ENTITY);
                }

                $data = $input;

                if (isset($data['password']) && $data['password']) {
                    if (Hash::check($data['current_password'], $user->password)) {
                        $user->password = Hash::make($data['password']);
                    } else {
                        return $this->failResponse([
                            'message' => trans('messages.old_password_error')
                        ], 400);
                    }
                }

                $user->name = $data['name'];
                $user->language = $data['language'];
                $user->save();

                DB::commit();

                $response = [
                    'message' => Lang::get('messages.user_profile_update'),
                    'data' => new UserResource($user)
                ];

                return $this->successResponse($response);
            }

            $response = [
                'data' => new UserResource($user)
            ];
            return $this->successResponse($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'error'     => $e->getMessage()
            ];
            return $this->failResponse($response);
        }
    }

    /**
     * Get or update the user company information.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function company(Request $request)
    {
        $user = $request->user();
        $company = $user->company;

        try {
            if ($request->isMethod('post')) {

                DB::beginTransaction();
                $input = $request->all();
                $userUpdateValidator = new ProfileValidator('company');

                if (!$userUpdateValidator->with($input)->passes()) {
                    $response = [
                        'message'   => $userUpdateValidator->getErrors()[0],
                        'messages'  => $userUpdateValidator->getErrors()
                    ];
                    return $this->failResponse($response, Response::HTTP_UNPROCESSABLE_ENTITY);
                }

                $resource_associated = $company->invoices->isNotEmpty() || $company->transactions->isNotEmpty() || $company->creditNotes->isNotEmpty() || $company->estimates->isNotEmpty() || $company->debitNotes->isNotEmpty() || $company->purchaseOrders->isNotEmpty();

                $data = [
                    'name' => $input['name'],
                    'role' => $input['role'],
                    'phone' => $input['phone'],
                    'mobile' => $input['mobile'],
                    'tax_id' => $input['tax_id'],
                    'support_email' => $input['support_email'],
                ];

                if (!$resource_associated) {
                    $data['currency_id'] = $input['currency'];
                }

                if (isset($input['logo'])) {
                    $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                    $dir = public_path("storage/profile-pics");
                    $path = $dir."/".$filename;

                    if( !\File::isDirectory($dir) ) { 
                        \File::makeDirectory($dir, 493, true);
                    }

                    $logoInstance = Image::make($input['logo']);
                    if ($logoInstance->height() > 300) {
                        $logoInstance->fit(300)->save($path);
                    } else {
                        $logoInstance->orientate()->save($path);
                    }
                    $data['logo'] = $filename;
                }

                if (isset($input['signature']) && $input['signature']) {
                    $filename = $user->id.'-'.substr( md5( $user->id . '-' . time() ), 0, 15) . '.jpg';
                    $dir = public_path("storage/signatures");
                    $path = $dir."/".$filename;

                    if( !\File::isDirectory($dir) ) { 
                        \File::makeDirectory($dir, 493, true);
                    }
                    $signatureInstance = Image::make($input['signature']);

                    if ($signatureInstance->height() > 150) {
                        $signatureInstance->fit(300, 150)->save($path);
                    } else {
                        $signatureInstance->orientate()->save($path);
                    }
                    $data['signature'] = $filename;
                }

                $company->update($data);

                if ($company->address()) {
                    $company->address()->updateOrCreate([], [
                        'line_1' => $input['address'],
                        'city' => $input['city'],
                        'state' => $input['state'],
                        'country_id' => $input['country'],
                        'zip' => $input['zip'],
                    ]);
                } else {
                    $company->address()->create([
                        'line_1' => $input['address'],
                        'city' => $input['city'],
                        'state' => $input['state'],
                        'country_id' => $input['country'],
                        'zip' => $input['zip'],
                    ]);                
                }

                DB::commit();

                $response = [
                    'message' => Lang::get('messages.company_info_update'),
                    'data' => new CompanyResource($company)
                ];

                return $this->successResponse($response);
            }

            $response = [
                'data' => new CompanyResource($company)
            ];
            return $this->successResponse($response);
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'error'     => $e->getMessage()
            ];
            return $this->failResponse($response);
        }
    }

    /**
     * Get user's currencyExchangeRates.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function currencyExchangeRates(Request $request)
    {
        $user = $request->user();
        $currency_rates = $user->currencyRates;

        $response = [
            // 'data' => $currency_rates
            'data' => CurrencyExchangeRateResource::collection($currency_rates)
        ];
        return $this->successResponse($response);
    }
}