<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\SupplierValidator;

use App\Http\Resources\Supplier as SupplierResource;
use App\Http\Resources\SupplierCollection;

use App\Supplier;

use DB;
use Auth;
use Config;

use App\Traits\ApiResponse;

class SupplierController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(Supplier::class, 'supplier');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $suppliers = $user->company->suppliers;

        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        if ($term) {
            $suppliers = $suppliers->reject(function ($element) use ($term) {
                return mb_strpos(strtolower($element->name), strtolower($term)) === false 
                    && mb_strpos(strtolower($element->email), strtolower($term)) === false 
                    && mb_strpos(strtolower($element->phone), strtolower($term)) === false 
                    && mb_strpos(strtolower($element->mobile), strtolower($term)) === false 
                    && mb_strpos(strtolower($element->company_name), strtolower($term)) === false;
            })->values();
        }

        $paginateData = $suppliers->paginate($perPage);
        $collection = new SupplierCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SupplierValidator $supplierValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$supplierValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $supplierValidator->getErrors()[0],
                    "messages" => $supplierValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            $data = [
                'identification_type_id' => $input['identification_type'],
                'tax_id' => $input['tax_id'],
                'name' => $input['name'],
                'company_name' => $input['company_name'],
                'email' => $input['email'],
                'sector_id' => $input['sector'],
                'phone' => $input['phone'],
                'mobile' => $input['mobile'],
                'contact_name' => $input['contact_name'] ?? null,
                'contact_phone_number' => $input['contact_phone_number'] ?? null,
                'user_id' => $user->id,
            ];

            $supplier = $user->company->suppliers()->create($data);

            if ($input['address'] || $input['zip'] || $input['country'] || $input['state'] || $input['city']) {
                $addressData = [
                    'line_1' => $input['address'],
                    'zip' => $input['zip'],
                    'country_id' => $input['country'],
                    'state' => $input['state'],
                    'city' => $input['city'],
                ];
                $address = $supplier->address()->create($addressData);
            }

            DB::commit();

            return $this->successResponse([
                'data' => new SupplierResource($supplier),
                'message' => trans('messages.supplier_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        return $this->successResponse(['data' => new SupplierResource($supplier)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $supplierValidator = new SupplierValidator('update', $supplier);

            if (!$supplierValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $supplierValidator->getErrors()[0],
                    "messages" => $supplierValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            $data = [
                'identification_type_id' => $input['identification_type'],
                'tax_id' => $input['tax_id'],
                'name' => $input['name'],
                'company_name' => $input['company_name'],
                'email' => $input['email'],
                'sector_id' => $input['sector'],
                'phone' => $input['phone'],
                'mobile' => $input['mobile'],
                'contact_name' => $input['contact_name'] ?? null,
                'contact_phone_number' => $input['contact_phone_number'] ?? null,
            ];

            $supplier->update($data);

            if ($input['address'] || $input['zip'] || $input['country'] || $input['state'] || $input['city']) {
                $addressData = [
                    'line_1' => $input['address'],
                    'zip' => $input['zip'],
                    'country_id' => $input['country'],
                    'state' => $input['state'],
                    'city' => $input['city'],
                ];

                $supplier->address()->updateOrCreate([], $addressData);
            }

            DB::commit();

            return $this->successResponse([
                'data' => new SupplierResource($supplier),
                'message' => trans('messages.supplier_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Supplier $supplier)
    {
        try {
            if ($supplier->invoices->isEmpty() && $supplier->transactions->isEmpty() && $supplier->creditNotes->isEmpty() && $supplier->estimates->isEmpty() && $supplier->debitNotes->isEmpty() && $supplier->purchaseOrders->isEmpty()) {
                $supplier->forceDelete();
                return $this->successResponse([
                    'message' => trans('messages.supplier_deleted')
                ]);
            } else {
                return $this->failResponse([
                    'message' => trans('messages.delete_prohibited')
                ], 403);
            }
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }
}