<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\RoleValidator;

use App\Http\Resources\Role as RoleResource;
use App\Http\Resources\RoleCollection;

use App\Role;

use DB;
use Auth;
use Config;

use App\Traits\ApiResponse;

class RoleController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(Role::class, 'role');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $user = $request->user();
            $roles = $user->company->roles;

            $perPage = $request->input('per_page') ?? 10;
            $page = $request->input('page') ?? 1;

            $paginateData = $roles->paginate($perPage);
            $collection = RoleResource::collection($paginateData->getCollection());
            $metaInfo = $paginateData->toArray();
            unset($metaInfo['data']);

            return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RoleValidator $roleValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$roleValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $roleValidator->getErrors()[0],
                    "messages" => $roleValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            // guard_name string is required because it will pick api instead of web.
            $role = new Role(['name' => $input['name'], 'guard_name' => 'web']);
            $role->setTranslation('custom_name', 'en', $input['name'])->setTranslation('custom_name', 'es', $input['name']);
            $user->company->roles()->save($role);

            $role->syncPermissions($input['permissions']);

            DB::commit();

            return $this->successResponse([
                'data' => new RoleResource($role),
                'message' => trans('messages.role_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Role $role)
    {
        return $this->successResponse(['data' => new RoleResource($role)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $user = $request->user();
        $locale = app()->getLocale();

        try {
            DB::beginTransaction();
            $roleValidator = new RoleValidator('update', $role);
            $input = $request->all();

            if (!$roleValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $roleValidator->getErrors()[0],
                    "messages" => $roleValidator->getErrors(),
                ], 422);
            }

            $role->setTranslation('custom_name', $locale, $input['name']);
            $role->save();

            $role->syncPermissions($input['permissions']);

            DB::commit();

            return $this->successResponse([
                'data' => new RoleResource($role),
                'message' => trans('messages.role_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Role $role)
    {
        try {
            $role->delete();
            return $this->successResponse([
                'message' => trans('messages.role_deleted')
            ]);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }
}