<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\InvoiceValidator;

use App\Http\Resources\Invoice as InvoiceResource;
use App\Http\Resources\InvoiceCollection;

use App\Events\SaleInvoiceCreatedOrUpdated;
use App\Events\SupplierInvoiceCreatedOrUpdated;

use App\Invoice;
use App\Item;
use App\ConceptItem;
use App\Inventory;

use Illuminate\Support\Facades\Mail;
use App\Mail\InvoiceCreated;
use App\Mail\InvoiceShared;

use App\Services\InvoiceService;
use App\Exceptions\ServiceException;

use DB;
use Auth;
use Config;
use Image;

use App\Traits\ApiResponse;

class InvoiceController extends Controller
{
    use ApiResponse;

    protected $invoiceService;

    public function __construct(InvoiceService $invoiceService)
    {
        $this->authorizeResource(Invoice::class, 'invoice');
        $this->invoiceService = $invoiceService;
    }

    /**
     * Get the map of resource methods to ability names.
     *
     * @return array
     */
    protected function resourceAbilityMap()
    {
        return [
            'show' => 'view',
            'store' => 'create',
            'update' => 'update',
            'destroy' => 'delete',
            'void' => 'void',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $type = self::SALE)
    {
        $user = $request->user();
        $invoices = $user->company->invoices->where('type', $type)->values();

        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        if ($term) {
            $invoices = $invoices->reject(function ($element) use ($term) {
                // search according to the data-tables.
                return 
                    mb_strpos(strtolower($element->internal_number), strtolower($term)) === false &&

                    mb_strpos(strtolower($element->contact->name), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->phone), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->tax_id), strtolower($term)) === false &&

                    mb_strpos(strtolower($element->calculation['total']), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->calculation['paidAmount']), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->calculation['pendingAmount']), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->calculation['status']), strtolower($term)) === false &&

                    mb_strpos(strtolower($element->term), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->frequency), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->frequency_type), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->term_in_locale), strtolower($term)) === false &&

                    mb_strpos(strtolower(dateFormat($element->start_date)), strtolower($term)) === false &&
                    mb_strpos(strtolower(dateFormat($element->expiration_date)), strtolower($term)) === false &&
                    mb_strpos(strtolower(dateFormat($element->end_date)), strtolower($term)) === false &&

                    mb_strpos(strtolower($element->notes), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->tac), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->privacy_policy), strtolower($term)) === false;
            })->values();
        }

        // Why do this because it loads appends attribute.
        // which make recursive call.
        $invoices->each(function ($invoice, $key) {
            $invoice->setAppends([]);
            $invoice->unsetRelation('items')->unsetRelation('conceptItems')->unsetRelation('transactions')->unsetRelation('creditNotes')->unsetRelation('debitNotes')->unsetRelation('currency');
        });

        $paginateData = $invoices->paginate($perPage);
        $collection = new InvoiceCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $type = self::SALE)
    {
        $invoiceValidator = new InvoiceValidator($type);
        $input = $request->all();

        if (!$invoiceValidator->with($input)->passes()) {
            return $this->failResponse([
                "message" => $invoiceValidator->getErrors()[0],
                "messages" => $invoiceValidator->getErrors(),
            ], 422);
        }

        try {
            DB::beginTransaction();
            $invoice = $this->invoiceService->store($request, $type);
            DB::commit();
            return $this->successResponse([
                'data' => new InvoiceResource($invoice),
                'message' => trans('messages.invoice_created')
            ]);         
        } catch (\Exception | ServiceException $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
                "messages" => [$e->getMessage()],
            ], $e->getCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        return $this->successResponse(['data' => new InvoiceResource($invoice)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        $type = $invoice->type;
        $input = $request->all();

        // Redirect if the invoice is voided. 
        if ($invoice->is_void) {
            return $this->failResponse([
                "message" => trans('messages.void_invoice_edit'),
                "messages" => [trans('messages.void_invoice_edit')],
            ], 400);
        }

        // Redirect if the invoice is already paid. 
        if ($invoice->calculation['status'] == trans('labels.paid')) {
            return $this->failResponse([
                "message" => trans('messages.paid_invoice_edit'),
                "messages" => [trans('messages.paid_invoice_edit')],
            ], 400);
        }

        $invoiceValidator = new InvoiceValidator($type);
        if (!$invoiceValidator->with($input)->passes()) {
            return $this->failResponse([
                "message" => $invoiceValidator->getErrors()[0],
                "messages" => $invoiceValidator->getErrors(),
            ], 422);
        }

        try {
            DB::beginTransaction();
            $invoice = $this->invoiceService->update($request, $invoice);
            DB::commit();
            return $this->successResponse([
                'data' => new InvoiceResource($invoice),
                'message' => trans('messages.invoice_updated')
            ]);

        } catch (\Exception | ServiceException $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
                "messages" => [$e->getMessage()],
            ], $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Invoice $invoice)
    {
        if ($invoice->is_void) {
            return $this->failResponse([
                "message" => trans('messages.void_invoice_delete'),
                "messages" => [trans('messages.void_invoice_delete')],
            ], 400);
        }

        try {
            DB::beginTransaction();
            $this->invoiceService->destroy($request, $invoice);
            DB::commit();
            return $this->successResponse([
                'message' => trans('messages.invoice_deleted')
            ]);
        } catch (\Exception | ServiceException $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
                "messages" => [$e->getMessage()],
            ], $e->getCode());
        }
    }

    /**
     * Void a created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function void(Request $request, Invoice $invoice)
    {
        try {
            DB::beginTransaction();
            $this->invoiceService->void($request, $invoice);
            DB::commit();
            return $this->successResponse([
                'message' => trans('messages.invoice_void')
            ]);
        } catch (\Exception | ServiceException $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
                "messages" => [$e->getMessage()],
            ], $e->getCode());
        }
    }
}