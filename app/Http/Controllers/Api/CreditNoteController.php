<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\CreditNoteValidator;

use App\Http\Resources\CreditNote as CreditNoteResource;
use App\Http\Resources\CreditNoteCollection;

use App\CreditNote;
use App\Item;

use Illuminate\Support\Facades\Mail;
use App\Mail\CreditNoteCreated;
use App\Mail\CreditNoteShared;

use DB;
use Auth;
use Config;

use App\Traits\ApiResponse;

class CreditNoteController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(CreditNote::class, 'credit_note');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $creditNotes = $user->company->creditNotes;

        // Why do this because it loads appends attribute.
        // which make recursive call.
        $creditNotes->each(function ($creditNote, $key) {
            $creditNote->setAppends([]);
        });

        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        if ($term) {
            $creditNotes = $creditNotes->reject(function ($element) use ($term) {
                // search according to the data-tables.
                return 
                    mb_strpos(strtolower($element->contact->name), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->phone), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->tax_id), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->calculation['total']), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->calculation['remaining_amount']), strtolower($term)) === false &&
                    mb_strpos(strtolower(dateFormat($element->start_date)), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->notes), strtolower($term)) === false;
            })->values();
        }

        $paginateData = $creditNotes->paginate($perPage);
        $collection = new CreditNoteCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, CreditNoteValidator $creditNoteValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$creditNoteValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $creditNoteValidator->getErrors()[0],
                    "messages" => $creditNoteValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                return $this->failResponse([
                    "message" => trans('validation.exists', ['attribute' => trans('labels.contact')]),
                    "messages" => [trans('validation.exists', ['attribute' => trans('labels.contact')])],
                ], 400);
            }

            $creditNoteItems = collect($input['products'])->filter(function($row) {
                return $row['quantity'] && $row['item'];
                })->map(function($item) {
                    $item['inventory_id'] = $item['item'];
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    //unset($item['reference']);
                    //unset($item['price']);
                    unset($item['description']);
                    return $item;
            })->values()->toArray();

            if (!$creditNoteItems) {
                return $this->failResponse([
                    "message" => trans('messages.credit_notes_without_items'),
                    "messages" => [trans('messages.credit_notes_without_items')],
                ], 400);
            }

            $creditNote = $contact->creditNotes()->create([
                'user_id' => $user->id,
                'company_id' => $user->company->id,
                'start_date' => $input['credit_note_date'],
                'notes' => @$input['notes'],
                'ncf_id' => @$input['ncf_id'],
                'exchange_currency_rate' => $input['exchange_rate'] ?? null,
                'exchange_currency_id' => $input['exchange_currency'] ?? null,
            ]);

            $creditNote->items()->createMany($creditNoteItems);
            
            if ($creditNote->ncf_number) {
                $creditNote->ncf->current_number = $creditNote->ncf_number;
                $creditNote->ncf->save();
            }

            /* upload documents */
            $documents = $input['documents'] ?? [];

            if ($documents) {
                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($documents as $file) {
                    $name = 'credit-note-' . uniqid() . '.' . $file->extension();
                    $file->move($dir, $name);
                    $documents_data[] = [
                        'name' => $name,
                    ];
                }
                $creditNote->documents()->createMany($documents_data);
            }

            Mail::to($contact->email)->send(new CreditNoteCreated($creditNote));

            /* Save exchange rate */
            if (isset($input['exchange_rate']) && $input['exchange_rate'] && isset($input['exchange_currency']) && $input['exchange_currency']) {
                $user->company->currencyRates()->updateOrCreate(
                    ['currency_id' => $input['exchange_currency'], 'user_id' => $user->id],
                    ['rate' => $input['exchange_rate']]
                );
            }

            DB::commit();

            return $this->successResponse([
                'data' => new CreditNoteResource($creditNote),
                'message' => trans('messages.credit_note_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function show(CreditNote $creditNote)
    {
        return $this->successResponse(['data' => new CreditNoteResource($creditNote)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CreditNote $creditNote)
    {
        $creditNoteValidator = new CreditNoteValidator('update');
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$creditNoteValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $creditNoteValidator->getErrors()[0],
                    "messages" => $creditNoteValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                return $this->failResponse([
                    "message" => trans('validation.exists', ['attribute' => trans('labels.contact')]),
                    "messages" => [trans('validation.exists', ['attribute' => trans('labels.contact')])],
                ], 400);
            }

            $creditNoteItems = collect($input['products'])->filter(function($row) {
                return $row['quantity'] && $row['item'];
                })->map(function($item) {
                    $item['inventory_id'] = $item['item'];
                    $item['tax_id'] = $item['tax'] ?: null;
                    unset($item['item']);
                    unset($item['tax']);
                    //unset($item['reference']);
                    //unset($item['price']);
                    unset($item['description']);
                    return $item;
                })->values();

            if ($creditNoteItems->isEmpty()) {
                $request->session()->flash('error', trans('messages.credit_note_without_items'));
                return back()
                    ->withInput();
            }

            $updateCreditNoteItems = $creditNoteItems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertCreditNoteItems = $creditNoteItems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deleteCreditNoteItemsExcept = $updateCreditNoteItems->pluck('id')->toArray();

            $creditNote->contact()->associate($contact);

            $creditNote->update([
                'start_date' => $input['credit_note_date'],
                'notes' => @$input['notes']
            ]);

            $credit_note_total = $input['credit_note_total_hidden'] ?? $creditNote->calculation['total'];

            // associate credit note to invoices.
            $payments = $input['payments'] ?? [];
            $paymentItems = collect($payments)->filter(function($row) {
                return $row['invoice_id'] && (isset($row['amount']) && $row['amount'] > 0);
            })->values();

            // remove the association of credit note with other invoices. 
            if ($paymentItems->isEmpty()) {
                $creditNote->invoices()->detach();
            } else {
                $pluckedInvoices = $creditNote->contact->invoices->pluck('calculation', 'id')->map(function($value, $key) {
                    return $value['pendingAmountWithoutCreditNote'];
                })->toArray();

                $amountCorrect = $paymentItems->every(function ($value, $key) use ($pluckedInvoices) {
                    return $value['amount'] <= $pluckedInvoices[$value['invoice_id']];
                });

                if (!$amountCorrect) {
                    $request->session()->flash('error', trans('messages.credit_note_with_incorrect_amount'));
                    return back()->withInput();
                }

                $total_amount = $paymentItems->sum('amount');

                if ($total_amount > $credit_note_total) {
                    $request->session()->flash('error', trans('messages.credit_note_with_incorrect_amount'));
                    return back()->withInput();
                }
                $creditNote->invoices()->sync($paymentItems->keyBy('invoice_id')->toArray());
            }

            if ($deleteCreditNoteItemsExcept) {
                $creditNote->items()->whereNotIn('id', $deleteCreditNoteItemsExcept)->delete();
            }

            if ($insertCreditNoteItems) {
                $creditNote->items()->createMany($insertCreditNoteItems);
            }

            if ($updateCreditNoteItems) {
                foreach ($updateCreditNoteItems as $updateCreditNoteItem) {
                    $updatedDate = $updateCreditNoteItem;
                    unset($updatedDate['id']);
                    Item::findOrFail($updateCreditNoteItem['id'])->update($updatedDate);
                }
            }

            /* update documents start */
            $documents = $input['documents'] ?? [];
            $deleted_documents_ids = $input['deleted_documents_ids'] ?? [];

            if ($deleted_documents_ids) {
                $creditNote->documents()->whereIn('id', $deleted_documents_ids)->delete();
            }
        
            if ($documents) {
                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($documents as $file) {
                    $name = 'credit-note-' . uniqid() . '.' . $file->extension();
                    $file->move($dir, $name);
                    $documents_data[] = [
                        'name' => $name,
                    ];
                }
                $creditNote->documents()->createMany($documents_data);
            }
            /* update documents end */

            DB::commit();

            return $this->successResponse([
                'data' => new CreditNoteResource($creditNote),
                'message' => trans('messages.credit_note_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, CreditNote $creditNote)
    {
        try {
            $creditNote->delete();
            return $this->successResponse([
                'message' => trans('messages.credit_note_deleted')
            ]);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }
}