<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Traits\ApiResponse;
use App\Validators\ClientValidator;

use App\Transformers\StoreTransformer;

use App\User;
use App\Company;
use App\Client;

use DB;
use Auth;
use Config;
use Cache;

class StoreController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->countries = Cache::get('countries');
    }

    /**
     * listing of all stores
     * 
     * @OA\Get(
     *     path="/api/v1/stores",
     *     tags={"Store"},
     *     operationId="store",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Parameter(
     *         name="city",
     *         in="query",
     *     ),
     *      @OA\Parameter(
     *         name="state",
     *         in="query",
     *     ),
     *      @OA\Parameter(
     *         name="country",
     *         in="query",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="city",
     *                         type="string",
     *                         description="City value to search with",
     *                         example="abc"
     *                     ),
     *                     @OA\Property(
     *                         property="state",
     *                         type="string",
     *                         description="State value to search with",
     *                         example="hry"
     *                     ),
     *                     @OA\Property(
     *                         property="country",
     *                         type="integer",
     *                         description="Country id to search with",
     *                         example="227"
     *                     ),
     *                 )
     *             )
     *         }
     *     )
     * )
     */
    public function index(Request $request)
    {
        $input = $request->all();
        try {
            $condData = [];
            if ($request->has('city') && !empty($input['city'])) {
                $condData[] = ['city', 'like', '%' . $input['city'] . '%'];
            }

            if ($request->has('state') && !empty($input['state'])) {
                $condData[] = ['state', 'like', '%'.$input['state'] . '%'];
            }

            if ($request->has('country')  && !empty($input['country'])) {
                $condData[] = ['country_id', $input['country']];
            }

            $data['filter_store'] = Company::whereHas('Address', function($q) use ($condData) { return $q->where($condData);})->get();

            $data = (new StoreTransformer())->transform($data['filter_store']);

            $response = ['message'=> trans('messages.store_list_fetch'), 'data' => $data];
            return $this->successResponse($response);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 200);
        }
    }

    /**
     * Save client favourite stores from kedas
     * 
     * @OA\Post(
     *     path="/api/v1/stores/favourites",
     *     tags={"Store"},
     *     operationId="saveFavouriteStores",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Parameter(
     *         name="store_ids",
     *         in="query",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     * )
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();
            $data = [];
            $user = $request->user();

            $clientValidator = new ClientValidator('app_store_check');
            if (!$clientValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $clientValidator->getErrors()[0],
                    "messages" => $clientValidator->getErrors(),
                ], 200);
            }

            $store_ids = explode(",", $input['store_ids']);
            foreach ($store_ids as $key => $value) {
                $data[$value] = ['primary' => (Config::get('constants.primary.zero')), 'priority' => $key];
                $user->stores()->detach($value);
            }
            $user->stores()->attach($data);
            $response = ['message' => trans('messages.favourite_store_add')];
            return $this->successResponse($response);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 200);
        }
    }

    /**
     * Get single store detail
     * 
     * @OA\Get(
     *     path="/api/v1/stores/{store_id}",
     *     tags={"Store"},
     *     operationId="singleStoreDetails",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Parameter(
     *         name="store_id",
     *         in="path",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     * )
     */
    public function show(Company $store)
    {
        try {
            $data = (new StoreTransformer())->singleTransform($store);

            $response = ['message'=> trans('messages.store_data_fetch'), 'data' => $data];
            return $this->successResponse($response);

        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 200);
        }

    }

    /**
     * Delete client favourite stores from kedas
     * 
     * @OA\Delete(
     *     path="/api/v1/stores/favourites/{store_id}",
     *     tags={"Store"},
     *     operationId="deleteFavouriteStores",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Parameter(
     *         name="store_id",
     *         in="path",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     * )
     */
    public function destroy(Request $request, $company_id)
    {
        try {
            $user = $request->user();
            $del_response = $user->stores()->detach($company_id);
            if ($del_response) {
                $response = ['message'=> trans('messages.favourite_store_deleted')];
                return $this->successResponse($response);
            } else {
                return $this->failResponse([
                    "message" => trans('messages.something_went_wrong'),
                ], 200);    
            }
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 200);
        }
    }

    /**
     * Get client favourite stores
     * 
     * @OA\Get(
     *     path="/api/v1/stores/favourites",
     *     tags={"Store"},
     *     operationId="clientStores",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     * )
     */
    public function clientFavouriteStores(Request $request)
    {
        try {
            $user = $request->user();
            $stores = (new StoreTransformer())->transform($user->stores);
            $response = ['message'=> trans('messages.store_list_fetch'), 'data' => $stores];
            return $this->successResponse($response);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 200);
        }
    }

    /**
     * Save client primary stores
     * 
     * @OA\Put(
     *     path="/api/v1/stores/{store_id}/make-primary",
     *     tags={"Store"},
     *     operationId="saveClientStores",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Parameter(
     *         name="store_id",
     *         in="path",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     * )
     */
    public function savePrimaryStore(Request $request, $id)
    {
        try {
            $data = [];
            $user = $request->user();
            $favouriteStoreIds = $user->stores->pluck('id')->toArray();
            $idExist = in_array($id, $favouriteStoreIds);
            if (!$idExist) {
                return $this->failResponse([
                    "message" => trans('messages.store_data_not_found'),
                ], 200);    
            }

            foreach ($favouriteStoreIds as $key => $value) {
                if($id == $value){
                    $data[$value] = ['primary' => true, 'priority' => $key];
                    $this->checkAndCreateClient($id, $user);
                    $user->company_id = $value;
                    $user->save();
                } else {
                    $data[$value] = ['primary' => (Config::get('constants.primary.zero')), 'priority' => $key];
                }
            }
            
            $user->stores()->sync($data);
            $response = ['message'=> trans('messages.primary_store_add')];
            return $this->successResponse($response);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 200);
        }
    }

    public function checkAndCreateClient($company_id, $app_user){
        try {
            $company = Company::find($company_id);
            $clientExists = Client::where(['email' => $app_user->email, 'company_id' => $company_id])->first();
            if ($clientExists) {
                return $clientExists;
            }else{
                $data = [
                    'user_id' => $company->user_id,
                    'company_id' => $company->id,
                    'name' => $app_user->name,
                    'email' => $app_user->email,
                    'phone' => $app_user->phone
                ];
                $client = Client::create($data);
                if($user_address = $app_user->addresses()->first()->toArray()){
                    $addressData = [
                        'line_1' => $user_address['line_1'],
                        'line_2' => $user_address['line_2'],
                        'zip' => $user_address['zip'],
                        'country_id' => $user_address['country_id'],
                        'state' => $user_address['state'],
                        'city' => $user_address['city'],
                        'lat' => $user_address['lat'],
                        'lng' => $user_address['lng'],
                    ];
                    $address = $client->address()->create($addressData);
                    return $client;
                }
            }
        } catch (\Exception $e) {
            return false;
        }  
    }
}
