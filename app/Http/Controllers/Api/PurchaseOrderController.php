<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\PurchaseOrderValidator;

use App\Http\Resources\PurchaseOrder as PurchaseOrderResource;
use App\Http\Resources\PurchaseOrderCollection;

use App\PurchaseOrder;
use App\Item;
use App\ConceptItem;
use App\Inventory;

use DB;
use Auth;
use Config;

use App\Traits\ApiResponse;

class PurchaseOrderController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(PurchaseOrder::class, 'purchase_order');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $purchaseOrders = $user->company->purchaseOrders;

        // Why do this because it loads appends attribute.
        // which make recursive call.
        $purchaseOrders->each(function ($purchaseOrder, $key) {
            $purchaseOrder->setAppends([]);
        });

        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        if ($term) {
            $purchaseOrders = $purchaseOrders->reject(function ($element) use ($term) {
                // search according to the data-tables.
                return 
                    mb_strpos(strtolower($element->contact->name), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->phone), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->contact->tax_id), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->calculation['total']), strtolower($term)) === false &&
                    mb_strpos(strtolower(dateFormat($element->start_date)), strtolower($term)) === false &&
                    mb_strpos(strtolower(dateFormat($element->expiration_date)), strtolower($term)) === false &&
                    mb_strpos(strtolower(dateFormat($element->internal_number)), strtolower($term)) === false &&
                    mb_strpos(strtolower($element->notes), strtolower($term)) === false;
            })->values();
        }

        $paginateData = $purchaseOrders->paginate($perPage);
        $collection = new PurchaseOrderCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PurchaseOrderValidator $purchaseOrderValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$purchaseOrderValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $purchaseOrderValidator->getErrors()[0],
                    "messages" => $purchaseOrderValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                return $this->failResponse([
                    "message" => trans('validation.exists', ['attribute' => trans('labels.contact')]),
                    "messages" => [trans('validation.exists', ['attribute' => trans('labels.contact')])],
                ], 400);
            }

            $conceptItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::CONCEPT)) === self::CONCEPT;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['user_accounting_head_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;
                unset($item['item']);
                unset($item['tax']);
                unset($item['tax_amount']);
                return $item;
            })->values()->toArray();

            $inventoryItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::INVENTORY)) === self::INVENTORY;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['inventory_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;
                unset($item['item']);
                unset($item['tax']);
                unset($item['reference']);
                unset($item['description']);
                unset($item['tax_amount']);
                return $item;
            })->values()->toArray();

            $purchaseOrderItems = collect([])->concat($conceptItems)->concat($inventoryItems);

            if ($purchaseOrderItems->isEmpty()) {
                return $this->failResponse([
                    "message" => trans('messages.purchase_orders_without_items'),
                    "messages" => [trans('messages.purchase_orders_without_items')],
                ], 400);
            }

            $purchaseOrder = $contact->purchaseOrders()->create([
                'user_id' => $user->id,
                'company_id' => $user->company->id,
                'start_date' => $input['purchase_order_date'],
                'expiration_date' => $input['purchase_order_expiration_date'],
                'tac' => @$input['tac'],
                'notes' => @$input['notes'],
                'exchange_currency_rate' => $input['exchange_rate'] ?? null,
                'exchange_currency_id' => $input['exchange_currency'] ?? null,
            ]);

            $purchaseOrder->conceptItems()->createMany($conceptItems);

            $purchaseOrder->items()->createMany($inventoryItems);

            /* upload documents */
            $documents = $input['documents'] ?? [];

            if ($documents) {
                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($documents as $file) {
                    $name = 'purchase-order-' . uniqid() . '.' . $file->extension();
                    $file->move($dir, $name);
                    $documents_data[] = [
                        'name' => $name,
                    ];
                }
                $purchaseOrder->documents()->createMany($documents_data);
            }

            /* Save exchange rate */
            if (isset($input['exchange_rate']) && $input['exchange_rate'] && isset($input['exchange_currency']) && $input['exchange_currency']) {
                $user->company->currencyRates()->updateOrCreate(
                    ['currency_id' => $input['exchange_currency'], 'user_id' => $user->id],
                    ['rate' => $input['exchange_rate']]
                );
            }

            DB::commit();
            
            return $this->successResponse([
                'data' => new PurchaseOrderResource($purchaseOrder),
                'message' => trans('messages.purchase_order_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseOrder $purchaseOrder)
    {
        return $this->successResponse(['data' => new PurchaseOrderResource($purchaseOrder)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PurchaseOrder $purchaseOrder)
    {
        $purchaseOrderValidator = new PurchaseOrderValidator('update');

        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$purchaseOrderValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $purchaseOrderValidator->getErrors()[0],
                    "messages" => $purchaseOrderValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            list($contact_type, $contact_id) = explode('-', $input['contact_id']);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                return $this->failResponse([
                    "message" => trans('validation.exists', ['attribute' => trans('labels.contact')]),
                    "messages" => [trans('validation.exists', ['attribute' => trans('labels.contact')])],
                ], 400);
            }

            $conceptItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::CONCEPT)) === self::CONCEPT;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['user_accounting_head_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;

                if ($item['id']) {
                    list($row_type, $row_id) = explode('-', $item['id']);
                    $item['id'] = ($row_type === self::CONCEPT) ? $row_id : 0;
                }

                unset($item['item']);
                unset($item['tax']);
                unset($item['tax_amount']);
                return $item;
            })->values();

            $inventoryItems = collect($input['concepts'])->filter(function ($concept) {
                return substr($concept['item'], 0, strlen(self::INVENTORY)) === self::INVENTORY;
            })->filter(function($row) {
                return $row['quantity'] && $row['price'] && $row['item'];
            })->map(function($item) {
                list($item_type, $item_id) = explode('-', $item['item']);
                $item['inventory_id'] = $item_id;
                $item['tax_id'] = $item['tax'] ?: null;

                if ($item['id']) {
                    list($row_type, $row_id) = explode('-', $item['id']);
                    $item['id'] = ($row_type === self::INVENTORY) ? $row_id : 0;
                }

                unset($item['item']);
                unset($item['tax']);
                unset($item['reference']);
                unset($item['description']);
                unset($item['tax_amount']);
                return $item;
            })->values();

            $purchaseOrderItems = collect([])->concat($conceptItems)->concat($inventoryItems);

            if ($purchaseOrderItems->isEmpty()) {
                $request->session()->flash('error', trans('messages.purchase_order_without_items'));
                return back()->withInput();
            }

            $updatePurchaseOrderItems = $inventoryItems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertPurchaseOrderItems = $inventoryItems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deletePurchaseOrderItemsExcept = $updatePurchaseOrderItems->pluck('id')->toArray();

            // The following is for purchaseOrder's concept items
            $updatePurchaseOrderConceptItems = $conceptItems->filter(function($row) {
                return $row['id'];
            })->values();

            $insertPurchaseOrderConceptItems = $conceptItems->filter(function($row) {
                return !$row['id'];
            })->map(function($item) {
                unset($item['id']);
                return $item;
            })->values()->toArray();

            $deletePurchaseOrderConceptItemsExcept = $updatePurchaseOrderConceptItems->pluck('id')->toArray();

            $purchaseOrder->contact()->associate($contact);

            // update basic information
            $purchaseOrder->update([
                'start_date' => $input['purchase_order_date'],
                'expiration_date' => $input['purchase_order_expiration_date'],
                'tac' => @$input['tac'],
                'notes' => @$input['notes']
            ]);

            if ($deletePurchaseOrderItemsExcept) {
                $purchaseOrder->items()->whereNotIn('id', $deletePurchaseOrderItemsExcept)->delete();
            }

            if ($insertPurchaseOrderItems) {
                $purchaseOrder->items()->createMany($insertPurchaseOrderItems);
            }

            if ($updatePurchaseOrderItems) {
                foreach ($updatePurchaseOrderItems as $updatePurchaseOrderItem) {
                    $updatedData = $updatePurchaseOrderItem;
                    unset($updatedData['id']);
                    Item::findOrFail($updatePurchaseOrderItem['id'])->update($updatedData);
                }
            }

            if ($deletePurchaseOrderConceptItemsExcept) {
                $purchaseOrder->conceptItems()->whereNotIn('id', $deletePurchaseOrderConceptItemsExcept)->delete();
            }

            if ($insertPurchaseOrderConceptItems) {
                $purchaseOrder->conceptItems()->createMany($insertPurchaseOrderConceptItems);
            }

            if ($updatePurchaseOrderConceptItems) {
                foreach ($updatePurchaseOrderConceptItems as $updatePurchaseOrderConceptItem) {
                    $updatedData = $updatePurchaseOrderConceptItem;
                    unset($updatedData['id']);
                    ConceptItem::findOrFail($updatePurchaseOrderConceptItem['id'])->update($updatedData);
                }
            }

            if ($inventoryItems->isEmpty()) {
                $purchaseOrder->items()->delete();
            }

            if ($conceptItems->isEmpty()) {
                $purchaseOrder->conceptItems()->delete();
            }

            /* update documents start */
            $documents = $input['documents'] ?? [];
            $deleted_documents_ids = $input['deleted_documents_ids'] ?? [];

            if ($deleted_documents_ids) {
                $purchaseOrder->documents()->whereIn('id', $deleted_documents_ids)->delete();
            }
        
            if ($documents) {
                $dir = public_path("storage/documents");
                if ( !\File::isDirectory($dir) ) { 
                    \File::makeDirectory($dir, 493, true);
                }

                foreach ($documents as $file) {
                    $name = 'purchase-order-' . uniqid() . '.' . $file->extension();
                    $file->move($dir, $name);
                    $documents_data[] = [
                        'name' => $name,
                    ];
                }
                $purchaseOrder->documents()->createMany($documents_data);
            }
            /* update documents end */

            DB::commit();

            return $this->successResponse([
                'data' => new PurchaseOrderResource($purchaseOrder),
                'message' => trans('messages.purchase_order_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, PurchaseOrder $purchaseOrder)
    {
        try {
            $purchaseOrder->delete();
            return $this->successResponse([
                'message' => trans('messages.purchase_order_deleted')
            ]);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }
}