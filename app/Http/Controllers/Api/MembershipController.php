<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\MembershipValidator;

use App\Http\Resources\Membership as MembershipResource;
use App\Http\Resources\MembershipCollection;

use App\Membership;

use DB;
use Auth;
use Config;

use App\Traits\ApiResponse;

class MembershipController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        $this->authorizeResource(Membership::class, 'membership');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $memberships = $user->company->memberships;

        $term = $request->input('term') ?? null;
        $perPage = $request->input('per_page') ?? 10;
        $page = $request->input('page') ?? 1;

        if ($term) {
            $memberships = $memberships->reject(function ($element) use ($term) {
                return mb_strpos(strtolower($element->name), strtolower($term)) === false && mb_strpos(strtolower($element->percentage), strtolower($term)) === false && mb_strpos(strtolower($element->description), strtolower($term)) === false;
            })->values();
        }

        $paginateData = $memberships->paginate($perPage);
        $collection = new MembershipCollection($paginateData->getCollection());
        $metaInfo = $paginateData->toArray();
        unset($metaInfo['data']);

        return $this->successResponse(['data' => $collection, 'meta' => $metaInfo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, MembershipValidator $membershipValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$membershipValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $membershipValidator->getErrors()[0],
                    "messages" => $membershipValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'description' => $input['description'] ?? null,
                'discount' => $input['discount'] ?? null,
                'point_percentage' => $input['point_percentage'] ?? null,
                'created_date' => $input['created_date'],
                'point_expire_date' => $input['point_expire_date'] ?? null,
                'user_id' => $user->id
            ];

            $membership = $user->company->memberships()->create($data);

            DB::commit();

            return $this->successResponse([
                'data' => new MembershipResource($membership),
                'message' => trans('messages.membership_created')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function show(Membership $membership)
    {
        return $this->successResponse(['data' => new MembershipResource($membership)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Membership $membership, MembershipValidator $membershipValidator)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$membershipValidator->with($input)->passes()) {
                return $this->failResponse([
                    "message" => $membershipValidator->getErrors()[0],
                    "messages" => $membershipValidator->getErrors(),
                ], 422);
            }

            $user = $request->user();

            $data = [
                'name' => $input['name'],
                'description' => $input['description'] ?? null,
                'discount' => $input['discount'] ?? null,
                'point_percentage' => $input['point_percentage'] ?? null,
                'created_date' => $input['created_date'],
                'point_expire_date' => $input['point_expire_date'] ?? null,
            ];

            $membership->update($data);

            DB::commit();

            return $this->successResponse([
                'data' => new MembershipResource($membership),
                'message' => trans('messages.membership_updated')
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Membership $membership)
    {
        try {
            $membership->delete();
            return $this->successResponse([
                'message' => trans('messages.membership_deleted')
            ]);
        } catch (\Exception $e) {
            return $this->failResponse([
                "message" => $e->getMessage(),
            ], 500);
        } 
    }
}