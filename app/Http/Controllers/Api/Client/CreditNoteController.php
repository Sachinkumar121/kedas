<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Traits\ApiResponse;
use App\Transformers\DebitCreditNoteTransformer;
use DB;
use Auth;

class CreditNoteController extends Controller
{
	use ApiResponse;

    /**
     * listing of all credit notes
     * 
     * @OA\Get(
     *     path="/api/v1/clients/credit-notes",
     *     tags={"Credit Notes"},
     *     operationId="creditnotes",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }	
     *     )
     * )
     */

    public function index(Request $request)
    {
    	try{
    		$client = $request->user()->getClientInfo();
    		$creditNotes = $client->creditNotes;
    		$response = (new DebitCreditNoteTransformer())->transform($creditNotes);
    		if($response){
    			return $this->successResponse([
    				"message" => trans('messages.data_fetched'),
    				"data" => $response
    			]);
    		}else{
    			return $this->failResponse([
    				"message" => trans('messages.no_result_found'),
    			], 200);
    		}
    	} catch (\Exception $e) {
    		return $this->failResponse([
    			"message" => $e->getMessage(),
    		], 200);
    	}
    }
}
