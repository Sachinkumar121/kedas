<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Transformers\InvoiceTransformer;

use App\Validators\User\ProfileValidator;
use App\Validators\ClientValidator;
use App\Traits\ApiResponse;

use App\User;
use App\Company;
use App\Client;
use App\Invoice;

use Exception; 
use DB;
use Auth;
use Config;
use Image;
use Cache;
use PDF;
use File;
use Validator;

class InvoiceController extends Controller
{
	use ApiResponse;

    /**
     * listing of all invoices type = sale|recurring, default is sale
     * 
     * @OA\Get(
     *     path="/api/v1/clients/invoices/{type}",
     *     tags={"Invoices"},
     *     operationId="invoices",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *      @OA\Parameter(
     *         name="type",
     *         in="path",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     * )
     */
    public function index(Request $request, $type = self::SALE)
    {
    	try{
    		$client = $request->user()->getClientInfo();
    		if ($client) {
    			$invoices = $client->invoices->where('type', $type)->values();
    			$response = (new InvoiceTransformer())->transform($invoices);

    			return $this->successResponse([
    				"message" => trans('messages.data_fetched'),
    				"data" => $response
    			]);
    		} else {
    			return $this->failResponse([
    				"message" => trans('messages.no_result_found'),
    			], 200);
    		}
    	} catch (\Exception $e) {
    		return $this->failResponse([
    			"message" => $e->getMessage(),
    		], 200);
    	}
    }

    /**
     * Get single invoice detail
     * 
     * @OA\Get(
     *     path="/api/v1/clients/invoices/{invoice_id}",
     *     tags={"Invoices"},
     *     operationId="singleInvoices",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Parameter(
     *         name="invoice_id",
     *         in="path",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     * )
     */
    public function show(Request $request, Invoice $invoice)
    {
      if (!$request->user()->can('view', [Invoice::class, $invoice] )) {
       return $this->failResponse([
        "message" => trans('messages.account_access')
      ], 200);
     }

     try {
      $invoice = (new InvoiceTransformer())->singleTransform($invoice);
      return $this->successResponse([
        "message" => trans('messages.data_fetched'),
        "data" => $invoice
      ]);
    } catch (\Exception $e) {
      return $this->failResponse([
       "message" => $e->getMessage(),
     ], 200);
    }
  }


    /**
     * Save client favourite stores from kedas
     * 
     * @OA\Post(
     *     path="/api/v1/stores/favourites",
     *     tags={"Store"},
     *     operationId="saveFavouriteStores",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Parameter(
     *         name="store_ids",
     *         in="query",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     * )
     */
    public function uploadReceipt(Request $request, Invoice $invoice)
    {
      if (!$request->user()->can('view', [Invoice::class, $invoice] )) {
        return $this->failResponse([
          "message" => trans('messages.account_access')
        ], 200);
      }

      try {
        $input = $request->all();

        $validator = Validator::make($request->all(), [
         'documents[]' => 'nullable|image|mimes:jpeg,bmp,png,jpg'
       ]);

        if ($validator->fails()) {
         return $this->failResponse([
          "message" => $validator->errors()->all()[0],
        ], 200);
       }

       if (isset($input['documents']) && $input['documents']) {
         $files = $input['documents'];

         $dir = public_path("storage/documents");
         if ( !\File::isDirectory($dir) ) {
          \File::makeDirectory($dir, 493, true);
        }

        foreach ($files as $key => $file) {

          if (isset($file)) {
           $filename = 'invoice-receipt-'.($key + 1).'-'.substr( md5( $invoice->id . '-' . time() ), 0, 15) .'.jpg';
           $dir = public_path("storage/documents");
           $path = $dir."/".$filename;

           if( !\File::isDirectory($dir) ) { 
            \File::makeDirectory($dir, 493, true);
          }

          $logoInstance = Image::make($file);
          if ($logoInstance->height() > 300) {
            $logoInstance->fit(300)->save($path);
          } else {
            $logoInstance->orientate()->save($path);
          }

          $documents_data[] = [
            'name' => $filename,
            'type' => self::PAYMENTRECEIPT,
            'uploaded_by' => Auth::user()->id
          ];
        }
      }

      $documents = $invoice->documents()->createMany($documents_data);

      if ($documents) {
        return $this->successResponse(["message" => trans('messages.receipt_uploaded_successfully')]);
      } else {
        return $this->failResponse([
         "message" => trans('messages.something_went_wrong'),
       ], 200);
      }
    } else {
     return $this->failResponse([
      "message" => trans('messages.upload_receipt'),
    ], 200);
   }

 } catch (\Exception $e) {
  return $this->failResponse([
   "message" => $e->getMessage(),
 ], 200);
}
}

    /**
     * download file
     * 
     */
    public function download(Request $request, Invoice $invoice)
    {
      if (!$request->user()->can('view', [Invoice::class, $invoice] )) {
        return $this->failResponse([
          "message" => trans('messages.account_access')
        ], 200);
      }

      try {
        $user = $invoice->user;
            // return view('pdf.invoice', compact('invoice'));
        $client = $request->user()->getClientInfo();
    		// preprint(Auth::user()->primaryStore());
        $filename = 'invoice-'.$invoice->internal_number.'.pdf';
          // $pdf = PDF::loadView('pdf.invoice-template-15', compact('invoice', 'user'));
        $viewType = 'view-order';
        $pdf =  view('pdf.invoice-template-15', compact('invoice', 'user', 'viewType'))->render();
            // echo $pdf; die();
          // preprint($response);
          // $pdf->setPaper('Letter');
          // $pdf->setOption('enable-javascript', true);
          // $pdf->setOption('enable-smart-shrinking', true);
          // $pdf->setOption('no-stop-slow-scripts', true);

          // return $pdf->download($filename);

        return $this->successResponse([
         "message" => trans('messages.data_fetched'),
         "data" => $pdf
       ]);
      } catch (\Exception $e) {
        return $this->failResponse([
         "message" => $e->getMessage(),
       ], 200);
      }
    }


    /**
     * Pay now api from kedas
     * 
     * @OA\Post(
     *     path="clients/invoices/{invoice_id}/pay-now",
     *     tags={"Store"},
     *     operationId="saveFavouriteStores",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Parameter(
     *         name="invoice_id",
     *         in="query",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     * )
     */
    public function payNow(Request $request, Invoice $invoice)
    {
      if (!$request->user()->can('view', [Invoice::class, $invoice] )) {
        return $this->failResponse([
          "message" => trans('messages.account_access')
        ], 200);
      }

      try {
        $input = $request->all();

        $validator = Validator::make($request->all(), [
         'documents[]' => 'nullable|image|mimes:jpeg,bmp,png,jpg'
       ]);

        if ($validator->fails()) {
         return $this->failResponse([
          "message" => $validator->errors()->all()[0],
        ], 200);
       }

       if (isset($input['documents']) && $input['documents']) {
         $files = $input['documents'];

         $dir = public_path("storage/documents");
         if ( !\File::isDirectory($dir) ) {
          \File::makeDirectory($dir, 493, true);
        }

        foreach ($files as $key => $file) {

          if (isset($file)) {
           $filename = 'invoice-receipt-'.($key + 1).'-'.substr( md5( $invoice->id . '-' . time() ), 0, 15) .'.jpg';
           $dir = public_path("storage/documents");
           $path = $dir."/".$filename;

           if( !\File::isDirectory($dir) ) { 
            \File::makeDirectory($dir, 493, true);
          }

          $logoInstance = Image::make($file);
          if ($logoInstance->height() > 300) {
            $logoInstance->fit(300)->save($path);
          } else {
            $logoInstance->orientate()->save($path);
          }

          $documents_data[] = [
            'name' => $filename,
            'type' => self::PAYMENTRECEIPT,
            'uploaded_by' => Auth::user()->id
          ];
        }
      }

      $documents = $invoice->documents()->createMany($documents_data);

      if ($documents) {
        return $this->successResponse(["message" => trans('messages.receipt_uploaded_successfully')]);
      } else {
        return $this->failResponse([
         "message" => trans('messages.something_went_wrong'),
       ], 200);
      }
    } else {
     return $this->failResponse([
      "message" => trans('messages.upload_receipt'),
    ], 200);
   }

 } catch (\Exception $e) {
  return $this->failResponse([
   "message" => $e->getMessage(),
 ], 200);
}
}

}
