<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Validators\User\ProfileValidator;
use App\Validators\ClientValidator;
use App\Traits\ApiResponse;

use App\Transformers\PaymentTransformer;

use App\User;
use App\Company;
use App\Client;
use App\Transaction;

use DB;
use Auth;
use Config;
use Image;
use Cache;
use PDF;

class PaymentController extends Controller
{
	use ApiResponse;

    /**
     * listing of all payments
     * 
     * @OA\Get(
     *     path="/api/v1/clients/payments",
     *     tags={"Payments"},
     *     operationId="payments",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *     )
     * )
     */
    public function index(Request $request)
    { 
    	try {
    		$client = $request->user()->getClientInfo();
    		$transactions = (new PaymentTransformer())->transform($client->transactions);
    		if ($transactions) {
    			return $this->successResponse([
    				"message" => trans('messages.data_fetched'),
    				"data" => $transactions
    			]);
    		}else{
    			return $this->failResponse([
    				"message" => trans('messages.no_result_found'),
    			], 200);
    		}
    	} catch (\Exception $e) {
    		return $this->failResponse([
    			"message" => $e->getMessage(),
    		], 200);
    	}
    }

    /**
     * Get single payment detail
     * 
     * @OA\Get(
     *     path="/api/v1/clients/payments/{payment_id}",
     *     tags={"Payments"},
     *     operationId="singlePayments",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Parameter(
     *         name="payment_id",
     *         in="path",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }
     *     )
     * )
     */
    public function show(Request $request, Transaction $transaction)
    {
        if (!$request->user()->can('view', [Transaction::class, $transaction] )) {
            return $this->failResponse([
                "message" => trans('messages.account_access')
            ], 200);
        }
        
    	try {
    		$transactions = (new PaymentTransformer())->singleTransform($transaction);
    		return $this->successResponse([
    			"message" => trans('messages.data_fetched'),
    			"data" => $transactions
    		]);
    	} catch (\Exception $e) {
    		return $this->failResponse([
    			"message" => $e->getMessage(),
    		], 200);
    	}
    }

}
