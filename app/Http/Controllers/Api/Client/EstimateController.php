<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Traits\ApiResponse;
use App\Transformers\EstimateTransformer;
use DB;
use Auth;

class EstimateController extends Controller
{
	use ApiResponse;

    /**
     * listing of all estimates
     * 
     * @OA\Get(
     *     path="/api/v1/clients/estimates",
     *     tags={"Estimates"},
     *     operationId="estimates",
     *     security={
     *          {"passport": {}},
     *      },
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ok",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *             )
     *         }	
     *     )
     * )
     */
    
    public function index(Request $request)
    {
    	try{
    		$client = $request->user()->getClientInfo();
    		$estimates = (new EstimateTransformer())->transform($client->estimates);
    		if($estimates){
    			return $this->successResponse([
    				"message" => trans('messages.data_fetched'),
    				"data" => $estimates
    			]);
    		}else{
    			return $this->failResponse([
    				"message" => trans('messages.no_result_found'),
    			], 200);
    		}
    	} catch (\Exception $e) {
    		return $this->failResponse([
    			"message" => $e->getMessage(),
    		], 200);
    	}
    }
}
