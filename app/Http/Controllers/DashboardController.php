<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

use App\Supplier;
use App\Client;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if ($request->user()->isSuperAdmin()) {
            return redirect('/admin/dashboard');
        }

        $type = $request->has('type') ? $request->input('type') : "last_month";
        $graphTypeLastMonths = $request->has('graphTypeLastMonths') ? $request->input('graphTypeLastMonths') : "";
        $user = $request->user();

        $client_tz = $_COOKIE['client_tz'] ?? date_default_timezone_get();
        $date = Carbon::now($client_tz);
        if($graphTypeLastMonths) {
            $year = Carbon::createFromFormat('Y-m', $graphTypeLastMonths)->year;
            $month = Carbon::createFromFormat('Y-m', $graphTypeLastMonths)->month;
            $startDate = Carbon::create($year, $month)->startOfMonth()->format('Y-m-d');
            $endDate = Carbon::create($year, $month)->lastOfMonth()->format('Y-m-d');
            $filterType = "day";
        } else {
            if($type=="this_week") {
               $startDate = $date->startOfWeek(Carbon::SUNDAY)->format('Y-m-d');
               $endDate = $date->endOfWeek(Carbon::SATURDAY)->format('Y-m-d'); 
               $filterType = "day";
            } else if($type=="this_month") {
                $startDate = $date->startOfMonth()->format('Y-m-d');
                $endDate = $date->endOfMonth()->format('Y-m-d');
                $filterType = "day";
            } else if($type=="last_7_days") {
                $startDate = $date->yesterday()->subDays(6);
                $endDate = $date->yesterday();
                $filterType = "day";
            } else if($type=="last_month") {
                $startDate = new Carbon('first day of last month', $client_tz);
                $startDate =  $startDate->format('Y-m-d');
                $endDate = new Carbon('last day of last month', $client_tz);
                $endDate = $endDate->format('Y-m-d');
                $filterType = "day";
            } else {
                $startDate = $date->copy()->startOfYear();
                $endDate   = $date->copy()->endOfYear();
                $filterType = "month";
            }
        }

        $period = CarbonPeriod::create($startDate, $endDate);
        $dates = $period->map(function (Carbon $date) {
            return $date->format('d');
        });

        if($type=="year") {
            $datesArray = array_fill(0, 12, null);
        } else {
            $datesArray = collect(iterator_to_array($dates))->mapWithKeys(function ($item) {
                return [$item => 0];
            })->toArray();
        }
        
        $report = [];
        $invoices = $user->company ? $user->company->invoices()->where('is_void', false)->where('start_date','>=',$startDate)->where('start_date','<=', $endDate)->get() : collect([]);

        $transactions = $user->company ? $user->company->transactions()->where('start_date','>=',$startDate)->where('start_date','<=', $endDate)->get() : collect([]);

        $saleInvoices = $invoices->where('type', 'sale')->values();
        $supplierInvoices = $invoices->where('type', 'supplier')->values();

        $inTransactions = $transactions->where('type', 'in')->values();
        $outTransactions = $transactions->where('type', 'out')->values();

        $groupedSaleInvoices = $saleInvoices->groupBy(function ($item, $key) use ($filterType) {
            return sprintf('%02d',date_parse_from_format("Y-m-d", $item['start_date'])[$filterType]);  
        })->map(function ($row) {
            return $row->sum(function ($invoice) {
                return $invoice->calculation['total'];
            });
        })->toArray();
        
        $groupedSupplierInvoices = $supplierInvoices->groupBy(function ($item, $key) use ($filterType) {
            return sprintf('%02d',date_parse_from_format("Y-m-d", $item['start_date'])[$filterType]);
        })->map(function ($row) {
            return $row->sum(function ($invoice) {
                return $invoice->calculation['total'];
            });
        })->toArray();

        $groupedInTransactions = $inTransactions->groupBy(function ($item, $key) use ($filterType) {
            // why minus 1 becuase the index start from the 0.
            return sprintf('%02d',date_parse_from_format("Y-m-d", $item['start_date'])[$filterType]);
        })->map(function ($row) {
            return $row->sum('amount');
        })->toArray();

        $groupedOutTransactions = $outTransactions->groupBy(function ($item, $key) use ($filterType) {
            // why minus 1 becuase the index start from the 0. 
            return sprintf('%02d',date_parse_from_format("Y-m-d", $item['start_date'])[$filterType]);
        })->map(function ($row) {
            return $row->sum('amount');
        })->toArray();
        
       /*  $sale_pending = array_fill(0, 12, null);
        $expenses_pending = array_fill(0, 12, null);

        $sale_paid = array_fill(0, 12, null);
        $expenses_paid = array_fill(0, 12, null); */

        $sale_pending = array_values(array_replace($datesArray , $groupedSaleInvoices));
       
        $expenses_pending = array_values(array_replace($datesArray, $groupedSupplierInvoices));

        $sale_paid = array_values(array_replace($datesArray, $groupedInTransactions));
        $expenses_paid = array_values(array_replace($datesArray, $groupedOutTransactions));

        $groupedSupplierInvoiceBasedOnContacts = $supplierInvoices->groupBy(function ($item, $key) {
            return str_replace('app\\', '', strtolower($item->contact_type)).'-'.$item->contact_id;
        })->map(function ($row) {
            return $row->sum(function ($invoice) {
                return $invoice->calculation['subtotal'];
            });
        });
        $totalOfSupplierInvoices = $groupedSupplierInvoiceBasedOnContacts->slice(0, 6)->sum();
        $expenses_suppliers = $groupedSupplierInvoiceBasedOnContacts->map(function($item, $key) use($totalOfSupplierInvoices, $user){
            list($contact_type, $contact_id) = explode('-', $key);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
                return back()->withInput();
            }
            return [
                'name' => $contact->name,
                'percentage' => round(($item * 100)/$totalOfSupplierInvoices, 2),
                'total' => $item
            ];
        })->slice(0, 6)->sortByDesc(function ($invoice, $key) {
            return $invoice['percentage'];
        })->values()->toArray();

        $groupedSaleInvoiceBasedOnContacts = $saleInvoices->groupBy(function ($item, $key) {
            return str_replace('app\\', '', strtolower($item->contact_type)).'-'.$item->contact_id;
        })->map(function ($row) {
            return $row->sum(function ($invoice) {
                return $invoice->calculation['subtotal'];
            });
        });
        $totalOfSaleInvoices = $groupedSaleInvoiceBasedOnContacts->slice(0, 6)->sum();
        $sale_best_clients = $groupedSaleInvoiceBasedOnContacts->map(function($item, $key) use($totalOfSaleInvoices, $user){
            list($contact_type, $contact_id) = explode('-', $key);
            $contact = $contact_type == self::SUPPLIER ? $user->company->suppliers->where('id', $contact_id)->first() : $user->company->clients->where('id', $contact_id)->first();

            if (!$contact) {
                $request->session()->flash('error', trans('validation.exists', ['attribute' => trans('labels.contact')]));
                return back()->withInput();
            }
            return [
                'name' => $contact->name,
                'percentage' => round(($item * 100)/$totalOfSaleInvoices, 2),
                'total' => $item
            ];
        })->slice(0, 6)->sortByDesc(function ($invoice, $key) {
            return $invoice['percentage'];
        })->values()->toArray();

        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = collect(
                [
                    'sales' => ['pending' => $sale_pending, 'paid' => $sale_paid, 'best_clients' => $sale_best_clients], 
                    'expenses' => ['pending' => $expenses_pending, 'paid' => $expenses_paid, 'suppliers' => $expenses_suppliers],
                ]
            );
            return response()->json(['data' => $jsonCollection]);
        }
        
        $have_pending_data = array_filter($sale_pending) || array_filter($expenses_pending);
        $have_paid_data = array_filter($sale_paid) || array_filter($expenses_paid);

        return view('dashboard', compact('have_pending_data', 'have_paid_data'));
    }
}