<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Supplier;
use App\Client;
use App\Item;
use App\ConceptItem;
use App\Inventory;
use App\User;
use App\Estimate;
use App\PurchaseOrder;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Validators\InvoiceValidator;
use App\Mail\InvoiceShared;
use App\Mail\InvoiceCreated;

use App\Events\SaleInvoiceCreatedOrUpdated;
use App\Events\SupplierInvoiceCreatedOrUpdated;
use App\Events\ProfileCompleted;

use App\Services\InvoiceService;
use App\Exceptions\ServiceException;

use App\Http\Resources\InvoiceCollection;

use DB;
use Auth;
use Config;
use Image;
use PDF;
use Cache;
use File;
use Session;

use Carbon\Carbon;

class InvoiceController extends Controller
{
    public function __construct(InvoiceService $invoiceService)
    {
        $this->authorizeResource(Invoice::class, 'invoice');
        $this->invoiceService = $invoiceService;
    }

    /**
     * Get the map of resource methods to ability names.
     *
     * @return array
     */
    protected function resourceAbilityMap()
    {
        return [
            'show' => 'view',
            'create' => 'create',
            'store' => 'create',
            'edit' => 'update',
            'update' => 'update',
            'destroy' => 'delete',
            'void' => 'void',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $type = self::SALE)
    {
        $user = $request->user();
        $invoices = $user->company->invoices->where('type', $type)->values();   


        // prePrint($invoices);   
        if ($request->wantsJson() || $request->ajax()) {
            $jsonCollection = new InvoiceCollection($invoices);
            /*$jsonCollection = collect();
            $invoices->each(function ($item, $key) use ($jsonCollection) {
                $jsonCollection->push([
                    'id' => $item->id,
                    'internal_number' => $item->internal_number,
                    'contact' => $item->contact,
                    'start_date' => $item->start_date,
                    'term' => $item->term ? (Config::get('constants.terms')[$item->term].($item->term == 'manual' ? '('.$item->manual_days.' '.trans('labels.days').')' : '')) : null,
                    'end_date' => $item->end_date,
                    'frequency' => $item->frequency,
                    'frequency_type' => $item->frequency_type,
                    'expiration_date' => $item->expiration_date,
                    'calculation' => $item->calculation,
                    'term_in_locale' => $item->term_in_locale,
                    'notes' => $item->notes,
                    'tac' => $item->tac,
                    'privacy_policy' => $item->privacy_policy,
                ]);
            });*/
            return response()->json(['data' => $jsonCollection]);
        }
        return view('invoices.'.$type.'.index', compact('invoices', 'type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $type = self::SALE)
    {
        $user = $request->user();
        $next_invoice_number = $user->company->invoices()->withTrashed() ? $user->company->invoices()->withTrashed()->where('type', $type)->count() + 1 : 1;

        $currencies = Cache::get('currencies');
        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $inventories = $user->company->inventories;
        $memberships = $user->company->memberships;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.invoice_without_contacts'));
            return redirect('dashboard');
        }

        if ($type != self::SUPPLIER) {
            if ($inventories->isEmpty()) {
                $request->session()->flash('error', trans('messages.invoice_without_items'));
                return redirect('dashboard');
            }
        }

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: '',
                    'data-membership_id' => $item->membership && $item->membership->discount ? $item->membership_id : ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: '',
                    'data-membership_id' => $item->membership && $item->membership->discount ? $item->membership_id : ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        $currency_rates = $user->currencyRates->pluck('rate', 'currency_id')->toArray();

        $currency_attributes = $currencies->mapWithKeys(function ($currency) use($currency_rates) {
            $currency_rate = isset($currency_rates[$currency->id]) ? $currency_rates[$currency->id] : '';
            return [$currency->id => [
                'data-code' => $currency->code ?: '',
                'data-symbol' => $currency->symbol ?: '',
                'data-rate' => $currency_rate
            ]];
        })->toArray();

        $user_currency = $user->company->currency;
        $user_currency_id = $user_currency->id;
        $currency_options = ['' => trans('labels.select_currency')] + $currencies->where('id', '<>', $user_currency_id)->pluck('formatted_name', 'id')->toArray();
        
        //$contact_options = ['' => trans('labels.select_contact')] + $contact_options->pluck('name', 'custom_id')->toArray();

        $inventory_attributes = $inventories->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-reference' => $item->reference ?: '',
                'data-description' => $item->description ?: '',
                'data-sale_price' => $item->sale_price ?: '',
                'data-unit_cost' => $item->unit_cost ?: 0,
                'data-tax_id' => $item->tax_id ?: ''
            ]];
        })->toArray();

        $inventory_options = ['' => trans('labels.select_item')] + $inventories->pluck('name', 'id')->toArray();

        $taxes = $user->company->taxes;
        $ncfs = $user->company->ncfs;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();
        $term_options = $this->mapOptionsBasedOnLocale(Config::get('constants.terms'));
        $ncf_options = [null => trans('labels.select_ncf')] + $ncfs->pluck('name', 'id')->toArray();

        $concepts = Cache::get('concepts');        
        $concept_options_with_products = [];

        $trackedInventories = $inventories->where('track_inventory', 1);
        $childConcepts = $concepts->whereNotNull('parent_id');

        if ($trackedInventories) {
            $concept_options_with_products[trans('labels.inventories')] = $trackedInventories;
        }
        $concept_options_with_products[trans('labels.concepts')] = $childConcepts;

        $concept_options_with_products_attr = collect($concept_options_with_products)->map(function($cp) {
            return collect($cp)->mapWithKeys(function ($item) {
                return [$item->custom_id => [
                    'data-reference' => $item->reference ?: '',
                    'data-description' => $item->description ?: '',
                    'data-unit_cost' => $item->unit_cost ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ]];
            })->toArray();
        })->toArray();

        $concept_options_with_products = [
            '' => trans('labels.select_concept'), 
            trans('labels.inventories') => $concept_options_with_products[trans('labels.inventories')]->pluck('name', 'custom_id')->toArray(),
            trans('labels.concepts') => $concept_options_with_products[trans('labels.concepts')]->pluck('title', 'custom_id')->toArray()
        ];

        $estimate_id = $request->input('estimate_id') ?? 0;
        $purchase_order_id = $request->input('purchase_order_id') ?? 0;
        $remission_id = $request->input('remission_id') ?? 0;

        $resourceData = null;
        if ($estimate_id || $purchase_order_id || $remission_id) {

            if (!$resourceData && $estimate_id) {
                $resourceData = $user->company->estimates()->where('id', $estimate_id)->first();
                $purchase_order_id = $remission_id = 0;
            }
            if (!$resourceData && $purchase_order_id) {
                $resourceData = $user->company->purchaseOrders()->where('id', $purchase_order_id)->first();
                $estimate_id = $remission_id = 0;
            }
            if (!$resourceData && $remission_id) {
                $resourceData = $user->company->remissions()->where('id', $remission_id)->whereNull('invoice_id')->first();
                $estimate_id = $purchase_order_id = 0;
            }

            if ($resourceData) {
                $resourceData->contact_custom_id = ($resourceData->contact instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $resourceData->contact->id;
                Session::flash('_old_input.contact_id', $request->old('contact_id') ?: $resourceData->contact_custom_id);
                Session::flash('_old_input.invoice_start_date', $request->old('invoice_start_date') ?: $resourceData->start_date);
                Session::flash('_old_input.invoice_expiration_date', $request->old('invoice_expiration_date') ?: $resourceData->expiration_date);
                Session::flash('_old_input.exchange_currency', $request->old('exchange_currency') ?: $resourceData->exchange_currency_id);
                Session::flash('_old_input.exchange_rate', $request->old('exchange_rate') ?: $resourceData->exchange_currency_rate);
                Session::flash('_old_input.notes', $request->old('notes') ?: $resourceData->notes);

                if ($purchase_order_id) {
                    $resourceConceptItems = $resourceData->conceptItems->map(function ($item) {
                        return $item->only(['price', 'discount', 'quantity', 'user_accounting_head_id', 'tax_id', 'calculation']);
                        })->map(function($data){
                            $data['item'] = self::CONCEPT .'-'. $data['user_accounting_head_id'];
                            $data['id'] = 0;
                            $data['tax_amount'] = $data['calculation']['taxAmount'];
                            $data['tax'] = $data['tax_id'];
                            unset($data['concept_id']);
                            unset($data['user_accounting_head_id']);
                            unset($data['calculation']);
                            unset($data['tax_id']);
                            return $data;
                    })->toArray();
                }

                $resourceDataItems = $resourceData->items->map(function ($item) {
                    return $item->only(['reference', 'price', 'discount', 'quantity', 'inventory_id', 'tax_id', 'calculation']);
                })->map(function($data) use($purchase_order_id, $estimate_id, $remission_id) {
                    if ($estimate_id || $remission_id) {
                        $data['item'] = $data['inventory_id'];
                    }
                    $data['id'] = 0;
                    $data['tax_amount'] = $data['calculation']['taxAmount'];
                    $data['tax'] = $data['tax_id'];
                    if ($purchase_order_id) {
                        unset($data['reference']);
                        $data['item'] = self::INVENTORY .'-'. $data['inventory_id'];
                    }
                    unset($data['inventory_id']);
                    unset($data['calculation']);
                    unset($data['tax_id']);
                    return $data;
                })->toArray();

                if ($purchase_order_id) {
                    Session::flash('_old_input.concepts', $request->old('concepts') ?: array_merge($resourceConceptItems,$resourceDataItems));
                } else {
                    Session::flash('_old_input.products', $request->old('products') ?: $resourceDataItems);
                }
            }
        }

        $accounting_heads = $user->company->accountingHeads;
        $all_accounts_options = collect([]);
        $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
        $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
        $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

        $all_accounts->each(function($all_account) use($all_accounts_options) {
            $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
        });

        $mapInventoryWithMembership = $inventories->mapWithKeys(function ($inventory) {
            return [$inventory->id => $inventory->memberships->pluck('id')->toArray()];
        })->toArray();

        $mapMembershipWithDiscount = $memberships->whereNotNull('discount')->pluck('discount', 'id')->toArray();

        return view('invoices.'.$type.'.create', compact('next_invoice_number', 'inventories', 'tax_options', 'contact_options', 'contact_attributes', 'term_options', 'inventory_options', 'inventory_attributes', 'tax_attributes', 'type', 'ncf_options', 'concept_options_with_products', 'concept_options_with_products_attr', 'currency_attributes', 'currency_options', 'trackedInventories', 'all_accounts_options', 'remission_id', 'mapInventoryWithMembership', 'mapMembershipWithDiscount'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $type = self::SALE)
    {
        $invoiceValidator = new InvoiceValidator($type);
        $input = $request->all();

        if (!$invoiceValidator->with($input)->passes()) {
            $request->session()->flash('error', $invoiceValidator->getErrors()[0]);
            return back()
                ->withErrors($invoiceValidator->getValidator())
                ->withInput();
        }

        try {
            DB::beginTransaction();
            $invoice = $this->invoiceService->store($request, $type);
            DB::commit();
            $request->session()->flash('success', trans('messages.invoice_created'));
            return redirect()->route('invoices.index', ['type' => $type]);       
        } catch (\Exception | ServiceException $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Invoice $invoice)
    {
        if ($request->has('view')) {
            $viewType = $request->input('view');
        } else {
            $viewType = 'view-order';
        }

        return view('invoices.'.$invoice->type.'.show', compact('invoice', 'viewType'));
        
        /*$profileCompletedExecuted = event(new ProfileCompleted($invoice->user));
        $saleInvoiceCreatedExecuted = event(new SaleInvoiceCreatedOrUpdated($invoice));
        if ($profileCompletedExecuted && $saleInvoiceCreatedExecuted) {
            $invoice->load('accountingEntries'); // why this beacuse the data of accountingEntries inserted through listener. 
        }*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Invoice $invoice)
    {
        $type = $invoice->type;

        if ($invoice->is_void) {
            $request->session()->flash('error', trans('messages.void_invoice_edit'));
            return redirect()->route('invoices.index', ['type' => $type]);
        }

        $resource_associated = $invoice->transactions->isNotEmpty();
        $invoice->contact_custom_id = ($invoice->contact instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $invoice->contact->id;

        if ($invoice->calculation['status'] == trans('labels.paid')) {
            $request->session()->flash('error', trans('messages.paid_invoice_edit'));
            return redirect()->route('invoices.index', ['type' => $type]);
        }

        $user = $request->user();
        $companyClients = $user->company->clients;
        $companySuppliers = $user->company->suppliers;
        $inventories = $user->company->inventories;
        $memberships = $user->company->memberships;

        $contacts = collect([]);
        $contacts = $contacts->concat($companyClients)->concat($companySuppliers)->sortByDate('created_at', true)->values();

        if ($contacts->isEmpty()) {
            $request->session()->flash('error', trans('messages.invoice_without_contacts'));
            return redirect('dashboard');
        }

        if ($type != self::SUPPLIER) {
            if ($inventories->isEmpty()) {
                $request->session()->flash('error', trans('messages.invoice_without_items'));
                return redirect('dashboard');
            }
        }

        $clients_attributes = collect([]);
        $suppliers_attributes = collect([]);

        $contact_options = $contacts->map(function($item, $key) use ($clients_attributes, $suppliers_attributes) {
            $item->custom_id = ($item instanceof Supplier ? self::SUPPLIER : self::CLIENT) .'-'. $item->id;
            if ($item instanceof Supplier) {
                $suppliers_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: '',
                    'data-membership_id' => $item->membership && $item->membership->discount ? $item->membership_id : ''
                ];
            } else {
                $clients_attributes[$item->custom_id] = [
                    'data-phone' => $item->phone ?: '',
                    'data-tax_id' => $item->tax_id ?: '',
                    'data-membership_id' => $item->membership && $item->membership->discount ? $item->membership_id : ''
                ];
            }
            return $item;
        });

        $contact_attributes[trans('labels.clients')] = $clients_attributes->toArray();
        $contact_attributes[trans('labels.suppliers')] = $suppliers_attributes->toArray();

        $contact_options = [
            '' => trans('labels.select_contact'), 
            trans('labels.clients') => $companyClients->pluck('name', 'custom_id')->toArray(),
            trans('labels.suppliers') => $companySuppliers->pluck('name', 'custom_id')->toArray()
        ];

        $currencies = Cache::get('currencies');
        $currency_attributes = $currencies->mapWithKeys(function ($currency) {
            return [$currency->id => [
                'data-code' => $currency->code ?: '',
                'data-symbol' => $currency->symbol ?: '',
            ]];
        })->toArray();


        //$contact_options = ['' => trans('labels.select_contact')] + $contact_options->pluck('name', 'custom_id')->toArray();

        $inventory_attributes = $inventories->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-reference' => $item->reference ?: '',
                'data-description' => $item->description ?: '',
                'data-sale_price' => $item->sale_price ?: '',
                'data-unit_cost' => $item->unit_cost ?: 0,
                'data-tax_id' => $item->tax_id ?: ''
            ]];
        })->toArray();

        $inventory_options = ['' => trans('labels.select_item')] + $inventories->pluck('name', 'id')->toArray();

        $taxes = $user->company->taxes;
        $ncfs = $user->company->ncfs;

        $tax_attributes = ['0' => ['data-percentage' => 0]] + $taxes->mapWithKeys(function ($item) {
            return [$item->id => [
                'data-percentage' => $item->percentage ?: 0
            ]];
        })->toArray();

        $tax_options = ['0' => trans('labels.none_tax')] + $taxes->pluck('formatted_name', 'id')->toArray();
        $term_options = Config::get('constants.terms');
        $ncf_options = [null => trans('labels.select_ncf')] + $ncfs->pluck('name', 'id')->toArray();

        $concepts = Cache::get('concepts');        
        $concept_options_with_products = [];

        $trackedInventories = $inventories->where('track_inventory', 1);
        $childConcepts = $concepts->whereNotNull('parent_id');

        if ($trackedInventories) {
            $concept_options_with_products['inventories'] = $trackedInventories;
        }
        $concept_options_with_products['concepts'] = $childConcepts;

        $concept_options_with_products_attr = collect($concept_options_with_products)->map(function($cp) {
            return collect($cp)->mapWithKeys(function ($item) {
                return [$item->custom_id => [
                    'data-reference' => $item->reference ?: '',
                    'data-description' => $item->description ?: '',
                    'data-unit_cost' => $item->unit_cost ?: '',
                    'data-tax_id' => $item->tax_id ?: ''
                ]];
            })->toArray();
        })->toArray();

        $concept_options_with_products = [
            '' => trans('labels.select_concept'), 
            trans('labels.inventories') => $concept_options_with_products['inventories']->pluck('name', 'custom_id')->toArray(),
            trans('labels.concepts') => $concept_options_with_products['concepts']->pluck('title', 'custom_id')->toArray()
        ];

        $invoice->conceptItemsWithProducts = $invoice->conceptItems->concat($invoice->items);

        $accounting_heads = $user->company->accountingHeads;
        $all_accounts_options = collect([]);
        $root_accounts = $accounting_heads->whereNull('parent_id')->pluck('id')->toArray();
        $account_types = $accounting_heads->whereNotNull('parent_id')->whereIn('parent_id', $root_accounts)->pluck('id')->toArray();
        $all_accounts = $accounting_heads->whereIn('parent_id', $account_types)->values();

        $all_accounts->each(function($all_account) use($all_accounts_options) {
            $this->generateAccountsLevels($all_account, $all_accounts_options, 1, true);
        });

        $mapInventoryWithMembership = $inventories->mapWithKeys(function ($inventory) {
            return [$inventory->id => $inventory->memberships->pluck('id')->toArray()];
        })->toArray();

        $mapMembershipWithDiscount = $memberships->whereNotNull('discount')->pluck('discount', 'id')->toArray();

        return view('invoices.'.$type.'.edit', compact('invoice', 'inventories', 'tax_options', 'contact_options', 'contact_attributes', 'term_options', 'inventory_options', 'inventory_attributes', 'tax_attributes', 'type', 'resource_associated', 'ncf_options', 'concept_options_with_products', 'concept_options_with_products_attr', 'currency_attributes', 'all_accounts_options', 'trackedInventories', 'mapInventoryWithMembership', 'mapMembershipWithDiscount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        $type = $invoice->type;
        $input = $request->all();

        // Redirect if the invoice is voided. 
        if ($invoice->is_void) {
            $request->session()->flash('error', trans('messages.void_invoice_edit'));
            return redirect()->route('invoices.index', ['type' => $type]);
        }

        // Redirect if the invoice is already paid. 
        if ($invoice->calculation['status'] == trans('labels.paid')) {
            $request->session()->flash('error', trans('messages.paid_invoice_edit'));
            return redirect()->route('invoices.index', ['type' => $type]);
        }

        $invoiceValidator = new InvoiceValidator($type);
        if (!$invoiceValidator->with($input)->passes()) {
            $request->session()->flash('error', $invoiceValidator->getErrors()[0]);
            return back()
                ->withErrors($invoiceValidator->getValidator())
                ->withInput();
        }

        try {
            DB::beginTransaction();
            $invoice = $this->invoiceService->update($request, $invoice);
            DB::commit();
            $request->session()->flash('success', trans('messages.invoice_updated'));
            return redirect()->route('invoices.index', ['type' => $type]);

        } catch (ServiceException $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return redirect()->route('invoices.index', ['type' => $type]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Invoice $invoice)
    {
        $type = $invoice->type;

        if ($invoice->is_void) {
            $request->session()->flash('error', trans('messages.void_invoice_delete'));
            return redirect()->route('invoices.index', ['type' => $type]);
        }

        try {
            DB::beginTransaction();
            $this->invoiceService->destroy($request, $invoice);
            DB::commit();
            $request->session()->flash('success', trans('messages.invoice_deleted'));
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
        } finally {
            return redirect()->route('invoices.index', ['type' => $type]);
        }
    }

    /**
     * Download the specified resource as PDF.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function pdf(Request $request, Invoice $invoice)
    {
        try {
            // return view('pdf.invoice', compact('invoice'));
            $templateNumber = $request->input('template_number') ?? 15;
            $user = $request->user();
            if ($request->has('view')) {
                $viewType = $request->input('view');
            } else {
                $viewType = 'view-order';
            }

            $filename = $invoice->type.'-invoice-'.$invoice->internal_number.'.pdf';
            $pdf = PDF::loadView('pdf.invoice-template-'.$templateNumber, compact('invoice', 'user', 'viewType'));
            $pdf->setPaper('Letter');
            $pdf->setOption('enable-javascript', true);
            $pdf->setOption('enable-smart-shrinking', true);
            $pdf->setOption('no-stop-slow-scripts', true);
            return $pdf->download($filename);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Print the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function print(Request $request, Invoice $invoice)
    {
        try {
            $user = $request->user();
            $showView = $request->input('display');
            $templateNumber = $request->input('template_number') ?? 15;
            $print = true;

            if ($request->has('view')) {
                $viewType = $request->input('view');
            } else {
                $viewType = 'view-order';
            }

            if ($showView) {
                return view('pdf.invoice-template-'.$templateNumber, compact('invoice', 'print', 'user', 'viewType'));
            }

            $filename = $invoice->type.'-invoice-'.$invoice->internal_number.'.pdf';
            $pdf = PDF::loadView('pdf.invoice-template-'.$templateNumber, compact('invoice', 'print', 'user', 'viewType'));
            $pdf->setPaper('Letter');
            $pdf->setOption('enable-javascript', true);
            $pdf->setOption('enable-smart-shrinking', true);
            $pdf->setOption('no-stop-slow-scripts', true);
            return $pdf->inline($filename);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * email the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function mail(Request $request, Invoice $invoice)
    {
        try {
            $templateNumber = $request->input('template_number') ?? 15;
            if ($request->has('view')) {
                $viewType = $request->input('view');
            } else {
                $viewType = 'view-order';
            }

            $input = $request->all();
            $emails = collect(explode(',', $input['emails']));
            $emails = $emails->map(function ($item, $key) {
                $item = trim(strtolower($item));
                return $item;
            })->uniqueStrict()->filter();

            if ($emails->isEmpty()) {
                $request->session()->flash('error', trans('messages.invoice_mail_without_email'));
                return redirect()->route('invoices.show', ['invoice' => $invoice]);
            }

            $user = $request->user();

            $pdf = PDF::loadView('pdf.invoice-template-'.$templateNumber, compact('invoice', 'user', 'viewType'));
            $pdf->setPaper('Letter');
            $pdf->setOption('enable-javascript', true);
            $pdf->setOption('enable-smart-shrinking', true);
            $pdf->setOption('no-stop-slow-scripts', true);
            
            if ($pdf) {
                foreach ($emails as $recipient) {
                    Mail::to($recipient)->send(new InvoiceShared($invoice, $pdf->output()));
                }
            }

            $request->session()->flash('success', trans('messages.invoice_mail_sent'));
            return redirect()->route('invoices.show', ['invoice' => $invoice, 'view' => $viewType]);
        }  catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back();
        }
    }

    /**
     * Void a created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function void(Request $request, Invoice $invoice)
    {
        try {
            DB::beginTransaction();
            $this->invoiceService->void($request, $invoice);
            DB::commit();
            $request->session()->flash('success', trans('messages.invoice_void'));
        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
        } finally {
            return redirect()->route('invoices.index', ['type' => $type]);
        }
    }

    /**
     * Print the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function printTest(Request $request, Invoice $invoice)
    {
        try {
            $users = User::all();
            $user = $request->user();
            $showView = $request->input('view');
            $html = view('pdf.temp')->render();
            if ($showView) {
                return $html;
            }
            $html = view('pdf.invoice-template-15', compact('invoice', 'user'))->render();

            // return $pdf = PDF::loadView('pdf.temp-2', compact('invoice', 'user'))->download('test.pdf');
            // prePrint($pdf);
            /*$dompdf = new Dompdf();
            $dompdf->loadHtml($html);

            // (Optional) Setup the paper size and orientation
            $dompdf->setPaper('A4', 'landscape');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser
            $dompdf->stream();
            die;*/

            /*$options= new \Dompdf\Options();
            $options->set('defaultFont', 'Arial');
            $options->set('isRemoteEnabled', true);

            $dompdf = new Dompdf($options);
            $dompdf->set_option('isHtml5ParserEnabled', true);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->loadHtml($html);
            $dompdf->render();
            $dompdf->output();

            $filename = 'oisreport.pdf';
            ob_end_clean();

            $dompdf->stream($filename);
            die;*/
            
            $filename = 'invoice-'.$invoice->internal_number.'.pdf';
            $print = true;
            // prePrint($html);
            // DomPDF::setOptions(['debugCss' => true]);
            // prePrint($pdf);
            // $pdf = PDF::loadHtml($html);
            // (Optional) Setup the paper size and orientation
            // $pdf->setWarnings(false)->setPaper('Legal', 'Landscape');
            // return $pdf->stream();

            // using app container
            // $html = '<h1>Bill</h1><p>You owe me money, dude.</p>';
            // $pdf = App::make('snappy.pdf.wrapper');
            $pdf = PDF::setOption('disable-javascript', true)
                ->setWarnings(false)
                ->setPaper('Letter')
                // ->setOrientation('landscape')
                ->setOption('no-background', true)
                // ->setOption('margin-bottom', 5)
                ->setOption('page-width', '255.9')
                // ->setOption('page-height', '1080')
                // ->setOption('toc', true)
                // ->setOption('disable-smart-shrinking', true)
                ->setOption('enable-smart-shrinking', false)
                // ->setOption('footer-right', 'Page [page]')
                // ->setOption('footer-font-size', 10)
                ->loadHtml($html);
            return $pdf->inline();

            $snappy = App::make('snappy.pdf');
            return $snappy->generate('http://www.github.com', '/tmp/github.pdf');
            return $snappy->generateFromHtml($html, '/tmp/bill-123.pdf');
            die;

            // Render the HTML as PDF
              // $pdf->render();
            return $pdf->output();

              // Output the generated PDF (1 = download and 0 = preview)
            return $pdf->download('invoice.pdf');

              $pdf->stream("codex",array("Attachment"=>1));
            /*$pdf->setOption('enable-javascript', true);
            $pdf->setOption('enable-smart-shrinking', true);
            $pdf->setOption('no-stop-slow-scripts', true);*/
            die;
            // return $pdf;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Print the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function printWithTemplate(Request $request, Invoice $invoice, $templateNumber, $viewType = null)
    {
        try {
            $user = $request->user();
            $showView = $request->input('view');
            if ($showView) {
                return view('pdf.invoice-template-'.$templateNumber, compact('invoice', 'user'));
            }
            return [
                'invoice_id' => $invoice->id,
                'invoice_template' => view('pdf.invoice-template-'.$templateNumber, compact('invoice', 'user', 'viewType'))->render()
            ];
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function showProductionOrder(Invoice $invoice)
    {
        $profileCompletedExecuted = event(new ProfileCompleted($invoice->user));
        $saleInvoiceCreatedExecuted = event(new SaleInvoiceCreated($invoice));
        //prePrint($saleInvoiceCreatedExecuted);
        if ($profileCompletedExecuted && $saleInvoiceCreatedExecuted) {
            //$invoice->load('accountingEntries'); // why this beacuse the data of accountingEntries inserted through listener. 
            return view('invoices.'.$invoice->type.'.show-production-order', compact('invoice'));
        }
    }
}