<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Validators\RoleValidator;

use App\Role;

use DB;
use Cache;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Role::class, 'role');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $company = $user->company;
        $roles = $company->roles;

        return view('roles.index', compact('roles'));
    }

    /**
     * create a role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $user = $request->user();
            $permissions = Cache::get('permissions');

            $permission_options = $permissions->pluck('custom_name', 'id')->toArray();

            return view('roles.create', compact('permission_options'));
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return redirect()->route('roles');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, RoleValidator $roleValidator)
    {
        $user = $request->user();
        try {
            DB::beginTransaction();
            $input = $request->all();

            if (!$roleValidator->with($input)->passes()) {
                $request->session()->flash('error', $roleValidator->getErrors()[0]);
                return back()
                    ->withErrors($roleValidator->getValidator())
                    ->withInput();
            }

            $role = new Role(['name' => $input['name']]);
            $role->setTranslation('custom_name', 'en', $input['name'])->setTranslation('custom_name', 'es', $input['name']);
            $user->company->roles()->save($role);

            $role->syncPermissions($input['permissions']);

            DB::commit();

            $request->session()->flash('success', trans('messages.role_created'));
            return redirect()->route('roles.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Role $role)
    {
        $permissions = Cache::get('permissions');
        $assignedPermissions = $role->permissions;

        $permission_options = $permissions->pluck('custom_name', 'id')->toArray();
        return view('roles.edit', compact('role', 'permission_options', 'assignedPermissions'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $user = $request->user();
        $locale = app()->getLocale();

        /*if (!$request->user()->can('update', [Role::class, $role] )) {
            $request->session()->flash('error', trans('messages.account_access'));
            return redirect()->route('roles');
        }*/

        try {
            DB::beginTransaction();
            $roleValidator = new RoleValidator('update', $role);
            $input = $request->all();

            if (!$roleValidator->with($input)->passes()) {
                $request->session()->flash('error', $roleValidator->getErrors()[0]);
                return back()
                    ->withErrors($roleValidator->getValidator())
                    ->withInput();
            }

            $role->setTranslation('custom_name', $locale, $input['name']);
            $role->save();

            $role->syncPermissions($input['permissions']);

            DB::commit();

            $request->session()->flash('success', trans('messages.role_updated'));
            return redirect()->route('roles.index');

        } catch (\Exception $e) {
            DB::rollback();
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Role $role)
    {
        try {
            $role->delete();
            $request->session()->flash('success', trans('messages.role_deleted'));
            return redirect()->route('roles.index');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back()->withInput();
        } 
    }
}
