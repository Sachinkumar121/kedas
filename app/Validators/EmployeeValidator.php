<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;

class EmployeeValidator extends Validator 
{
    /**
     * Rules for employee creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Employee registration
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add', $employee = null)
    {
        $this->messages = [
            'department_id.in' => trans('messages.invalid_department'),
            /* 'designation_id.in' => trans('messages.invalid_designation'), */
            'insurance_brand_id.in' => trans('messages.invalid_insurance'),
        ];
        $this->rules = [
            'name' => 'required|string|min:3|max:255',
            'hiring_date' => 'required',
            'birth_date' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('employees')->where(function ($query) {
                return $query->whereNull('deleted_at');
            }), Rule::unique('users')->where(function ($query) {
                return $query->whereNull('deleted_at');
            })],
            'address' => 'nullable|string|min:3|max:255',
            'phone' => 'nullable|min:10|max:19',
            'mobile' => 'required|min:10|max:19',
            'username' => ['required', 'string', 'min:3', 'max:255', 'regex:/^[A-Za-z_-][A-Za-z0-9_]*$/', Rule::unique('employees')->where(function ($query) {
                return $query->whereNull('deleted_at');
            })],
            'id_number' => ['required', Rule::unique('employees')->where(function ($query) {
                return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
            })],
            'password' => 'required|string|min:6',
            'department_id' => 'required|in:'.Auth::user()->company->departments->implode('id', ','),
            /* 'designation_id' => 'required|in:'.Auth::user()->company->designations->implode('id', ','), */
            'insurance_brand_id' => 'nullable|in:'.Auth::user()->company->insuranceBrands->implode('id', ','),
            'salary' => 'nullable|string|regex:/^\d+(\.\d{1,2})?$/',
            'afp' => 'nullable|string|regex:/^\d+(\.\d{1,2})?$/',
            'role' => 'required',
            'status' => 'required',
        ];

        if ($validationFor == 'update') {
            $rulesForUpdate = [
                'email' => ['required', 'string', 'email', 'max:255', Rule::unique('employees')->where(function ($query) {
                    return $query->whereNull('deleted_at');
                })->ignore($employee->id), Rule::unique('users')->where(function ($query) {
                    return $query->whereNull('deleted_at');
                })->ignore($employee->user->id)],
                'password' => 'nullable|string|min:6',
                'username' => ['required', 'string', 'min:3', 'max:255', 'regex:/^[A-Za-z_-][A-Za-z0-9_]*$/', Rule::unique('employees')->where(function ($query) {
                    return $query->whereNull('deleted_at');
                })->ignore($employee->id)],
                'id_number' => ['required', Rule::unique('employees')->where(function ($query) {
                return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
                })->ignore($employee->id)],
            ];
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
