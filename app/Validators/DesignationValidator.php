<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;
use App\Rules\InternationalChar;

class DesignationValidator extends Validator 
{
    /**
     * Rules for tax creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Tax
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add', $designation = null)
    {
        $this->messages = [
            'name.unique' => trans('messages.designation_duplicate'),
        ];

        $this->rules = [
            'name' => ['required', 'string', 'min:3', 'max:255', new InternationalChar(false, true),  Rule::unique('designations')->where(function ($query) {
                return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
            })],
            //'description' => 'nullable|min:10|max:255',
        ];
        if ($validationFor == 'update') {
            $rulesForUpdate = [
                'name' => ['required', 'string', 'min:3', 'max:255', Rule::unique('designations')->where(function ($query) {
                    return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
                })->ignore($designation->id)],
            ];
            
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
