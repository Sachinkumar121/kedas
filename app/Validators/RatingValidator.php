<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;

use App\Company;

class RatingValidator extends Validator 
{
    /**
     * Rules for client creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Client registration
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add', $client = null)
    {
        $this->rules = [
            'rating' => 'min:1|max:5|numeric',
            'review' => 'nullable|string|max:191',
        ];
    }

    public function getRules() {
        return $this->rules;
    }
}
