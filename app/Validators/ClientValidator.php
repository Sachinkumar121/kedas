<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;

use App\Company;

class ClientValidator extends Validator 
{
    /**
     * Rules for client creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Client registration
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add', $client = null)
    {
        $this->messages = [
            'email.unique' => trans('validation.client_email'),
            // 'store_ids.required' => trans('validation.distinct', ['attrtibute' => 'store_ids']),
        ];
        $this->rules = [
            'name' => 'required|string|min:3|max:255',
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('clients')->where(function ($query) {
                return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
            })],
            'address' => 'nullable|string|min:3|max:255',
            'phone' => 'nullable|min:10|max:19',
            'mobile' => 'nullable|min:10|max:19',
            'membership_id' => 'nullable|in:'.Auth::user()->company->memberships->implode('id', ','),
            // 'phone' => 'nullable|min:10|max:12|regex:/[0-9]{9}/'
        ];

        if ($validationFor == 'update') {
            $rulesForUpdate = [
                'email' => ['required', 'string', 'email', 'max:255', Rule::unique('clients')->where(function ($query) {
                    return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
                })->ignore($client->id)],
            ];
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }

        if ($validationFor == 'app_store_check') {
            $this->rules = [
                'store_ids.*' => 'required|distinct|in:'. Company::all()->pluck('id')->implode(","),
            ];
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
