<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;

class ItemCategoryValidator extends Validator 
{
    /**
     * Rules for Item Category creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Item Category
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add', $itemCategory = null)
    {
       
        $this->messages = [
            'name.unique' => trans('messages.item_categories_duplicate'),
            'estimate_expiration_date.date_format' => trans('messages.exp_date_format'),
            'products.required' => trans('messages.estimate_without_items'),
            'logo.dimensions' => trans('messages.dimensions_invalid'),
            'signature.dimensions' => trans('messages.dimensions_invalid'),
            'image.dimensions' => trans('messages.dimensions_invalid')
        ];

        $this->rules = [
            'name' => ['required', 'string', 'min:3', 'max:255', Rule::unique('item_categories')->where(function ($query) {
                return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
            })],
            'description' => 'nullable|min:10|max:255',
            'image' => 'nullable|image|mimes:jpeg,bmp,png,jpg',
        ];
        if ($validationFor == 'update') {
            $rulesForUpdate = [
                'name' => ['required', 'string', 'min:3', 'max:255', Rule::unique('item_categories')->where(function ($query) {
                    return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
                })->ignore($itemCategory->id)],
            ];
            
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
