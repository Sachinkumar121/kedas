<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;

class LeaveTypeValidator extends Validator 
{
    /**
     * Rules for Leave Type creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Leave Type
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add', $leaveType = null)
    {
        $this->rules = [
            'name' => ['required', 'string', 'min:3', 'max:255', Rule::unique('leave_types')->where(function ($query) {
                return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
            })],
            'number_of_days' => 'required|numeric|min:1',
        ];
        if ($validationFor == 'update') {
            $rulesForUpdate = [
                'name' => ['required', 'string', 'min:3', 'max:255', Rule::unique('leave_types')->where(function ($query) {
                    return $query->where('company_id', Auth::user()->company->id)->whereNull('deleted_at');
                })->ignore($leaveType->id)],
            ];
            
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
