<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;
use Config;
use App\Rules\InternationalChar;

class FixedAssetValidator extends Validator 
{
    /**
     * Rules for Item Category creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Item Category
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add', $fixedAsset = null)
    {
       
        $this->messages = [];

        $this->rules = [
            'name' => ['required', 'string', 'min:3', 'max:255', new InternationalChar(false, true)],
            'serial_number' => 'nullable|string|min:3|max:255',
            'price' => 'required|string|regex:/^\d+(\.\d{1,2})?$/|max:16',
            'item_category_id' => 'nullable|in:'.Auth::user()->company->itemCategories->implode('id', ','),
            'account' => 'required|string',
            'expense_account' => 'required|string',
            'date_of_purchase' => 'nullable|date_format:Y-m-d',
            'legal_category' => 'required|string',
            'location' => 'required|in:'.implode(array_keys(Config::get('constants.location')), ','),
            'description' => ['nullable', 'string', 'min:3', 'max:255', new InternationalChar(true, true)],

            'depr_should_cal' => 'required',

            'cal_type' => 'bail|nullable|required_if:depr_should_cal,1|in:'.implode(array_keys(Config::get('constants.cal_type')), ','),
            'years' => 'bail|nullable|required_if:depr_should_cal,1|numeric|min:1|max:30',
            'scrap_value' => 'bail|nullable|required_if:depr_should_cal,1|numeric|between:0.1,99.99',

            'total_unit_produced' => 'bail|nullable|required_if:cal_type,units_produced|numeric|min:1',
            'unit_produced_per_year' => 'bail|nullable|required_if:cal_type,units_produced|numeric|min:1',
            'same_for_year' => 'bail|nullable|required_if:cal_type,units_produced',
            'per_year_prod' => 'bail|nullable|required_if:same_for_year,0',
            // 'depreciation_items' => 'required|array',
            // 'depreciation_items.*' => 'required|min:1',
            // 'depreciation_items.*.year' => 'required',
            // 'depreciation_items.*.percentage' => 'required',
            // 'depreciation_items' => 'required',
            // 'legal_category' => 'nullable|in:'.implode(array_keys(Config::get('constants.legal_category')), ','),
            // 'percentage' => 'required|numeric|between:0.1,99.99',
            // 'book_value' => 'required|string|regex:/^\d+(\.\d{1,2})?$/|max:16',
            'final_date' => 'nullable|date_format:Y-m-d',
            'image' => 'nullable|image|mimes:jpeg,bmp,png,jpg',
        ];
        if ($validationFor == 'update') {
            $rulesForUpdate = [];
            
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
