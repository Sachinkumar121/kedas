<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use App\Rules\InternationalChar;

class MembershipValidator extends Validator 
{
    /**
     * Rules for tax creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Tax
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add')
    {
        $this->messages = [
            'point_percentage.between' => trans('messages.invalid_percentage', ['attribute' => trans('labels.point_percentage')]),
            'discount.between' => trans('messages.invalid_percentage', ['attribute' => trans('labels.discount')]),
        ];
        $this->rules = [
            'name' => ['required', 'string', 'min:3', 'max:255', new InternationalChar(false, true)],
            'description' => ['nullable', 'string', 'min:10', 'max:255', new InternationalChar(true, true)],

            'point_percentage' => 'bail|required_without:discount|nullable|numeric|between:0.1,99.99',
            'discount' => 'bail|required_without:point_percentage|nullable|numeric|between:0.1,99.99',
            'created_date' => 'required|date_format:Y-m-d',
            'point_expire_date' => 'bail|required_with:point_percentage|nullable|date_format:Y-m-d|after_or_equal:created_date',
        ];
        if ($validationFor == 'update') {
            $rulesForUpdate = [];
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
