<?php

namespace App\Validators;

use App\Validators\Validator;
use Illuminate\Validation\Rule;
use Auth;

class LeaveValidator extends Validator 
{
    /**
     * Rules for Leave creation and updation.
     *
     * @var array
     */
    protected $rules;

    /**
     * Messages for Leave
     *
     * @var array
     */
    protected $messages = [];

    public function __construct($validationFor = 'add')
    {
         $this->messages = [
            'from_date.date_format' => trans('messages.date_format'),
            'to_date.date_format' => trans('messages.date_format'),
        ];

        $this->rules = [
            'from_date' => 'required|date_format:Y-m-d',
            'to_date' => 'required|date_format:Y-m-d',
            'note' => 'nullable|min:10|max:255',
            'leave_type' => 'required',
            'number_of_days' => 'required',
            'employee_id' => 'required',
        ];

        if ($validationFor == 'update') {
            $rulesForUpdate = [];
            $this->rules = array_merge($this->rules, $rulesForUpdate);
        }
    }

    public function getRules() {
        return $this->rules;
    }
}
