<?php

namespace App\Listeners;

use App\Events\UserLoggedIn;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Request;

use App\Interfaces\CommonConstants;
use App;
use GuzzleHttp;

class SaveUserCoords implements CommonConstants
{
    /**
     * The name of the connection the job should be sent to.
     *
     * @var string|null
     */
    public $connection = 'sync';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SaleInvoiceCreated  $event
     * @return void
     */
    public function handle($event)
    {
        $data = $event->data;
        $user = $event->user;

        if (!App::Environment(['development', 'production'])) {
            return true;
        }

        try {
            $lat = $data['lat'] ?? null;
            $lon = $data['lon'] ?? null;

            if ($lat && $lon) {
                // convert lat lon to address and push into data.
                // https://maps.googleapis.com/maps/api/geocode/json?latlng=18.5108636,-69.8478165&key=AIzaSyBnK1qvbt0QxNP8I6VuQNmR4D1qmtmB--4
            } else {
                // convert ip address to address and push into data.
                $ip = Request::ip();
                $http = new GuzzleHttp\Client;
                $ipToCoords = $http->post("http://www.geoplugin.net/json.gp?ip=$ip");
                $ipToCoords = json_decode((string) $ipToCoords->getBody(), true);
                $data['lat'] = $ipToCoords['geoplugin_latitude'] ?? null;
                $data['lon'] = $ipToCoords['geoplugin_longitude'] ?? null;
            }
            activity('login-log')
                ->causedBy($user)
                ->withProperties(['data' => $data])
                ->log('The user with email '.$user->email.' logged in.');

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Determine whether the listener should be queued.
     *
     * @param  \App\Events\OrderPlaced  $event
     * @return bool
     */
    public function shouldQueue(UserLoggedIn $event)
    {
        return false;
    }
}
