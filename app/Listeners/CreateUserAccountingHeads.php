<?php

namespace App\Listeners;

use App\Events\ProfileCompleted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\AccountingHead;
use App\UserAccountingHead;
use App\Company;

use Cache;
use App;

class CreateUserAccountingHeads
{
    /**
     * The name of the connection the job should be sent to.
     *
     * @var string|null
     */
    public $connection = 'sync';
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProfileCompleted  $event
     * @return void
     */
    public function handle(ProfileCompleted $event)
    {
        /*if (App::Environment(['development', 'production'])) {
            return true;
        }*/
        try {
            $user = $event->user;
            if ($user->company_id && $user->isCompanyOwner()) {
                $company = Company::findOrFail($user->company_id);
                if ($company->accountingHeads->isEmpty()) {
                    $accounting_heads = collect(Cache::get('accounting_heads')->toTree()->toArray());

                    $accounting_heads->each(function ($accountingHead) use($user, $company) {
                        /*$accountingHead['accounting_head_id'] = $accountingHead['id'];
                        $accountingHead['company_id'] = $user->company->id;
                        $accountingHead['user_id'] = $user->id;
                        if (count($accountingHead['children'])) {
                            foreach ($accountingHead['children'] as &$child) {
                                $child['accounting_head_id'] = $child['id'];
                                $child['company_id'] = $user->company->id;
                                $child['user_id'] = $user->id;
                                if (count($child['children'])) {
                                    foreach ($child['children'] as &$subChild) {
                                        $subChild['accounting_head_id'] = $subChild['id'];
                                        $subChild['company_id'] = $user->company->id;
                                        $subChild['user_id'] = $user->id;
                                    }
                                }
                            }
                        }*/
                        $this->recursiveSet($accountingHead, $user, $company);
                        recursiveUnset($accountingHead, 'id');
                        $userAccountingHead = UserAccountingHead::create($accountingHead);

                        // $userAccountingHead = $accountingHead->replicate();
                        // $userAccountingHead->setTable('user_accounting_heads');
                        // $userAccountingHead->company_id = $user->company->id;
                        // $userAccountingHead->user_id = $user->id;
                        // $userAccountingHead->accounting_head_id = $accountingHead->id;
                        // $userAccountingHead->save();
                    });
                    /*Cache::rememberForever('user_accounting_heads_'.$company->id, function () use($company) {
                        $company->load('accountingHeads');
                        return $company->accountingHeads;
                    });*/
                    // prePrint(Cache::get('user_accounting_heads_'.$company->id));
                }
            }
            return true; 
        } catch (\Exception $e) {
            throw $e;
            return false;
        }        
    }

    protected function recursiveSet(&$accountingHead, $user, $company) {
        $accountingHead['accounting_head_id'] = $accountingHead['id'];
        $accountingHead['custom_code'] = $accountingHead['code'];
        $accountingHead['company_id'] = $company->id;
        $accountingHead['user_id'] = $user->id;
        if (count($accountingHead['children'])) {
            foreach ($accountingHead['children'] as &$child) {
                $this->recursiveSet($child, $user, $company);
            }
        }
    }
}
