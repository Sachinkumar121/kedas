<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;

class ItemCategory extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'item_categories';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image_url'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::deleted(function($itemCategory) {
            $itemCategory->inventories()->each(function($inventory) {
                $inventory->itemCategory()->dissociate();
                $inventory->save();
            });
        });
    }

    /**
     * The inventory that belong to the itemCategory.
     */
    public function inventories()
    {
        return $this->hasMany('App\Inventory');
    }

    public function getImageUrlAttribute() {
    	return $this->attributes['image'] ? Storage::url('item-categories/'.$this->attributes['image']) : null;
    }
}
