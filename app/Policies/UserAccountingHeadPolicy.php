<?php

namespace App\Policies;

use App\UserAccountingHead;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class UserAccountingHeadPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\UserAccountingHead  $userAccountingHead
     * @return mixed
     */
    public function view(User $user, UserAccountingHead $userAccountingHead)
    {
        return $user->company->id == $userAccountingHead->company_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\UserAccountingHead  $userAccountingHead
     * @return mixed
     */
    public function update(User $user, UserAccountingHead $userAccountingHead)
    {
        return $user->id == $userAccountingHead->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\UserAccountingHead  $userAccountingHead
     * @return mixed
     */
    public function delete(User $user, UserAccountingHead $userAccountingHead)
    {
        return $user->id == $userAccountingHead->user_id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\UserAccountingHead  $userAccountingHead
     * @return mixed
     */
    public function restore(User $user, UserAccountingHead $userAccountingHead)
    {
        return $user->id == $userAccountingHead->user_id;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\UserAccountingHead  $userAccountingHead
     * @return mixed
     */
    public function forceDelete(User $user, UserAccountingHead $userAccountingHead)
    {
        return $user->id == $userAccountingHead->user_id;
    }
}
