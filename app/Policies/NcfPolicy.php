<?php

namespace App\Policies;

use App\Ncf;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class NcfPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Ncf  $ncf
     * @return mixed
     */
    public function view(User $user, Ncf $ncf)
    {
        return $user->company->id == $ncf->company_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Ncf  $ncf
     * @return mixed
     */
    public function update(User $user, Ncf $ncf)
    {
        return $user->id == $ncf->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Ncf  $ncf
     * @return mixed
     */
    public function delete(User $user, Ncf $ncf)
    {
        return $user->id == $ncf->user_id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Ncf  $ncf
     * @return mixed
     */
    public function restore(User $user, Ncf $ncf)
    {
        return $user->id == $ncf->user_id;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Ncf  $ncf
     * @return mixed
     */
    public function forceDelete(User $user, Ncf $ncf)
    {
        return $user->id == $ncf->user_id;
    }
}
