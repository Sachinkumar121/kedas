<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;
use App\Interfaces\CommonConstants;

class CompanyUserPolicy implements CommonConstants
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Client  $client
     * @return mixed
     */
    public function view(User $user, User $companyUser)
    {
        return $user->company->id == $companyUser->company_id && $companyUser->user_role == self::COMPANY_USER;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Client  $client
     * @return mixed
     */
    public function update(User $user, User $companyUser)
    {
        return $user->company->id == $companyUser->company_id && $companyUser->user_role == self::COMPANY_USER;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Client  $client
     * @return mixed
     */
    public function delete(User $user, User $companyUser)
    {
        return $user->company->id == $companyUser->company_id && $companyUser->user_role == self::COMPANY_USER;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Client  $client
     * @return mixed
     */
    public function restore(User $user, User $companyUser)
    {
        return $user->company->id == $companyUser->company_id && $companyUser->user_role == self::COMPANY_USER;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Client  $client
     * @return mixed
     */
    public function forceDelete(User $user, User $companyUser)
    {
        return $user->company->id == $companyUser->company_id && $companyUser->user_role == self::COMPANY_USER;
    }
}
