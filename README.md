
## Installation
- clone the project on the local system.
- create a new branch from the development branch. The branch name should be valid and related to the work. Like in case if you are start working on the "super admin" feature then the branch name should be "superadmin" or "super_admin"
- Run the "composer install" command.
- Run the "npm install" command.

## Setup project on the local

- Either create .env file or Copy the content of env.example file and paste onto .env file. 
- Update the value of the env variable according to the local enviornment.
- Make new database name "kedas".
- Go to the project directory root path and run "php artisan migrate --seed" command.
- Run the "php artisan passport:client --password" command the copy the value of the "CLIENT_ID" and "CLIENT_SECRET" and paste into the .env file.
- Open the project on the local system. Url - "http://localhost/kedas"

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
