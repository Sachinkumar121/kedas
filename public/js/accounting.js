$(document).ready(function() {
    let addPrimaryAccountingHeadFormEle = $("#add-primary-accounting-head-form");
    let addSecondaryAccountingHeadFormEle = $("#add-secondary-accounting-head-form");
    let editAccountingHeadFormEle = $("#edit-accounting-head-form");

    // Submit event for each modal form.
    $(addPrimaryAccountingHeadFormEle).on("submit", async function(e) {
        if (addPrimaryAccountingHeadFormEle.valid()) {
            data = addPrimaryAccountingHeadFormEle.serialize();
            let result = await addAccountingHead(data, addPrimaryAccountingHeadFormEle);

            if (result && result['success']) {
                $(addPrimaryAccountingHeadFormEle).find('#add-accounting-head-error').hide().html("");
            	$('#addPrimarychartOfAccountModal').modal('hide');
            	toastr.success(result['message']);
            	setTimeout(function() {
		            location.reload();
		        }, 1000);
            }
        }
    });

    $(addSecondaryAccountingHeadFormEle).on("submit", async function(e) {
        if (addSecondaryAccountingHeadFormEle.valid()) {
            data = addSecondaryAccountingHeadFormEle.serialize();
            let result = await addAccountingHead(data, addSecondaryAccountingHeadFormEle);

            if (result && result['success']) {
                $(addSecondaryAccountingHeadFormEle).find('#add-accounting-head-error').hide().html("");
                $('#addSecondaryChartOfAccountModal').modal('hide');
                toastr.success(result['message']);
                setTimeout(function() {
                    location.reload();
                }, 1000);
            }
        }
    });

    $(editAccountingHeadFormEle).on("submit", async function(e) {
        if (editAccountingHeadFormEle.valid()) {
            data = editAccountingHeadFormEle.serialize();
            accountId = $("input[name='account-id']").val()
            let result = await updateAccountingHead(accountId, data);

            if (result && result['success']) {
                $('#edit-accounting-head-error').hide().html("");
                $('#editChartOfAccountModal').modal('hide');
                toastr.success(result['message']);
                setTimeout(function() {
                    location.reload();
                }, 1000);
            }
        }
    });

    // on hide and show event for a modal
    $('#addPrimaryChartOfAccountModal').on('hide.bs.modal', function () {
		addPrimaryAccountingHeadFormEle[0].reset();
		primaryAccountingHeadFormValidator.resetForm();
        $('#edit-accounting-head-error').html("");
	});

    $('#addSecondaryChartOfAccountModal').on('hide.bs.modal', function () {
        addSecondaryAccountingHeadFormEle[0].reset();
        secondaryAccountingHeadFormValidator.resetForm();
        $("#account-type").val('').trigger('change')
    });

    $('#editChartOfAccountModal').on('hide.bs.modal', function () {
        editAccountingHeadFormEle[0].reset();
        editAccountingHeadFormValidator.resetForm();
        $('#edit-accounting-head-error').html("");
    });

    $('#addSecondaryChartOfAccountModal').on('show.bs.modal', function (event) {

        generateHierarchySelect('multilevelselect', 'input[name="parent-account"]');
        /*var account_type = $(event.relatedTarget).attr('data-account_type') || 'secondary';
        if (account_type == "primary") {
            $("form#add-primary-accounting-head-form select#account-type").next(".select2-container").hide();
            $('label[for="account-type"]').hide();
        } else {
            $('label[for="account-type"]').show();
            $("form#add-primary-accounting-head-form select#account-type").next(".select2-container").show();
        }
        console.log(account_type);*/
    });

    // Populate form for edit and view.
    $('a.edit-accounting-head').on('click', function () {
        $("#edit-accounting-head-form input[name='account-name']").val($(this).data("name"))
        $("#edit-accounting-head-form input[name='account-id']").val($(this).data("id"))
        $("#edit-accounting-head-form input[name='code']").val($(this).data("code"))
        $("#edit-accounting-head-form input[name='opening_balance']").val($(this).data("opening_balance"))
        $("#edit-accounting-head-form textarea[name='description']").val($(this).data("description"))
    });
    $('a.view-accounting-head').on('click', function () {
        $("#view-accounting-head-form p#account-name").html($(this).data("name"))
        $("#view-accounting-head-form p#code").html($(this).data("code"))
        $("#view-accounting-head-form p#description").html($(this).data("description"))
    });

    // Change event for form elements.
    $(document).on('change', 'form#add-secondary-accounting-head-form select[name="account-type"]', async function(e) {
        let account_type = $(this).find('option:selected').val();
        // let chart_type = $('form#add-secondary-accounting-head-form input[name="chart-type"]:checked').val() || 'control';
        if (account_type) {
            $('form#add-secondary-accounting-head-form input[name="is_sub_account"]').prop('checked', false);
            // generate details type options.
            $("form#add-secondary-accounting-head-form div#multilevelselect").hide();
            await generateControlOptions(account_type, 'control', $("select#account-control-options"));
            // $("form#add-secondary-accounting-head-form select#all-coa-options").next(".select2-container").hide();
            $("div.account-head-control-container").show();
        } else {
            $("div.account-head-control-container").hide();
        }
    });

    /*$(document).on('change', 'form#add-secondary-accounting-head-form input[name="chart-type"]', async function(e) {
        let account_type = $('form#add-secondary-accounting-head-form select[name="account-type"]').find('option:selected').val();
        let chart_type = $('form#add-secondary-accounting-head-form input[name="chart-type"]:checked').val();
        await generateControlOptions(account_type, chart_type);
    });*/

    $(document).on('change', 'form#add-secondary-accounting-head-form select[name="account-control"]', async function(e) {
        let account_type = $(this).find('option:selected').val();
        let account_type_text = $(this).find('option:selected').text();
        let can_be_parent = $(this).find('option:selected').data("can_be_parent");

        $('form#add-secondary-accounting-head-form input[name="account-name"]').val(account_type_text);
        if (can_be_parent == false) {
            $('form#add-secondary-accounting-head-form input[name="is_sub_account"]').prop('checked', false);
            $('form#add-secondary-accounting-head-form input[name="is_sub_account"]').prop('disabled', true);
            $('form#add-secondary-accounting-head-form div.checkbox').addClass('checkbox-disabled');
            return;
        }
        $('form#add-secondary-accounting-head-form input[name="is_sub_account"]').prop('disabled', false);
            $('form#add-secondary-accounting-head-form div.checkbox').removeClass('checkbox-disabled');

        // let account_type = $('form#add-secondary-accounting-head-form select[name="account-type"]').find('option:selected').val();
        let chart_type = 'details';

        if (account_type) {
            $('form#add-secondary-accounting-head-form input[name="is_sub_account"]').prop('checked', false);
            // generate details type options.
            $("form#add-secondary-accounting-head-form div#multilevelselect").hide();
            await generateControlOptions(account_type, chart_type, $("div#multilevelselect"));
            // $("form#add-secondary-accounting-head-form select#all-coa-options").next(".select2-container").hide();
            $("div.account-head-control-container").show();
        } else {
            $("div.account-head-control-container").hide();
        }
    });


    // Show hide accouting heads based on sub account check box.
    $(document).on('change', 'form#add-secondary-accounting-head-form input[name="is_sub_account"]', function(e) {
        if (this.checked) {
            /*$('select#all-coa-options').show();
            $("form#add-secondary-accounting-head-form select#all-coa-options").next(".select2-container").show();*/
            $("form#add-secondary-accounting-head-form div#multilevelselect").show();
        } else {
            /*$('select#all-coa-options').hide();
            $("form#add-secondary-accounting-head-form select#all-coa-options").next(".select2-container").hide();*/
            $("form#add-secondary-accounting-head-form div#multilevelselect").hide();
        }
    });
    if ($("form#add-secondary-accounting-head-form").length) {
        if ($('form#add-secondary-accounting-head-form input[name="is_sub_account"]').is(":checked")) {
            /*$('select#all-coa-options').show();
            $("form#add-secondary-accounting-head-form select#all-coa-options").next(".select2-container").show();*/
            $("form#add-secondary-accounting-head-form div#multilevelselect").show();
        } else {
            /*$('select#all-coa-options').hide();
            $("form#add-secondary-accounting-head-form select#all-coa-options").next(".select2-container").hide();*/
            $("form#add-secondary-accounting-head-form div#multilevelselect").hide();
        }
    }
    // default hide the select options.
    // $("form#add-secondary-accounting-head-form div#multilevelselect").hide();
    // $("form#add-secondary-accounting-head-form select#all-coa-options").next(".select2-container").show();

    $(document).on('click', "a.dropdown-item", function(e) {
        e.preventDefault();
        let level = $(this).data('level') || 1;
        console.log(level);
        $("input[name='account-level']").val(level);
    });

    $('a[data-toggle="tab"]:first').trigger('click');
    $('a[data-toggle="tab"]').on('shown.bs.tab', async function (e) {
        var activeTab = e.target // activated tab
        // get the category
        var category = $(activeTab).data('category');
        $("span#loader").show();

        await getAccountingReport(category);
        $("span#loader").hide();
    });
});

async function addAccountingHead(data, formEle) {
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/accounting/charts",
            type: "POST",
            data,
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            // success:function(response){
            //     console.log(response);
            // },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        let alertElement = '<div class="alert alert-danger alert-dismissible" role="alert">' + errorObj.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        toastr.error(errorObj.message);
        // $(formEle).find('#add-accounting-head-error').html(alertElement);
    }
}

async function updateAccountingHead(id, data) {
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/accounting/charts/" + id,
            type: "PUT",
            data,
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            // success:function(response){
            //     console.log(response);
            // },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        let alertElement = '<div class="alert alert-danger alert-dismissible" role="alert">' + errorObj.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        $('#edit-accounting-head-error').html(alertElement);
    }
}

async function generateControlOptions(id, type="control", ele='"select#account-control-options"') {
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/accounting/charts/generate-control-options/" + id,
            type: "GET",
            data: {
                type
            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "html",
            success: function(newRawHtml) {
                $(ele).replaceWith(newRawHtml);
                generateSelect2("single-search-selection", {
                    selectionCssClass: 'selector-selection',
                    dropdownCssClass: 'selector-dropdown',
                });
                generateHierarchySelect('multilevelselect', 'input[name="parent-account"]');
            },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        console.log(errorObj);
    }
}

async function getAccountingReport(category) {
    let result;
    try {
        result = await $.ajax({
            url: SITE_URL + "/accounting/reports/" + category,
            type: "GET",
            data: {raw: true},
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "html",
            success: function(reportHtmlContent) {
                $("div#" + category).html(reportHtmlContent);
                setTimeout(function() {
                    if ($('#accounting-journals-list-table').length && !$.fn.dataTable.isDataTable( '#accounting-journals-list-table' )) {
                        $('#accounting-journals-list-table').DataTable(Object.assign({}, dataTableOptions));
                    }
                }, 100);
            },
        });
        return result;
    } catch (error) {
        $("span#loader").hide();
        errorObj = $.parseJSON(error.responseText);
        console.log(errorObj);
    }
}