(function() {
	if (!navigator.geolocation) {
		return false;
	}
}())

const askLocationPermission = function() {
	var startPos;
	var geoOptions = {
		maximumAge: 5 * 60 * 1000,
		timeout: 10 * 1000,
		enableHighAccuracy: true
	}
	var geoSuccess = function(position) {
		startPos = position;
		document.getElementById('lat').value = startPos.coords.latitude;
		document.getElementById('lon').value = startPos.coords.longitude;
	};
	var geoError = function(error) {
		console.log('Error occurred. Error code: ' + error.code);
		// error.code can be:
		//   0: unknown error
		//   1: permission denied
		//   2: position unavailable (error response from location provider)
		//   3: timed out
		document.getElementById('lat').value = null;
		document.getElementById('lon').value = null;
	};
	navigator.geolocation.getCurrentPosition(geoSuccess, geoError, startPos);
}

window.onload = function() {
	askLocationPermission();
};

function handlePermission() {
	navigator.permissions.query({name:'geolocation'}).then(function(result) {
		if (result.state == 'granted') {
			report(result.state);
		} else if (result.state == 'prompt') {
			report(result.state);
		} else if (result.state == 'denied') {
			report(result.state);
		}
		result.onchange = function() {
			report(result.state);
			askLocationPermission();
		}
	});
}

function report(state) {
  console.log('Permission ' + state);
}

handlePermission();

