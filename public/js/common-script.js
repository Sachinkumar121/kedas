$(document).ready(function () {
	$(document).on('change', 'select[name="locale_selector"]', async function(e) {
        let locale = $(this).find('option:selected').val();
        let result = await setAppLocale(locale);
        if (result.success) {
            location.reload();
        } else {
            console.error(result.error);
        }
    });
});

async function setAppLocale(locale) {
    try {
        result = await $.ajax({
            url: SITE_URL + "/app/set-locale",
            type: "POST",
            data: {locale},
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            dataType:"json",
            success:function(response){
                // console.log(response);
            },
        });
        return result;
    } catch (error) {
        errorObj = $.parseJSON(error.responseText);
        console.log(errorObj);
    }
}