function addDropZone(documents = null, type = 'image', maxFilesize = 10) {
    //extensions = type == 'image' ?  '.jpeg,.jpg,.png,.gif' : 'application/pdf';
    if (type == 'image') {
        extensions = 'image/jpeg,image/png,image/gif,image/jpg';
    } else if (type == 'pdf') {
        extensions = 'application/pdf';
    } else {
        extensions = 'image/jpeg,image/png,image/gif,image/jpg,application/pdf';
    }
    let uploadedDocumentMap = {};
    let csrf = $('meta[name="csrf-token"]').attr('content');
    Dropzone.options.documentDropzone = {
        url: SITE_URL + '/documents/upload-files',
        maxFilesize,
        addRemoveLinks: true,
        acceptedFiles: extensions,
        headers: {
            'X-CSRF-TOKEN': csrf
        },
        processing: function() {
            $('.documents-submit-btn').prop('disabled', true);
        },
        sending: function(file, xhr, formData) {
            formData.append('type', type);
        },
        success: function(file, response) {
            $('form').append('<input type="hidden" name="documents[]" value="' + response.name + '">');
            uploadedDocumentMap[file.name] = response.name;
            file.file_name = response.name;
            $('#document-error').hide();
            $('.documents-submit-btn').prop('disabled', false);
        },
        error: function(file, errors, xhr) {
            $('#document-error').text(errors);
            $('#document-error').show();
            $(file.previewElement).remove();
            //$(file.previewElement).addClass("dz-error").find('.dz-error-message').text(errors.error);
            $('.documents-submit-btn').prop('disabled', false);
        },
        removedfile: function(file) {
            file.previewElement.remove();
            let name = '';
            if (typeof file.name !== 'undefined') {
                name = file.file_name;
            } else {
                name = uploadedDocumentMap[file.name];
            }

            // Delete files from server //
            $.ajax({
                type: 'POST',
                url: SITE_URL + '/documents/delete-file',
                headers: {
                    'X-CSRF-TOKEN': csrf
                },
                data: { name: name },
                sucess: function(file, response) {

                },
                error: function(file, errors, xhr) {
                    //$(file.previewElement).addClass("dz-error").find('.dz-error-message').text(errors.error);
                    $('#document-error').text(errors.error);
                    $('#document-error').show();
                },
            });

            $('form').find('input[name="documents[]"][value="' + name + '"]').remove();
        },
        init: function() {
            if (documents != null) {
                let files = documents;
                for (let i in files) {
                    let file = files[i]
                    this.options.addedfile.call(this, file);
                    file.previewElement.classList.add('dz-complete');
                    file.file_name = file.name;
                    let ext = file.name.split('.').pop();
                    if (ext == "pdf") {
                        this.options.thumbnail.call(this, file, SITE_URL + "/public/images/pdf.png");
                    } else {
                        this.options.thumbnail.call(this, file, SITE_URL + '/public/storage/documents/' + file.name);
                    }
                    $('form').append('<input type="hidden" name="documents[]" value="' + file.name + '">')
                }
            }
            this.on('addedfile', function(file) {
                let ext = file.name.split('.').pop();
                if (ext == "pdf") {
                    this.options.thumbnail.call(this, file, SITE_URL + "/public/images/pdf.png");
                } else if (ext.indexOf("doc") != -1) {
                    this.options.thumbnail.call(this, file, SITE_URL + "/public/images/pdf.png");
                } else if (ext.indexOf("xls") != -1) {
                    this.options.thumbnail.call(this, file, SITE_URL + "/public/images/pdf.png");
                }
            });
        }
    }
}